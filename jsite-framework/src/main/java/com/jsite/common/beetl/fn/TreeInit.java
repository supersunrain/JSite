package com.jsite.common.beetl.fn;

import com.jsite.common.config.Global;
import com.jsite.common.lang.StringUtils;
import com.jsite.modules.sys.entity.Menu;
import com.jsite.modules.sys.utils.UserUtils;
import org.beetl.core.Context;
import org.beetl.core.Function;

import java.util.List;

public class TreeInit implements Function {

	@Override
	public Object call(Object[] paras, Context ctx) {
		List<Menu> menuList = UserUtils.getMenuList();

		return initMenuTree(menuList);
	}


	private String initMenuTree(List<Menu> menuList) {
		StringBuilder htmlStr = new StringBuilder();

		String href;
		for (Menu menu : menuList) {
			//子菜单缓冲值
			StringBuilder htmlStr2 = new StringBuilder();
			if(menu.getParent().getId().equals("0") && menu.getIsShow().equals("1")) {// 一级菜单isEmpty(menu.parentId)
				href = StringUtils.isBlank(menu.getHref()) ? "blank" : menu.getHref();
				htmlStr.append(href.equals("blank")?"<li class='nav-item has-treeview'>\n":"");
				htmlStr.append(href.equals("blank")?"<a title='" + i18n(menu.getName()) + "' href='javascript:' data-href='" + href + "' class='nav-link addTabPage'>\n":"");
				htmlStr.append(href.equals("blank")?"<i class='nav-icon " + menu.getIcon() + "'></i>":"");
				htmlStr.append(href.equals("blank")?"<p>" + i18n(menu.getName()) + "<i class='right fas fa-angle-left'></i> </p>\n":"");
				htmlStr.append(href.equals("blank")?"</a>\n":"");

				htmlStr.append(href.equals("blank")?"":"<li class='nav-item'><a title='" + i18n(menu.getName()) + "' href='javascript:' data-href='" + href + "' class='nav-link addTabPage'><i class='nav-icon " + menu.getIcon() + "'></i><p>" + i18n(menu.getName()) + "</p></a></li>\n");
				//递归获取二级->N级菜单
				StringBuilder subMenu = subMenu(menuList, menu, href, htmlStr2);
				htmlStr.append(subMenu);
				htmlStr.append("</li>\n");
			}
		}
		return htmlStr.toString();
	}

	/**
	 * 递归获取二级->N级菜单
	 * @param menuList
	 * @param menu
	 * @param href
	 * @param htmlStr2
	 * @author lihy
	 * @data 2019年3月19日23:43
	 * @return
	 */
	public StringBuilder subMenu(List<Menu> menuList,Menu menu,String href,StringBuilder htmlStr2){
		//获取每级菜单
		if(href.equals("blank")) {
			htmlStr2.append("<ul class='nav nav-treeview'>");
			for(Menu menu1 : menuList) {
				if(menu1.getParent().getId().equals(menu.getId()) && menu1.getIsShow().equals("1")) {
					href = StringUtils.isBlank(menu1.getHref())?"blank":menu1.getHref();

					htmlStr2.append(href.equals("blank")?"<li class='nav-item has-treeview'>\n":"");
					htmlStr2.append(href.equals("blank")?"<a title='" + i18n(menu1.getName()) + "' href='javascript:' data-href='" + href + "' class='nav-link addTabPage'>\n":"");
					htmlStr2.append(href.equals("blank")?"<i class='nav-icon " + menu1.getIcon() + "'></i>" + " \n":"");
					htmlStr2.append(href.equals("blank")?"<p>" + i18n(menu1.getName()) + "<i class='right fas fa-angle-left'></i></p>" + " \n":"");
					htmlStr2.append(href.equals("blank")?"</a>\n":"");

					htmlStr2.append(href.equals("blank")?"":"<li class='nav-item'><a title='" + i18n(menu1.getName()) + "' href='javascript:' data-href='" + href + "' class='nav-link addTabPage'><i class='nav-icon " + menu1.getIcon() + "'></i><p>" + i18n(menu1.getName()) + "</p></a></li>\n");
					subMenu(menuList,menu1, href, htmlStr2);
				}
			}
			htmlStr2.append("</ul>\n");
		}

		return htmlStr2;
	}


	public String i18n(String code) {
		return Global.getText(code, new String[0]);
	}
}
