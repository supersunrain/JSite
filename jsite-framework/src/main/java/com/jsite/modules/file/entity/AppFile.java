package com.jsite.modules.file.entity;

import com.jsite.common.persistence.BaseEntity;

import java.util.Date;

public class AppFile extends BaseEntity<AppFile> {

    private String fileTreeId;		// 文件所在文件夹
    private String mainId;		// 主表住建
    private String fileTreeName;	//文件所在文件夹名称
    private String name;		// 文件名称
    private String path;		// 文件路径
    private String thumPath;
    private String fileSize;    // 文件大小

    private Date createDate;	// 创建日期
    private Date updateDate;	// 更新日期


    public String getFileTreeId() {
        return fileTreeId;
    }

    public void setFileTreeId(String fileTreeId) {
        this.fileTreeId = fileTreeId;
    }

    public String getMainId() {
        return mainId;
    }

    public void setMainId(String mainId) {
        this.mainId = mainId;
    }

    public String getFileTreeName() {
        return fileTreeName;
    }

    public void setFileTreeName(String fileTreeName) {
        this.fileTreeName = fileTreeName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getThumPath() {
        return thumPath;
    }

    public void setThumPath(String thumPath) {
        this.thumPath = thumPath;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public void preInsert() {

    }

    @Override
    public void preUpdate() {

    }
}
