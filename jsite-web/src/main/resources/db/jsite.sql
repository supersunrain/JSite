/*
 Navicat Premium Data Transfer

 Source Server         : JSite
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : 39.105.173.191:3306
 Source Schema         : jsite

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 13/02/2020 10:25:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ACT_DE_DATABASECHANGELOG
-- ----------------------------
DROP TABLE IF EXISTS `ACT_DE_DATABASECHANGELOG`;
CREATE TABLE `ACT_DE_DATABASECHANGELOG`  (
  `ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_DE_DATABASECHANGELOG
-- ----------------------------
INSERT INTO `ACT_DE_DATABASECHANGELOG` VALUES ('1', 'flowable', 'META-INF/liquibase/flowable-modeler-app-db-changelog.xml', '2020-01-02 15:23:50', 1, 'EXECUTED', '8:e70d1d9d3899a734296b2514ccc71501', 'createTable tableName=ACT_DE_MODEL; createIndex indexName=idx_proc_mod_created, tableName=ACT_DE_MODEL; createTable tableName=ACT_DE_MODEL_HISTORY; createIndex indexName=idx_proc_mod_history_proc, tableName=ACT_DE_MODEL_HISTORY; createTable tableN...', '', NULL, '3.6.2', NULL, NULL, '7949827397');
INSERT INTO `ACT_DE_DATABASECHANGELOG` VALUES ('3', 'flowable', 'META-INF/liquibase/flowable-modeler-app-db-changelog.xml', '2020-01-02 15:23:51', 2, 'EXECUTED', '8:3a9143bef2e45f2316231cc1369138b6', 'addColumn tableName=ACT_DE_MODEL; addColumn tableName=ACT_DE_MODEL_HISTORY', '', NULL, '3.6.2', NULL, NULL, '7949827397');

-- ----------------------------
-- Table structure for ACT_DE_DATABASECHANGELOGLOCK
-- ----------------------------
DROP TABLE IF EXISTS `ACT_DE_DATABASECHANGELOGLOCK`;
CREATE TABLE `ACT_DE_DATABASECHANGELOGLOCK`  (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_DE_DATABASECHANGELOGLOCK
-- ----------------------------
INSERT INTO `ACT_DE_DATABASECHANGELOGLOCK` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for ACT_DE_MODEL
-- ----------------------------
DROP TABLE IF EXISTS `ACT_DE_MODEL`;
CREATE TABLE `ACT_DE_MODEL`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(400) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `model_key` varchar(400) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `description` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `model_comment` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `created` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `last_updated` datetime(6) NULL DEFAULT NULL,
  `last_updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `version` int(11) NULL DEFAULT NULL,
  `model_editor_json` longtext CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `thumbnail` longblob NULL,
  `model_type` int(11) NULL DEFAULT NULL,
  `tenant_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_proc_mod_created`(`created_by`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_DE_MODEL
-- ----------------------------
INSERT INTO `ACT_DE_MODEL` VALUES ('02d0e7c3-3675-11ea-aa63-00163e2e65eb', 'sdgsd', 'sdgsg', 'sdgsgsg', NULL, '2020-01-14 10:24:37.454000', 'admin', '2020-01-14 10:24:51.979000', 'admin', 1, '{\"name\":\"sdgsd\",\"key\":\"sdgsg\",\"version\":0,\"fields\":[{\"fieldType\":\"FormField\",\"id\":\"label\",\"name\":\"Label\",\"type\":\"text\",\"required\":false,\"readOnly\":false,\"overrideId\":false},{\"fieldType\":\"FormField\",\"id\":\"label\",\"name\":\"Label\",\"type\":\"text\",\"required\":false,\"readOnly\":false,\"overrideId\":false},{\"fieldType\":\"FormField\",\"id\":\"label\",\"name\":\"Label\",\"type\":\"text\",\"required\":false,\"readOnly\":false,\"overrideId\":false}],\"outcomes\":[]}', 0x89504E470D0A1A0A0000000D494844520000012C0000006C0806000000D0528FAD0000070349444154785EEDDDCB2B755F1CC7F12FC7FD7E2BA13021131919F807CC0D949831F70790BB44FE05332931306120A544463A294303CA2DC95D38AE4FFBFCF22BCFCFAF7D8EF3DDEB9C75F67BD7D333B0ADF55DAFEFEED3F3B0CE5E29A150E853B8104000010B0452082C0BBA44890820101620B078101040C01A0102CB9A565128020810583C030820608D0081654DAB28140104082C9E010410B04680C0B2A655148A0002AE81F5F4F424D9D9D948218000024604AEAFAFA5B8B8F8C7B9082C232D601204108854E0EEEE4E0A0A0A08AC48C1B80F0104E2274060C5CF9E991140204A01B5C01A1C1C94D1D1D17FA73F393991AAAAAA6FE5CCCFCF4B4747479425723B020820F08F807A601D1E1ECAE9E9A9D4D4D4C8D1D191949797CBDADA9A9C9F9F4B5D5D1D81C593870002BF16500FACD9D959696F6F97CBCBCBF0BFB0565656A4BABA5AF6F6F624353595C0FA75ABF8460410500B2C2811400001AF05082CAF85191F0104D404D402EBF6F656727272D40AF3FB40CE7F9FB9EC15F8F8F8B0B7F804AC3C3D3D5DF787EE9F9F9F92999999804BA5240410B05DE0E5E585C0B2BD89D48F805F04082CBF749A752290040204561234912520E01701CF02EBE2E242A6A7A7A5ADAD4D5A5A5ABE790E0F0F8BF3870B0104108846C0B3C0728A98989890BEBE3E595A5A926030284D4D4DE14F586F6C6CC8F8F8B8A4A4A444532BF7228080CF058C04D6E4E4A438EFCCCACACA92E7E767090402E27CE6900B0104108846C0D3C08AA610EE45000104DC04D4032B140AB9CDC9D7A31060E368145809782B1B47759BF2B5C7536DA7BB6E798C86000208FC5720E6C0727E3EC5850002089810703E02585454F4E354AEEF743751207320800002910810589128710F02082484008195106DA0080410884480C08A44897B104020210408AC846803452080402402AE81E5EC35E163369150720F0208680838EFDDFBBF3D8AAE81C551F51A2D600C04108854C0D9D6505858F8E3ED0456A48ADC870002460462DE389A9D9D6DA4502641000104082C9E010410B046402DB038AADE9A9E532802D60AA807D6C1C1813883969595C9D5D595A4A5A5C9EEEEAEECEFEF4B7D7D3D273F5BFBA8503802F117500FAC999919E9EEEE96B3B3B3F051F5ABABAB525959295B5B5B525C5C4C60C5BFE7548080B5026A8165AD00852380803502049635ADA2500410500B2CE79DED797979882A09F0090225C8380DE3ECC8E6D213787B7B0B0FA616581C55AFD71C46420081EF02EAEF7427B078C41040C02B0102CB2B59C64500017501024B9D94011140C02B01CF02EBE6E646A6A6A6A4B3B3531A1B1BBFD53F3434242323235EAD897111402049053C0B2CC7EBEBA8FAF5F575D9DCDC94E6E666292D2D95E5E565191B1B4B5252968500025E091809ACFEFE7E797D7D959C9C1C797C7C94FCFC7C191818F06A4D8C8B0002492AE0696025A919CB4200813809A807569CD6C1B40820E02301B58DA33E3263A9082010278198032B3333334EA5332D0208F84DC0D985505252F2E3B25DDFE9EE372CD68B0002892B4060256E6FA80C0104FE1220B078241040C01A0102CB9A565128020810583C030820608D806B60BDBFBF7354BD35EDA45004EC17705E631508047EF75B428EAAB7FF01600508D82410F33E2C4E7EB6A9DDD48A80DD020496DDFDA37A047C254060F9AADD2C1601BB05D4028BA3EAED7E10A81E011B04D403CB39AADEF9EDA1F3B3AD5028143E96E7F8F858767676A4A1A181939F6D782AA811810415500DACBEBE3E999B9B93AEAE2EB9BCBC94B2B232D9DEDE0E9F57E8FC5D5E5E4E6025E883405908D820A01658362C961A1140C06E0102CBEEFE513D02BE12500B2CE715A61C55EFAB6787C522604C40FD15C99CFC6CAC774C8480EF04082CDFB59C052360AF0081656FEFA81C01DF091058BE6B390B46C05E01CF02EBE1E1217C547D4F4F8FD4D6D67E13FA7B37BCBD7C548E000226053C0B2C67115F47D52F2E2E4A301894D6D6D6F026D28585058EAA37D965E6422049048C04566F6F6F982B373757EEEFEFA5A2A2429CE3EBB91040008168043C0DAC680AE15E041040C04D403DB0DC26E4EB08208040AC026A3BDD632D84EF47000104DC04620EAC8C8C0CB739F83A020820A022E01C555F5A5AFAE358AEA7E6A854C02008208080820081A580C8100820604680C032E3CC2C0820A0204060292032040208981120B0CC38330B0208280810580A880C8100026604082C33CECC8200020A020496022243208080190102CB8C33B3208080820081A580C8100820604680C032E3CC2C0820A0204060292032040208981120B0CC38330B0208280810580A880C8100026604082C33CECC8200020A020496022243208080190102CB8C33B3208080820081A580C8100820604680C032E3CC2C0820A0204060292032040208981120B0CC38330B0208280810580A880C8100026604082C33CECC8200020A020496022243208080190102CB8C33B320808082C01F758BB961F585E63A0000000049454E44AE426082, 2, NULL);
INSERT INTO `ACT_DE_MODEL` VALUES ('4ac6109e-4cb4-11ea-9b4e-00163e2e65eb', 'fdsa', 'fdsa', 'fdsafds', NULL, '2020-02-11 17:53:02.041000', 'admin', '2020-02-11 17:53:07.264000', 'admin', 1, '{\"modelId\":\"4ac6109e-4cb4-11ea-9b4e-00163e2e65eb\",\"bounds\":{\"lowerRight\":{\"x\":1200,\"y\":1050},\"upperLeft\":{\"x\":0,\"y\":0}},\"properties\":{\"process_id\":\"fdsa\",\"name\":\"fdsa\",\"documentation\":\"fdsafds\",\"process_author\":\"\",\"process_version\":\"\",\"process_namespace\":\"http://www.flowable.org/processdef\",\"process_historylevel\":\"\",\"isexecutable\":true,\"dataproperties\":\"\",\"executionlisteners\":\"\",\"eventlisteners\":\"\",\"signaldefinitions\":\"\",\"messagedefinitions\":\"\",\"process_potentialstarteruser\":\"\",\"process_potentialstartergroup\":\"\",\"iseagerexecutionfetch\":\"false\"},\"childShapes\":[{\"resourceId\":\"startEvent1\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"executionlisteners\":\"\",\"initiator\":\"\",\"formkeydefinition\":\"\",\"formreference\":\"\",\"formproperties\":\"\"},\"stencil\":{\"id\":\"StartNoneEvent\"},\"childShapes\":[],\"outgoing\":[],\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"dockers\":[]}],\"stencil\":{\"id\":\"BPMNDiagram\"},\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\",\"url\":\"../editor/stencilsets/bpmn2.0/bpmn2.0.json\"}}', 0x89504E470D0A1A0A0000000D494844520000002D0000002D08060000003A1AE29A000002E04944415478DAED58DF6B526118DEC52EFA23BA080AEACFF08F48747A5051F157F3D74C10744CA6178A8C5D0B42188B2E4236314B6585919814132744B46A3936E7C19B31AA63FEC8AFF71D79135DCC73BEF38DE03C7038777ECF7978BEE77D5E1716142850F0FF821072ADDD6EDBB3D9ECAB442271EEF178C626938968B55AC271DCD4E9740EC3E1F0613299DC82E7F69593CDE5720F63B1D8D8E7F391743A4D1A8D06E976BB6430181004BEFBFD3ED9DBDB23F051C4EBF54EFD7E7FC766B35999130672E6783CFE33180C5E109A4C26E4B268B55A24140A4DEC76FB07B55A7D8B85BA8BF97CFEA5C3E1208542612EB27FA3542A11B3D92CC8AA3A12AE56AB6FDD6E373938382034D0E97408082058ADD67BB290468591307A9426F0F740ED6F4B4B4B1C750FA3256829FC2FC50D06C31910BF432D25D6D7D787E86139512C1605BD5EFF860AE9EDEDED479812A3D188C80D97CBC54BB609AA8C398CB1C602FBFBFB43C96AE3A4C3CB2725DAE605C4200FD3F48668D2389A33990C6189CDCDCD1690BE2F9A3476091CCD2C51AFD73B40BA209A34969F5EAFC79434CFF37D207D2C9A34B6B559F961053C0F480F4493C67A7915C0734593361A8D53D64A0B82C04B521AE26EC8DAD3A7A7A71F25797A7575F513EBF42897CBCF24A5472A95CAB0CEE9B5B5B5A2A49C8E4422379797977F319C88039D4EF745D24444ACACAC7C66D53DC01A5B40F8B5E49667B158D4814060CC40ED3350B9496D198025B42D779FC6FB832AAB54AA452AA4D163B884CAB5B9409BCCC3193D6A9BCB0C1CC7E941F11FB477C49393931AD8E288FA8E38031474332EA1B414478591B046A309C9FADF071CA005D5CF777676BE4BB89C671B1B1B0FD012F09898FCC3841E07855E805DF866B329CC93C3954AE5317CF43BBC74D43D7C49F277913C54D83E2C0CEF6BB5DA51B7DBEDCD4A16961FEC12BBBBBB4FA3D1E873B0D721924575A9A58404AF5F07D57C40E6093C5FB1A561BDFCF33EC62E81A359F2A453A0408102050AE4C66FB109509232650D3B0000000049454E44AE426082, 0, NULL);
INSERT INTO `ACT_DE_MODEL` VALUES ('607b0f4b-3a7a-11ea-aa63-00163e2e65eb', '测试', 'ZYP', '', NULL, '2020-01-19 13:13:06.664000', 'admin', '2020-01-19 13:13:06.664000', 'admin', 1, '{\"id\":\"canvas\",\"resourceId\":\"canvas\",\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\"},\"properties\":{\"process_id\":\"ZYP\",\"name\":\"测试\"},\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"childShapes\":[],\"dockers\":[],\"outgoing\":[],\"resourceId\":\"startEvent1\",\"stencil\":{\"id\":\"StartNoneEvent\"}}]}', 0x89504E470D0A1A0A0000000D494844520000002D0000002D08060000003A1AE29A000002E04944415478DAED58DF6B526118DEC52EFA23BA080AEACFF08F48747A5051F157F3D74C10744CA6178A8C5D0B42188B2E4236314B6585919814132744B46A3936E7C19B31AA63FEC8AFF71D79135DCC73BEF38DE03C7038777ECF7978BEE77D5E1716142850F0FF821072ADDD6EDBB3D9ECAB442271EEF178C626938968B55AC271DCD4E9740EC3E1F0613299DC82E7F69593CDE5720F63B1D8D8E7F391743A4D1A8D06E976BB6430181004BEFBFD3ED9DBDB23F051C4EBF54EFD7E7FC766B35999130672E6783CFE33180C5E109A4C26E4B268B55A24140A4DEC76FB07B55A7D8B85BA8BF97CFEA5C3E1208542612EB27FA3542A11B3D92CC8AA3A12AE56AB6FDD6E373938382034D0E97408082058ADD67BB290468591307A9426F0F740ED6F4B4B4B1C750FA3256829FC2FC50D06C31910BF432D25D6D7D787E86139512C1605BD5EFF860AE9EDEDED479812A3D188C80D97CBC54BB609AA8C398CB1C602FBFBFB43C96AE3A4C3CB2725DAE605C4200FD3F48668D2389A33990C6189CDCDCD1690BE2F9A3476091CCD2C51AFD73B40BA209A34969F5EAFC79434CFF37D207D2C9A34B6B559F961053C0F480F4493C67A7915C0734593361A8D53D64A0B82C04B521AE26EC8DAD3A7A7A71F25797A7575F513EBF42897CBCF24A5472A95CAB0CEE9B5B5B5A2A49C8E4422379797977F319C88039D4EF745D24444ACACAC7C66D53DC01A5B40F8B5E49667B158D4814060CC40ED3350B9496D198025B42D779FC6FB832AAB54AA452AA4D163B884CAB5B9409BCCC3193D6A9BCB0C1CC7E941F11FB477C49393931AD8E288FA8E38031474332EA1B414478591B046A309C9FADF071CA005D5CF777676BE4BB89C671B1B1B0FD012F09898FCC3841E07855E805DF866B329CC93C3954AE5317CF43BBC74D43D7C49F277913C54D83E2C0CEF6BB5DA51B7DBEDCD4A16961FEC12BBBBBB4FA3D1E873B0D721924575A9A58404AF5F07D57C40E6093C5FB1A561BDFCF33EC62E81A359F2A453A0408102050AE4C66FB109509232650D3B0000000049454E44AE426082, 0, NULL);
INSERT INTO `ACT_DE_MODEL` VALUES ('66067b65-383e-11ea-aa63-00163e2e65eb', '测试财务', 'sfafd', 'afda', NULL, '2020-01-16 16:58:43.894000', 'admin', '2020-02-11 10:20:02.984000', 'admin', 1, '{\"modelId\":\"66067b65-383e-11ea-aa63-00163e2e65eb\",\"bounds\":{\"lowerRight\":{\"x\":1200,\"y\":1050},\"upperLeft\":{\"x\":0,\"y\":0}},\"properties\":{\"process_id\":\"sfafd\",\"name\":\"测试财务\",\"documentation\":\"afda\",\"process_author\":\"\",\"process_version\":\"\",\"process_namespace\":\"http://www.flowable.org/processdef\",\"process_historylevel\":\"\",\"isexecutable\":true,\"dataproperties\":\"\",\"executionlisteners\":\"\",\"eventlisteners\":\"\",\"signaldefinitions\":\"\",\"messagedefinitions\":\"\",\"process_potentialstarteruser\":\"\",\"process_potentialstartergroup\":\"\",\"iseagerexecutionfetch\":\"false\"},\"childShapes\":[{\"resourceId\":\"startEvent1\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"executionlisteners\":\"\",\"initiator\":\"\",\"formkeydefinition\":\"66666\",\"formreference\":null,\"formproperties\":\"\"},\"stencil\":{\"id\":\"StartNoneEvent\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-46D8CDFD-7A14-4123-9FEA-FDD4D9CB9554\"}],\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"dockers\":[]},{\"resourceId\":\"sid-FB64850D-4B91-4F38-B0B9-5542F19822A5\",\"properties\":{\"overrideid\":\"\",\"name\":\"行政审批\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"executionlisteners\":\"\",\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":\"\",\"formkeydefinition\":\"\",\"formreference\":\"\",\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":\"\",\"skipexpression\":\"\",\"categorydefinition\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-028CFBF6-3348-4F24-B442-90DA65570220\"}],\"bounds\":{\"lowerRight\":{\"x\":400,\"y\":218},\"upperLeft\":{\"x\":300,\"y\":138}},\"dockers\":[]},{\"resourceId\":\"sid-46D8CDFD-7A14-4123-9FEA-FDD4D9CB9554\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-FB64850D-4B91-4F38-B0B9-5542F19822A5\"}],\"bounds\":{\"lowerRight\":{\"x\":299.4296875,\"y\":178},\"upperLeft\":{\"x\":130.6875,\"y\":178}},\"dockers\":[{\"x\":15,\"y\":15},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"sid-FB64850D-4B91-4F38-B0B9-5542F19822A5\"}},{\"resourceId\":\"sid-D3DCE6CB-CBCB-4078-9CB9-08655680DB3E\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"sequencefloworder\":\"\"},\"stencil\":{\"id\":\"ExclusiveGateway\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-468B75AD-926A-4592-A701-C50073309EFE\"},{\"resourceId\":\"sid-204517EA-77C0-47F4-B265-E03A2B1DE424\"}],\"bounds\":{\"lowerRight\":{\"x\":610,\"y\":198},\"upperLeft\":{\"x\":570,\"y\":158}},\"dockers\":[]},{\"resourceId\":\"sid-028CFBF6-3348-4F24-B442-90DA65570220\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-D3DCE6CB-CBCB-4078-9CB9-08655680DB3E\"}],\"bounds\":{\"lowerRight\":{\"x\":570.25,\"y\":178},\"upperLeft\":{\"x\":400.6875,\"y\":178}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":20,\"y\":20}],\"target\":{\"resourceId\":\"sid-D3DCE6CB-CBCB-4078-9CB9-08655680DB3E\"}},{\"resourceId\":\"sid-94BE1046-01F3-4743-8C5B-13DC30A6AD89\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"executionlisteners\":\"\"},\"stencil\":{\"id\":\"EndNoneEvent\"},\"childShapes\":[],\"outgoing\":[],\"bounds\":{\"lowerRight\":{\"x\":1063,\"y\":192},\"upperLeft\":{\"x\":1035,\"y\":164}},\"dockers\":[]},{\"resourceId\":\"sid-A61F4F8F-59F3-47C1-A9AA-2C811ECFA8CB\",\"properties\":{\"overrideid\":\"\",\"name\":\"重新报告\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"executionlisteners\":\"\",\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":\"\",\"formkeydefinition\":\"\",\"formreference\":\"\",\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":\"\",\"skipexpression\":\"\",\"categorydefinition\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-66425FB7-523F-472B-94D9-90B44FE8144B\"}],\"bounds\":{\"lowerRight\":{\"x\":640,\"y\":395},\"upperLeft\":{\"x\":540,\"y\":315}},\"dockers\":[]},{\"resourceId\":\"sid-468B75AD-926A-4592-A701-C50073309EFE\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-A61F4F8F-59F3-47C1-A9AA-2C811ECFA8CB\"}],\"bounds\":{\"lowerRight\":{\"x\":590.4424796501771,\"y\":314.0117227625271},\"upperLeft\":{\"x\":590.1161140998229,\"y\":198.8046834874729}},\"dockers\":[{\"x\":20.5,\"y\":20.5},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"sid-A61F4F8F-59F3-47C1-A9AA-2C811ECFA8CB\"}},{\"resourceId\":\"sid-66425FB7-523F-472B-94D9-90B44FE8144B\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-FB64850D-4B91-4F38-B0B9-5542F19822A5\"}],\"bounds\":{\"lowerRight\":{\"x\":539.4160243417239,\"y\":354.788793421051},\"upperLeft\":{\"x\":350.1161221476268,\"y\":218.87499596464218}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":350.5,\"y\":354},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"sid-FB64850D-4B91-4F38-B0B9-5542F19822A5\"}},{\"resourceId\":\"sid-5CDE2724-35F1-4273-832A-D99F4C48C5A6\",\"properties\":{\"overrideid\":\"\",\"name\":\"财务审批\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"executionlisteners\":\"\",\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":\"\",\"formkeydefinition\":\"\",\"formreference\":\"\",\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":\"\",\"skipexpression\":\"\",\"categorydefinition\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-121EDD75-95FF-48FA-9F22-48A9358E3717\"}],\"bounds\":{\"lowerRight\":{\"x\":849.5,\"y\":218},\"upperLeft\":{\"x\":749.5,\"y\":138}},\"dockers\":[]},{\"resourceId\":\"sid-204517EA-77C0-47F4-B265-E03A2B1DE424\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-5CDE2724-35F1-4273-832A-D99F4C48C5A6\"}],\"bounds\":{\"lowerRight\":{\"x\":748.6992216116438,\"y\":178.45268578734843},\"upperLeft\":{\"x\":610.2773408883562,\"y\":178.12153296265157}},\"dockers\":[{\"x\":20.5,\"y\":20.5},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"sid-5CDE2724-35F1-4273-832A-D99F4C48C5A6\"}},{\"resourceId\":\"sid-121EDD75-95FF-48FA-9F22-48A9358E3717\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-94BE1046-01F3-4743-8C5B-13DC30A6AD89\"}],\"bounds\":{\"lowerRight\":{\"x\":1034.35546875,\"y\":178},\"upperLeft\":{\"x\":850.205078125,\"y\":178}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":14,\"y\":14}],\"target\":{\"resourceId\":\"sid-94BE1046-01F3-4743-8C5B-13DC30A6AD89\"}}],\"stencil\":{\"id\":\"BPMNDiagram\"},\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\",\"url\":\"../editor/stencilsets/bpmn2.0/bpmn2.0.json\"}}', 0x89504E470D0A1A0A0000000D494844520000011E00000058080600000023BF9813000007804944415478DAED9D7F6894651CC0F7C78A0526524603FDC38282FAC72021130A132BA1A151DE9D7737F783740B7596D1FA6783066B780D6BBA0646BF230A6D857F9451ED36B75BD8C18292C851B2A5D3CBB65372D676DE96BE7DBFF3BDD8D696BB6DF76CF7DEE7035F9E77AFC72BF7FD3ECFE79EE77DEFDE372B0B0000000000000000000000AE8D6559D9DDDDDD8DE170F8727373B3D5D4D434EB110C06AD5028744E229F8C537B802CED78EDEDED565F5F9F158FC75316D168D46A6B6BEB978EB891AC537BC870F4D32ED1F18E1D2E1F891476C0B87C0A7692F5F955FB54D79DDAC37FD02976A27398E880D2F986C9FAFCAABD89BA537B1883AEC3478BE7C4D1FDA9EE7C16599F5FB53751776A0F9376BED141E74B8AEC74ADBDA9BA231E9874C663221CD8F9D649E87B2AA2F68807E87C26582BF18744A5C45989DDD43E33C5E372B972376DDA542451A5ADDFEF5F3AE3835655552D0E0402C1B2B2B2981CD4DAB66DDB504D4DCD37BA1FF1646CE74B4827CFFE3B57A243E290C4026A9F39E2F1F97C3BF3F3F32FA91B46C5B0D7EB7D76DA072D2A2A5AB47DFBF63FEBEBEBADAEAE2E2B168B593D3D3D564343830AE8627979792EE2C9B8CE375E3A09726CF1FC28B18CDA3B5F3C858585BB12B2A9ADADB51A1B1BAD3D7BF65822A32BF6FEE7A775E0CACACA4695CE44A87C2A2A2ABE403C19D5F9D64D229D31936489D3122BA9BD73C5A3CB2B99E9C45530E170788C1BF46F5B3EC3D35A76C9ACA65F673A13118944AC929292BFD2357192942374BEE4F29575F54472F5145EFEA6FDDA45D4DE79E2D1FC490412339D89080402317BD653349DFFC01A181898F0C0434343D6B8755DDAC55C74BE74CED7AA55ABAC9C9C1C1D404BA7B214A3F6CEA9FD64A1CBAB893878F060DC7E4D55D2E2D9BA756BDFFFCD78B66CD97281A556C67DEA55D9E771162471FE87DA3B6CA9655FC59A74C6535D5D7D7EDA339ED2D2D286BD7BF70E4F74E0FAFAFA61596ABD8D7832B2F335DA2792B3D3513AD47E76CEF1F87CBE81C9CEF178BDDEBFA77D8E47AF6A49F4D4D5D55D3A79F2E4C8F24A673A0D0D0DB1C2C2C25372F0C58827233B9F5EC1D2CBE7BBD3513AD47E76F0783C3BC65FD50A040283B674A67F552B219FCD9B37D749FB9B1EACB8B8382ADB6FA8F1D23D7174BE19A1F5D72F0E7E906ED2A1F6B32E9FD8F8EFF1CC483A4E87CE376356645DBD7A9547EDF9E6F2AC7E7319F1D0F9AEC122FB64A345EDF9AD164C81D1F7E331103127DF9325DDC443ED61CE08854267537DEBCB44442291034EBE0B5DBA8987DAC39CD1D2D2B2A1B5B5F5426F6FEF602A3FEDB4E30583C1534EBEEF6EBA8987DAC39C221D224F3E8D3A742A9C8A270DD8C7ED747AC74B37F1507B00C40300D7C2ED763F2403ED0A99403C00C6F07ABD6D32D02E9309C403600CBFDFBF50065A3F99403C00C670B95CB7C8408B9209C40360523C4B64A09D21138807C0183E9FEF7619685D6402F10018C3E3F1DC2503ED3899403C0026673CF7887CBE27138807C0E420BB4F224C26100F8031DC6EF783FA5D1E328178008C21D2795806DAD76402F10018C3E3F1E8A35B3E23138807C0E48CE74991CF276402F100981C643E890FC904E20130B9D42A9659CF3B6402F100981C644F4BEC27138807C0E48CE7191968FBC804E20130299E72596AD59209C403607290554A549309C40360729055AB7CC804E20130862EB374B94526100F80C941B64F622799403C00C690D9CEEB32EB2925138807C0E4207B57A2884C201E009383EC2399F178C904E2013039C83E95E5D61364E2DF7C1C413C00A91F689F4B3CE6B4F76559567677777763381CBEDCDCDC9C8A67895BC160D00A8542E724F2E949004920ABAC26B7DBBDD669EF4BA5D3DEDE6EF5F5F559F1783C65118D46ADB6B6B67E91D0467A13C0D4673C21BDFDA9D3DE97CE74543AC70E978F448AE51397195027BD0960EAE209EB0DDF9DF6BE7479A55230211E0D11CF30BD09608AE8A36DF411374E7B5F7A0E26219E1347F79B100F27A4019298F11CD787FA39513C89D98E89590FE201484E3C5DFA1863A7CE784C05E201484E3C675C2ED712C48378004C8A27EAF57A17231EC403600C91CEC5F5EBD7DF8878100F80C9194F5C965AD7231EC40360523C96C4B0C4A0CE7EA43D2FF1BBC469895F257E91FD3F49FB83C47712DFEA970E259A25BED29F5C783C9E43F29A8FED1F9CBE2FED5BFAE40A89D7245E957F7F59DA1A892AD9AE90D7BC20ED73D296D94FB9784AB60BF5195FB2DF25DB8FEB134E7D3EDFA3B2BD46F63F20EDFDD2AE90FDCB4594774B7BA7B4B7F9FDFEA5F26FB7161414DC2CDB0B65DF0D252525D7211E8079CEEAD5ABB375C0EA924BDA9B7420EB809681BE4C06FF1DF6405F2EDBF74ABBD216C11AD97E447FE7658B4285E195B64045A24291ED1DD2EEB2455321DB2FCAF64B2A22695F91B6DE16948AEA3D7DB0A00A4C45A68F5596F64B5B702ABAA3121D2A405B843F4B74DB8254519E93E85781AA48E535880700586A0100E2413C0080780000F1201E00403C008078100F008C25713F1E4311E37E3C0090150A85CEA6FAB6A78988442207B803210064B5B4B46C686D6DBDD0DBDB3B98CA998E4A27180C9EE29ECB003082C8204F66221DBA0C4AC55326ECE376221D0000000000000000000000000000000000000000000030C33F44C22D02EDE69DBF0000000049454E44AE426082, 0, NULL);
INSERT INTO `ACT_DE_MODEL` VALUES ('75160c4d-4be3-11ea-9b4e-00163e2e65eb', '送货流程', 'sss', '实施', NULL, '2020-02-10 16:58:08.024000', 'admin', '2020-02-10 16:58:08.024000', 'admin', 1, '{\"id\":\"canvas\",\"resourceId\":\"canvas\",\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\"},\"properties\":{\"process_id\":\"sss\",\"name\":\"送货流程\",\"documentation\":\"实施\"},\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"childShapes\":[],\"dockers\":[],\"outgoing\":[],\"resourceId\":\"startEvent1\",\"stencil\":{\"id\":\"StartNoneEvent\"}}]}', 0x89504E470D0A1A0A0000000D494844520000002D0000002D08060000003A1AE29A000002E04944415478DAED58DF6B526118DEC52EFA23BA080AEACFF08F48747A5051F157F3D74C10744CA6178A8C5D0B42188B2E4236314B6585919814132744B46A3936E7C19B31AA63FEC8AFF71D79135DCC73BEF38DE03C7038777ECF7978BEE77D5E1716142850F0FF821072ADDD6EDBB3D9ECAB442271EEF178C626938968B55AC271DCD4E9740EC3E1F0613299DC82E7F69593CDE5720F63B1D8D8E7F391743A4D1A8D06E976BB6430181004BEFBFD3ED9DBDB23F051C4EBF54EFD7E7FC766B35999130672E6783CFE33180C5E109A4C26E4B268B55A24140A4DEC76FB07B55A7D8B85BA8BF97CFEA5C3E1208542612EB27FA3542A11B3D92CC8AA3A12AE56AB6FDD6E373938382034D0E97408082058ADD67BB290468591307A9426F0F740ED6F4B4B4B1C750FA3256829FC2FC50D06C31910BF432D25D6D7D787E86139512C1605BD5EFF860AE9EDEDED479812A3D188C80D97CBC54BB609AA8C398CB1C602FBFBFB43C96AE3A4C3CB2725DAE605C4200FD3F48668D2389A33990C6189CDCDCD1690BE2F9A3476091CCD2C51AFD73B40BA209A34969F5EAFC79434CFF37D207D2C9A34B6B559F961053C0F480F4493C67A7915C0734593361A8D53D64A0B82C04B521AE26EC8DAD3A7A7A71F25797A7575F513EBF42897CBCF24A5472A95CAB0CEE9B5B5B5A2A49C8E4422379797977F319C88039D4EF745D24444ACACAC7C66D53DC01A5B40F8B5E49667B158D4814060CC40ED3350B9496D198025B42D779FC6FB832AAB54AA452AA4D163B884CAB5B9409BCCC3193D6A9BCB0C1CC7E941F11FB477C49393931AD8E288FA8E38031474332EA1B414478591B046A309C9FADF071CA005D5CF777676BE4BB89C671B1B1B0FD012F09898FCC3841E07855E805DF866B329CC93C3954AE5317CF43BBC74D43D7C49F277913C54D83E2C0CEF6BB5DA51B7DBEDCD4A16961FEC12BBBBBB4FA3D1E873B0D721924575A9A58404AF5F07D57C40E6093C5FB1A561BDFCF33EC62E81A359F2A453A0408102050AE4C66FB109509232650D3B0000000049454E44AE426082, 0, NULL);
INSERT INTO `ACT_DE_MODEL` VALUES ('781fb679-3735-11ea-aa63-00163e2e65eb', '11', '111', '111', NULL, '2020-01-15 09:22:17.600000', 'admin', '2020-01-15 12:57:35.540000', 'admin', 1, '{\"modelId\":\"781fb679-3735-11ea-aa63-00163e2e65eb\",\"bounds\":{\"lowerRight\":{\"x\":1200,\"y\":1050},\"upperLeft\":{\"x\":0,\"y\":0}},\"properties\":{\"process_id\":\"111\",\"name\":\"11\",\"documentation\":\"111\",\"process_author\":\"\",\"process_version\":\"\",\"process_namespace\":\"http://www.flowable.org/processdef\",\"process_historylevel\":\"\",\"isexecutable\":true,\"dataproperties\":\"\",\"executionlisteners\":\"\",\"eventlisteners\":\"\",\"signaldefinitions\":\"\",\"messagedefinitions\":\"\",\"process_potentialstarteruser\":\"\",\"process_potentialstartergroup\":\"\",\"iseagerexecutionfetch\":\"false\"},\"childShapes\":[{\"resourceId\":\"startEvent1\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"executionlisteners\":\"\",\"initiator\":\"\",\"formkeydefinition\":\"\",\"formreference\":\"\",\"formproperties\":\"\"},\"stencil\":{\"id\":\"StartNoneEvent\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-CD07268B-B2AE-41E5-B1C7-E7E07BD83EBC\"}],\"bounds\":{\"lowerRight\":{\"x\":120,\"y\":180},\"upperLeft\":{\"x\":90,\"y\":150}},\"dockers\":[]},{\"resourceId\":\"sid-DBD66162-A92A-4BD0-A0A3-9267BD16EF4B\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"dataproperties\":\"\",\"executionlisteners\":\"\",\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"istransaction\":\"false\"},\"stencil\":{\"id\":\"CollapsedSubProcess\"},\"childShapes\":[{\"resourceId\":\"sid-52ADD891-605A-417A-9486-E23A83575D9C\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"executionlisteners\":\"\",\"initiator\":\"\",\"formkeydefinition\":\"\",\"formreference\":\"\",\"formproperties\":\"\"},\"stencil\":{\"id\":\"StartNoneEvent\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-75977DC8-ACE1-408C-8AF9-0F4AE3BC8926\"}],\"bounds\":{\"lowerRight\":{\"x\":140,\"y\":207},\"upperLeft\":{\"x\":110,\"y\":177}},\"dockers\":[]},{\"resourceId\":\"sid-EF8993C8-5472-4AA3-A073-5D0ADA388D4F\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"executionlisteners\":\"\",\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":\"\",\"formkeydefinition\":\"\",\"formreference\":\"\",\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":\"\",\"skipexpression\":\"\",\"categorydefinition\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-C364C11C-FCCB-4370-A54B-5712A3997882\"}],\"bounds\":{\"lowerRight\":{\"x\":285,\"y\":232},\"upperLeft\":{\"x\":185,\"y\":152}},\"dockers\":[]},{\"resourceId\":\"sid-75977DC8-ACE1-408C-8AF9-0F4AE3BC8926\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-EF8993C8-5472-4AA3-A073-5D0ADA388D4F\"}],\"bounds\":{\"lowerRight\":{\"x\":184.15625,\"y\":192},\"upperLeft\":{\"x\":140.609375,\"y\":192}},\"dockers\":[{\"x\":15,\"y\":15},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"sid-EF8993C8-5472-4AA3-A073-5D0ADA388D4F\"}},{\"resourceId\":\"sid-98553457-5CA2-4444-8CA2-32EE951931F8\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"executionlisteners\":\"\",\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":\"\",\"formkeydefinition\":\"\",\"formreference\":\"\",\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":\"\",\"skipexpression\":\"\",\"categorydefinition\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-38EED183-80D6-45EA-AE4B-2593A7B2BC2E\"}],\"bounds\":{\"lowerRight\":{\"x\":430,\"y\":232},\"upperLeft\":{\"x\":330,\"y\":152}},\"dockers\":[]},{\"resourceId\":\"sid-C364C11C-FCCB-4370-A54B-5712A3997882\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-98553457-5CA2-4444-8CA2-32EE951931F8\"}],\"bounds\":{\"lowerRight\":{\"x\":329.15625,\"y\":192},\"upperLeft\":{\"x\":285.84375,\"y\":192}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"sid-98553457-5CA2-4444-8CA2-32EE951931F8\"}},{\"resourceId\":\"sid-822813DC-2C64-4313-AA0A-55838501B56E\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"executionlisteners\":\"\"},\"stencil\":{\"id\":\"EndNoneEvent\"},\"childShapes\":[],\"outgoing\":[],\"bounds\":{\"lowerRight\":{\"x\":503,\"y\":206},\"upperLeft\":{\"x\":475,\"y\":178}},\"dockers\":[]},{\"resourceId\":\"sid-38EED183-80D6-45EA-AE4B-2593A7B2BC2E\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-822813DC-2C64-4313-AA0A-55838501B56E\"}],\"bounds\":{\"lowerRight\":{\"x\":474.375,\"y\":192},\"upperLeft\":{\"x\":430.390625,\"y\":192}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":14,\"y\":14}],\"target\":{\"resourceId\":\"sid-822813DC-2C64-4313-AA0A-55838501B56E\"}}],\"outgoing\":[{\"resourceId\":\"sid-99A0B554-40FE-4EA7-8B58-04C9588F58F0\"}],\"bounds\":{\"lowerRight\":{\"x\":385,\"y\":200},\"upperLeft\":{\"x\":285,\"y\":120}},\"dockers\":[]},{\"resourceId\":\"sid-CD07268B-B2AE-41E5-B1C7-E7E07BD83EBC\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-DBD66162-A92A-4BD0-A0A3-9267BD16EF4B\"}],\"bounds\":{\"lowerRight\":{\"x\":284.3125,\"y\":165},\"upperLeft\":{\"x\":120.47265625,\"y\":160}},\"dockers\":[{\"x\":15,\"y\":15},{\"x\":202.5,\"y\":165},{\"x\":202.5,\"y\":160},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"sid-DBD66162-A92A-4BD0-A0A3-9267BD16EF4B\"}},{\"resourceId\":\"sid-50D47603-2260-41E7-8BD7-3CB65B488DC8\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"executionlisteners\":\"\",\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":\"\",\"formkeydefinition\":\"\",\"formreference\":\"\",\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":\"\",\"skipexpression\":\"\",\"categorydefinition\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-F7DE5786-A576-405B-AAD4-A02B62677457\"}],\"bounds\":{\"lowerRight\":{\"x\":563,\"y\":195},\"upperLeft\":{\"x\":463,\"y\":115}},\"dockers\":[]},{\"resourceId\":\"sid-99A0B554-40FE-4EA7-8B58-04C9588F58F0\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-50D47603-2260-41E7-8BD7-3CB65B488DC8\"}],\"bounds\":{\"lowerRight\":{\"x\":462.6328125,\"y\":160},\"upperLeft\":{\"x\":385.3671875,\"y\":155}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":424,\"y\":160},{\"x\":424,\"y\":155},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"sid-50D47603-2260-41E7-8BD7-3CB65B488DC8\"}},{\"resourceId\":\"sid-E4418ECE-B4C2-406B-87A7-1DE5761940B6\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"executionlisteners\":\"\"},\"stencil\":{\"id\":\"EndNoneEvent\"},\"childShapes\":[],\"outgoing\":[],\"bounds\":{\"lowerRight\":{\"x\":636,\"y\":169},\"upperLeft\":{\"x\":608,\"y\":141}},\"dockers\":[]},{\"resourceId\":\"sid-F7DE5786-A576-405B-AAD4-A02B62677457\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-E4418ECE-B4C2-406B-87A7-1DE5761940B6\"}],\"bounds\":{\"lowerRight\":{\"x\":607.375,\"y\":155},\"upperLeft\":{\"x\":563.390625,\"y\":155}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":14,\"y\":14}],\"target\":{\"resourceId\":\"sid-E4418ECE-B4C2-406B-87A7-1DE5761940B6\"}}],\"stencil\":{\"id\":\"BPMNDiagram\"},\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\",\"url\":\"../editor/stencilsets/bpmn2.0/bpmn2.0.json\"}}', 0x89504E470D0A1A0A0000000D4948445200000111000000460806000000EB68A075000005E34944415478DAED9D5D4C5B6518C767B2A8179A98E885512F34DE182FD40B355E7841A237980535A1C00A14963AB2401BC0AC010D2017AB0D0B5E69C2A60BC478B3844413C8088E82AC2406C6206C53216E214147C80AD34960A58EF1FA3CCD395B871BF683C869F8FD9227EFE9E969CFE1DFE7FDBF5FA765CF1E0000000000000000000000D8DD1863F6CECECE768F8E8EDE1A1C1C340303038E8E70386C2291C89244199F1E8003500319191931D168D4C4E3F19C88C5C54573E6CC99BFC4500AF90481967387511D930DE4A7EF9BCDF9538144E97023898BBED3D408A0E5DC61D48893AF550DC40EA7EB2A2672931A0159B79C3964248E6C39B5B794C32662A8119075CB994BE1C4961313815DC7E6A4CF3113314ED7F3D28F1D0903D1123D011321E953D7736DCD5CECFFF8AE9E883ED6FDE809980849FF9F7A269BC7E6404FC044487AF40420E9D1131301921E13C9493DF7EFDFFF664949C96189D6E2E2E24629F3A889980826829EA998C77B6218D312E61E71499EE7FB3B980826829EFF262F2F6FAFF438BEB20DC3EFF79BE3C78F9BEEEE6ED3D5D565EAEAEA92CDE41B97CBF508351313C144D0F336B6815456569ABEBE3EB3BEBE6E92D1C7BAFFC08103B76C2351E3A176622298087AEE114328B40DE4C2850B662B6666666E1BC9B60D6DD48D741C254EF69DDBED5ED537D752F6F5CBBE12B9B087B101921E3D1D6D22893910ED69A4821E67CF9164DD1B292D2D7D46E26C2010981B1B1B5B5C5E5E4E9C647575D54C4E4EAE343636FE2ECFFFA2C7610524FD4E21C9FE43F2EA027ADED1C45A85313E9F6F63F310E67EE8717EBF3FAEAF2B2A2A7A3B2B03F1783CBFF5F6F65EDBEAE4A74F9FBE5E515171594EF82C768089EC9089D82D67A2E2A0E75D9A68DD34274E9C30E9D0D9D99918D2E8F26FC64398B2B2B2F19E9E9E682A27943F7C550CE75766746939B729F1B38A5CD773BB74480E5D8549073DDE7A6D6B461FA6CE813434345C49B5FBA3B4B4B45CD339122C8296331B5D30E5ED1DCE583794259673D3E1D8B1632B568E1ECEE802CACBCBFBCF9D3BB79ECE49A7A6A6745637920315D77141CB7927181E6EEFF050347D5575ADAAAABA198BC552AACB7A9CD7EB5DB186336F64746219CAACD993A8A9A293ADF2BAB8934D84E10CAB33BB74756644F3BFA3A3C3A432BA905E887DAFC878C6AB33FA06996055D407F2F3F31F2A2828785486454FB85CAEA7259E93ED17C4D55E76BBDDAF5B33C66FC9F63B52BE2F214F157BA43C28A54FE24339E6231D8F491992F84CB6BFD01B6664FB6BD93EA94BCE52F649842D91CE4A9CB796B36625E62596E4F86529E34E3211921E3DFF4F3DA59EBD22F91FD33A100C06CDC2C2C23DEBAFEE0F85421B9681C432EE85583D9178263D111906FD2D2757178B5B957751E28A55A9B5724F498C59953E2C17794ACA6FD5142C73F852E273CB343E95F844CD444D45B66B64FB03CB6C8AAD7B57F2D58CD49464FB35295F52B352D392FD4F793C9EC7D5CCE4F183ACCE6022BB594F5DAAB5EA63A2B10F040289151BFBB6F7E6E666535A5A6AAC21CC9F12FBB23A617575F5E8C4C48449774EA4B6B6F667A64D497AF474A69E9595958F69236DDF347A8F5893E89246F7C9AC4FE6F57A5D478E1C496B62F5E8D1A31B7EBFFF209640D2A3A7B3F5D45B31B4A761FD0480FD5300856A32DB76129D4C3974E8D05C7F7F7F4A0632343464A4F7B2C47D22243D7AF2A344B7D13B566B6A6A6EA8916C35A3AB06525F5F1F6B6D6D7D113B20E9D11313B98BA6A6A6E77D3EDF423018DCD039129D3CB52751F5DB80A150C888D1FC818190F4E889896C39B4696B6B6B102E57545424D68FB594C773EDEDED418630243D7A622240D26322E8099808498F9E808990F4E889890049BFF3E4F03F488F39F11FA40326B2EB4C2412892C44A3D19CD3727E7EFEA4E8394D8D005ACE1D666868E8DDE1E1E1EB57AF5EBD912B3AAA8184C3E13989426A04D0723A00A98CFBE4DAC6D5E4B4B7E4F0D06B9CC64080961300683969390100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000E0BEFC03D443E5575B80931E0000000049454E44AE426082, 0, NULL);
INSERT INTO `ACT_DE_MODEL` VALUES ('79291b4a-3908-11ea-aa63-00163e2e65eb', '调度流程', 'ddlc', '调度', NULL, '2020-01-17 17:05:14.324000', 'admin', '2020-01-17 17:05:14.324000', 'admin', 1, '{\"id\":\"canvas\",\"resourceId\":\"canvas\",\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\"},\"properties\":{\"process_id\":\"ddlc\",\"name\":\"调度流程\",\"documentation\":\"调度\"},\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"childShapes\":[],\"dockers\":[],\"outgoing\":[],\"resourceId\":\"startEvent1\",\"stencil\":{\"id\":\"StartNoneEvent\"}}]}', 0x89504E470D0A1A0A0000000D494844520000002D0000002D08060000003A1AE29A000002E04944415478DAED58DF6B526118DEC52EFA23BA080AEACFF08F48747A5051F157F3D74C10744CA6178A8C5D0B42188B2E4236314B6585919814132744B46A3936E7C19B31AA63FEC8AFF71D79135DCC73BEF38DE03C7038777ECF7978BEE77D5E1716142850F0FF821072ADDD6EDBB3D9ECAB442271EEF178C626938968B55AC271DCD4E9740EC3E1F0613299DC82E7F69593CDE5720F63B1D8D8E7F391743A4D1A8D06E976BB6430181004BEFBFD3ED9DBDB23F051C4EBF54EFD7E7FC766B35999130672E6783CFE33180C5E109A4C26E4B268B55A24140A4DEC76FB07B55A7D8B85BA8BF97CFEA5C3E1208542612EB27FA3542A11B3D92CC8AA3A12AE56AB6FDD6E373938382034D0E97408082058ADD67BB290468591307A9426F0F740ED6F4B4B4B1C750FA3256829FC2FC50D06C31910BF432D25D6D7D787E86139512C1605BD5EFF860AE9EDEDED479812A3D188C80D97CBC54BB609AA8C398CB1C602FBFBFB43C96AE3A4C3CB2725DAE605C4200FD3F48668D2389A33990C6189CDCDCD1690BE2F9A3476091CCD2C51AFD73B40BA209A34969F5EAFC79434CFF37D207D2C9A34B6B559F961053C0F480F4493C67A7915C0734593361A8D53D64A0B82C04B521AE26EC8DAD3A7A7A71F25797A7575F513EBF42897CBCF24A5472A95CAB0CEE9B5B5B5A2A49C8E4422379797977F319C88039D4EF745D24444ACACAC7C66D53DC01A5B40F8B5E49667B158D4814060CC40ED3350B9496D198025B42D779FC6FB832AAB54AA452AA4D163B884CAB5B9409BCCC3193D6A9BCB0C1CC7E941F11FB477C49393931AD8E288FA8E38031474332EA1B414478591B046A309C9FADF071CA005D5CF777676BE4BB89C671B1B1B0FD012F09898FCC3841E07855E805DF866B329CC93C3954AE5317CF43BBC74D43D7C49F277913C54D83E2C0CEF6BB5DA51B7DBEDCD4A16961FEC12BBBBBB4FA3D1E873B0D721924575A9A58404AF5F07D57C40E6093C5FB1A561BDFCF33EC62E81A359F2A453A0408102050AE4C66FB109509232650D3B0000000049454E44AE426082, 0, NULL);
INSERT INTO `ACT_DE_MODEL` VALUES ('aee0d905-2d31-11ea-91ac-c85b7643dd9e', '请假流程', 'jsite_leave', '请假流程', NULL, '2020-01-02 07:29:59.672000', 'admin', '2020-01-16 15:57:39.110000', 'admin', 1, '{\"modelId\":\"aee0d905-2d31-11ea-91ac-c85b7643dd9e\",\"bounds\":{\"lowerRight\":{\"x\":1200,\"y\":1050},\"upperLeft\":{\"x\":0,\"y\":0}},\"properties\":{\"process_id\":\"jsite_leave\",\"name\":\"请假流程\",\"documentation\":\"请假流程\",\"process_author\":\"\",\"process_version\":\"\",\"process_namespace\":\"http://www.flowable\",\"process_historylevel\":\"\",\"isexecutable\":true,\"dataproperties\":\"\",\"executionlisteners\":\"{\\\"executionListeners\\\":[]}\",\"eventlisteners\":\"{\\\"eventListeners\\\":[]}\",\"signaldefinitions\":\"[]\",\"messagedefinitions\":\"[]\",\"process_potentialstarteruser\":\"\",\"process_potentialstartergroup\":\"\",\"iseagerexecutionfetch\":\"false\",\"messages\":[]},\"childShapes\":[{\"resourceId\":\"startevent1\",\"properties\":{\"overrideid\":\"startevent1\",\"name\":\"开始节点\",\"documentation\":\"\",\"executionlisteners\":{\"executionListeners\":[]},\"initiator\":\"applyUserId\",\"formkeydefinition\":\"\",\"formreference\":\"\",\"formproperties\":\"\",\"interrupting\":true},\"stencil\":{\"id\":\"StartNoneEvent\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-C72C3F94-FFC6-4861-984C-84884B66DD47\"}],\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"dockers\":[]},{\"resourceId\":\"leave_leader_audit\",\"properties\":{\"overrideid\":\"leave_leader_audit\",\"name\":\"领导审批\",\"documentation\":\"\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]},\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"assignee\":\"${leaderName}\"}},\"formkeydefinition\":\"leave_leader_audit\",\"formreference\":\"\",\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":{\"taskListeners\":[{\"event\":\"complete\",\"className\":\"com.jsite.modules.flowable.listener.TaskBusinessCallListener\",\"implementation\":\"com.jsite.modules.flowable.listener.TaskBusinessCallListener\",\"$$hashKey\":\"uiGrid-000D\",\"fields\":[]}]},\"skipexpression\":\"\",\"categorydefinition\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-CA63049C-E8AD-45DB-A214-86AD0653D313\"}],\"bounds\":{\"lowerRight\":{\"x\":295,\"y\":218},\"upperLeft\":{\"x\":195,\"y\":138}},\"dockers\":[]},{\"resourceId\":\"sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26\",\"properties\":{\"overrideid\":\"sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26\",\"name\":\"\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"sequencefloworder\":\"\",\"executionlisteners\":{\"executionListeners\":[]}},\"stencil\":{\"id\":\"ExclusiveGateway\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-8EAC8A36-0C40-4596-8181-A33B1C95E544\"},{\"resourceId\":\"sid-57B75BD7-E1A6-4569-89FB-3C88F62C9384\"}],\"bounds\":{\"lowerRight\":{\"x\":400,\"y\":198},\"upperLeft\":{\"x\":360,\"y\":158}},\"dockers\":[]},{\"resourceId\":\"end\",\"properties\":{\"overrideid\":\"end\",\"name\":\"流程结束\",\"documentation\":\"\",\"executionlisteners\":{\"executionListeners\":[{\"event\":\"end\",\"implementation\":\"com.jsite.modules.flowable.listener.TaskBusinessCallListener\",\"className\":\"com.jsite.modules.flowable.listener.TaskBusinessCallListener\",\"expression\":\"\",\"delegateExpression\":\"\",\"$$hashKey\":\"uiGrid-0013\",\"fields\":[]}]}},\"stencil\":{\"id\":\"EndNoneEvent\"},\"childShapes\":[],\"outgoing\":[],\"bounds\":{\"lowerRight\":{\"x\":553,\"y\":193},\"upperLeft\":{\"x\":525,\"y\":165}},\"dockers\":[]},{\"resourceId\":\"adjustApply\",\"properties\":{\"overrideid\":\"adjustApply\",\"name\":\"调整申请\",\"documentation\":\"\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]},\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"assignee\":\"${applyUserId}\"}},\"formkeydefinition\":\"\",\"formreference\":\"\",\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":{\"taskListeners\":[]},\"skipexpression\":\"\",\"categorydefinition\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-9907B39C-4DF1-40A5-9A15-D6AEE19F792E\"}],\"bounds\":{\"lowerRight\":{\"x\":430,\"y\":365},\"upperLeft\":{\"x\":330,\"y\":285}},\"dockers\":[]},{\"resourceId\":\"sid-C72C3F94-FFC6-4861-984C-84884B66DD47\",\"properties\":{\"overrideid\":\"sid-C72C3F94-FFC6-4861-984C-84884B66DD47\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"leave_leader_audit\"}],\"bounds\":{\"lowerRight\":{\"x\":194.234375,\"y\":178},\"upperLeft\":{\"x\":130.21875,\"y\":178}},\"dockers\":[{\"x\":15,\"y\":15},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"leave_leader_audit\"}},{\"resourceId\":\"sid-52E803B4-4779-43D2-944C-3312F05EDEC0\",\"properties\":{\"overrideid\":\"sid-52E803B4-4779-43D2-944C-3312F05EDEC0\",\"name\":\"\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"sequencefloworder\":\"\",\"executionlisteners\":{\"executionListeners\":[]}},\"stencil\":{\"id\":\"ExclusiveGateway\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-6D5AB85C-A62B-4B08-820F-294FE68F0132\"},{\"resourceId\":\"sid-14B77A49-FD2A-47A2-8DA0-24B3F7715E47\"}],\"bounds\":{\"lowerRight\":{\"x\":400,\"y\":445},\"upperLeft\":{\"x\":360,\"y\":405}},\"dockers\":[]},{\"resourceId\":\"sid-CA63049C-E8AD-45DB-A214-86AD0653D313\",\"properties\":{\"overrideid\":\"sid-CA63049C-E8AD-45DB-A214-86AD0653D313\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26\"}],\"bounds\":{\"lowerRight\":{\"x\":360.015625,\"y\":178},\"upperLeft\":{\"x\":295.5703125,\"y\":178}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":20,\"y\":20}],\"target\":{\"resourceId\":\"sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26\"}},{\"resourceId\":\"sid-6D5AB85C-A62B-4B08-820F-294FE68F0132\",\"properties\":{\"overrideid\":\"sid-6D5AB85C-A62B-4B08-820F-294FE68F0132\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"${!auditPass}\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"end\"}],\"bounds\":{\"lowerRight\":{\"x\":539,\"y\":425},\"upperLeft\":{\"x\":399.6328125,\"y\":193.453125}},\"dockers\":[{\"x\":20,\"y\":20},{\"x\":539,\"y\":425},{\"x\":14,\"y\":14}],\"target\":{\"resourceId\":\"end\"}},{\"resourceId\":\"sid-9907B39C-4DF1-40A5-9A15-D6AEE19F792E\",\"properties\":{\"overrideid\":\"sid-9907B39C-4DF1-40A5-9A15-D6AEE19F792E\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-52E803B4-4779-43D2-944C-3312F05EDEC0\"}],\"bounds\":{\"lowerRight\":{\"x\":380,\"y\":405.25},\"upperLeft\":{\"x\":380,\"y\":365.0625}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":20,\"y\":20}],\"target\":{\"resourceId\":\"sid-52E803B4-4779-43D2-944C-3312F05EDEC0\"}},{\"resourceId\":\"sid-14B77A49-FD2A-47A2-8DA0-24B3F7715E47\",\"properties\":{\"overrideid\":\"sid-14B77A49-FD2A-47A2-8DA0-24B3F7715E47\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"${auditPass}\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"leave_leader_audit\"}],\"bounds\":{\"lowerRight\":{\"x\":360.015625,\"y\":425},\"upperLeft\":{\"x\":245,\"y\":218.55859375}},\"dockers\":[{\"x\":20,\"y\":20},{\"x\":245,\"y\":425},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"leave_leader_audit\"}},{\"resourceId\":\"sid-8EAC8A36-0C40-4596-8181-A33B1C95E544\",\"properties\":{\"overrideid\":\"sid-8EAC8A36-0C40-4596-8181-A33B1C95E544\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"${auditPass}\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"end\"}],\"bounds\":{\"lowerRight\":{\"x\":524.3359572771119,\"y\":178.90777331620825},\"upperLeft\":{\"x\":400.25388647288804,\"y\":178.12738293379175}},\"dockers\":[{\"x\":20,\"y\":20},{\"x\":14,\"y\":14}],\"target\":{\"resourceId\":\"end\"}},{\"resourceId\":\"sid-57B75BD7-E1A6-4569-89FB-3C88F62C9384\",\"properties\":{\"overrideid\":\"sid-57B75BD7-E1A6-4569-89FB-3C88F62C9384\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"${!auditPass}\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"adjustApply\"}],\"bounds\":{\"lowerRight\":{\"x\":380,\"y\":284.953125},\"upperLeft\":{\"x\":380,\"y\":198.5234375}},\"dockers\":[{\"x\":20,\"y\":20},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"adjustApply\"}}],\"stencil\":{\"id\":\"BPMNDiagram\"},\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\",\"url\":\"../editor/stencilsets/bpmn2.0/bpmn2.0.json\"}}', 0x89504E470D0A1A0A0000000D4948445200000105000000B60806000000D0BF98FE0000109F4944415478DAED9D0D90556519C7778A492A9CC871D2262D6BA868D4898A0A4BA7ADA184E4A31C76EF9EBBF7C28248488BE08662B1E886222AF9C1BA299484A2A316A3965F65BB8BB01B49AD4C868C6B30428ADBCAAEB62A222E2B9CFECFE5BC7459F7F3EEC5BBE7DEDF7FE6997BEEE572EFD9FFF3BCBFFBBEEF79CF397979082184104208218410420821847A96EFFB4376EEDCB96EF3E6CD076B6B6BFDEAEAEAD0464D4D8D5F5757F78A224666114A510684FAFA7ABFA5A5C56F6F6F0F7DB4B6B6FA1B376E7C5D80984276114A41D643C816202481A15D3D8746B28B500AB22143E746B5EDF1C5FE3F1FBD34117BDB9A42090641A183EC2294826C2CDEB941392058343E715D58A1E0935D848E0114B6555700058480C2FFA1B0635315504028D7A1B0EBA9B547A010D6C946A080509AA060138BC93D85ED9B6E010A08E52A140C00C940488EB01D85000A08A5010ADD01218C438910436138558906DD9C42364448A1304E61FBCD326D04148042DE58459BA25CD1AA28A53A1150C85D2838204C089E8F54EC522C530CA14A43A8929292E1B367CF5E3A67CE9C1D53A74E7DBBA8A8C88FC7E30766CD9AF5FCB469D3AEF13CEF44A00014FA0804A75314CF28D60086D4959F9F3F2412898C511B9CAF7659A158A0C82F28281876CCBE540DFF92193366ECABACAC3CD4D0D0E0EFDFBFDF37D9E3D34F3FEDAF58B1A27DFAF4E97B63B1581950000A5DCC21740584E449C727150F2B86D2C4FB0D83CB05801DF623DD45341B24F4839E5E5FD53BB871D1A245FB9B9A9AFC9E646721EA7DAF58AF012800854E4018D7CBFB860650A8CFE3C8449FA45EC00835F87A0780B973E7FA6BD6ACF1D7AD5BE7DF79E79DFEFCF9F393E1D0A05EC4C8B47CB1860A0B0C08AE67D09BDE79E79D0418065B8F01286474C830AE8FEF1F120C236C387132CDBE6720A891FFDBC160CB962D89B6D759D68B2F2B2B3BD26B50AFE28C01CF2168C8F0566F3D84AE7A0CEA2DFC573B7E3250C859288CED65C8D093960713902369FE5D0F195C0F61E9D2A57E6F3FD8068BEBAFBFFE488F6140F30CEA252CB339043F05AD58B1E2F56834BA0428E42414060204A705C1678C0603472B9843F04B4B4B0FEDDBB7AFCF3DF879F3E61D0AC05091F297EB4B775AB7241559B765FAF4E9CF02859C83C2B83400C129D6CFE147AEF41212938AFD6D9B5BB76E3D328C48B9B7A021C081BECE257496FDBF582CF6760669FA0FFDF1B3DD1F0F148EBDC7C104A1EDCF5569FC9AF2E0337376F231D9673BECE87A095DCD21F4A68B2FBEF8ED000CF929ED8CFDE781C8FE7FA68C4C9A75DDAB5803148EBDC70AEBEA17E51D5EA9382A0D5F7176F05925B9DC3B48F6D9F3BCBFD9B61D654845765422F8AC0599EA29B47773ECF43D8F6C86C260F1D845DEE1A5CBBB1523D20084A24E0D8350D861C75464FF6F40F30AF3E6CDDB3D9039850B2EB8607B26BB5C8A523B82C2F0E1BDF1B893ACE8EC4AD3A9AC721D1D0081CBD777F2D91EAD51DF7EFBED29B5CBD5AB57BF39A09EC2C2850BAB2A2B2B53FAF2AAAAAAF6783C7E35138D39BD4EE136458362583F7B086D799C41D91D2012730AFAC1ED3870E040BFDAA4CD41E8FFED1DD09C829169E6CC99EDCDCDCDFD5EA7A02F6F639D022B1AA507CDFEBCBE9DD7E0805044F3EF718EE1193724EE8F92869ABBEC2846CA3B505656B6A8BCBCFC407F56345E71C5156F080A0B0793914021631A1A40E1AE5EC0E080C090A177284C5174C462B183CF3DF75C9FDAE5F6EDDBFDA953A726E6F83CCF1B782F4CC388DF2C5EBCB8A3B71E83F510962C59F266696969E5603312286454C38361C4F25EE610E821F41D0CCBAD8147A3D143AB56ADF2DBDADABA6C93B6B8C98E541840ECFD1A7EDC94B69DB01EC38C19330E54555525164D743E4B72E5CA95072FBCF0C23734DC583C184D040A19970D256DE972793747199843E8FFFC82AD6CEC082E61E05F79E59589438EEE84A865CB96D90242B78AD1DE573EA0614377730C36F9684725A64D9BE676A663CE9C39BB15370FA63904A03028759AA25931330908CD0C1906048633D40ED7A9C7B0AF9BC397B658E9E1600D09020A8352A382B9830A860C69FDC11E5A585838568DFF4677C851C098D0CDE1620414069DEC8429DBEF122A33ED730DB76572253150000A039A7CA478D3ABE0DC8836F3951E025008EBAF1A5048AF9F25497309B37104280005A0D0E0A0604BA3710428000584AF400128207C050A4001E12B50000A085F8F816A6B6BB3110AFB05850E8A17E16B0AAAABAB6BB613B6B2090A4D4D4DF7090A8D142FC2D714B47EFDFAC91B366C786DCF9E3D6F65430FC180505353F382620AC58BF03545A9014DD02F6B8375B96D2C1EE2B0FD6F0C3310285E7CCD2945A3D1512486E2C55774449EE7AD54620EE104C58BAF889352285E7C45EF4A0827A550BCF88A8E4A0827A550BCF88A8E563C1EFFB012F3264E50BCF88A129A3469D2F19EE7BD8113142FBEA28482DB7BB5E104C58BAF28A1828282139498577182E2C5579490860E272A31AD3841F1E22B7250384989791927285E7C4509C562B18F2B31FFC1098A175F919B53F88412F3124E50BCF88A5C524E55BC8813142FBE229794D314BB7082E2C5579450341AFD8C12F33C4E50BCF88ADC9CC2082566074E50BCF88A1C143EEF79DE733841F1E22B4A2812897C4189791627285E7C4509A99770BA12B30D27285E7C45091517179FA9DEC2569CA078F1152564176EE5022B142FBEA2E439852F2B315B7082E2C557E49232DA2ECB8613142FBE229794AF2B36E304C58BAF2821CFF3CE5262FE8A13142FBEA2840A0B0BBFA9C4FC0527285E7C452E29E728EA7082E2C557E49292AF780227285E7C456E4EE13B4A4C2D4E50BCF88ADC9CC25881A11A27285E7C450945A3D1732391C89F7082E2C557949080305E89790C27BA2D5AEEB70914722E29E7291EC1896EFDE1CEDC4021B7E479DE44FD023E84135DABA0A06098DD56CF8AD76EB18723402117A0F00315FD8338D163E1DE46F102855C9A53385F89B93F8CFBEEFBFE909D3B77AEDBBC79F3C1DADA5ABFBABA3AB4515353E3D7D5D5BDA288010594E9A44C51AC0BE3BE1B10EAEBEBFD969616BFBDBD3DF4D1DADAEA6FDCB8F17501620A5040994C4A44715F18F7DD7A08D902842430B4ABE7D008145026E7143C25E69E30EEBB0D19921BD4B6C717FBFF7CF4D244EC6D6B0A2D1804850EA0803239A7502C2EDC1DC67DB7B17872637240B0687CE2BA3043C1070A289349892BD6661B14B655570005A080524C8A2DCE59936D50D8B1A90A280005946252662856670314763DB5F60814C23CD9081450A6271A674622915F871D0A36B198DC53D8BEE916A00014508A50F891A0B032CC50300024032139C278140228A08C4A40B84889B935CC50E80E08611D4A000594E9A4FC5851950D730AD9124001657AF83057BD854AA0001480027250982F28DC0414800250406E4EA14C60B801280005A0805C521628960305A00014904BCA42C5B54001280005E4E6147EAAC45C0314800250406E4E6191C0703550000A4001B9A42C562C010A4001282097940AC5954001280005E492B2C47A0B4001280005E4261AAFB67905A00014800272505826285C0E1480025040090908D7090C970105A00014904BCA725BD50814800250406EF870839DFF0014800250400E0A370B0AF3800250000AC8CD2954DA3515800250000AC825A5CAAEBE0414800250402E29769BF5D94001280005E492B24A310B280005A080DC9CC2AFEDDE0F4001280005E492B2DAEE120514800250402E2977A8A7300D280005A0805C52D6DA9DA7810250000A2821F512EE8E4422C54001280005E4A070AFA0500414800250402E29BF15180A810250000AC825659D620A50000A4001B9A4DCAFE1C3F961DCF7DADADA6C84C27E41A10328A04C26E5F782C2E430EE7B5D5D5D734B4B4B5641A1A9A9E93E41A11128A08C494078C8F3BC8961DCF7F5EBD74FDEB061C36B7BF6EC792B1B7A0806849A9A9A1714538002CA64521E519C17D6FD57039AA05FD606EB72DB583CC461FBDF984B40000A8337297F544F611C4E50BCF88A5C521ED710E27B3841F1E22B4A48BD84EAC2C2C2B13841F1E22B7249A91518BE8313142FBE2297940D82C2B77082E2C557E49252A7380727285E7C452E297F292C2CFC264E50BCF88A5C529E8C4422637082E2C557E492F2B76834FA359CA078F115B9A4342846E304C58BAFC825658B860F5FC6098A175F914BCAD382C2177182E2C557949080B0B5B8B8F84C9CA078F115B9A46CF33CEF749CA078F115B9A4340A0A237182E2C557E492F22F0D213E8713142FBE2297941D050505237082E2C557E492F27C341AFD0C4E74EB8FADE3F02DD4A3FA078E00855C48CABF05854FE144B7FE9438282866E30850C885A4BCA8381527BA968656C3D44368B3E22D2929198E2340211792F2920AFF1338D1BD264F9EFC1B8A1728E452529A05859371A25BD9456DFD3163C650BC40216792B2271E8F7F0C27BA945DBBB24D517EDC71C759F196620950C885A4B47A9E77224E740B8409C1735BE0B54BB14C31047B80423627E5550D1F4EC0891E81E0748AE219C51AC00014B23929AF090A1FC189A3E610BA0282931D817852F1B06228760185AC93860E6F4C9A34E9789C380A08BDDD316B680085FA00120828645552DE8CC7E31FC689234386BEDE426F48308CB0E104476F80425625657F4949C95080D0E390A1272DCF3B3C01C999A640216B92D25E5050F001809012109C16049FC1B52E81425624A5233F3F3F5767D2C7A501084EB17E0E3F105018B44939585151F1BE1CFCD36D82D00AF2AA347E6679F0994C3E0285702745F18EE26D9B74B44394B6A0C9963F2B762B76D93517EC0A4D8A67ECF4E1E074E22715F58A27ECCED57AFCA3E261C5037AFE3B3DDEA358AB6D3B6FE0578A5BF57F2BF578A35EBB5E8FD72896E8B52BF4F833BD7699B6CBF438578F17E9B50BF5385D8F71BDE669BB40DB3FD4E6446D8FD7E3770B0B0BBF6DB7BCD3F6597AEDABDAFE921ECFB02B49D93522F4FCB4E2E2E2536C19B72DD0B2139AEC04279B43097A47458A56C5A834587976F05925F644DFF3FEF1E3C71F3771E2C40FD9D19D6834FA51DB07DB173BD744F1493B655DF159EDF3176CBFB53D4AF115BB0F87F6FD1BF6B7D9DF687705D76BE7EAF9798A497AEFF9E687775831C534BD7E816296B6E798878AF97ABE40B150EF5D643EEBB59FEBF952C5B57AFE0BC54DDABEC572A358A558ADB843EFBB5B716F90C707147FD07B1F55FC49DB3596F3E076837FB5FB86D815C1ED02C0561F419D6C57EC54BC60E7D6285EB69AB213CBEC6897B6DFB2616B5077406130CA15B01D85B086E38AD71A94352C6B60D6D05CE1DA7D22ECAE527AED6C6DE75BD16A7B9C5E9B600DD715AC35E8A0615B03BFC88A55DB9768FB526DFF54DB8B5DA1EAB5EBB47D43000E2BD25F0540596B807105AA7F7F488F8F29FEACD7D62715E7DF0360B9C2DC11006D7700B8D6E06CC7BD36B96AC326C5A1BCC34B97772B46A401084501680F2581765FD010FE9B04DB978206F37CD0809E4D02EE534143DB647F9BFD8D06DDA0413E620D5471BFF9610D57DB77594356DC6E0D5BEFFBA5791834F8E50600BDEF6AF3D901D840A1E73F3170E8B5D200C2B314330C307A5EAC287220361029EFDF373059AE2DE70EC60630BB45805D11DC01D940A79AF9B481CF00A8D74E72503640EAB50FDA3C96D51DAD0F0D5655281A15A92CF71E1D00610A3622945DBA4DD1A018D6CF1E425B30C98810CA423DA8A8CEEBDB790D0E0845D88650F66A680085BB7A018303024306847240C38361C4F25EE610E821209443B273196CE97279173D8456E61010CA4D9DA66856CC4C024233430684725BA382B9830A860C0821273B61CA56DD95600542C88973191042082184104208218410420821841042082184104208BD5BFF0348366F0A515148200000000049454E44AE426082, 0, NULL);
INSERT INTO `ACT_DE_MODEL` VALUES ('af2f7a31-2f98-11ea-9129-00163e2e65eb', 'qweqwe', 'qqq', 'qq', NULL, '2020-01-05 16:52:20.888000', 'admin', '2020-01-05 16:52:20.888000', 'admin', 1, '{\"version\":0}', NULL, 2, NULL);
INSERT INTO `ACT_DE_MODEL` VALUES ('de1c6c0a-4d6d-11ea-9b4e-00163e2e65eb', '12', '12', '12', NULL, '2020-02-12 16:01:26.100000', 'admin', '2020-02-12 16:06:40.233000', 'admin', 1, '{\"modelId\":\"de1c6c0a-4d6d-11ea-9b4e-00163e2e65eb\",\"bounds\":{\"lowerRight\":{\"x\":1200,\"y\":1050},\"upperLeft\":{\"x\":0,\"y\":0}},\"properties\":{\"process_id\":\"12\",\"name\":\"12\",\"documentation\":\"12\",\"process_author\":\"\",\"process_version\":\"\",\"process_namespace\":\"http://www.flowable.org/processdef\",\"process_historylevel\":\"\",\"isexecutable\":true,\"dataproperties\":\"\",\"executionlisteners\":\"\",\"eventlisteners\":\"\",\"signaldefinitions\":\"\",\"messagedefinitions\":\"\",\"process_potentialstarteruser\":\"\",\"process_potentialstartergroup\":\"\",\"iseagerexecutionfetch\":\"false\"},\"childShapes\":[{\"resourceId\":\"startEvent1\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"executionlisteners\":\"\",\"initiator\":\"\",\"formkeydefinition\":\"\",\"formreference\":\"\",\"formproperties\":\"\"},\"stencil\":{\"id\":\"StartNoneEvent\"},\"childShapes\":[],\"outgoing\":[],\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"dockers\":[]}],\"stencil\":{\"id\":\"BPMNDiagram\"},\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\",\"url\":\"../editor/stencilsets/bpmn2.0/bpmn2.0.json\"}}', 0x89504E470D0A1A0A0000000D494844520000002D0000002D08060000003A1AE29A000002E04944415478DAED58DF6B526118DEC52EFA23BA080AEACFF08F48747A5051F157F3D74C10744CA6178A8C5D0B42188B2E4236314B6585919814132744B46A3936E7C19B31AA63FEC8AFF71D79135DCC73BEF38DE03C7038777ECF7978BEE77D5E1716142850F0FF821072ADDD6EDBB3D9ECAB442271EEF178C626938968B55AC271DCD4E9740EC3E1F0613299DC82E7F69593CDE5720F63B1D8D8E7F391743A4D1A8D06E976BB6430181004BEFBFD3ED9DBDB23F051C4EBF54EFD7E7FC766B35999130672E6783CFE33180C5E109A4C26E4B268B55A24140A4DEC76FB07B55A7D8B85BA8BF97CFEA5C3E1208542612EB27FA3542A11B3D92CC8AA3A12AE56AB6FDD6E373938382034D0E97408082058ADD67BB290468591307A9426F0F740ED6F4B4B4B1C750FA3256829FC2FC50D06C31910BF432D25D6D7D787E86139512C1605BD5EFF860AE9EDEDED479812A3D188C80D97CBC54BB609AA8C398CB1C602FBFBFB43C96AE3A4C3CB2725DAE605C4200FD3F48668D2389A33990C6189CDCDCD1690BE2F9A3476091CCD2C51AFD73B40BA209A34969F5EAFC79434CFF37D207D2C9A34B6B559F961053C0F480F4493C67A7915C0734593361A8D53D64A0B82C04B521AE26EC8DAD3A7A7A71F25797A7575F513EBF42897CBCF24A5472A95CAB0CEE9B5B5B5A2A49C8E4422379797977F319C88039D4EF745D24444ACACAC7C66D53DC01A5B40F8B5E49667B158D4814060CC40ED3350B9496D198025B42D779FC6FB832AAB54AA452AA4D163B884CAB5B9409BCCC3193D6A9BCB0C1CC7E941F11FB477C49393931AD8E288FA8E38031474332EA1B414478591B046A309C9FADF071CA005D5CF777676BE4BB89C671B1B1B0FD012F09898FCC3841E07855E805DF866B329CC93C3954AE5317CF43BBC74D43D7C49F277913C54D83E2C0CEF6BB5DA51B7DBEDCD4A16961FEC12BBBBBB4FA3D1E873B0D721924575A9A58404AF5F07D57C40E6093C5FB1A561BDFCF33EC62E81A359F2A453A0408102050AE4C66FB109509232650D3B0000000049454E44AE426082, 0, NULL);
INSERT INTO `ACT_DE_MODEL` VALUES ('de8d86ae-4c90-11ea-9b4e-00163e2e65eb', 'Mytest', 'Mytest', 'This is a test program', NULL, '2020-02-11 13:39:28.056000', 'admin', '2020-02-11 13:39:28.056000', 'admin', 1, '{\"id\":\"canvas\",\"resourceId\":\"canvas\",\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\"},\"properties\":{\"process_id\":\"Mytest\",\"name\":\"Mytest\",\"documentation\":\"This is a test program\"},\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"childShapes\":[],\"dockers\":[],\"outgoing\":[],\"resourceId\":\"startEvent1\",\"stencil\":{\"id\":\"StartNoneEvent\"}}]}', 0x89504E470D0A1A0A0000000D494844520000002D0000002D08060000003A1AE29A000002E04944415478DAED58DF6B526118DEC52EFA23BA080AEACFF08F48747A5051F157F3D74C10744CA6178A8C5D0B42188B2E4236314B6585919814132744B46A3936E7C19B31AA63FEC8AFF71D79135DCC73BEF38DE03C7038777ECF7978BEE77D5E1716142850F0FF821072ADDD6EDBB3D9ECAB442271EEF178C626938968B55AC271DCD4E9740EC3E1F0613299DC82E7F69593CDE5720F63B1D8D8E7F391743A4D1A8D06E976BB6430181004BEFBFD3ED9DBDB23F051C4EBF54EFD7E7FC766B35999130672E6783CFE33180C5E109A4C26E4B268B55A24140A4DEC76FB07B55A7D8B85BA8BF97CFEA5C3E1208542612EB27FA3542A11B3D92CC8AA3A12AE56AB6FDD6E373938382034D0E97408082058ADD67BB290468591307A9426F0F740ED6F4B4B4B1C750FA3256829FC2FC50D06C31910BF432D25D6D7D787E86139512C1605BD5EFF860AE9EDEDED479812A3D188C80D97CBC54BB609AA8C398CB1C602FBFBFB43C96AE3A4C3CB2725DAE605C4200FD3F48668D2389A33990C6189CDCDCD1690BE2F9A3476091CCD2C51AFD73B40BA209A34969F5EAFC79434CFF37D207D2C9A34B6B559F961053C0F480F4493C67A7915C0734593361A8D53D64A0B82C04B521AE26EC8DAD3A7A7A71F25797A7575F513EBF42897CBCF24A5472A95CAB0CEE9B5B5B5A2A49C8E4422379797977F319C88039D4EF745D24444ACACAC7C66D53DC01A5B40F8B5E49667B158D4814060CC40ED3350B9496D198025B42D779FC6FB832AAB54AA452AA4D163B884CAB5B9409BCCC3193D6A9BCB0C1CC7E941F11FB477C49393931AD8E288FA8E38031474332EA1B414478591B046A309C9FADF071CA005D5CF777676BE4BB89C671B1B1B0FD012F09898FCC3841E07855E805DF866B329CC93C3954AE5317CF43BBC74D43D7C49F277913C54D83E2C0CEF6BB5DA51B7DBEDCD4A16961FEC12BBBBBB4FA3D1E873B0D721924575A9A58404AF5F07D57C40E6093C5FB1A561BDFCF33EC62E81A359F2A453A0408102050AE4C66FB109509232650D3B0000000049454E44AE426082, 0, NULL);

-- ----------------------------
-- Table structure for ACT_DE_MODEL_HISTORY
-- ----------------------------
DROP TABLE IF EXISTS `ACT_DE_MODEL_HISTORY`;
CREATE TABLE `ACT_DE_MODEL_HISTORY`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(400) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `model_key` varchar(400) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `description` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `model_comment` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `created` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `last_updated` datetime(6) NULL DEFAULT NULL,
  `last_updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `removal_date` datetime(6) NULL DEFAULT NULL,
  `version` int(11) NULL DEFAULT NULL,
  `model_editor_json` longtext CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `model_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `model_type` int(11) NULL DEFAULT NULL,
  `tenant_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_proc_mod_history_proc`(`model_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_DE_MODEL_HISTORY
-- ----------------------------
INSERT INTO `ACT_DE_MODEL_HISTORY` VALUES ('1ff25555-3675-11ea-aa63-00163e2e65eb', '请假流程11', 'jsite_leave111', '请假流程111', NULL, '2020-01-05 16:48:46.511000', 'admin', '2020-01-14 10:25:15.949000', 'admin', '2020-01-14 10:25:26.327000', 1, '{\"modelId\":\"2f70fe40-2f98-11ea-9129-00163e2e65eb\",\"bounds\":{\"lowerRight\":{\"x\":1200,\"y\":1050},\"upperLeft\":{\"x\":0,\"y\":0}},\"properties\":{\"process_id\":\"jsite_leave111\",\"name\":\"请假流程11\",\"documentation\":\"请假流程111\",\"process_author\":\"\",\"process_version\":\"\",\"process_namespace\":\"http://www.flowable.org/processdef\",\"process_historylevel\":\"\",\"isexecutable\":true,\"dataproperties\":\"\",\"executionlisteners\":\"{\\\"executionListeners\\\":[]}\",\"eventlisteners\":\"{\\\"eventListeners\\\":[]}\",\"signaldefinitions\":\"[]\",\"messagedefinitions\":\"[]\",\"process_potentialstarteruser\":\"\",\"process_potentialstartergroup\":\"\",\"iseagerexecutionfetch\":\"false\",\"messages\":[]},\"childShapes\":[{\"resourceId\":\"startevent1\",\"properties\":{\"overrideid\":\"startevent1\",\"name\":\"开始节点\",\"documentation\":\"\",\"executionlisteners\":{\"executionListeners\":[]},\"initiator\":\"applyUserId\",\"formkeydefinition\":\"\",\"formreference\":{\"id\":\"af2f7a31-2f98-11ea-9129-00163e2e65eb\",\"name\":\"qweqwe\",\"key\":\"qqq\"},\"formproperties\":\"\",\"interrupting\":true},\"stencil\":{\"id\":\"StartNoneEvent\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-C72C3F94-FFC6-4861-984C-84884B66DD47\"}],\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"dockers\":[]},{\"resourceId\":\"leave_leader_audit\",\"properties\":{\"overrideid\":\"leave_leader_audit\",\"name\":\"领导审批\",\"documentation\":\"\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]},\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"assignee\":\"${leaderName}\"}},\"formkeydefinition\":\"leave_leader_audit\",\"formreference\":{\"id\":\"02d0e7c3-3675-11ea-aa63-00163e2e65eb\",\"name\":\"sdgsd\",\"key\":\"sdgsg\"},\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":{\"taskListeners\":[{\"event\":\"complete\",\"className\":\"com.jsite.modules.flowable.listener.TaskBusinessCallListener\",\"implementation\":\"com.jsite.modules.flowable.listener.TaskBusinessCallListener\",\"$$hashKey\":\"uiGrid-000D\",\"fields\":[]}]},\"skipexpression\":\"\",\"categorydefinition\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-CA63049C-E8AD-45DB-A214-86AD0653D313\"}],\"bounds\":{\"lowerRight\":{\"x\":295,\"y\":218},\"upperLeft\":{\"x\":195,\"y\":138}},\"dockers\":[]},{\"resourceId\":\"sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26\",\"properties\":{\"overrideid\":\"sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26\",\"name\":\"\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"sequencefloworder\":\"\",\"executionlisteners\":{\"executionListeners\":[]}},\"stencil\":{\"id\":\"ExclusiveGateway\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-8EAC8A36-0C40-4596-8181-A33B1C95E544\"},{\"resourceId\":\"sid-57B75BD7-E1A6-4569-89FB-3C88F62C9384\"}],\"bounds\":{\"lowerRight\":{\"x\":400,\"y\":198},\"upperLeft\":{\"x\":360,\"y\":158}},\"dockers\":[]},{\"resourceId\":\"end\",\"properties\":{\"overrideid\":\"end\",\"name\":\"流程结束\",\"documentation\":\"\",\"executionlisteners\":{\"executionListeners\":[]}},\"stencil\":{\"id\":\"EndNoneEvent\"},\"childShapes\":[],\"outgoing\":[],\"bounds\":{\"lowerRight\":{\"x\":553,\"y\":193},\"upperLeft\":{\"x\":525,\"y\":165}},\"dockers\":[]},{\"resourceId\":\"adjustApply\",\"properties\":{\"overrideid\":\"adjustApply\",\"name\":\"调整申请\",\"documentation\":\"\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]},\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"assignee\":\"${applyUserId}\"}},\"formkeydefinition\":\"\",\"formreference\":\"\",\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":{\"taskListeners\":[]},\"skipexpression\":\"\",\"categorydefinition\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-9907B39C-4DF1-40A5-9A15-D6AEE19F792E\"}],\"bounds\":{\"lowerRight\":{\"x\":430,\"y\":365},\"upperLeft\":{\"x\":330,\"y\":285}},\"dockers\":[]},{\"resourceId\":\"sid-C72C3F94-FFC6-4861-984C-84884B66DD47\",\"properties\":{\"overrideid\":\"sid-C72C3F94-FFC6-4861-984C-84884B66DD47\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"leave_leader_audit\"}],\"bounds\":{\"lowerRight\":{\"x\":194.234375,\"y\":178},\"upperLeft\":{\"x\":130.21875,\"y\":178}},\"dockers\":[{\"x\":15,\"y\":15},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"leave_leader_audit\"}},{\"resourceId\":\"sid-52E803B4-4779-43D2-944C-3312F05EDEC0\",\"properties\":{\"overrideid\":\"sid-52E803B4-4779-43D2-944C-3312F05EDEC0\",\"name\":\"\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"sequencefloworder\":\"\",\"executionlisteners\":{\"executionListeners\":[]}},\"stencil\":{\"id\":\"ExclusiveGateway\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-6D5AB85C-A62B-4B08-820F-294FE68F0132\"},{\"resourceId\":\"sid-14B77A49-FD2A-47A2-8DA0-24B3F7715E47\"}],\"bounds\":{\"lowerRight\":{\"x\":400,\"y\":445},\"upperLeft\":{\"x\":360,\"y\":405}},\"dockers\":[]},{\"resourceId\":\"sid-CA63049C-E8AD-45DB-A214-86AD0653D313\",\"properties\":{\"overrideid\":\"sid-CA63049C-E8AD-45DB-A214-86AD0653D313\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26\"}],\"bounds\":{\"lowerRight\":{\"x\":360.015625,\"y\":178},\"upperLeft\":{\"x\":295.5703125,\"y\":178}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":20,\"y\":20}],\"target\":{\"resourceId\":\"sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26\"}},{\"resourceId\":\"sid-6D5AB85C-A62B-4B08-820F-294FE68F0132\",\"properties\":{\"overrideid\":\"sid-6D5AB85C-A62B-4B08-820F-294FE68F0132\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"${!auditPass}\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"end\"}],\"bounds\":{\"lowerRight\":{\"x\":539,\"y\":425},\"upperLeft\":{\"x\":399.6328125,\"y\":193.453125}},\"dockers\":[{\"x\":20,\"y\":20},{\"x\":539,\"y\":425},{\"x\":14,\"y\":14}],\"target\":{\"resourceId\":\"end\"}},{\"resourceId\":\"sid-9907B39C-4DF1-40A5-9A15-D6AEE19F792E\",\"properties\":{\"overrideid\":\"sid-9907B39C-4DF1-40A5-9A15-D6AEE19F792E\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-52E803B4-4779-43D2-944C-3312F05EDEC0\"}],\"bounds\":{\"lowerRight\":{\"x\":380,\"y\":405.25},\"upperLeft\":{\"x\":380,\"y\":365.0625}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":20,\"y\":20}],\"target\":{\"resourceId\":\"sid-52E803B4-4779-43D2-944C-3312F05EDEC0\"}},{\"resourceId\":\"sid-14B77A49-FD2A-47A2-8DA0-24B3F7715E47\",\"properties\":{\"overrideid\":\"sid-14B77A49-FD2A-47A2-8DA0-24B3F7715E47\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"${auditPass}\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"leave_leader_audit\"}],\"bounds\":{\"lowerRight\":{\"x\":360.015625,\"y\":425},\"upperLeft\":{\"x\":245,\"y\":218.55859375}},\"dockers\":[{\"x\":20,\"y\":20},{\"x\":245,\"y\":425},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"leave_leader_audit\"}},{\"resourceId\":\"sid-8EAC8A36-0C40-4596-8181-A33B1C95E544\",\"properties\":{\"overrideid\":\"sid-8EAC8A36-0C40-4596-8181-A33B1C95E544\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"${auditPass}\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"end\"}],\"bounds\":{\"lowerRight\":{\"x\":524.3359572771119,\"y\":178.90777331620825},\"upperLeft\":{\"x\":400.25388647288804,\"y\":178.12738293379175}},\"dockers\":[{\"x\":20,\"y\":20},{\"x\":14,\"y\":14}],\"target\":{\"resourceId\":\"end\"}},{\"resourceId\":\"sid-57B75BD7-E1A6-4569-89FB-3C88F62C9384\",\"properties\":{\"overrideid\":\"sid-57B75BD7-E1A6-4569-89FB-3C88F62C9384\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"${!auditPass}\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"adjustApply\"}],\"bounds\":{\"lowerRight\":{\"x\":380,\"y\":284.953125},\"upperLeft\":{\"x\":380,\"y\":198.5234375}},\"dockers\":[{\"x\":20,\"y\":20},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"adjustApply\"}}],\"stencil\":{\"id\":\"BPMNDiagram\"},\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\",\"url\":\"../editor/stencilsets/bpmn2.0/bpmn2.0.json\"}}', '2f70fe40-2f98-11ea-9129-00163e2e65eb', 0, NULL);
INSERT INTO `ACT_DE_MODEL_HISTORY` VALUES ('b5388e4f-3674-11ea-aa63-00163e2e65eb', '工作日志', 'job_diary1', '', NULL, '2020-01-06 16:56:20.269000', 'admin', '2020-01-06 16:56:20.269000', 'admin', '2020-01-14 10:22:27.270000', 1, '{\"id\":\"canvas\",\"resourceId\":\"canvas\",\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\"},\"properties\":{\"process_id\":\"job_diary1\",\"name\":\"工作日志\"},\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"childShapes\":[],\"dockers\":[],\"outgoing\":[],\"resourceId\":\"startEvent1\",\"stencil\":{\"id\":\"StartNoneEvent\"}}]}', '684a76db-3062-11ea-9129-00163e2e65eb', 0, NULL);
INSERT INTO `ACT_DE_MODEL_HISTORY` VALUES ('b7485b80-3674-11ea-aa63-00163e2e65eb', '是支持支持', '水电费', '', NULL, '2020-01-06 20:43:19.047000', 'admin', '2020-01-06 20:43:19.047000', 'admin', '2020-01-14 10:22:30.730000', 1, '{\"id\":\"canvas\",\"resourceId\":\"canvas\",\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\"},\"properties\":{\"process_id\":\"水电费\",\"name\":\"是支持支持\"},\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"childShapes\":[],\"dockers\":[],\"outgoing\":[],\"resourceId\":\"startEvent1\",\"stencil\":{\"id\":\"StartNoneEvent\"}}]}', '1db64f2c-3082-11ea-9129-00163e2e65eb', 0, NULL);
INSERT INTO `ACT_DE_MODEL_HISTORY` VALUES ('b9378151-3674-11ea-aa63-00163e2e65eb', 'fggf', '222', '22', NULL, '2020-01-06 15:52:19.713000', 'admin', '2020-01-06 15:52:19.713000', 'admin', '2020-01-14 10:22:33.975000', 1, '{\"id\":\"canvas\",\"resourceId\":\"canvas\",\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\"},\"properties\":{\"process_id\":\"222\",\"name\":\"fggf\",\"documentation\":\"22\"},\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"childShapes\":[],\"dockers\":[],\"outgoing\":[],\"resourceId\":\"startEvent1\",\"stencil\":{\"id\":\"StartNoneEvent\"}}]}', '77235cbb-3059-11ea-9129-00163e2e65eb', 0, NULL);
INSERT INTO `ACT_DE_MODEL_HISTORY` VALUES ('bb834612-3674-11ea-aa63-00163e2e65eb', '出差申请', 'yeyb', '', NULL, '2020-01-05 16:09:57.326000', 'admin', '2020-01-05 16:11:06.369000', 'admin', '2020-01-14 10:22:37.827000', 1, '{\"modelId\":\"c33396bf-2f92-11ea-9129-00163e2e65eb\",\"bounds\":{\"lowerRight\":{\"x\":1200,\"y\":1050},\"upperLeft\":{\"x\":0,\"y\":0}},\"properties\":{\"process_id\":\"yeyb\",\"name\":\"出差申请\",\"documentation\":\"\",\"process_author\":\"\",\"process_version\":\"\",\"process_namespace\":\"http://www.flowable.org/processdef\",\"process_historylevel\":\"\",\"isexecutable\":true,\"dataproperties\":\"\",\"executionlisteners\":\"\",\"eventlisteners\":\"\",\"signaldefinitions\":\"\",\"messagedefinitions\":\"\",\"process_potentialstarteruser\":\"\",\"process_potentialstartergroup\":\"\",\"iseagerexecutionfetch\":\"false\"},\"childShapes\":[{\"resourceId\":\"startEvent1\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"executionlisteners\":\"\",\"initiator\":\"\",\"formkeydefinition\":\"\",\"formreference\":\"\",\"formproperties\":\"\"},\"stencil\":{\"id\":\"StartNoneEvent\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-A1790EB8-8C0C-4CCB-844B-891723EFF134\"}],\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"dockers\":[]},{\"resourceId\":\"sid-91E9D67B-90FF-418E-B350-3F97F0104E0F\",\"properties\":{\"overrideid\":\"\",\"name\":\"出差\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"executionlisteners\":\"\",\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"assignee\":\"jsite\"}},\"formkeydefinition\":\"\",\"formreference\":\"\",\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":\"\",\"skipexpression\":\"\",\"categorydefinition\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-FE45A69F-C0DE-473D-818F-D66F663ABF89\"}],\"bounds\":{\"lowerRight\":{\"x\":275,\"y\":218},\"upperLeft\":{\"x\":175,\"y\":138}},\"dockers\":[]},{\"resourceId\":\"sid-A1790EB8-8C0C-4CCB-844B-891723EFF134\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-91E9D67B-90FF-418E-B350-3F97F0104E0F\"}],\"bounds\":{\"lowerRight\":{\"x\":174.15625,\"y\":178},\"upperLeft\":{\"x\":130.609375,\"y\":178}},\"dockers\":[{\"x\":15,\"y\":15},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"sid-91E9D67B-90FF-418E-B350-3F97F0104E0F\"}},{\"resourceId\":\"sid-D15AEFF5-2BC7-449C-BEEA-9185A685E0EF\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"executionlisteners\":\"\"},\"stencil\":{\"id\":\"EndNoneEvent\"},\"childShapes\":[],\"outgoing\":[],\"bounds\":{\"lowerRight\":{\"x\":348,\"y\":192},\"upperLeft\":{\"x\":320,\"y\":164}},\"dockers\":[]},{\"resourceId\":\"sid-FE45A69F-C0DE-473D-818F-D66F663ABF89\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-D15AEFF5-2BC7-449C-BEEA-9185A685E0EF\"}],\"bounds\":{\"lowerRight\":{\"x\":319.375,\"y\":178},\"upperLeft\":{\"x\":275.390625,\"y\":178}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":14,\"y\":14}],\"target\":{\"resourceId\":\"sid-D15AEFF5-2BC7-449C-BEEA-9185A685E0EF\"}}],\"stencil\":{\"id\":\"BPMNDiagram\"},\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\",\"url\":\"../editor/stencilsets/bpmn2.0/bpmn2.0.json\"}}', 'c33396bf-2f92-11ea-9129-00163e2e65eb', 0, NULL);
INSERT INTO `ACT_DE_MODEL_HISTORY` VALUES ('debce476-2d31-11ea-91ac-c85b7643dd9e', '请假流程', 'leave', '请假流程演示', NULL, '2020-01-02 07:27:58.766000', 'admin', '2020-01-02 07:27:58.766000', 'admin', '2020-01-02 07:31:20.179000', 1, '{\"bounds\":{\"lowerRight\":{\"x\":1485.0,\"y\":700.0},\"upperLeft\":{\"x\":0.0,\"y\":0.0}},\"resourceId\":\"canvas\",\"stencil\":{\"id\":\"BPMNDiagram\"},\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\",\"url\":\"../editor/stencilsets/bpmn2.0/bpmn2.0.json\"},\"properties\":{\"process_id\":\"leave\",\"name\":\"请假流程\",\"documentation\":\"请假流程演示\",\"process_namespace\":\"com.jsite.modules.oa.leave\",\"iseagerexecutionfetch\":false,\"messages\":[],\"executionlisteners\":{\"executionListeners\":[]},\"eventlisteners\":{\"eventListeners\":[]},\"signaldefinitions\":[],\"messagedefinitions\":[]},\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":195.0,\"y\":122.5},\"upperLeft\":{\"x\":165.0,\"y\":92.5}},\"resourceId\":\"startevent1\",\"childShapes\":[],\"stencil\":{\"id\":\"StartNoneEvent\"},\"properties\":{\"overrideid\":\"startevent1\",\"name\":\"发起流程\",\"initiator\":\"applyUserId\",\"formkeydefinition\":\"/oa/leave/form\",\"interrupting\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow2\"}]},{\"bounds\":{\"lowerRight\":{\"x\":356.1111107840623,\"y\":135.0},\"upperLeft\":{\"x\":251.1111107840623,\"y\":80.0}},\"resourceId\":\"deptLeaderAudit\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"deptLeaderAudit\",\"name\":\"部门领导审批\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"candidateGroups\":[{\"value\":\"dept\"}]}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"tasklisteners\":{\"taskListeners\":[]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow3\"}]},{\"bounds\":{\"lowerRight\":{\"x\":457.7777769601558,\"y\":127.0},\"upperLeft\":{\"x\":417.7777769601558,\"y\":87.0}},\"resourceId\":\"exclusivegateway5\",\"childShapes\":[],\"stencil\":{\"id\":\"ExclusiveGateway\"},\"properties\":{\"overrideid\":\"exclusivegateway5\",\"name\":\"Exclusive Gateway\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow4\"},{\"resourceId\":\"flow5\"}]},{\"bounds\":{\"lowerRight\":{\"x\":490.27777696015585,\"y\":245.0},\"upperLeft\":{\"x\":385.2777769601558,\"y\":190.0}},\"resourceId\":\"modifyApply\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"modifyApply\",\"name\":\"调整申请\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"assignee\":\"${applyUserId}\"}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"tasklisteners\":{\"taskListeners\":[{\"event\":\"complete\",\"delegateExpression\":\"${leaveModifyProcessor}\"}]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow11\"}]},{\"bounds\":{\"lowerRight\":{\"x\":652.4074062082284,\"y\":135.0},\"upperLeft\":{\"x\":547.4074062082284,\"y\":80.0}},\"resourceId\":\"hrAudit\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"hrAudit\",\"name\":\"人事审批\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"candidateGroups\":[{\"value\":\"hr\"}]}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"tasklisteners\":{\"taskListeners\":[]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow6\"}]},{\"bounds\":{\"lowerRight\":{\"x\":754.0740723843219,\"y\":127.5},\"upperLeft\":{\"x\":714.0740723843219,\"y\":87.5}},\"resourceId\":\"exclusivegateway6\",\"childShapes\":[],\"stencil\":{\"id\":\"ExclusiveGateway\"},\"properties\":{\"overrideid\":\"exclusivegateway6\",\"name\":\"Exclusive Gateway\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow9\"},{\"resourceId\":\"flow7\"}]},{\"bounds\":{\"lowerRight\":{\"x\":928.722220096405,\"y\":311.0},\"upperLeft\":{\"x\":900.722220096405,\"y\":283.0}},\"resourceId\":\"endevent1\",\"childShapes\":[],\"stencil\":{\"id\":\"EndNoneEvent\"},\"properties\":{\"overrideid\":\"endevent1\",\"name\":\"End\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[]},{\"bounds\":{\"lowerRight\":{\"x\":457.7777769601558,\"y\":317.0},\"upperLeft\":{\"x\":417.7777769601558,\"y\":277.0}},\"resourceId\":\"exclusivegateway7\",\"childShapes\":[],\"stencil\":{\"id\":\"ExclusiveGateway\"},\"properties\":{\"overrideid\":\"exclusivegateway7\",\"name\":\"Exclusive Gateway\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow12\"},{\"resourceId\":\"flow10\"}]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow6\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":52.5,\"y\":27.5},{\"x\":20.0,\"y\":20.0}],\"outgoing\":[{\"resourceId\":\"exclusivegateway6\"}],\"target\":{\"resourceId\":\"exclusivegateway6\"},\"properties\":{\"overrideid\":\"flow6\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow3\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":52.5,\"y\":27.5},{\"x\":20.0,\"y\":20.0}],\"outgoing\":[{\"resourceId\":\"exclusivegateway5\"}],\"target\":{\"resourceId\":\"exclusivegateway5\"},\"properties\":{\"overrideid\":\"flow3\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow2\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":15.0,\"y\":15.0},{\"x\":52.5,\"y\":27.5}],\"outgoing\":[{\"resourceId\":\"deptLeaderAudit\"}],\"target\":{\"resourceId\":\"deptLeaderAudit\"},\"properties\":{\"overrideid\":\"flow2\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow4\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":52.50000000000003,\"y\":27.5}],\"outgoing\":[{\"resourceId\":\"modifyApply\"}],\"target\":{\"resourceId\":\"modifyApply\"},\"properties\":{\"overrideid\":\"flow4\",\"name\":\"不同意\",\"conditionsequenceflow\":\"${!auditPass}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow11\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":52.50000000000003,\"y\":27.5},{\"x\":20.0,\"y\":20.0}],\"outgoing\":[{\"resourceId\":\"exclusivegateway7\"}],\"target\":{\"resourceId\":\"exclusivegateway7\"},\"properties\":{\"overrideid\":\"flow11\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow12\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":14.0,\"y\":14.0}],\"outgoing\":[{\"resourceId\":\"endevent1\"}],\"target\":{\"resourceId\":\"endevent1\"},\"properties\":{\"overrideid\":\"flow12\",\"name\":\"结束流程\",\"conditionsequenceflow\":\"${!auditPass}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow10\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":303.0,\"y\":297.0},{\"x\":52.5,\"y\":27.5}],\"outgoing\":[{\"resourceId\":\"deptLeaderAudit\"}],\"target\":{\"resourceId\":\"deptLeaderAudit\"},\"properties\":{\"overrideid\":\"flow10\",\"name\":\"重新申请\",\"conditionsequenceflow\":\"${auditPass}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow9\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":734.0,\"y\":217.0},{\"x\":52.50000000000003,\"y\":27.5}],\"outgoing\":[{\"resourceId\":\"modifyApply\"}],\"target\":{\"resourceId\":\"modifyApply\"},\"properties\":{\"overrideid\":\"flow9\",\"name\":\"不同意\",\"conditionsequenceflow\":\"${!auditPass}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow7\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":914.0,\"y\":106.0},{\"x\":14.0,\"y\":14.0}],\"outgoing\":[{\"resourceId\":\"endevent1\"}],\"target\":{\"resourceId\":\"endevent1\"},\"properties\":{\"overrideid\":\"flow7\",\"name\":\"同意\",\"conditionsequenceflow\":\"${auditPass}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow5\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":52.5,\"y\":27.5}],\"outgoing\":[{\"resourceId\":\"hrAudit\"}],\"target\":{\"resourceId\":\"hrAudit\"},\"properties\":{\"overrideid\":\"flow5\",\"name\":\"同意\",\"conditionsequenceflow\":\"${auditPass}\"}}]}', '66db8014-2d31-11ea-91ac-c85b7643dd9e', 0, NULL);

-- ----------------------------
-- Table structure for ACT_DE_MODEL_RELATION
-- ----------------------------
DROP TABLE IF EXISTS `ACT_DE_MODEL_RELATION`;
CREATE TABLE `ACT_DE_MODEL_RELATION`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `parent_model_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `model_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `relation_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_relation_parent`(`parent_model_id`) USING BTREE,
  INDEX `fk_relation_child`(`model_id`) USING BTREE,
  CONSTRAINT `fk_relation_child` FOREIGN KEY (`model_id`) REFERENCES `ACT_DE_MODEL` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_relation_parent` FOREIGN KEY (`parent_model_id`) REFERENCES `ACT_DE_MODEL` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_EVT_LOG
-- ----------------------------
DROP TABLE IF EXISTS `ACT_EVT_LOG`;
CREATE TABLE `ACT_EVT_LOG`  (
  `LOG_NR_` bigint(20) NOT NULL AUTO_INCREMENT,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_STAMP_` timestamp(3) NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DATA_` longblob NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `IS_PROCESSED_` tinyint(4) NULL DEFAULT 0,
  PRIMARY KEY (`LOG_NR_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_GE_BYTEARRAY
-- ----------------------------
DROP TABLE IF EXISTS `ACT_GE_BYTEARRAY`;
CREATE TABLE `ACT_GE_BYTEARRAY`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTES_` longblob NULL,
  `GENERATED_` tinyint(4) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_FK_BYTEARR_DEPL`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_BYTEARR_DEPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `ACT_RE_DEPLOYMENT` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_GE_BYTEARRAY
-- ----------------------------
INSERT INTO `ACT_GE_BYTEARRAY` VALUES ('4eafd3ff-2df3-11ea-aa23-c85b7643dd9e', 1, '请假流程.bpmn20.xml', '4eafd3fe-2df3-11ea-aa23-c85b7643dd9e', 0x3C3F786D6C2076657273696F6E3D27312E302720656E636F64696E673D275554462D38273F3E0A3C646566696E6974696F6E7320786D6C6E733D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F4D4F44454C2220786D6C6E733A7873693D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D612D696E7374616E63652220786D6C6E733A7873643D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D612220786D6C6E733A666C6F7761626C653D22687474703A2F2F666C6F7761626C652E6F72672F62706D6E2220786D6C6E733A62706D6E64693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F44492220786D6C6E733A6F6D6764633D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F44432220786D6C6E733A6F6D6764693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F44492220747970654C616E67756167653D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D61222065787072657373696F6E4C616E67756167653D22687474703A2F2F7777772E77332E6F72672F313939392F585061746822207461726765744E616D6573706163653D22687474703A2F2F7777772E666C6F7761626C652E6F72672F70726F63657373646566223E0A20203C70726F636573732069643D226A736974655F6C6561766522206E616D653D22E8AFB7E58187E6B581E7A88B2220697345786563757461626C653D2274727565223E0A202020203C646F63756D656E746174696F6E3EE8AFB7E58187E6B581E7A88B3C2F646F63756D656E746174696F6E3E0A202020203C73746172744576656E742069643D2273746172746576656E743122206E616D653D22E5BC80E5A78BE88A82E782B92220666C6F7761626C653A696E69746961746F723D226170706C79557365724964222F3E0A202020203C757365725461736B2069643D226C656176655F6C65616465725F617564697422206E616D653D22E9A286E5AFBCE5AEA1E689B92220666C6F7761626C653A61737369676E65653D22247B6C65616465724E616D657D2220666C6F7761626C653A666F726D4B65793D226C656176655F6C65616465725F6175646974223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C666C6F7761626C653A7461736B4C697374656E6572206576656E743D22636F6D706C6574652220636C6173733D22636F6D2E6A736974652E6D6F64756C65732E666C6F7761626C652E6C697374656E65722E5461736B427573696E65737343616C6C4C697374656E6572222F3E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C6578636C7573697665476174657761792069643D227369642D44394344303945332D363830342D344232362D394443452D333144433533423634453236222F3E0A202020203C656E644576656E742069643D22656E6422206E616D653D22E6B581E7A88BE7BB93E69D9F222F3E0A202020203C757365725461736B2069643D2261646A7573744170706C7922206E616D653D22E8B083E695B4E794B3E8AFB72220666C6F7761626C653A61737369676E65653D22247B6170706C795573657249647D223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C73657175656E6365466C6F772069643D227369642D43373243334639342D464643362D343836312D393834432D3834383834423636444434372220736F757263655265663D2273746172746576656E743122207461726765745265663D226C656176655F6C65616465725F6175646974222F3E0A202020203C6578636C7573697665476174657761792069643D227369642D35324538303342342D343737392D343344322D393434432D333331324630354544454330222F3E0A202020203C73657175656E6365466C6F772069643D227369642D43413633303439432D453841442D343544422D413231342D3836414430363533443331332220736F757263655265663D226C656176655F6C65616465725F617564697422207461726765745265663D227369642D44394344303945332D363830342D344232362D394443452D333144433533423634453236222F3E0A202020203C73657175656E6365466C6F772069643D227369642D36443541423835432D413632422D344230382D383230462D3239344645363846303133322220736F757263655265663D227369642D35324538303342342D343737392D343344322D393434432D33333132463035454445433022207461726765745265663D22656E64223E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B216175646974506173737D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D39393037423339432D344446312D343041352D394131352D4436414545313946373932452220736F757263655265663D2261646A7573744170706C7922207461726765745265663D227369642D35324538303342342D343737392D343344322D393434432D333331324630354544454330222F3E0A202020203C73657175656E6365466C6F772069643D227369642D31344237374134392D464432412D343741322D384441302D3234423346373731354534372220736F757263655265663D227369642D35324538303342342D343737392D343344322D393434432D33333132463035454445433022207461726765745265663D226C656176655F6C65616465725F6175646974223E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B6175646974506173737D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D38454143384133362D304334302D343539362D383138312D4133334231433935453534342220736F757263655265663D227369642D44394344303945332D363830342D344232362D394443452D33314443353342363445323622207461726765745265663D22656E64223E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B6175646974506173737D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D35374237354244372D453141362D343536392D383946422D3343383846363243393338342220736F757263655265663D227369642D44394344303945332D363830342D344232362D394443452D33314443353342363445323622207461726765745265663D2261646A7573744170706C79223E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B216175646974506173737D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A20203C2F70726F636573733E0A20203C62706D6E64693A42504D4E4469616772616D2069643D2242504D4E4469616772616D5F6A736974655F6C65617665223E0A202020203C62706D6E64693A42504D4E506C616E652062706D6E456C656D656E743D226A736974655F6C65617665222069643D2242504D4E506C616E655F6A736974655F6C65617665223E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D2273746172746576656E7431222069643D2242504D4E53686170655F73746172746576656E7431223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2233302E30222077696474683D2233302E302220783D223130302E302220793D223136332E30222F3E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D226C656176655F6C65616465725F6175646974222069643D2242504D4E53686170655F6C656176655F6C65616465725F6175646974223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223139352E302220793D223133382E30222F3E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D44394344303945332D363830342D344232362D394443452D333144433533423634453236222069643D2242504D4E53686170655F7369642D44394344303945332D363830342D344232362D394443452D333144433533423634453236223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2234302E30222077696474683D2234302E302220783D223336302E302220793D223135382E30222F3E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D22656E64222069643D2242504D4E53686170655F656E64223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2232382E30222077696474683D2232382E302220783D223532352E302220793D223136352E30222F3E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D2261646A7573744170706C79222069643D2242504D4E53686170655F61646A7573744170706C79223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223333302E302220793D223238352E30222F3E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D35324538303342342D343737392D343344322D393434432D333331324630354544454330222069643D2242504D4E53686170655F7369642D35324538303342342D343737392D343344322D393434432D333331324630354544454330223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2234302E30222077696474683D2234302E302220783D223336302E302220793D223430352E30222F3E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D43413633303439432D453841442D343544422D413231342D383641443036353344333133222069643D2242504D4E456467655F7369642D43413633303439432D453841442D343544422D413231342D383641443036353344333133223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223239342E39353030303030303030303030352220793D223137382E30222F3E0A20202020202020203C6F6D6764693A776179706F696E7420783D223336302E302220793D223137382E30222F3E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D39393037423339432D344446312D343041352D394131352D443641454531394637393245222069643D2242504D4E456467655F7369642D39393037423339432D344446312D343041352D394131352D443641454531394637393245223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223338302E302220793D223336342E3935303030303030303030303035222F3E0A20202020202020203C6F6D6764693A776179706F696E7420783D223338302E302220793D223430352E30222F3E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D36443541423835432D413632422D344230382D383230462D323934464536384630313332222069643D2242504D4E456467655F7369642D36443541423835432D413632422D344230382D383230462D323934464536384630313332223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223339392E393433373330333538313334352220793D223432352E30222F3E0A20202020202020203C6F6D6764693A776179706F696E7420783D223533392E302220793D223432352E30222F3E0A20202020202020203C6F6D6764693A776179706F696E7420783D223533392E302220793D223139322E3934393932313932373331333036222F3E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D35374237354244372D453141362D343536392D383946422D334338384636324339333834222069643D2242504D4E456467655F7369642D35374237354244372D453141362D343536392D383946422D334338384636324339333834223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223338302E302220793D223139372E3934333231383839383730383335222F3E0A20202020202020203C6F6D6764693A776179706F696E7420783D223338302E302220793D223238352E30222F3E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D38454143384133362D304334302D343539362D383138312D413333423143393545353434222069643D2242504D4E456467655F7369642D38454143384133362D304334302D343539362D383138312D413333423143393545353434223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223339392E38313931353939303030363234362220793D223137382E3132343638373530303030303032222F3E0A20202020202020203C6F6D6764693A776179706F696E7420783D223532352E303030303633383935313136332220793D223137382E3931313935313339303639313436222F3E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D31344237374134392D464432412D343741322D384441302D323442334637373135453437222069643D2242504D4E456467655F7369642D31344237374134392D464432412D343741322D384441302D323442334637373135453437223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223336302E302220793D223432352E30222F3E0A20202020202020203C6F6D6764693A776179706F696E7420783D223234352E302220793D223432352E30222F3E0A20202020202020203C6F6D6764693A776179706F696E7420783D223234352E302220793D223231372E3935303030303030303030303032222F3E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D43373243334639342D464643362D343836312D393834432D383438383442363644443437222069643D2242504D4E456467655F7369642D43373243334639342D464643362D343836312D393834432D383438383442363644443437223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223132392E39343939393839313836393131342220793D223137382E30222F3E0A20202020202020203C6F6D6764693A776179706F696E7420783D223139342E39393939393939393939393032322220793D223137382E30222F3E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A202020203C2F62706D6E64693A42504D4E506C616E653E0A20203C2F62706D6E64693A42504D4E4469616772616D3E0A3C2F646566696E6974696F6E733E, 0);
INSERT INTO `ACT_GE_BYTEARRAY` VALUES ('4f063160-2df3-11ea-aa23-c85b7643dd9e', 1, '请假流程.jsite_leave.png', '4eafd3fe-2df3-11ea-aa23-c85b7643dd9e', 0x89504E470D0A1A0A0000000D4948445200000233000001C708060000004AB9D9500000202A4944415478DAEDDD0D9095F5BD1FF04D62A675D2A9B19334B5A9F5DA7B9B9BB6699A5A9CE8A4A6D4C6914CE8D51875C38BC0900C24349AC4973689A158890DE6659884DB89C945632857632925642F176559E44584116291A0285C4102045C2E2564232F417DFAFF9D7B9E9DC3D97376F7ECCBE19CDDCF67E63FE7D9F3B2E0E1E7EFF99EFFF37F9ED3D20200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000D0AB2CCBCEDBB367CF92CD9B37BFD1D1D191B5B7B71B751EAB57AFCED6AF5FFFD7694C56910050A308321B366CC83A3B3BB3D3A74F1BE7681C3972245BB76EDDF1146C6E5495005083989111641A26D09C6E6F6FDFA92A01A00671684990689C91C2CC195509003588351B42444385994C5502C0308499DFFDE650B667CB8FB3E757DF5B18B11DF70920C20C00347C98E93A7630DBF1C4ECECB915779D35E2BE784C08116600A0A1C3CCFEE77FDE23C8E4E3C0F36D42883003008D1D66763E797FD530138F0921C20C00347498D9D17E4FD530138F0921C20C00083386300300C31566E2ECA56A61261E134284190068E830B37BE39F560D33F1981022CC0040438799A307B7673B56CDE9798829DD178F0921C20C0034749889B177EBA21E6126EE134084190068FC3073EA54B6FBE91FF43CC494EE8BC784106106001A36CCC4157E776D5C5075CD4C3CE62AC0C20C00345E9839752A3BB47B4DF6CBC7BF5635C8E4239E13CF354B23CC0040438499BE6663CCD2083300D0D061A63FB331BDCDD20825C24C8379A7B700609485998106997C0825C24C0319974656BC0560B48419439819213E96C6B134E6176FC77B4B008419439869B62033BE6486E688400320CC18C24C330699BEEE0740983184998631AE8FC0923F6E0D0D507F5996FDEDEDDBB7CFFCC94F7EB26EDEBC79C76FBBEDB633D3A64DCB3EFDE94F6793274F7EF3F39FFFFCE9BBEFBE7BCFFDF7DFBF388D3FF68E0933C2CCA80D327D0515333440FD43CCD2A54B17CD9D3BF7CC97BEF4A5EC873FFC61B679F3E6ECE0C183D9C99327B310B79D9D9DD92F7EF18B2C859DEC8B5FFCE29B5FFEF2975F993163C667BD83C28C30332A7CACC619975A9F0FBD9A3469D23F6A6D6DFD42FA80BD248DDD6974C587ED344EA6B1373DB62CDDDE19CFF36E8D3229B44CFFC637BE71EAAEBBEE2A0495D75F7F3DEBAF6DDBB6655FF9CA575E9F3973E6CE9B6EBAE98FBC9BC28C3033E283CCF801BCAE4BA06130264C98707D1AED699C2E8697FE8C27D3F3277BF746FE6CCC79CB972F5FF3B9CF7D2E6B6B6BAB29C4947BFCF1C7B3E9D3A79F304B23CC083382CC30BC9E512A05923F4881E4F11A024CA5B1C187ED111C64D6AE5DFBCCADB7DE9AEDDAB52B1B0AAFBCF24A9682D189CF7EF6B3FFC93B2CCC083323465F8B7DEBFD7B18255A5B5B3F3D71E2C4FF571A4C264D9A947DF39BDFCCD6AC5993FDEA57BFCA7EFBDBDF16F63FAFBDF65A6159C4C68D1BB3F9F3E767B7DC724B79A0E9324B3302C58C4C049958033394E2F7CD983143D10833C2CCC80A32437588C80C0DFD0D32E3D37EE4F5D210B378F1E27EEFB38E1D3B56787EBCAE34D4A4DFE9E8C148116B64E2D0D250CDC8549AA1993265CAB15434EFF76E0B33C24CD32A5FBCFB9634A6166F6B51FE3A8B82E97346A634C8DC7EFBED8559988188D7C57AD0B240E3C376B38BB396EEBDF7DED3B1466638AD58B1E2444AC49BBCE3C28C30D3D441667C492079B0E56FBE7FE9C11A024DB5D7093454146B644A0F2DCD9933A7FB50D240C521A8FBEEBBAFFC90930FDBCD6CD9B2657F1E29F5F7BFFF7D36DC66CD9A75580216668499A60F322DC59995AC64F427D09406997C4C2979DC1A1A7A48416675E98CCC60834C69A0299BA1D9E0DD6EE25999B88E4C9C7E5D0FCF3DF7DC69B333C28C30D354AA058C4AC1A4B74053E9F90B2B3CDF1A1ABAC5E9D7A56B64067A68A9B7434EA56B687CD86E527165DF58F43B9853B06B357DFAF4C3316D3882FFE78BEB1EFC5B61469869B69AEC25C88CAB21A0540A34FD0D32E581C621A7116AECD8B1E7F5B776F3A0118B7787C3A38F3E6A76A6D9C557142C5CB830ABA7F9F3E76F8BAB318ED4F7B424E1B7B7B6B65E21CC0833CD5293030C147D059A5A834CE99FEFDBB6476E4DEEEC6B1624AED89B2FFA8DD993A3478F0ECB3E29CE722A3B6DFB0FFC0B3599F8AEA5F88A827A7AFAE9A75F49C5D236D2771C256365A54FC5C28C30D368355921C8F43748540B346F1D6090299F191268466E4DEE8E33952ACDD414BFA2A0F0BC6F7DEB5BC3FD21BBF4FF8F3BFD0B3599F8D2C843870ED535CC1C3E7CB83315CBFE51B4E3E8FE545CBA031166849946ABC9010699DE02CD4B83083283FDFBD044359982CBFF8DEBC8943D6749FEF8DAB56B8775BF1417D62BF9FBB4F9176A32F1EDD7F99746D64BFC79F10560A36DC751FAA938A6FA851961A6D16A720866422A059AC10499F240630DCDC8AFC92D29608F2B3E67777EFF502FFC2D17570A2EF93BECF72FD48405752E0CF23B3546C410661A6B8CF67ABCE1861BF2D0317F906DE5AD1566645E2ADE3F18F38B7FD737D3887514BF4FE3541A278ADF927C3C05B263E9F6681A47D278358D43691C8C9D531AFBE21B94D378B9B8937CA9B86EE3F974FBCB349E8BD98174FB8BD8A1A6B1398DA7D3782A8DF569AC4DCF5D936EE334E155E9B9F1DD407F99C65FA4ED9FA7DB9FA5F17FD2F6FF4ECFFB5F69FBA7693C92B617A7DB45693C9CC683E9F13F4BB73F4CE30769FB7FA4DB05697C2F6DCF4FCFFD6EDAFE76DABE3F6D7F336DDF97C6DCB4FDDFD2ED9C3466A7C7EE4E3F7F356DFF97381C927EBE3DFDFCA5B47D5B1C9649DBB3D2F6E7D2981157B64DB7D3D398961E9B126B54D2F6C4E205E86E4EDB37A6ED1B8A670CFD49CC8CA4F1F18913275E9BEEBB26DDF71F6EBEF9E67F9FB6FF5DDABE2A6D7F246D5F99B63F9C9E77797ADEBF49DBFF3A6DFFAB499326FDCBF4D8BF48DBFFECA69B6EFAE3F4D83F4DDB7F98B62F4DE31FC7FA97F4DC7F98B6FFC12DB7DCF2F7FB51935B8BDF7A5DF879A84EC7EEED34ED923FFBA474D064A64E9DFA66BD67664E9C38717894CECC6C8966911F1BAE779879F1C5170B3B965FFFFAD7D9A64D9BB2D4980A63CA9429D9F2E5CB0B8FFDE8473FEA7EFE33CF3C937DF2939FEC7E4EB5E7551AF19CEF7CE73B7D3EE7CE3BEF2C6C3FF6D8638519BBFCFE386BC1CC4CFD6BB265F05F55D0DBCC4C2D17D6EB6D66E62D6987F8B6193366BCFDE31FFFF8DF4ADBE7A7F177D2CEF2EF4E9B36ED9D69FBEFA59DEABB6287193BCED881C68E3476A8C58BAFFD93F882C1F4DFFDBEB8485ADAFEE769FB0369FB83E9B10FA5EDCBD2F3C6C40E3B76DCB1038F1D79ECD063C71E3BF8D8D1C70E3F76FCE9E74FA49FFF63DABE2E6D7F326D7F2A6DDF94B65BD3F684B43D296DDF12812282453160CC88C011C1A3B82EE4B60824114C22A0A4EDFF9CB6BF92B6BF96C6D7D3F67F4DB7F7A4716F7AEC1BE9F6BFA7312F824F04A00842118822181503D20F22301583D38369FC383DEF27E9F67FA6F1E769FBD174FB58F130CED2F4BC65E976791C5E8980560C6AAB22B81503DCDA62A07BAA18F022E83D530C1CCFA6B12DBD667BBADD91C60BE9352FA6DB5D69FC551A7B8A41727F31581E2A06CDBEC2CC9BA53FD7FB43B674D0646EBDF5D6D3F55E339376A62F8DB23533E53B8C96731166DEFEF6B767175C7041AC59EA0E0E2B56AC28EC68E25349F9F3DFFDEE7767575F7D75C510521E36D6AD5B57083A79E0B9E8A28BB2ABAEBAAAB01D63E9D2A5D9A5975EDAE3F7C49974F9F6A2458BBAB723DC0833F5AFC992E0D0D5323C6B66061268AC99195D01FBC934C6169FD36566867E993D7BF6EE7A9FCDF4C4134FFCE528399B69534CDF56BB9E423DC34C5757576167F2C20B2F146E235C44908870336BD6ACEE1997D25013E1E2C20B2F2C5C71B37C66267ECF810307CE0A26A501247E9E3B77EE5941EAAB5FFD6A617BEFDEBDDDBFE7A31FFD68E1CF89ED9839CA5FBB7DFBF6C2EDF7BEF73D61A68E3559652664A04166614BE5B3996A0934CE661A3D61A6C71976D6CCD06FDFFEF6B717D6FB3A3373E6CC5931C2AF33D3DBA7DE731266622772DB6DB765BB77EFCEF6EDDB97CD9C39B370DFF1E3C70BC1227FCEB265CBBA5F73C9259714C246F9EF8A55FF715D86787E1E68222C75747474CFC4C463D3A74F2F6CC7A1AA084295FE4EA53333EF79CF7BB255AB5615B6631627667BCCCCD4B726073823D2D775646ABD52706E4C8BEBCC8C86301387BAC654798EB399E89FAF7FFDEB7FF8852F7CE18D3A5E01F8E4C489135F7651A2FA859998F188F52F796878E8A1877ACC806CDBB6AD4798897012F75D79E595D935D75C933DF0C003D965975D56B83DFFFCF30BDBE5E164C99225DDDB79485AB972656106A8529889B0B475EBD6C276ACE3C9EF5FB06081C34C8DA1AF9991FE5E10AFD640931FEABADE3FC1880D334B62AD526FCF719D196A72FBEDB7FF55BDBE9BE989279E58EC72D1F59F9989199908171FFAD0870AC1240F2F314B531A6A22CCC4A1A6083031F29996D2434C71E827C2466CEFDFBFFFAC70F2918F7CA4C7CC4CAC9FB9EEBAEBBA9F177F562CFC6D2959485C1E84AC996928D566686ABDB26F7F034DFEE70932A35CF10CA833F5B802F0E4C993DF7405E026F799CF7CE6A63BEEB8E34C1D66678E4D9C38F1595FE455FF301301A5F46CA17C3664C78E1D3DD6D2E4CF2F0F19F9CFD5D6B1F437CCC402E4975F7EF9ACD99BF2309387ABD27539C2CC399FA1293FE433B5A5F6EBC8F8D66C6AE2BB99A8C9CC9933B7B7B5B50D6B9289F5395128351EB7176686601C3972248B7FDF38E4130B70E3BE8B2FBE38FBE0073FD81D2262BD4AF9EBF2199C389EDC579889DF5D1A6CF2A01287B9222CF5B56626024C1EB2E2EF187FAF3C8009330D13684A17059706935A2E8857ED752E8C47A53033ECDF9A9D3E64BFE95BB3478898564B9FA44FECDAB56B58824CDA51C5350C0EC5751DBCDBF50F33F975636207F2C8238F147ECE6763E25051AC83C94344F9EB627D4CAC67C917E7E6B3377D2D38CEC34CA533ABF2B3AB1E7EF8E1EEE7C788F012B773E6CC29DCE6674009330DA3FC90D35B8A332BB59E6E5DFEBAC15EDF8691BD7F7A320F1B7176E5509DA61D33D077DC71C7EBA5B3323E6C8F0093274F9E3473E6CCD73A3B3B8734C81C3870E0A9947CF749BCE736CCE4A7485F7BEDB585338562816FE963317373F9E597577C5D848C58DF1201E3D9679F1D5498891181A8A56CD16FBEEDEB0C9A2ED00CC58C8FB396E8F5C376F1AACE85D0111F76061B6822C8DC77DF7DA5EB64BA7CD81E41264D9A347DC68C195D4335431333321164E26A96DEDD731F660C61A6C1028D0BE2D12FF1150CF962E07C8666A0879CE2756533320E2F8DD4A2993C79F2F19FFDEC67BF1BC4A2E063DFFDEE777F5CBC74F534EFAA3023CC8C38833D34E4D012B5CED04C2B0D34B186261605C7D948FD3D6B2916FB96AE91297E63B70FDB23795A2FFD8377CC9C39F3F0B3CF3E7BA296EBC8AC5AB5EAD11486E28BDA3698B6136684995111686A9D59B1D897017FD82E7EA168561A6AE23A347161BD9875C90F41C5A1A4B8B26F9CC0F0FDEF7F3F9B3A75EA1B65571DEEF2617BF4849A1B23D44C9B36AD73DEBC79CF3FF5D453FB52711CCABF9C32BE3432BE6B69F5EAD57F71CF3DF7AC4C45155F2AB6210AC4422A61469819156A3D54E4D01283FEB05D7ACAF600870FDBA3515CBCA8F835F37179E9BD255FCD7EB2F86DA8F16DAB77BAD8903023CC0834BD88CBD6BBB22F43F661BB186ACED412627CD806614698A19A71C5A032AE97C78F08320CF787EDB4FD860FDB20CC08330CF50C8D434BD4456B6BEB15F94C4D0A35D664813023CC3024814690A19E616659C921A52DDE1110668419061B68E60B32D44B0A2F63CAD7CFA470A3F64098116618B071B1336971FA35F50B336D1516FC6EB1D81784196186C1EC5CBCA79CB3599992ABFB5A740EC28C30833043C3D7DACA5E4EC7DEE41D026146984198A1296765AC9D016146984198A119EAACAD1F17CB7366130833C20CC20C6A0F106684193B14507B20CC18C28C1D0AA83D10660C61C60E05B5070833C20C7628A83D1066840861C60E05D41E083386306387026A0F84194398B14341ED01C28C30831D0A6A0F84194398B14301B507C28C21CCD8A1A0F6BC0B20CC0833D8A1A0F680DE75747408118D334EA630734655DAA1A0F6801AAC5FBFFE506767A720D100E3E0C1833F4D6166A7AAB44341ED013558B366CD756BD7AEFDCDABAFBE7A42A038773332116456AF5EBD2F8D1B55A51D0A6A0FA851DA818E6F6F6FDF12873862CD46B38D471F7D348BA6D18C7FF7E288F77DA720638782DA03466FC3688BA63176ECD8F3BC1BD8A1A0F680A6D2DADA7A456A1867A2694C9830619C77043B14D41ED06C616659348CE2D8E21DC10E05B5073453A31893CFCAE423859BF1DE19EC50507B40B3348AB6D22093CFCE583B831D0A6A0F688626D16356261F13264CB8DE3B841D0A6A0F68F426B1B25290298E4DDE21EC50507B40233788AAB332D6CE608782DA039AA141B4F516649CD9841D0A6A0FD034501BA0F6004D03B581DA03D034501BA83D40D3406D80DA03340DD406A83D40D3406DA0F600340DD4066A0FD034501BA0F6004D03B5016A0FD034501BA83D004D03B581DA03340DD406A83D40D3406D80DA03340DD4066A0FD034406DA0F6004D03B5016A0FD034501BA83D004D03B581DA03340DD406A83D40D3406D80DA03340DD4066A0F40D3406DA0F6004D03B5016A0FD034501BA0F6004D03B581DA03D034501BA83D40D3406D80DA03340DD406A83D40D3406DA0F6004D03D4066A0FD034501BA0F6004D03B581DA03D034501BA83D40D3406D80DA03340DD406A83D40D3406DA0F600340DD4066A0FD034501BA0F6004D03B5016A0FD034501BA83D40D3D034501BA83D40D3406D80DA03340DD406A83D40D3406DA0F6004D03D4066A0FD034501BA0F6004D03B581DA03D034E8571DB4452DF431B678A7D097004D8346AD8331699CE92DCCB4B6B68EF74EA12F019A068D5C0B2B7B09339BC68E1D7B9E77097D09D03468E45A18532DCC4C9830E17AEF10FA12A069D00CF55069EDCC16B332E84B80A641B3D4438FB533D6CAA02F019A064D258597656665D097004D83660E335794AC9519E71D415F02340D9AB12E0A6B67CCCAA02F019A06CD5A1763D406FA12A0698C5259969DB767CF9E259B376F7EA3A3A3236B6F6F37EA3C56AF5E9DAD5FBFFEAFD398AC22F52540D3A0461164366CD89075767666A74F9F36CED13872E448B66EDDBAE329D8DCA82AF52540D3A006312323C8344CA039DDDEDEBE5355EA4B80A6410DE2D09220D13823859933AA525F02340D6A106B368488860A336A5C5F02340D8623CCFCEE3787B23D5B7E9C3DBFFADEC288EDB84F001166F4257D09D0341A3ECC741D3B98ED786276F6DC8ABBCE1A715F3C26840833FA1280A6D1D06166FFF33FEF1164F271E0F936214498D19700348DC60E333B9FBCBF6A9889C7841061465F02D0341A3ACCEC68BFA76A9889C7841061465F02D03484194398D197004D83E10A3371F652B530138F0921C28CBE04A069347498D9BDF14FAB8699784C081166F425004DA3A1C3CCD183DBB31DABE6F43CC494EE8BC7841061465F02D0341A3ACCC4D8BB75518F3013F70920C28CBEA42F019A46E3879953A7B2DD4FFFA0E721A6745F3C26840833FA1280A6D1B06126AEF0BB6BE382AA6B66E23157011666F425004DA3F1C2CCA953D9A1DD6BB25F3EFEB5AA41261FF19C78AE591A61465F02D0341A22CCF4351B63964698415F02348D860E33FD998DE96D9646281166F425004DE39C86998106997C0825C28CBE04A0699CD330630833E84B80A621CC18C28CBE04A0690833C20CFA12A06920CC0833E84B80A621CC18C20CFA12A06908338630A32F01681AC28C3083BE04681A0833C20CFA12A0690833863083BE04681AC28C21CCE84B80A6813023CCA02F019A863023440833E84B80A621CC18C28CBE04A0690833C28C30A32F019A06C28C3083BE04681AC28C21CCA02F019A8630630833FA1280A621CC0833E84B80A6813023CCA02F019A8630630833E84B80A621CC9CCE8E1C3912FF86D992254BB2D75E7B2DBBE4924BB2ABAFBE3A9B32654A612C5FBEBCF078F9EBE2BE79F3E655FDB9DA58B66C5976F1C517577DFC85175EC85E7CF1C58A7FDEC30F3F2CCCA02F019A8630D3733CF3CC33D945175D949D3C79B2101A2250C4ED82050BBA8344FEDCCB2FBFBC3BE03CF4D043D9BBDEF5AE6CEBD6AD859F3FF1894F743F16BF237FCD3BDEF18EECDA6BAF2D3C367BF6ECECC20B2FCCEEBEFBEEEEE76EDAB4A9477089BF534747473671E2C4B3FEBCA54B9716B60F1C3820CCA02F019A863073BAE20CC8A2458B0AB763C78ECD366CD8705698D9BD7B776176250F17575E7965D6D6D6962D5EBCB8FBF58F3DF6588FDFF9C8238F14B62398C4EC4FE963E5CFDFB87163F7AC51FC1D4AC34CDC46C8313383BE04681AC24C61762382C2F9E79F5F082D79B878E08107BAC343CCCE9486997C76250F3D6F7BDBDBB2B973E71676FA1142E2FE083CE561E6B2CB2EAB3A33F3E4934F761F62CAEFDBBF7F7F61FB539FFA54F77DA5DBF96B8419F42540D318E53333F9A1A5387C93078F9879290F23F9F6DEBD7B0BB33071DF073EF08142B888FB8F1E3D5AB8EFE69B6FAE38DB93CFBEF43633136B76F2FBB66FDF5EF8F9BDEF7D6F76CD35D7145E9707AF083A6666D097004D4398392B50E401A6A57848288243ACA3397CF8F05961E6C31FFE70B662C58AEE752BEF7BDFFB0A879962716EBE983846BCAED630537A5F2C48CEB7E3CF2C5D8C9CCF220933E84B80A621CC7407861D3B7664C78F1FEF3EC494CFB4C4FD7998891991982D895011FFF6717BE9A59766B366CD2A6CE78B73CBD7B4B4F4F33053E9F36366266681627BDDBA7515838E3083BE04681AC2CC59EB64DEFFFEF7772FF88DD992B8CD0345F9A1A93803296EF34012B7B17E265F085CFEFBF380533E33B370E1C28ACFBFE0820BBA67856211B09919F42540D310667A0D337198286E63E6256EF7EDDB97AD5AB5AAC79A99D2D76CDBB6AD4738E96BF4E7F9F1BBA3B6F2ED7C3D8F9919F42540D310662A8EAEAEAEC285EAE29052E9FDF9F563E2DA3295AE33130168CE9C39856BCDE4878DF25014EB672AFD5971B8E9AAABAEEAF3427EA5E125FE6EB13E273F65BC743D8E3083BE04681AC24CCD673FF93A03F42540D3A069C38CEF66425F02340D841961067D09D03484194398415F02340D61C61066F42540D3D034841961067D09D03410668419F42540D310660C61067D09D03484194398D197004D03614698415F02340D61C61066D097004D43983184197D0940D31066841935AE2F019A06C28C3083BE04681AC28C21CCA02F019A8630630833FA1280A621CC0833E84B80A6813023CCA02F019A8630630833E84B80A621CC18C28CBE04681A0833C20CFA12A06920CC0833E84B80A621CC18C20CFA12A06908338630A32F019A06C28C3083BE04681AC28C21CCA02F019A4613EAE8E810221A679C4C61E68CAAD497004D831AAC5FBFFE506767A720D100E3E0C1833F4D6166A7AAD497004D831AAC59B3E6BAB56BD7FEE6D5575F3D21509CBB19990832AB57AFDE97C68DAA525F02340D6A9476A0E3DBDBDBB7C4218E58B3D18C236AA359FFEEC5F77DA720A32F019A066A03D41EA069A03640ED019A066A03B507A069A036507B80A681DA00B507681AA80D507B80A681DA40ED01681AA80DD41EA069A03640ED019A066A03D41EA069A036507B80A6016A03B507681AA80D507B80A681DA40ED01681AA80DD41EA069A03640ED019A066A03D41EA069A036507B009A066A03B507681AA80D507B80A681DA00B507681AA80DD41EA069681AA80DD41EA069A03640ED019A066A03D41EA069A036507B80A6016A03B507681AA80D507B80A681DA40ED01681AF4B30EDAA216FA185BBC53E84B80A641A3D6C19834CEF416665A5B5BC77BA7D097004D8346AE8595BD84994D63C78E3DCFBB84BE04681A34722D8CA91666264C9870BD77087D09D03468867AA8B476668B5919F42540D3A059EAA1C7DA196B65D097004D83A692C2CB32B332E84B80A6413387992B4AD6CA8CF38EA02F019A06CD581785B5336665D097004D8366AD8B3137DC7083DA405F02340D9A561C5ECA8AB7A02F019A064DE563691C4B637EF1D6D94CE84B80A641D305993CC0C4CCCC1181067D09D03468C620D3D7FDA02F019A060D635C1F81257FDC1A1AF42540D3A061834C5F41C50C0DFA12A069D0703ED652DB8C4BADCF077D09D03418F620337E00AFEB1268D097004D83660C3243F57AD097004D8301EB6BB16FBD7F0FE84B80A641CD0164A80E1199A1415F02340DEAA67CF1EE5BD2985ABCAD45F9EB2C0A465F02340DEA1664C6970492075BFEE6FB971EAC21D0547B9D4083BE04681AD42DC884A9C54092D510684A834C3EA6943C6E0D0DFA12A06930E4AA058C4AC1A4B74053E9F90B2B3CDF1A1AF42540D360C883CCB81A024AA540D3DF20531E681C72425F02340D06ACBF81A2AF40536B9029FDF37DDB36FA12A06930A820D3DF20512DD0BC75804126670D0DFA12A06930EC41A6B740F3D22082CC60FF3EE84B009AC62834D899904A81663041A63CD0584383BE040C4FD33046CE48FFA4F30759126FAD3023F352F1FEC198EFDFC7A8B1960118C53333039D01E96D66A6960BEB99990100062C824357CBF0AC991948A0B16606001850A0A96526A4DAE9D795CE66AA25D0389B09001874A0E92B48F4751D995AAF149C1BD3E23A3300C020F53533D2DF0BE2D51A68F2435DD7FB27000006ABDA0C4DAD57F6ED6FA0C9FF3C41060018323143537EC8676A4BEDD791F1ADD900C0390D34A58B824B83492D17C4ABF63AA75F0300C3AEFC90530491292DB59F6E5DFEBAC15EDF060060C08166B02A1DC20200688A40E3827800C03933D843430E2D01000D13686A9D59B1D817006818B51E2A7268090068DA40135F51E0CABE0040431A570C2AE37A79FC8820030034B26A33340E2D01004D1B68041900A06903CD7C4106006856B146266B71FA3500D0C4DEE92D0000000000000000000000000000000000009ACDFF0702C7D708634C4B1B0000000049454E44AE426082, 1);

-- ----------------------------
-- Table structure for ACT_GE_PROPERTY
-- ----------------------------
DROP TABLE IF EXISTS `ACT_GE_PROPERTY`;
CREATE TABLE `ACT_GE_PROPERTY`  (
  `NAME_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VALUE_` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`NAME_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_GE_PROPERTY
-- ----------------------------
INSERT INTO `ACT_GE_PROPERTY` VALUES ('cfg.execution-related-entities-count', 'true', 1);
INSERT INTO `ACT_GE_PROPERTY` VALUES ('cfg.task-related-entities-count', 'true', 1);
INSERT INTO `ACT_GE_PROPERTY` VALUES ('common.schema.version', '6.4.0.0', 1);
INSERT INTO `ACT_GE_PROPERTY` VALUES ('identitylink.schema.version', '6.4.0.0', 1);
INSERT INTO `ACT_GE_PROPERTY` VALUES ('job.schema.version', '6.4.0.0', 1);
INSERT INTO `ACT_GE_PROPERTY` VALUES ('next.dbid', '1', 1);
INSERT INTO `ACT_GE_PROPERTY` VALUES ('schema.history', 'create(6.3.2.0)', 1);
INSERT INTO `ACT_GE_PROPERTY` VALUES ('schema.version', '6.4.0.0', 1);
INSERT INTO `ACT_GE_PROPERTY` VALUES ('task.schema.version', '6.4.0.0', 1);
INSERT INTO `ACT_GE_PROPERTY` VALUES ('variable.schema.version', '6.4.0.0', 1);

-- ----------------------------
-- Table structure for ACT_HI_ACTINST
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_ACTINST`;
CREATE TABLE `ACT_HI_ACTINST`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT 1,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALL_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(20) NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_START`(`START_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_END`(`END_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_PROCINST`(`PROC_INST_ID_`, `ACT_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_EXEC`(`EXECUTION_ID_`, `ACT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_HI_ACTINST
-- ----------------------------
INSERT INTO `ACT_HI_ACTINST` VALUES ('006ae1c0-3838-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '006ae1ba-3838-11ea-aa63-00163e2e65eb', '006ae1bf-3838-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-16 16:12:56.455', '2020-01-16 16:12:56.455', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('006ae1c1-3838-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '006ae1ba-3838-11ea-aa63-00163e2e65eb', '006ae1bf-3838-11ea-aa63-00163e2e65eb', 'leave_leader_audit', '006ae1c2-3838-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsitehr', '2020-01-16 16:12:56.455', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('061057b9-305c-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'e318c61c-305b-11ea-9129-00163e2e65eb', 'e318ed31-305b-11ea-9129-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-06 16:10:38.508', '2020-01-06 16:10:38.509', 1, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('06107eca-305c-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'e318c61c-305b-11ea-9129-00163e2e65eb', 'e318ed31-305b-11ea-9129-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-06 16:10:38.509', '2020-01-06 16:10:38.509', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('084c75df-2df4-11ea-aa23-c85b7643dd9e', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79d02c27-2df3-11ea-aa23-c85b7643dd9e', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-03 14:41:12.272', '2020-01-03 14:41:12.276', 4, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('084d1220-2df4-11ea-aa23-c85b7643dd9e', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79d02c27-2df3-11ea-aa23-c85b7643dd9e', 'adjustApply', '084d1221-2df4-11ea-aa23-c85b7643dd9e', NULL, '调整申请', 'userTask', 'jsite', '2020-01-03 14:41:12.276', '2020-01-03 14:43:24.600', 132324, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('0ad9c752-3223-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c751-3223-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-08 22:27:47.551', '2020-01-08 22:27:47.551', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('0ad9c753-3223-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c751-3223-11ea-aa63-00163e2e65eb', 'leave_leader_audit', '0ad9ee64-3223-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsite', '2020-01-08 22:27:47.551', '2020-01-10 16:59:46.875', 153119324, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('1035ced6-2f8c-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', '1035a7c5-2f8c-11ea-9129-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-05 15:22:00.211', '2020-01-05 15:22:00.211', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('1035ced7-2f8c-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', '1035a7c5-2f8c-11ea-9129-00163e2e65eb', 'leave_leader_audit', '1035ced8-2f8c-11ea-9129-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsite', '2020-01-05 15:22:00.211', '2020-01-05 15:22:17.768', 17557, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('10368ea7-31f9-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', '1035cb56-31f9-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-08 17:27:17.685', '2020-01-08 17:27:17.689', 4, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('10377908-31f9-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', '1035cb56-31f9-11ea-aa63-00163e2e65eb', 'leave_leader_audit', '103bbec9-31f9-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsitehr', '2020-01-08 17:27:17.691', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('143fa909-3029-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '143f81f3-3029-11ea-9129-00163e2e65eb', '143fa908-3029-11ea-9129-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-06 10:05:57.973', '2020-01-06 10:05:57.973', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('143fa90a-3029-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '143f81f3-3029-11ea-9129-00163e2e65eb', '143fa908-3029-11ea-9129-00163e2e65eb', 'leave_leader_audit', '143fa90b-3029-11ea-9129-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsite', '2020-01-06 10:05:57.973', '2020-01-08 11:12:24.917', 176786944, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('191d0e6a-38fc-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'f5d44383-3733-11ea-aa63-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-17 15:36:39.247', '2020-01-17 15:36:39.247', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('191d0e6b-38fc-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'f5d44383-3733-11ea-aa63-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-17 15:36:39.247', '2020-01-17 15:36:39.247', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('1aaccb2d-2f8c-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', '1035a7c5-2f8c-11ea-9129-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-05 15:22:17.768', '2020-01-05 15:22:17.770', 2, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('1aad194e-2f8c-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', '1035a7c5-2f8c-11ea-9129-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-05 15:22:17.770', '2020-01-05 15:22:17.772', 2, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('208a434d-3743-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'caeab42d-36ae-11ea-aa63-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-15 11:00:03.649', '2020-01-15 11:00:03.649', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('208a434e-3743-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'caeab42d-36ae-11ea-aa63-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-15 11:00:03.649', '2020-01-15 11:00:03.649', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('264a9a4c-3678-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '264a7336-3678-11ea-aa63-00163e2e65eb', '264a733b-3678-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-14 10:47:05.462', '2020-01-14 10:47:05.462', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('264a9a4d-3678-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '264a7336-3678-11ea-aa63-00163e2e65eb', '264a733b-3678-11ea-aa63-00163e2e65eb', 'leave_leader_audit', '264a9a4e-3678-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsite', '2020-01-14 10:47:05.462', '2020-01-14 14:08:19.301', 12073839, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('2ba48e5e-38fc-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '979e7168-3368-11ea-aa63-00163e2e65eb', '979e716d-3368-11ea-aa63-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-17 15:37:10.334', '2020-01-17 15:37:10.335', 1, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('2ba4b56f-38fc-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '979e7168-3368-11ea-aa63-00163e2e65eb', '979e716d-3368-11ea-aa63-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-17 15:37:10.335', '2020-01-17 15:37:10.335', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('3414e5ea-3046-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '3414bed4-3046-11ea-9129-00163e2e65eb', '3414bed9-3046-11ea-9129-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-06 13:34:26.785', '2020-01-06 13:34:26.785', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('3414e5eb-3046-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '3414bed4-3046-11ea-9129-00163e2e65eb', '3414bed9-3046-11ea-9129-00163e2e65eb', 'leave_leader_audit', '3414e5ec-3046-11ea-9129-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsitehr', '2020-01-06 13:34:26.785', '2020-01-07 16:49:14.552', 98087767, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('34d30523-38fc-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'a0af89e0-2df4-11ea-aa23-c85b7643dd9e', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-17 15:37:25.738', '2020-01-17 15:37:25.738', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('34d30524-38fc-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'a0af89e0-2df4-11ea-aa23-c85b7643dd9e', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-17 15:37:25.738', '2020-01-17 15:37:25.738', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('3a5e26d5-3694-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '979e7168-3368-11ea-aa63-00163e2e65eb', '979e716d-3368-11ea-aa63-00163e2e65eb', 'sid-52E803B4-4779-43D2-944C-3312F05EDEC0', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-14 14:08:05.053', '2020-01-14 14:08:05.053', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('3a5e26d6-3694-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '979e7168-3368-11ea-aa63-00163e2e65eb', '979e716d-3368-11ea-aa63-00163e2e65eb', 'leave_leader_audit', '3a5e4de7-3694-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'dept', '2020-01-14 14:08:05.053', '2020-01-17 15:37:10.334', 264545281, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('3d30c524-3029-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d30c523-3029-11ea-9129-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-06 10:07:06.662', '2020-01-06 10:07:06.662', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('3d30c525-3029-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d30c523-3029-11ea-9129-00163e2e65eb', 'leave_leader_audit', '3d30c526-3029-11ea-9129-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsite', '2020-01-06 10:07:06.662', '2020-01-06 10:07:41.669', 35007, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('42dc395b-3694-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '264a7336-3678-11ea-aa63-00163e2e65eb', '264a733b-3678-11ea-aa63-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-14 14:08:19.301', '2020-01-14 14:08:19.301', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('42dc395c-3694-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '264a7336-3678-11ea-aa63-00163e2e65eb', '264a733b-3678-11ea-aa63-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-14 14:08:19.301', '2020-01-14 14:08:19.302', 1, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('4f4e7369-4d82-11ea-9b4e-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'd698cf10-4d81-11ea-9b4e-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-02-12 18:27:45.985', '2020-02-12 18:27:45.985', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('4f4e736a-4d82-11ea-9b4e-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'd698cf10-4d81-11ea-9b4e-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-02-12 18:27:45.985', '2020-02-12 18:27:45.985', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('520e6a1b-3029-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d30c523-3029-11ea-9129-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-06 10:07:41.669', '2020-01-06 10:07:41.670', 1, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('520e912c-3029-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d30c523-3029-11ea-9129-00163e2e65eb', 'adjustApply', '520e912d-3029-11ea-9129-00163e2e65eb', NULL, '调整申请', 'userTask', 'jsite', '2020-01-06 10:07:41.670', '2020-01-06 10:09:48.508', 126838, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('5466d248-38fc-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'ec0a0e01-38fb-11ea-aa63-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-17 15:38:18.716', '2020-01-17 15:38:18.716', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('5466d249-38fc-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'ec0a0e01-38fb-11ea-aa63-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-17 15:38:18.716', '2020-01-17 15:38:18.716', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('5663132c-383f-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '56631326-383f-11ea-aa63-00163e2e65eb', '5663132b-383f-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-16 17:05:27.165', '2020-01-16 17:05:27.165', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('5663132d-383f-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '56631326-383f-11ea-aa63-00163e2e65eb', '5663132b-383f-11ea-aa63-00163e2e65eb', 'leave_leader_audit', '5663132e-383f-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsitehr', '2020-01-16 17:05:27.165', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('572c4374-2df4-11ea-aa23-c85b7643dd9e', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79d02c27-2df3-11ea-aa23-c85b7643dd9e', 'sid-52E803B4-4779-43D2-944C-3312F05EDEC0', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-03 14:43:24.601', '2020-01-03 14:43:24.601', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('572c4375-2df4-11ea-aa23-c85b7643dd9e', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79d02c27-2df3-11ea-aa23-c85b7643dd9e', 'leave_leader_audit', '572c4376-2df4-11ea-aa23-c85b7643dd9e', NULL, '领导审批', 'userTask', 'jsite', '2020-01-03 14:43:24.601', '2020-01-03 14:44:02.001', 37400, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('5da64e4d-32b9-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '5da62737-32b9-11ea-aa63-00163e2e65eb', '5da64e4c-32b9-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-09 16:23:50.974', '2020-01-09 16:23:50.974', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('5da64e4e-32b9-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '5da62737-32b9-11ea-aa63-00163e2e65eb', '5da64e4c-32b9-11ea-aa63-00163e2e65eb', 'leave_leader_audit', '5da64e4f-32b9-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsiteuser', '2020-01-09 16:23:50.974', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('66bf8995-3827-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '66bf898f-3827-11ea-aa63-00163e2e65eb', '66bf8994-3827-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-16 14:14:06.693', '2020-01-16 14:14:06.694', 1, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('66bfb0a6-3827-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '66bf898f-3827-11ea-aa63-00163e2e65eb', '66bf8994-3827-11ea-aa63-00163e2e65eb', 'leave_leader_audit', '66bfb0a7-3827-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsitehr', '2020-01-16 14:14:06.694', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('6d773409-2df4-11ea-aa23-c85b7643dd9e', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79d02c27-2df3-11ea-aa23-c85b7643dd9e', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-03 14:44:02.002', '2020-01-03 14:44:02.002', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('6d77340a-2df4-11ea-aa23-c85b7643dd9e', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79d02c27-2df3-11ea-aa23-c85b7643dd9e', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-03 14:44:02.002', '2020-01-03 14:44:02.004', 2, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('70803b81-337a-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'aa3c2982-3349-11ea-aa63-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-10 15:25:55.470', '2020-01-10 15:25:55.471', 1, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('70806292-337a-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'aa3c2982-3349-11ea-aa63-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-10 15:25:55.471', '2020-01-10 15:25:55.473', 2, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('7842a67b-2e95-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '783cda15-2e95-11ea-9129-00163e2e65eb', '78400e6a-2e95-11ea-9129-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-04 09:56:49.086', '2020-01-04 09:56:49.092', 6, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('7843defc-2e95-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '783cda15-2e95-11ea-9129-00163e2e65eb', '78400e6a-2e95-11ea-9129-00163e2e65eb', 'leave_leader_audit', '784d2dcd-2e95-11ea-9129-00163e2e65eb', NULL, '领导审批', 'userTask', 'dept', '2020-01-04 09:56:49.094', '2020-01-06 14:56:15.732', 190766638, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('79d0c868-2df3-11ea-aa23-c85b7643dd9e', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79d02c27-2df3-11ea-aa23-c85b7643dd9e', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-03 14:37:13.226', '2020-01-03 14:37:13.229', 3, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('79d1d9d9-2df3-11ea-aa23-c85b7643dd9e', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79d02c27-2df3-11ea-aa23-c85b7643dd9e', 'leave_leader_audit', '79d709fa-2df3-11ea-aa23-c85b7643dd9e', NULL, '领导审批', 'userTask', 'jsite', '2020-01-03 14:37:13.232', '2020-01-03 14:41:12.272', 239040, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('7c8a32cb-35d1-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', '7c8a32ca-35d1-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-13 14:54:04.209', '2020-01-13 14:54:04.209', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('7c8a59dc-35d1-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', '7c8a32ca-35d1-11ea-aa63-00163e2e65eb', 'leave_leader_audit', '7c8a59dd-35d1-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsite', '2020-01-13 14:54:04.210', '2020-01-13 15:24:20.827', 1816617, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('829118f9-3387-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '8290f1e3-3387-11ea-aa63-00163e2e65eb', '829118f8-3387-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-10 16:59:29.237', '2020-01-10 16:59:29.237', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('829118fa-3387-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '8290f1e3-3387-11ea-aa63-00163e2e65eb', '829118f8-3387-11ea-aa63-00163e2e65eb', 'leave_leader_audit', '829118fb-3387-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsitehr', '2020-01-10 16:59:29.237', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('8d147161-3387-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c751-3223-11ea-aa63-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-10 16:59:46.875', '2020-01-10 16:59:46.876', 1, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('8d149872-3387-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c751-3223-11ea-aa63-00163e2e65eb', 'adjustApply', '8d149873-3387-11ea-aa63-00163e2e65eb', NULL, '调整申请', 'userTask', 'jsite', '2020-01-10 16:59:46.876', '2020-01-13 15:23:45.375', 253438499, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('8db9e169-3379-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '8db9ba53-3379-11ea-aa63-00163e2e65eb', '8db9e168-3379-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-10 15:19:35.005', '2020-01-10 15:19:35.005', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('8db9e16a-3379-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '8db9ba53-3379-11ea-aa63-00163e2e65eb', '8db9e168-3379-11ea-aa63-00163e2e65eb', 'leave_leader_audit', '8db9e16b-3379-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsitehr', '2020-01-10 15:19:35.005', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('90239b26-35d5-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '90237410-35d5-11ea-aa63-00163e2e65eb', '90237415-35d5-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-13 15:23:15.078', '2020-01-13 15:23:15.078', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('90239b27-35d5-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '90237410-35d5-11ea-aa63-00163e2e65eb', '90237415-35d5-11ea-aa63-00163e2e65eb', 'leave_leader_audit', '90239b28-35d5-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsite', '2020-01-13 15:23:15.078', '2020-01-13 15:24:11.692', 56614, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('917e4c43-3678-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '979e7168-3368-11ea-aa63-00163e2e65eb', '979e716d-3368-11ea-aa63-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-14 10:50:05.317', '2020-01-14 10:50:05.317', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('917e7354-3678-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '979e7168-3368-11ea-aa63-00163e2e65eb', '979e716d-3368-11ea-aa63-00163e2e65eb', 'adjustApply', '917e7355-3678-11ea-aa63-00163e2e65eb', NULL, '调整申请', 'userTask', 'jsite', '2020-01-14 10:50:05.318', '2020-01-14 14:08:05.053', 11879735, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('94f25b82-312a-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '3414bed4-3046-11ea-9129-00163e2e65eb', '3414bed9-3046-11ea-9129-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-07 16:49:14.552', '2020-01-07 16:49:14.552', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('94f25b83-312a-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '3414bed4-3046-11ea-9129-00163e2e65eb', '3414bed9-3046-11ea-9129-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-07 16:49:14.552', '2020-01-07 16:49:14.552', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('979e716e-3368-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '979e7168-3368-11ea-aa63-00163e2e65eb', '979e716d-3368-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-10 13:18:10.158', '2020-01-10 13:18:10.158', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('979e716f-3368-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '979e7168-3368-11ea-aa63-00163e2e65eb', '979e716d-3368-11ea-aa63-00163e2e65eb', 'leave_leader_audit', '979e9880-3368-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'dept', '2020-01-10 13:18:10.158', '2020-01-14 10:50:05.317', 336715159, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('9da88290-3029-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d30c523-3029-11ea-9129-00163e2e65eb', 'sid-52E803B4-4779-43D2-944C-3312F05EDEC0', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-06 10:09:48.509', '2020-01-06 10:09:48.509', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('9da8a9a1-3029-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d30c523-3029-11ea-9129-00163e2e65eb', 'leave_leader_audit', '9da8a9a2-3029-11ea-9129-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsite', '2020-01-06 10:09:48.509', '2020-01-07 14:48:24.519', 103116010, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('a0af89e1-2df4-11ea-aa23-c85b7643dd9e', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'a0af89e0-2df4-11ea-aa23-c85b7643dd9e', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-03 14:45:27.935', '2020-01-03 14:45:27.936', 1, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('a0afb0f2-2df4-11ea-aa23-c85b7643dd9e', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'a0af89e0-2df4-11ea-aa23-c85b7643dd9e', 'leave_leader_audit', 'a0afb0f3-2df4-11ea-aa23-c85b7643dd9e', NULL, '领导审批', 'userTask', 'dept', '2020-01-03 14:45:27.936', '2020-01-17 15:37:25.738', 1212717802, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('a20ac421-3051-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '783cda15-2e95-11ea-9129-00163e2e65eb', '78400e6a-2e95-11ea-9129-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-06 14:56:15.732', '2020-01-06 14:56:15.732', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('a20ac422-3051-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '783cda15-2e95-11ea-9129-00163e2e65eb', '78400e6a-2e95-11ea-9129-00163e2e65eb', 'adjustApply', 'a20ac423-3051-11ea-9129-00163e2e65eb', NULL, '调整申请', 'userTask', 'jsite', '2020-01-06 14:56:15.732', '2020-01-06 14:57:07.099', 51367, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('a2328fbd-35d5-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c751-3223-11ea-aa63-00163e2e65eb', 'sid-52E803B4-4779-43D2-944C-3312F05EDEC0', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-13 15:23:45.375', '2020-01-13 15:23:45.375', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('a2328fbe-35d5-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c751-3223-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 'a2328fbf-35d5-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsite', '2020-01-13 15:23:45.375', '2020-01-13 15:24:04.084', 18709, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('aa3c2983-3349-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'aa3c2982-3349-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-10 09:36:46.992', '2020-01-10 09:36:46.993', 1, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('aa3c5094-3349-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'aa3c2982-3349-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 'aa3c5095-3349-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsite', '2020-01-10 09:36:46.993', '2020-01-10 15:25:55.470', 20948477, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('ad595413-35d5-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c751-3223-11ea-aa63-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-13 15:24:04.084', '2020-01-13 15:24:04.084', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('ad595414-35d5-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c751-3223-11ea-aa63-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-13 15:24:04.084', '2020-01-13 15:24:04.085', 1, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('b17a8861-31c4-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '143f81f3-3029-11ea-9129-00163e2e65eb', '143fa908-3029-11ea-9129-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-08 11:12:24.917', '2020-01-08 11:12:24.917', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('b17a8862-31c4-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '143f81f3-3029-11ea-9129-00163e2e65eb', '143fa908-3029-11ea-9129-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-08 11:12:24.917', '2020-01-08 11:12:24.917', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('b1e23798-35d5-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '90237410-35d5-11ea-aa63-00163e2e65eb', '90237415-35d5-11ea-aa63-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-13 15:24:11.692', '2020-01-13 15:24:11.692', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('b1e23799-35d5-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '90237410-35d5-11ea-aa63-00163e2e65eb', '90237415-35d5-11ea-aa63-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-13 15:24:11.692', '2020-01-13 15:24:11.692', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('b3971e6e-3119-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d30c523-3029-11ea-9129-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-07 14:48:24.520', '2020-01-07 14:48:24.520', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('b397457f-3119-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d30c523-3029-11ea-9129-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-07 14:48:24.520', '2020-01-07 14:48:24.520', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('b7541b8d-35d5-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', '7c8a32ca-35d1-11ea-aa63-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-13 15:24:20.827', '2020-01-13 15:24:20.827', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('b7541b8e-35d5-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', '7c8a32ca-35d1-11ea-aa63-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-01-13 15:24:20.827', '2020-01-13 15:24:20.828', 1, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('c0a8bf97-3051-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '783cda15-2e95-11ea-9129-00163e2e65eb', '78400e6a-2e95-11ea-9129-00163e2e65eb', 'sid-52E803B4-4779-43D2-944C-3312F05EDEC0', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-01-06 14:57:07.099', '2020-01-06 14:57:07.099', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('c0a8bf98-3051-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '783cda15-2e95-11ea-9129-00163e2e65eb', '78400e6a-2e95-11ea-9129-00163e2e65eb', 'leave_leader_audit', 'c0a8e6a9-3051-11ea-9129-00163e2e65eb', NULL, '领导审批', 'userTask', 'dept', '2020-01-06 14:57:07.099', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('c524723a-3156-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'c5247234-3156-11ea-9129-00163e2e65eb', 'c5247239-3156-11ea-9129-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-07 22:05:33.267', '2020-01-07 22:05:33.267', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('c524994b-3156-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'c5247234-3156-11ea-9129-00163e2e65eb', 'c5247239-3156-11ea-9129-00163e2e65eb', 'leave_leader_audit', 'c524994c-3156-11ea-9129-00163e2e65eb', NULL, '领导审批', 'userTask', 'dept', '2020-01-07 22:05:33.268', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('c8200c69-36b7-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'c81fe558-36b7-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-14 18:22:35.268', '2020-01-14 18:22:35.268', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('c8200c6a-36b7-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'c81fe558-36b7-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 'c8200c6b-36b7-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsiteuser', '2020-01-14 18:22:35.268', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('c87ffc98-32bd-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'c87ffc97-32bd-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-09 16:55:28.226', '2020-01-09 16:55:28.227', 1, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('c88023a9-32bd-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'c87ffc97-32bd-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 'c88023aa-32bd-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsitehr', '2020-01-09 16:55:28.227', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('ca5a8cc5-4d6c-11ea-9b4e-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'ca5a8cc4-4d6c-11ea-9b4e-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-02-12 15:53:43.496', '2020-02-12 15:53:43.496', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('ca5a8cc6-4d6c-11ea-9b4e-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'ca5a8cc4-4d6c-11ea-9b4e-00163e2e65eb', 'leave_leader_audit', 'ca5a8cc7-4d6c-11ea-9b4e-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsitehr', '2020-02-12 15:53:43.496', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('caeab42e-36ae-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'caeab42d-36ae-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-14 17:18:14.481', '2020-01-14 17:18:14.481', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('caeab42f-36ae-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'caeab42d-36ae-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 'caeab430-36ae-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsitehr', '2020-01-14 17:18:14.481', '2020-01-15 11:00:03.648', 63709167, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('d698cf11-4d81-11ea-9b4e-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'd698cf10-4d81-11ea-9b4e-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-02-12 18:24:23.468', '2020-02-12 18:24:23.468', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('d698cf12-4d81-11ea-9b4e-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'd698cf10-4d81-11ea-9b4e-00163e2e65eb', 'leave_leader_audit', 'd698cf13-4d81-11ea-9b4e-00163e2e65eb', NULL, '领导审批', 'userTask', 'dept', '2020-02-12 18:24:23.468', '2020-02-12 18:27:45.984', 202516, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('dafee14e-367d-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'dafee14d-367d-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-14 11:27:56.117', '2020-01-14 11:27:56.117', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('dafee14f-367d-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'dafee14d-367d-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 'dafee150-367d-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'dept', '2020-01-14 11:27:56.117', '2020-02-11 14:29:46.278', 2430110161, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('de656aa8-4bdf-11ea-9b4e-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'de648047-4bdf-11ea-9b4e-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-02-10 16:32:26.920', '2020-02-10 16:32:26.924', 4, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('de665509-4bdf-11ea-9b4e-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'de648047-4bdf-11ea-9b4e-00163e2e65eb', 'leave_leader_audit', 'de6d80fa-4bdf-11ea-9b4e-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsitehr', '2020-02-10 16:32:26.927', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('e08f45f5-4c97-11ea-9b4e-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'e08f45f4-4c97-11ea-9b4e-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-02-11 14:29:37.948', '2020-02-11 14:29:37.948', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('e08f45f6-4c97-11ea-9b4e-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'e08f45f4-4c97-11ea-9b4e-00163e2e65eb', 'leave_leader_audit', 'e08f45f7-4c97-11ea-9b4e-00163e2e65eb', NULL, '领导审批', 'userTask', 'dept', '2020-02-11 14:29:37.948', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('e318ed32-305b-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'e318c61c-305b-11ea-9129-00163e2e65eb', 'e318ed31-305b-11ea-9129-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-06 16:09:39.844', '2020-01-06 16:09:39.844', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('e318ed33-305b-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'e318c61c-305b-11ea-9129-00163e2e65eb', 'e318ed31-305b-11ea-9129-00163e2e65eb', 'leave_leader_audit', 'e318ed34-305b-11ea-9129-00163e2e65eb', NULL, '领导审批', 'userTask', 'dept', '2020-01-06 16:09:39.844', '2020-01-06 16:10:38.508', 58664, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('e5867bac-4c97-11ea-9b4e-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'dafee14d-367d-11ea-aa63-00163e2e65eb', 'sid-D9CD09E3-6804-4B26-9DCE-31DC53B64E26', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2020-02-11 14:29:46.279', '2020-02-11 14:29:46.280', 1, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('e586a2bd-4c97-11ea-9b4e-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'dafee14d-367d-11ea-aa63-00163e2e65eb', 'end', NULL, NULL, '流程结束', 'endEvent', NULL, '2020-02-11 14:29:46.280', '2020-02-11 14:29:46.283', 3, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('ec0a0e02-38fb-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'ec0a0e01-38fb-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-17 15:35:23.625', '2020-01-17 15:35:23.625', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('ec0a0e03-38fb-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'ec0a0e01-38fb-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 'ec0a0e04-38fb-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsitehr', '2020-01-17 15:35:23.625', '2020-01-17 15:38:18.716', 175091, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('f5d44384-3733-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'f5d44383-3733-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-15 09:11:29.541', '2020-01-15 09:11:29.541', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('f5d44385-3733-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'f5d44383-3733-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 'f5d44386-3733-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'dept', '2020-01-15 09:11:29.541', '2020-01-17 15:36:39.247', 195909706, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('f9e02287-38f5-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'f9e02286-38f5-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-17 14:52:49.858', '2020-01-17 14:52:49.858', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('f9e02288-38f5-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'f9e02286-38f5-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 'f9e04999-38f5-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsitehr', '2020-01-17 14:52:49.858', NULL, NULL, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('fc718303-36a6-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'fc718302-36a6-11ea-aa63-00163e2e65eb', 'startevent1', NULL, NULL, '开始节点', 'startEvent', NULL, '2020-01-14 16:22:21.599', '2020-01-14 16:22:21.599', 0, NULL, '');
INSERT INTO `ACT_HI_ACTINST` VALUES ('fc718304-36a6-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'fc718302-36a6-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 'fc718305-36a6-11ea-aa63-00163e2e65eb', NULL, '领导审批', 'userTask', 'jsitehr', '2020-01-14 16:22:21.599', NULL, NULL, NULL, '');

-- ----------------------------
-- Table structure for ACT_HI_ATTACHMENT
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_ATTACHMENT`;
CREATE TABLE `ACT_HI_ATTACHMENT`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `URL_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CONTENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_HI_COMMENT
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_COMMENT`;
CREATE TABLE `ACT_HI_COMMENT`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACTION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `MESSAGE_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `FULL_MSG_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_HI_COMMENT
-- ----------------------------
INSERT INTO `ACT_HI_COMMENT` VALUES ('0605d067-305c-11ea-9129-00163e2e65eb', 'comment', '2020-01-06 16:10:38.439', NULL, 'e318ed34-305b-11ea-9129-00163e2e65eb', 'e318c61c-305b-11ea-9129-00163e2e65eb', 'AddComment', '321312313', 0x333231333132333133);
INSERT INTO `ACT_HI_COMMENT` VALUES ('08035eed-2df4-11ea-aa23-c85b7643dd9e', 'comment', '2020-01-03 14:41:11.793', NULL, '79d709fa-2df3-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', 'AddComment', '驳回1111', 0xE9A9B3E59B9E31313131);
INSERT INTO `ACT_HI_COMMENT` VALUES ('1912fc47-38fc-11ea-aa63-00163e2e65eb', 'comment', '2020-01-17 15:36:39.181', 'jsite', 'f5d44386-3733-11ea-aa63-00163e2e65eb', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('1aa1f5bb-2f8c-11ea-9129-00163e2e65eb', 'comment', '2020-01-05 15:22:17.697', NULL, '1035ced8-2f8c-11ea-9129-00163e2e65eb', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('2080583a-3743-11ea-aa63-00163e2e65eb', 'comment', '2020-01-15 11:00:03.584', 'jsite', 'caeab430-36ae-11ea-aa63-00163e2e65eb', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('2b9af16c-38fc-11ea-aa63-00163e2e65eb', 'comment', '2020-01-17 15:37:10.271', 'jsite', '3a5e4de7-3694-11ea-aa63-00163e2e65eb', '979e7168-3368-11ea-aa63-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('34c74550-38fc-11ea-aa63-00163e2e65eb', 'comment', '2020-01-17 15:37:25.661', 'jsite', 'a0afb0f3-2df4-11ea-aa23-c85b7643dd9e', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('3a55e973-3694-11ea-aa63-00163e2e65eb', 'comment', '2020-01-14 14:08:04.999', 'jsite', '917e7355-3678-11ea-aa63-00163e2e65eb', '979e7168-3368-11ea-aa63-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('42d29c69-3694-11ea-aa63-00163e2e65eb', 'comment', '2020-01-14 14:08:19.238', NULL, '264a9a4e-3678-11ea-aa63-00163e2e65eb', '264a7336-3678-11ea-aa63-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('4f463606-4d82-11ea-9b4e-00163e2e65eb', 'comment', '2020-02-12 18:27:45.931', 'jsite', 'd698cf13-4d81-11ea-9b4e-00163e2e65eb', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('519cbc89-3029-11ea-9129-00163e2e65eb', 'comment', '2020-01-06 10:07:40.924', NULL, '3d30c526-3029-11ea-9129-00163e2e65eb', '3d309e0e-3029-11ea-9129-00163e2e65eb', 'AddComment', '驳回', 0xE9A9B3E59B9E);
INSERT INTO `ACT_HI_COMMENT` VALUES ('545d3555-38fc-11ea-aa63-00163e2e65eb', 'comment', '2020-01-17 15:38:18.653', 'jsite', 'ec0a0e04-38fb-11ea-aa63-00163e2e65eb', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('5635d583-2df4-11ea-aa23-c85b7643dd9e', 'comment', '2020-01-03 14:43:22.986', NULL, '084d1221-2df4-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', 'AddComment', '驳回1111', 0xE9A9B3E59B9E31313131);
INSERT INTO `ACT_HI_COMMENT` VALUES ('6d232098-2df4-11ea-aa23-c85b7643dd9e', 'comment', '2020-01-03 14:44:01.451', NULL, '572c4376-2df4-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', 'AddComment', '驳回1111', 0xE9A9B3E59B9E31313131);
INSERT INTO `ACT_HI_COMMENT` VALUES ('70720aae-337a-11ea-aa63-00163e2e65eb', 'comment', '2020-01-10 15:25:55.377', 'dept', 'aa3c5095-3349-11ea-aa63-00163e2e65eb', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('8d0aad5e-3387-11ea-aa63-00163e2e65eb', 'comment', '2020-01-10 16:59:46.811', 'jsite', '0ad9ee64-3223-11ea-aa63-00163e2e65eb', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', 'AddComment', '驳回', 0xE9A9B3E59B9E);
INSERT INTO `ACT_HI_COMMENT` VALUES ('91760ee1-3678-11ea-aa63-00163e2e65eb', 'comment', '2020-01-14 10:50:05.263', NULL, '979e9880-3368-11ea-aa63-00163e2e65eb', '979e7168-3368-11ea-aa63-00163e2e65eb', 'AddComment', '驳回', 0xE9A9B3E59B9E);
INSERT INTO `ACT_HI_COMMENT` VALUES ('94eba4c0-312a-11ea-9129-00163e2e65eb', 'comment', '2020-01-07 16:49:14.508', NULL, '3414e5ec-3046-11ea-9129-00163e2e65eb', '3414bed4-3046-11ea-9129-00163e2e65eb', 'AddComment', '不批准', 0xE4B88DE689B9E58786);
INSERT INTO `ACT_HI_COMMENT` VALUES ('9d9fa8ef-3029-11ea-9129-00163e2e65eb', 'comment', '2020-01-06 10:09:48.450', NULL, '520e912d-3029-11ea-9129-00163e2e65eb', '3d309e0e-3029-11ea-9129-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('a2034a0f-3051-11ea-9129-00163e2e65eb', 'comment', '2020-01-06 14:56:15.683', NULL, '784d2dcd-2e95-11ea-9129-00163e2e65eb', '783cda15-2e95-11ea-9129-00163e2e65eb', 'AddComment', '啊啊啊啊啊啊啊啊啊啊啊 驳回', 0xE5958AE5958AE5958AE5958AE5958AE5958AE5958AE5958AE5958AE5958AE5958A2020E9A9B3E59B9E);
INSERT INTO `ACT_HI_COMMENT` VALUES ('a2282f7b-35d5-11ea-aa63-00163e2e65eb', 'comment', '2020-01-13 15:23:45.307', 'jsite', '8d149873-3387-11ea-aa63-00163e2e65eb', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('ad4f6901-35d5-11ea-aa63-00163e2e65eb', 'comment', '2020-01-13 15:24:04.019', 'jsite', 'a2328fbf-35d5-11ea-aa63-00163e2e65eb', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('b1730e4f-31c4-11ea-9129-00163e2e65eb', 'comment', '2020-01-08 11:12:24.868', NULL, '143fa90b-3029-11ea-9129-00163e2e65eb', '143f81f3-3029-11ea-9129-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('b1d89aa5-35d5-11ea-aa63-00163e2e65eb', 'comment', '2020-01-13 15:24:11.629', 'jsite', '90239b28-35d5-11ea-aa63-00163e2e65eb', '90237410-35d5-11ea-aa63-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('b390b5cd-3119-11ea-9129-00163e2e65eb', 'comment', '2020-01-07 14:48:24.476', NULL, '9da8a9a2-3029-11ea-9129-00163e2e65eb', '3d309e0e-3029-11ea-9129-00163e2e65eb', 'AddComment', 'ddsfsfsdfsdfs', 0x64647366736673646673646673);
INSERT INTO `ACT_HI_COMMENT` VALUES ('b74a7e9a-35d5-11ea-aa63-00163e2e65eb', 'comment', '2020-01-13 15:24:20.763', 'jsite', '7c8a59dd-35d1-11ea-aa63-00163e2e65eb', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);
INSERT INTO `ACT_HI_COMMENT` VALUES ('c0a27e06-3051-11ea-9129-00163e2e65eb', 'comment', '2020-01-06 14:57:07.058', NULL, 'a20ac423-3051-11ea-9129-00163e2e65eb', '783cda15-2e95-11ea-9129-00163e2e65eb', 'AddComment', '啊啊啊啊啊啊啊啊啊啊啊 驳回', 0xE5958AE5958AE5958AE5958AE5958AE5958AE5958AE5958AE5958AE5958AE5958A2020E9A9B3E59B9E);
INSERT INTO `ACT_HI_COMMENT` VALUES ('e57b581a-4c97-11ea-9b4e-00163e2e65eb', 'comment', '2020-02-11 14:29:46.206', NULL, 'dafee150-367d-11ea-aa63-00163e2e65eb', 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'AddComment', '同意', 0xE5908CE6848F);

-- ----------------------------
-- Table structure for ACT_HI_DETAIL
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_DETAIL`;
CREATE TABLE `ACT_HI_DETAIL`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DOUBLE_` double NULL DEFAULT NULL,
  `LONG_` bigint(20) NULL DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_PROC_INST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_ACT_INST`(`ACT_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_TIME`(`TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_NAME`(`NAME_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_TASK_ID`(`TASK_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_HI_IDENTITYLINK
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_IDENTITYLINK`;
CREATE TABLE `ACT_HI_IDENTITYLINK`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_USER`(`USER_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_TASK`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_PROCINST`(`PROC_INST_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_HI_IDENTITYLINK
-- ----------------------------
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('006ae1bc-3838-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-16 16:12:56.455', '006ae1ba-3838-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('006b08d3-3838-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsitehr', '006ae1c2-3838-11ea-aa63-00163e2e65eb', '2020-01-16 16:12:56.456', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('006b08d4-3838-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsitehr', NULL, '2020-01-16 16:12:56.456', '006ae1ba-3838-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('084d1222-2df4-11ea-aa23-c85b7643dd9e', NULL, 'assignee', 'jsite', '084d1221-2df4-11ea-aa23-c85b7643dd9e', '2020-01-03 14:41:12.276', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('0ad9c74e-3223-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-08 22:27:47.551', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('0ad9ee65-3223-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsite', '0ad9ee64-3223-11ea-aa63-00163e2e65eb', '2020-01-08 22:27:47.552', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('0ad9ee66-3223-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-08 22:27:47.552', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('10357d33-31f9-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-08 17:27:17.679', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('1035a7c2-2f8c-11ea-9129-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-05 15:22:00.210', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('1035ced9-2f8c-11ea-9129-00163e2e65eb', NULL, 'assignee', 'jsite', '1035ced8-2f8c-11ea-9129-00163e2e65eb', '2020-01-05 15:22:00.211', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('1035ceda-2f8c-11ea-9129-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-05 15:22:00.211', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('103c5b0a-31f9-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsitehr', '103bbec9-31f9-11ea-aa63-00163e2e65eb', '2020-01-08 17:27:17.723', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('103c5b0b-31f9-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsitehr', NULL, '2020-01-08 17:27:17.723', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('143f81f5-3029-11ea-9129-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-06 10:05:57.972', '143f81f3-3029-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('143fa90c-3029-11ea-9129-00163e2e65eb', NULL, 'assignee', 'jsite', '143fa90b-3029-11ea-9129-00163e2e65eb', '2020-01-06 10:05:57.973', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('143fa90d-3029-11ea-9129-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-06 10:05:57.973', '143f81f3-3029-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('191a7659-38fc-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-17 15:36:39.230', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('2087ab3c-3743-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-15 11:00:03.632', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('264a7338-3678-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-14 10:47:05.461', '264a7336-3678-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('264a9a4f-3678-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsite', '264a9a4e-3678-11ea-aa63-00163e2e65eb', '2020-01-14 10:47:05.462', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('264a9a50-3678-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-14 10:47:05.462', '264a7336-3678-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('2ba1f64d-38fc-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-17 15:37:10.317', '979e7168-3368-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('3414bed6-3046-11ea-9129-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-06 13:34:26.784', '3414bed4-3046-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('3414e5ed-3046-11ea-9129-00163e2e65eb', NULL, 'assignee', 'jsitehr', '3414e5ec-3046-11ea-9129-00163e2e65eb', '2020-01-06 13:34:26.785', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('3414e5ee-3046-11ea-9129-00163e2e65eb', NULL, 'participant', 'jsitehr', NULL, '2020-01-06 13:34:26.785', '3414bed4-3046-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('34d01ef2-38fc-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-17 15:37:25.719', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('3a5b8ec4-3694-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-14 14:08:05.036', '979e7168-3368-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('3a5e4de8-3694-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'dept', '3a5e4de7-3694-11ea-aa63-00163e2e65eb', '2020-01-14 14:08:05.054', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('3d30c520-3029-11ea-9129-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-06 10:07:06.662', '3d309e0e-3029-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('3d30ec37-3029-11ea-9129-00163e2e65eb', NULL, 'assignee', 'jsite', '3d30c526-3029-11ea-9129-00163e2e65eb', '2020-01-06 10:07:06.663', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('3d30ec38-3029-11ea-9129-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-06 10:07:06.663', '3d309e0e-3029-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('4f4c2978-4d82-11ea-9b4e-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-02-12 18:27:45.970', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('520e912e-3029-11ea-9129-00163e2e65eb', NULL, 'assignee', 'jsite', '520e912d-3029-11ea-9129-00163e2e65eb', '2020-01-06 10:07:41.670', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('54643a37-38fc-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-17 15:38:18.699', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('56631328-383f-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-16 17:05:27.165', '56631326-383f-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('56633a3f-383f-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsitehr', '5663132e-383f-11ea-aa63-00163e2e65eb', '2020-01-16 17:05:27.166', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('56633a40-383f-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsitehr', NULL, '2020-01-16 17:05:27.166', '56631326-383f-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('572c4377-2df4-11ea-aa23-c85b7643dd9e', NULL, 'assignee', 'jsite', '572c4376-2df4-11ea-aa23-c85b7643dd9e', '2020-01-03 14:43:24.601', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('5da64e49-32b9-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-09 16:23:50.974', '5da62737-32b9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('5da67560-32b9-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsiteuser', '5da64e4f-32b9-11ea-aa63-00163e2e65eb', '2020-01-09 16:23:50.975', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('5da67561-32b9-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsiteuser', NULL, '2020-01-09 16:23:50.975', '5da62737-32b9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('66bf8991-3827-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-16 14:14:06.693', '66bf898f-3827-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('66bfb0a8-3827-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsitehr', '66bfb0a7-3827-11ea-aa63-00163e2e65eb', '2020-01-16 14:14:06.694', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('66bfb0a9-3827-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsitehr', NULL, '2020-01-16 14:14:06.694', '66bf898f-3827-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('707da370-337a-11ea-aa63-00163e2e65eb', NULL, 'participant', 'dept', NULL, '2020-01-10 15:25:55.454', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('783fe757-2e95-11ea-9129-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-04 09:56:49.068', '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('784df11e-2e95-11ea-9129-00163e2e65eb', NULL, 'assignee', 'dept', '784d2dcd-2e95-11ea-9129-00163e2e65eb', '2020-01-04 09:56:49.159', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('784df11f-2e95-11ea-9129-00163e2e65eb', NULL, 'participant', 'dept', NULL, '2020-01-04 09:56:49.160', '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('79cfb6f4-2df3-11ea-aa23-c85b7643dd9e', NULL, 'starter', 'jsite', NULL, '2020-01-03 14:37:13.218', '79cea582-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('79d81b6b-2df3-11ea-aa23-c85b7643dd9e', NULL, 'assignee', 'jsite', '79d709fa-2df3-11ea-aa23-c85b7643dd9e', '2020-01-03 14:37:13.273', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('79d81b6c-2df3-11ea-aa23-c85b7643dd9e', NULL, 'participant', 'jsite', NULL, '2020-01-03 14:37:13.273', '79cea582-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('7c8a32c7-35d1-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-13 14:54:04.209', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('7c8a59de-35d1-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsite', '7c8a59dd-35d1-11ea-aa63-00163e2e65eb', '2020-01-13 14:54:04.210', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('7c8a59df-35d1-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-13 14:54:04.210', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('8290f1e5-3387-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-10 16:59:29.236', '8290f1e3-3387-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('829118fc-3387-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsitehr', '829118fb-3387-11ea-aa63-00163e2e65eb', '2020-01-10 16:59:29.237', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('829118fd-3387-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsitehr', NULL, '2020-01-10 16:59:29.237', '8290f1e3-3387-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('8d11d950-3387-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-10 16:59:46.858', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('8d149874-3387-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsite', '8d149873-3387-11ea-aa63-00163e2e65eb', '2020-01-10 16:59:46.876', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('8db9e165-3379-11ea-aa63-00163e2e65eb', NULL, 'starter', 'dept', NULL, '2020-01-10 15:19:35.005', '8db9ba53-3379-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('8db9e16c-3379-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsitehr', '8db9e16b-3379-11ea-aa63-00163e2e65eb', '2020-01-10 15:19:35.005', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('8db9e16d-3379-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsitehr', NULL, '2020-01-10 15:19:35.006', '8db9ba53-3379-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('90237412-35d5-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-13 15:23:15.077', '90237410-35d5-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('90239b29-35d5-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsite', '90239b28-35d5-11ea-aa63-00163e2e65eb', '2020-01-13 15:23:15.078', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('90239b2a-35d5-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-13 15:23:15.078', '90237410-35d5-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('917e7356-3678-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsite', '917e7355-3678-11ea-aa63-00163e2e65eb', '2020-01-14 10:50:05.318', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('91802107-3678-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-14 10:50:05.329', '979e7168-3368-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('979e716a-3368-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-10 13:18:10.158', '979e7168-3368-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('979e9881-3368-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'dept', '979e9880-3368-11ea-aa63-00163e2e65eb', '2020-01-10 13:18:10.159', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('979e9882-3368-11ea-aa63-00163e2e65eb', NULL, 'participant', 'dept', NULL, '2020-01-10 13:18:10.159', '979e7168-3368-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('9da8a9a3-3029-11ea-9129-00163e2e65eb', NULL, 'assignee', 'jsite', '9da8a9a2-3029-11ea-9129-00163e2e65eb', '2020-01-06 10:09:48.509', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('a0af89dd-2df4-11ea-aa23-c85b7643dd9e', NULL, 'starter', 'jsite', NULL, '2020-01-03 14:45:27.935', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('a0afb0f4-2df4-11ea-aa23-c85b7643dd9e', NULL, 'assignee', 'dept', 'a0afb0f3-2df4-11ea-aa23-c85b7643dd9e', '2020-01-03 14:45:27.936', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('a0afb0f5-2df4-11ea-aa23-c85b7643dd9e', NULL, 'participant', 'dept', NULL, '2020-01-03 14:45:27.936', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('a20aeb34-3051-11ea-9129-00163e2e65eb', NULL, 'assignee', 'jsite', 'a20ac423-3051-11ea-9129-00163e2e65eb', '2020-01-06 14:56:15.733', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('a20b8775-3051-11ea-9129-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-06 14:56:15.737', '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('a22ff7ac-35d5-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-13 15:23:45.358', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('a232b6d0-35d5-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsite', 'a2328fbf-35d5-11ea-aa63-00163e2e65eb', '2020-01-13 15:23:45.375', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('aa3c297f-3349-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-10 09:36:46.992', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('aa3c5096-3349-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsite', 'aa3c5095-3349-11ea-aa63-00163e2e65eb', '2020-01-10 09:36:46.993', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('aa3c5097-3349-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-10 09:36:46.993', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('ad566de2-35d5-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-13 15:24:04.065', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('b1df9f87-35d5-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-13 15:24:11.675', '90237410-35d5-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('b751aa8c-35d5-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsite', NULL, '2020-01-13 15:24:20.811', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('c0a8e6aa-3051-11ea-9129-00163e2e65eb', NULL, 'assignee', 'dept', 'c0a8e6a9-3051-11ea-9129-00163e2e65eb', '2020-01-06 14:57:07.100', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('c5247236-3156-11ea-9129-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-07 22:05:33.267', 'c5247234-3156-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('c524994d-3156-11ea-9129-00163e2e65eb', NULL, 'assignee', 'dept', 'c524994c-3156-11ea-9129-00163e2e65eb', '2020-01-07 22:05:33.268', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('c524994e-3156-11ea-9129-00163e2e65eb', NULL, 'participant', 'dept', NULL, '2020-01-07 22:05:33.268', 'c5247234-3156-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('c81fe555-36b7-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-14 18:22:35.267', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('c8200c6c-36b7-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsiteuser', 'c8200c6b-36b7-11ea-aa63-00163e2e65eb', '2020-01-14 18:22:35.268', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('c8200c6d-36b7-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsiteuser', NULL, '2020-01-14 18:22:35.268', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('c87ffc94-32bd-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-09 16:55:28.226', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('c88023ab-32bd-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsitehr', 'c88023aa-32bd-11ea-aa63-00163e2e65eb', '2020-01-09 16:55:28.227', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('c88023ac-32bd-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsitehr', NULL, '2020-01-09 16:55:28.227', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('ca5a65b1-4d6c-11ea-9b4e-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-02-12 15:53:43.495', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('ca5a8cc8-4d6c-11ea-9b4e-00163e2e65eb', NULL, 'assignee', 'jsitehr', 'ca5a8cc7-4d6c-11ea-9b4e-00163e2e65eb', '2020-02-12 15:53:43.496', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('ca5a8cc9-4d6c-11ea-9b4e-00163e2e65eb', NULL, 'participant', 'jsitehr', NULL, '2020-02-12 15:53:43.496', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('caea8d1a-36ae-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-14 17:18:14.480', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('caeab431-36ae-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsitehr', 'caeab430-36ae-11ea-aa63-00163e2e65eb', '2020-01-14 17:18:14.481', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('caeab432-36ae-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsitehr', NULL, '2020-01-14 17:18:14.481', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('d698cf0d-4d81-11ea-9b4e-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-02-12 18:24:23.468', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('d698cf14-4d81-11ea-9b4e-00163e2e65eb', NULL, 'assignee', 'dept', 'd698cf13-4d81-11ea-9b4e-00163e2e65eb', '2020-02-12 18:24:23.468', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('d698f625-4d81-11ea-9b4e-00163e2e65eb', NULL, 'participant', 'dept', NULL, '2020-02-12 18:24:23.469', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('dafeba3a-367d-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-14 11:27:56.116', 'dafeba38-367d-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('dafee151-367d-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'dept', 'dafee150-367d-11ea-aa63-00163e2e65eb', '2020-01-14 11:27:56.117', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('dafee152-367d-11ea-aa63-00163e2e65eb', NULL, 'participant', 'dept', NULL, '2020-01-14 11:27:56.117', 'dafeba38-367d-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('de645934-4bdf-11ea-9b4e-00163e2e65eb', NULL, 'starter', 'dept', NULL, '2020-02-10 16:32:26.913', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('de6e1d3b-4bdf-11ea-9b4e-00163e2e65eb', NULL, 'assignee', 'jsitehr', 'de6d80fa-4bdf-11ea-9b4e-00163e2e65eb', '2020-02-10 16:32:26.977', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('de6e444c-4bdf-11ea-9b4e-00163e2e65eb', NULL, 'participant', 'jsitehr', NULL, '2020-02-10 16:32:26.978', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('e08f1ee1-4c97-11ea-9b4e-00163e2e65eb', NULL, 'starter', 'dept', NULL, '2020-02-11 14:29:37.947', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('e08f45f8-4c97-11ea-9b4e-00163e2e65eb', NULL, 'assignee', 'dept', 'e08f45f7-4c97-11ea-9b4e-00163e2e65eb', '2020-02-11 14:29:37.948', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('e08f6d09-4c97-11ea-9b4e-00163e2e65eb', NULL, 'participant', 'dept', NULL, '2020-02-11 14:29:37.949', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('e318ed2e-305b-11ea-9129-00163e2e65eb', NULL, 'starter', 'dept', NULL, '2020-01-06 16:09:39.844', 'e318c61c-305b-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('e318ed35-305b-11ea-9129-00163e2e65eb', NULL, 'assignee', 'dept', 'e318ed34-305b-11ea-9129-00163e2e65eb', '2020-01-06 16:09:39.844', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('e318ed36-305b-11ea-9129-00163e2e65eb', NULL, 'participant', 'dept', NULL, '2020-01-06 16:09:39.844', 'e318c61c-305b-11ea-9129-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('ec0a0dfe-38fb-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-17 15:35:23.625', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('ec0a0e05-38fb-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsitehr', 'ec0a0e04-38fb-11ea-aa63-00163e2e65eb', '2020-01-17 15:35:23.625', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('ec0a3516-38fb-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsitehr', NULL, '2020-01-17 15:35:23.626', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('f5d44380-3733-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-15 09:11:29.541', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('f5d46a97-3733-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'dept', 'f5d44386-3733-11ea-aa63-00163e2e65eb', '2020-01-15 09:11:29.542', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('f5d46a98-3733-11ea-aa63-00163e2e65eb', NULL, 'participant', 'dept', NULL, '2020-01-15 09:11:29.542', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('f9e02283-38f5-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-17 14:52:49.858', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('f9e0499a-38f5-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsitehr', 'f9e04999-38f5-11ea-aa63-00163e2e65eb', '2020-01-17 14:52:49.859', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('f9e0499b-38f5-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsitehr', NULL, '2020-01-17 14:52:49.859', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('fc715bef-36a6-11ea-aa63-00163e2e65eb', NULL, 'starter', 'jsite', NULL, '2020-01-14 16:22:21.598', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('fc718306-36a6-11ea-aa63-00163e2e65eb', NULL, 'assignee', 'jsitehr', 'fc718305-36a6-11ea-aa63-00163e2e65eb', '2020-01-14 16:22:21.599', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_HI_IDENTITYLINK` VALUES ('fc718307-36a6-11ea-aa63-00163e2e65eb', NULL, 'participant', 'jsitehr', NULL, '2020-01-14 16:22:21.599', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for ACT_HI_PROCINST
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_PROCINST`;
CREATE TABLE `ACT_HI_PROCINST`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT 1,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(20) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `END_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUPER_PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `PROC_INST_ID_`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PRO_INST_END`(`END_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_PRO_I_BUSKEY`(`BUSINESS_KEY_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_HI_PROCINST
-- ----------------------------
INSERT INTO `ACT_HI_PROCINST` VALUES ('006ae1ba-3838-11ea-aa63-00163e2e65eb', 1, '006ae1ba-3838-11ea-aa63-00163e2e65eb', '627901d2c68e469189224b66471c9676', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-16 16:12:56.455', NULL, NULL, 'jsite', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('0ad9c74c-3223-11ea-aa63-00163e2e65eb', 2, '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '5ea728e17994433aac9a509ac9944ed0', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-08 22:27:47.550', '2020-01-13 15:24:04.124', 406576574, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('1034b9e1-31f9-11ea-aa63-00163e2e65eb', 1, '1034b9e1-31f9-11ea-aa63-00163e2e65eb', '190b101831714360a8385c1d46a84df3', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-08 17:27:17.673', NULL, NULL, 'jsite', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('1035a7c0-2f8c-11ea-9129-00163e2e65eb', 2, '1035a7c0-2f8c-11ea-9129-00163e2e65eb', '54546dadb08e4dcbbc3c90ba739fcd91', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-05 15:22:00.210', '2020-01-05 15:22:17.843', 17633, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('143f81f3-3029-11ea-9129-00163e2e65eb', 2, '143f81f3-3029-11ea-9129-00163e2e65eb', '81eb2f7819294ad6a5e065c42b1d0236', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-06 10:05:57.972', '2020-01-08 11:12:24.954', 176786982, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('264a7336-3678-11ea-aa63-00163e2e65eb', 2, '264a7336-3678-11ea-aa63-00163e2e65eb', '20d017eef151490e86afa85894aa23c6', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-14 10:47:05.461', '2020-01-14 14:08:19.347', 12073886, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('3414bed4-3046-11ea-9129-00163e2e65eb', 2, '3414bed4-3046-11ea-9129-00163e2e65eb', '16957f196e45430982cf67f4c07586d2', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-06 13:34:26.784', '2020-01-07 16:49:14.599', 98087815, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('3d309e0e-3029-11ea-9129-00163e2e65eb', 2, '3d309e0e-3029-11ea-9129-00163e2e65eb', 'a9e2126951284868bceb43e81caca869', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-06 10:07:06.661', '2020-01-07 14:48:24.548', 103277887, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('56631326-383f-11ea-aa63-00163e2e65eb', 1, '56631326-383f-11ea-aa63-00163e2e65eb', '699ddfb824f04d96b76c42354dc2964f', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-16 17:05:27.165', NULL, NULL, 'jsite', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('5da62737-32b9-11ea-aa63-00163e2e65eb', 1, '5da62737-32b9-11ea-aa63-00163e2e65eb', '346fbb981ff44d0eb231846927c61715', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-09 16:23:50.973', NULL, NULL, 'jsite', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('66bf898f-3827-11ea-aa63-00163e2e65eb', 1, '66bf898f-3827-11ea-aa63-00163e2e65eb', '2d498acf528f46b395a00fcdf93b5c54', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-16 14:14:06.693', NULL, NULL, 'jsite', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('783cda15-2e95-11ea-9129-00163e2e65eb', 1, '783cda15-2e95-11ea-9129-00163e2e65eb', '6dac5249254843e2a25d59de898686b9', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-04 09:56:49.048', NULL, NULL, 'jsite', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('79cea582-2df3-11ea-aa23-c85b7643dd9e', 2, '79cea582-2df3-11ea-aa23-c85b7643dd9e', 'bd03bf2b04664b51884db6b5a85b8abb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-03 14:37:13.211', '2020-01-03 14:44:02.441', 409230, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('7c8a32c5-35d1-11ea-aa63-00163e2e65eb', 2, '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', 'f89ed621413846589bd433fee03826c5', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-13 14:54:04.209', '2020-01-13 15:24:20.867', 1816658, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('8290f1e3-3387-11ea-aa63-00163e2e65eb', 1, '8290f1e3-3387-11ea-aa63-00163e2e65eb', '797aa0d32095404f9c5796fd3bb69545', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-10 16:59:29.236', NULL, NULL, 'jsite', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('8db9ba53-3379-11ea-aa63-00163e2e65eb', 1, '8db9ba53-3379-11ea-aa63-00163e2e65eb', '1a801d4529984f36998d9ac51851c353', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-10 15:19:35.004', NULL, NULL, 'dept', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('90237410-35d5-11ea-aa63-00163e2e65eb', 2, '90237410-35d5-11ea-aa63-00163e2e65eb', 'ef2d9d44ab6e4c89a379f5b8c71d1f73', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-13 15:23:15.077', '2020-01-13 15:24:11.754', 56677, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('979e7168-3368-11ea-aa63-00163e2e65eb', 2, '979e7168-3368-11ea-aa63-00163e2e65eb', '0bcda03d39e843088a4c3d6fab230dab', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-10 13:18:10.158', '2020-01-17 15:37:10.376', 613140218, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('a0af89db-2df4-11ea-aa23-c85b7643dd9e', 2, 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', '75294dca525741a480260d8820a46953', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-03 14:45:27.935', '2020-01-17 15:37:25.777', 1212717842, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('aa3c297d-3349-11ea-aa63-00163e2e65eb', 2, 'aa3c297d-3349-11ea-aa63-00163e2e65eb', '621b45fe147b4e1291d5acd272ba2f2e', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-10 09:36:46.992', '2020-01-10 15:25:55.512', 20948520, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('c5247234-3156-11ea-9129-00163e2e65eb', 1, 'c5247234-3156-11ea-9129-00163e2e65eb', '815a7d8c847946a59dd4dcef7427fd2f', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-07 22:05:33.267', NULL, NULL, 'jsite', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('c81fe553-36b7-11ea-aa63-00163e2e65eb', 1, 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'b393f21fd12648bb9c7cf79241e30b40', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-14 18:22:35.267', NULL, NULL, 'jsite', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('c87ffc92-32bd-11ea-aa63-00163e2e65eb', 1, 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'dc37012c6fc649c3a82868288c8b5bd4', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-09 16:55:28.226', NULL, NULL, 'jsite', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 1, 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', '6bcbe514462a45ff8778f010595169ef', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-02-12 15:53:43.495', NULL, NULL, 'jsite', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('caea8d18-36ae-11ea-aa63-00163e2e65eb', 2, 'caea8d18-36ae-11ea-aa63-00163e2e65eb', '785f9f3ba20d443091c3969187f9b9a0', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-14 17:18:14.480', '2020-01-15 11:00:03.690', 63709210, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('d698a7fb-4d81-11ea-9b4e-00163e2e65eb', 2, 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'd40bdbaf8d9e426885097b21f390293a', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-02-12 18:24:23.467', '2020-02-12 18:27:46.018', 202551, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('dafeba38-367d-11ea-aa63-00163e2e65eb', 2, 'dafeba38-367d-11ea-aa63-00163e2e65eb', '352b30c8d8e14b3592244daeca9a4a11', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-14 11:27:56.116', '2020-02-11 14:29:46.338', 2430110222, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 1, 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', '42d4054b569745f181d2d369703e5998', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-02-10 16:32:26.908', NULL, NULL, 'dept', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 1, 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', '1ca7c4347a9349d1aeec97e9bf828091', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-02-11 14:29:37.947', NULL, NULL, 'dept', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('e318c61c-305b-11ea-9129-00163e2e65eb', 2, 'e318c61c-305b-11ea-9129-00163e2e65eb', 'fc8a20a02b1d4b15a8bab8e2ee0efde9', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-06 16:09:39.843', '2020-01-06 16:10:38.561', 58718, 'dept', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 2, 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', '9ea6742d060140979d2fa6309d8d8fe1', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-17 15:35:23.625', '2020-01-17 15:38:18.757', 175132, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('f5d4437e-3733-11ea-aa63-00163e2e65eb', 2, 'f5d4437e-3733-11ea-aa63-00163e2e65eb', '0150e3a778874b609905ba4f3c05aa14', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-15 09:11:29.541', '2020-01-17 15:36:39.297', 195909756, 'jsite', 'startevent1', 'end', NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('f9e02281-38f5-11ea-aa63-00163e2e65eb', 1, 'f9e02281-38f5-11ea-aa63-00163e2e65eb', '38bc9bbdfcf14f09a9b8f3fdc50409ff', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-17 14:52:49.858', NULL, NULL, 'jsite', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);
INSERT INTO `ACT_HI_PROCINST` VALUES ('fc715bed-36a6-11ea-aa63-00163e2e65eb', 1, 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'eed2fae2591d46b3917cd0980e6c76a6', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', '2020-01-14 16:22:21.598', NULL, NULL, 'jsite', 'startevent1', NULL, NULL, NULL, '', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for ACT_HI_TASKINST
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_TASKINST`;
CREATE TABLE `ACT_HI_TASKINST`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT 1,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `CLAIM_TIME_` datetime(3) NULL DEFAULT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(20) NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PRIORITY_` int(11) NULL DEFAULT NULL,
  `DUE_DATE_` datetime(3) NULL DEFAULT NULL,
  `FORM_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `LAST_UPDATED_TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_INST_PROCINST`(`PROC_INST_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_HI_TASKINST
-- ----------------------------
INSERT INTO `ACT_HI_TASKINST` VALUES ('006ae1c2-3838-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '006ae1ba-3838-11ea-aa63-00163e2e65eb', '006ae1bf-3838-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsitehr', '2020-01-16 16:12:56.455', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-16 16:12:56.456');
INSERT INTO `ACT_HI_TASKINST` VALUES ('084d1221-2df4-11ea-aa23-c85b7643dd9e', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'adjustApply', '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79d02c27-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, '调整申请', NULL, NULL, NULL, 'jsite', '2020-01-03 14:41:12.276', NULL, '2020-01-03 14:43:24.487', 132211, NULL, 50, NULL, NULL, NULL, '', '2020-01-03 14:43:24.487');
INSERT INTO `ACT_HI_TASKINST` VALUES ('0ad9ee64-3223-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c751-3223-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsite', '2020-01-08 22:27:47.552', NULL, '2020-01-10 16:59:46.866', 153119314, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-10 16:59:46.866');
INSERT INTO `ACT_HI_TASKINST` VALUES ('1035ced8-2f8c-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', '1035a7c5-2f8c-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsite', '2020-01-05 15:22:00.211', NULL, '2020-01-05 15:22:17.757', 17546, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-05 15:22:17.757');
INSERT INTO `ACT_HI_TASKINST` VALUES ('103bbec9-31f9-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', '1035cb56-31f9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsitehr', '2020-01-08 17:27:17.720', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-08 17:27:17.723');
INSERT INTO `ACT_HI_TASKINST` VALUES ('143fa90b-3029-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '143f81f3-3029-11ea-9129-00163e2e65eb', '143fa908-3029-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsite', '2020-01-06 10:05:57.973', NULL, '2020-01-08 11:12:24.910', 176786937, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-08 11:12:24.910');
INSERT INTO `ACT_HI_TASKINST` VALUES ('264a9a4e-3678-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '264a7336-3678-11ea-aa63-00163e2e65eb', '264a733b-3678-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsite', '2020-01-14 10:47:05.462', NULL, '2020-01-14 14:08:19.292', 12073830, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-14 14:08:19.292');
INSERT INTO `ACT_HI_TASKINST` VALUES ('3414e5ec-3046-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '3414bed4-3046-11ea-9129-00163e2e65eb', '3414bed9-3046-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsitehr', '2020-01-06 13:34:26.785', NULL, '2020-01-07 16:49:14.547', 98087762, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-07 16:49:14.547');
INSERT INTO `ACT_HI_TASKINST` VALUES ('3a5e4de7-3694-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '979e7168-3368-11ea-aa63-00163e2e65eb', '979e716d-3368-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'dept', '2020-01-14 14:08:05.054', NULL, '2020-01-17 15:37:10.326', 264545272, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-17 15:37:10.326');
INSERT INTO `ACT_HI_TASKINST` VALUES ('3d30c526-3029-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d30c523-3029-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsite', '2020-01-06 10:07:06.662', NULL, '2020-01-06 10:07:41.456', 34794, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-06 10:07:41.456');
INSERT INTO `ACT_HI_TASKINST` VALUES ('520e912d-3029-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'adjustApply', '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d30c523-3029-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, '调整申请', NULL, NULL, NULL, 'jsite', '2020-01-06 10:07:41.670', NULL, '2020-01-06 10:09:48.498', 126828, NULL, 50, NULL, NULL, NULL, '', '2020-01-06 10:09:48.498');
INSERT INTO `ACT_HI_TASKINST` VALUES ('5663132e-383f-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '56631326-383f-11ea-aa63-00163e2e65eb', '5663132b-383f-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsitehr', '2020-01-16 17:05:27.165', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-16 17:05:27.166');
INSERT INTO `ACT_HI_TASKINST` VALUES ('572c4376-2df4-11ea-aa23-c85b7643dd9e', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79d02c27-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsite', '2020-01-03 14:43:24.601', NULL, '2020-01-03 14:44:01.925', 37324, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-03 14:44:01.925');
INSERT INTO `ACT_HI_TASKINST` VALUES ('5da64e4f-32b9-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '5da62737-32b9-11ea-aa63-00163e2e65eb', '5da64e4c-32b9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsiteuser', '2020-01-09 16:23:50.974', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-09 16:23:50.975');
INSERT INTO `ACT_HI_TASKINST` VALUES ('66bfb0a7-3827-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '66bf898f-3827-11ea-aa63-00163e2e65eb', '66bf8994-3827-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsitehr', '2020-01-16 14:14:06.694', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-16 14:14:06.694');
INSERT INTO `ACT_HI_TASKINST` VALUES ('784d2dcd-2e95-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '783cda15-2e95-11ea-9129-00163e2e65eb', '78400e6a-2e95-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'dept', '2020-01-04 09:56:49.156', NULL, '2020-01-06 14:56:15.725', 190766569, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-06 14:56:15.725');
INSERT INTO `ACT_HI_TASKINST` VALUES ('79d709fa-2df3-11ea-aa23-c85b7643dd9e', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79d02c27-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsite', '2020-01-03 14:37:13.268', NULL, '2020-01-03 14:41:12.205', 238937, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-03 14:41:12.205');
INSERT INTO `ACT_HI_TASKINST` VALUES ('7c8a59dd-35d1-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', '7c8a32ca-35d1-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsite', '2020-01-13 14:54:04.210', NULL, '2020-01-13 15:24:20.819', 1816609, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-13 15:24:20.819');
INSERT INTO `ACT_HI_TASKINST` VALUES ('829118fb-3387-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '8290f1e3-3387-11ea-aa63-00163e2e65eb', '829118f8-3387-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsitehr', '2020-01-10 16:59:29.237', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-10 16:59:29.237');
INSERT INTO `ACT_HI_TASKINST` VALUES ('8d149873-3387-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'adjustApply', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c751-3223-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '调整申请', NULL, NULL, NULL, 'jsite', '2020-01-10 16:59:46.876', NULL, '2020-01-13 15:23:45.366', 253438490, NULL, 50, NULL, NULL, NULL, '', '2020-01-13 15:23:45.366');
INSERT INTO `ACT_HI_TASKINST` VALUES ('8db9e16b-3379-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '8db9ba53-3379-11ea-aa63-00163e2e65eb', '8db9e168-3379-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsitehr', '2020-01-10 15:19:35.005', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-10 15:19:35.005');
INSERT INTO `ACT_HI_TASKINST` VALUES ('90239b28-35d5-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '90237410-35d5-11ea-aa63-00163e2e65eb', '90237415-35d5-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsite', '2020-01-13 15:23:15.078', NULL, '2020-01-13 15:24:11.683', 56605, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-13 15:24:11.683');
INSERT INTO `ACT_HI_TASKINST` VALUES ('917e7355-3678-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'adjustApply', '979e7168-3368-11ea-aa63-00163e2e65eb', '979e716d-3368-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '调整申请', NULL, NULL, NULL, 'jsite', '2020-01-14 10:50:05.318', NULL, '2020-01-14 14:08:05.045', 11879727, NULL, 50, NULL, NULL, NULL, '', '2020-01-14 14:08:05.045');
INSERT INTO `ACT_HI_TASKINST` VALUES ('979e9880-3368-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '979e7168-3368-11ea-aa63-00163e2e65eb', '979e716d-3368-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'dept', '2020-01-10 13:18:10.159', NULL, '2020-01-14 10:50:05.310', 336715151, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-14 10:50:05.310');
INSERT INTO `ACT_HI_TASKINST` VALUES ('9da8a9a2-3029-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d30c523-3029-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsite', '2020-01-06 10:09:48.509', NULL, '2020-01-07 14:48:24.514', 103116005, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-07 14:48:24.514');
INSERT INTO `ACT_HI_TASKINST` VALUES ('a0afb0f3-2df4-11ea-aa23-c85b7643dd9e', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'a0af89e0-2df4-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'dept', '2020-01-03 14:45:27.936', NULL, '2020-01-17 15:37:25.730', 1212717794, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-17 15:37:25.730');
INSERT INTO `ACT_HI_TASKINST` VALUES ('a20ac423-3051-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'adjustApply', '783cda15-2e95-11ea-9129-00163e2e65eb', '78400e6a-2e95-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, '调整申请', NULL, NULL, NULL, 'jsite', '2020-01-06 14:56:15.733', NULL, '2020-01-06 14:57:07.092', 51359, NULL, 50, NULL, NULL, NULL, '', '2020-01-06 14:57:07.092');
INSERT INTO `ACT_HI_TASKINST` VALUES ('a2328fbf-35d5-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c751-3223-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsite', '2020-01-13 15:23:45.375', NULL, '2020-01-13 15:24:04.073', 18698, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-13 15:24:04.073');
INSERT INTO `ACT_HI_TASKINST` VALUES ('aa3c5095-3349-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'aa3c2982-3349-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsite', '2020-01-10 09:36:46.993', NULL, '2020-01-10 15:25:55.461', 20948468, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-10 15:25:55.461');
INSERT INTO `ACT_HI_TASKINST` VALUES ('c0a8e6a9-3051-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', '783cda15-2e95-11ea-9129-00163e2e65eb', '78400e6a-2e95-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'dept', '2020-01-06 14:57:07.100', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-06 14:57:07.100');
INSERT INTO `ACT_HI_TASKINST` VALUES ('c524994c-3156-11ea-9129-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'c5247234-3156-11ea-9129-00163e2e65eb', 'c5247239-3156-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'dept', '2020-01-07 22:05:33.268', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-07 22:05:33.268');
INSERT INTO `ACT_HI_TASKINST` VALUES ('c8200c6b-36b7-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'c81fe558-36b7-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsiteuser', '2020-01-14 18:22:35.268', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-14 18:22:35.268');
INSERT INTO `ACT_HI_TASKINST` VALUES ('c88023aa-32bd-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'c87ffc97-32bd-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsitehr', '2020-01-09 16:55:28.227', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-09 16:55:28.227');
INSERT INTO `ACT_HI_TASKINST` VALUES ('ca5a8cc7-4d6c-11ea-9b4e-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'ca5a8cc4-4d6c-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsitehr', '2020-02-12 15:53:43.496', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-02-12 15:53:43.496');
INSERT INTO `ACT_HI_TASKINST` VALUES ('caeab430-36ae-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'caeab42d-36ae-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsitehr', '2020-01-14 17:18:14.481', NULL, '2020-01-15 11:00:03.640', 63709159, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-15 11:00:03.640');
INSERT INTO `ACT_HI_TASKINST` VALUES ('d698cf13-4d81-11ea-9b4e-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'd698cf10-4d81-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'dept', '2020-02-12 18:24:23.468', NULL, '2020-02-12 18:27:45.977', 202509, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-02-12 18:27:45.977');
INSERT INTO `ACT_HI_TASKINST` VALUES ('dafee150-367d-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'dafee14d-367d-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'dept', '2020-01-14 11:27:56.117', NULL, '2020-02-11 14:29:46.268', 2430110151, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-02-11 14:29:46.268');
INSERT INTO `ACT_HI_TASKINST` VALUES ('de6d80fa-4bdf-11ea-9b4e-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'de648047-4bdf-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsitehr', '2020-02-10 16:32:26.974', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-02-10 16:32:26.977');
INSERT INTO `ACT_HI_TASKINST` VALUES ('e08f45f7-4c97-11ea-9b4e-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'e08f45f4-4c97-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'dept', '2020-02-11 14:29:37.948', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-02-11 14:29:37.948');
INSERT INTO `ACT_HI_TASKINST` VALUES ('e318ed34-305b-11ea-9129-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'e318c61c-305b-11ea-9129-00163e2e65eb', 'e318ed31-305b-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'dept', '2020-01-06 16:09:39.844', NULL, '2020-01-06 16:10:38.498', 58654, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-06 16:10:38.498');
INSERT INTO `ACT_HI_TASKINST` VALUES ('ec0a0e04-38fb-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'ec0a0e01-38fb-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsitehr', '2020-01-17 15:35:23.625', NULL, '2020-01-17 15:38:18.707', 175082, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-17 15:38:18.707');
INSERT INTO `ACT_HI_TASKINST` VALUES ('f5d44386-3733-11ea-aa63-00163e2e65eb', 2, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'f5d44383-3733-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'dept', '2020-01-15 09:11:29.542', NULL, '2020-01-17 15:36:39.238', 195909696, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-17 15:36:39.238');
INSERT INTO `ACT_HI_TASKINST` VALUES ('f9e04999-38f5-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'f9e02286-38f5-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsitehr', '2020-01-17 14:52:49.859', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-17 14:52:49.859');
INSERT INTO `ACT_HI_TASKINST` VALUES ('fc718305-36a6-11ea-aa63-00163e2e65eb', 1, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'fc718302-36a6-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, NULL, 'jsitehr', '2020-01-14 16:22:21.599', NULL, NULL, NULL, NULL, 50, NULL, 'leave_leader_audit', NULL, '', '2020-01-14 16:22:21.599');

-- ----------------------------
-- Table structure for ACT_HI_VARINST
-- ----------------------------
DROP TABLE IF EXISTS `ACT_HI_VARINST`;
CREATE TABLE `ACT_HI_VARINST`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT 1,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DOUBLE_` double NULL DEFAULT NULL,
  `LONG_` bigint(20) NULL DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_UPDATED_TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_NAME_TYPE`(`NAME_`, `VAR_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_VAR_SCOPE_ID_TYPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_VAR_SUB_ID_TYPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_PROC_INST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_TASK_ID`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_EXE`(`EXECUTION_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_HI_VARINST
-- ----------------------------
INSERT INTO `ACT_HI_VARINST` VALUES ('006ae1bb-3838-11ea-aa63-00163e2e65eb', 0, '006ae1ba-3838-11ea-aa63-00163e2e65eb', '006ae1ba-3838-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-16 16:12:56.455', '2020-01-16 16:12:56.455');
INSERT INTO `ACT_HI_VARINST` VALUES ('006ae1bd-3838-11ea-aa63-00163e2e65eb', 0, '006ae1ba-3838-11ea-aa63-00163e2e65eb', '006ae1ba-3838-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-16 16:12:56.455', '2020-01-16 16:12:56.455');
INSERT INTO `ACT_HI_VARINST` VALUES ('006ae1be-3838-11ea-aa63-00163e2e65eb', 0, '006ae1ba-3838-11ea-aa63-00163e2e65eb', '006ae1ba-3838-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL, '2020-01-16 16:12:56.455', '2020-01-16 16:12:56.455');
INSERT INTO `ACT_HI_VARINST` VALUES ('060cfc58-305c-11ea-9129-00163e2e65eb', 0, 'e318c61c-305b-11ea-9129-00163e2e65eb', 'e318c61c-305b-11ea-9129-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL, '2020-01-06 16:10:38.487', '2020-01-06 16:10:38.487');
INSERT INTO `ACT_HI_VARINST` VALUES ('0837b55e-2df4-11ea-aa23-c85b7643dd9e', 1, '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-03 14:41:12.137', '2020-01-03 14:44:01.846');
INSERT INTO `ACT_HI_VARINST` VALUES ('0ad9c74d-3223-11ea-aa63-00163e2e65eb', 2, '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-08 22:27:47.551', '2020-01-13 15:24:04.064');
INSERT INTO `ACT_HI_VARINST` VALUES ('0ad9c74f-3223-11ea-aa63-00163e2e65eb', 3, '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-08 22:27:47.551', '2020-01-13 15:24:04.055');
INSERT INTO `ACT_HI_VARINST` VALUES ('0ad9c750-3223-11ea-aa63-00163e2e65eb', 3, '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-08 22:27:47.551', '2020-01-13 15:24:04.055');
INSERT INTO `ACT_HI_VARINST` VALUES ('10350802-31f9-11ea-aa63-00163e2e65eb', 0, '1034b9e1-31f9-11ea-aa63-00163e2e65eb', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-08 17:27:17.677', '2020-01-08 17:27:17.677');
INSERT INTO `ACT_HI_VARINST` VALUES ('1035a444-31f9-11ea-aa63-00163e2e65eb', 0, '1034b9e1-31f9-11ea-aa63-00163e2e65eb', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-08 17:27:17.679', '2020-01-08 17:27:17.679');
INSERT INTO `ACT_HI_VARINST` VALUES ('1035a445-31f9-11ea-aa63-00163e2e65eb', 0, '1034b9e1-31f9-11ea-aa63-00163e2e65eb', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL, '2020-01-08 17:27:17.679', '2020-01-08 17:27:17.679');
INSERT INTO `ACT_HI_VARINST` VALUES ('1035a7c1-2f8c-11ea-9129-00163e2e65eb', 1, '1035a7c0-2f8c-11ea-9129-00163e2e65eb', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-05 15:22:00.210', '2020-01-05 15:22:17.752');
INSERT INTO `ACT_HI_VARINST` VALUES ('1035a7c3-2f8c-11ea-9129-00163e2e65eb', 1, '1035a7c0-2f8c-11ea-9129-00163e2e65eb', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-05 15:22:00.210', '2020-01-05 15:22:17.744');
INSERT INTO `ACT_HI_VARINST` VALUES ('1035a7c4-2f8c-11ea-9129-00163e2e65eb', 1, '1035a7c0-2f8c-11ea-9129-00163e2e65eb', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-05 15:22:00.210', '2020-01-05 15:22:17.744');
INSERT INTO `ACT_HI_VARINST` VALUES ('143f81f4-3029-11ea-9129-00163e2e65eb', 1, '143f81f3-3029-11ea-9129-00163e2e65eb', '143f81f3-3029-11ea-9129-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-06 10:05:57.972', '2020-01-08 11:12:24.905');
INSERT INTO `ACT_HI_VARINST` VALUES ('143f81f6-3029-11ea-9129-00163e2e65eb', 1, '143f81f3-3029-11ea-9129-00163e2e65eb', '143f81f3-3029-11ea-9129-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-06 10:05:57.972', '2020-01-08 11:12:24.898');
INSERT INTO `ACT_HI_VARINST` VALUES ('143f81f7-3029-11ea-9129-00163e2e65eb', 1, '143f81f3-3029-11ea-9129-00163e2e65eb', '143f81f3-3029-11ea-9129-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-06 10:05:57.972', '2020-01-08 11:12:24.898');
INSERT INTO `ACT_HI_VARINST` VALUES ('1919b308-38fc-11ea-aa63-00163e2e65eb', 0, 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL, '2020-01-17 15:36:39.225', '2020-01-17 15:36:39.225');
INSERT INTO `ACT_HI_VARINST` VALUES ('1aa9bdec-2f8c-11ea-9129-00163e2e65eb', 0, '1035a7c0-2f8c-11ea-9129-00163e2e65eb', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-05 15:22:17.748', '2020-01-05 15:22:17.748');
INSERT INTO `ACT_HI_VARINST` VALUES ('2086c0db-3743-11ea-aa63-00163e2e65eb', 0, 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL, '2020-01-15 11:00:03.627', '2020-01-15 11:00:03.627');
INSERT INTO `ACT_HI_VARINST` VALUES ('264a7337-3678-11ea-aa63-00163e2e65eb', 1, '264a7336-3678-11ea-aa63-00163e2e65eb', '264a7336-3678-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-14 10:47:05.461', '2020-01-14 14:08:19.285');
INSERT INTO `ACT_HI_VARINST` VALUES ('264a7339-3678-11ea-aa63-00163e2e65eb', 1, '264a7336-3678-11ea-aa63-00163e2e65eb', '264a7336-3678-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-14 10:47:05.461', '2020-01-14 14:08:19.277');
INSERT INTO `ACT_HI_VARINST` VALUES ('264a733a-3678-11ea-aa63-00163e2e65eb', 1, '264a7336-3678-11ea-aa63-00163e2e65eb', '264a7336-3678-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-14 10:47:05.461', '2020-01-14 14:08:19.277');
INSERT INTO `ACT_HI_VARINST` VALUES ('3414bed5-3046-11ea-9129-00163e2e65eb', 1, '3414bed4-3046-11ea-9129-00163e2e65eb', '3414bed4-3046-11ea-9129-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-06 13:34:26.784', '2020-01-07 16:49:14.543');
INSERT INTO `ACT_HI_VARINST` VALUES ('3414bed7-3046-11ea-9129-00163e2e65eb', 1, '3414bed4-3046-11ea-9129-00163e2e65eb', '3414bed4-3046-11ea-9129-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-06 13:34:26.784', '2020-01-07 16:49:14.537');
INSERT INTO `ACT_HI_VARINST` VALUES ('3414bed8-3046-11ea-9129-00163e2e65eb', 1, '3414bed4-3046-11ea-9129-00163e2e65eb', '3414bed4-3046-11ea-9129-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-06 13:34:26.784', '2020-01-07 16:49:14.537');
INSERT INTO `ACT_HI_VARINST` VALUES ('34cf0d81-38fc-11ea-aa63-00163e2e65eb', 0, 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL, '2020-01-17 15:37:25.712', '2020-01-17 15:37:25.712');
INSERT INTO `ACT_HI_VARINST` VALUES ('3d309e0f-3029-11ea-9129-00163e2e65eb', 2, '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d309e0e-3029-11ea-9129-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-06 10:07:06.662', '2020-01-07 14:48:24.510');
INSERT INTO `ACT_HI_VARINST` VALUES ('3d30c521-3029-11ea-9129-00163e2e65eb', 3, '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d309e0e-3029-11ea-9129-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-06 10:07:06.662', '2020-01-07 14:48:24.505');
INSERT INTO `ACT_HI_VARINST` VALUES ('3d30c522-3029-11ea-9129-00163e2e65eb', 3, '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d309e0e-3029-11ea-9129-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-06 10:07:06.662', '2020-01-07 14:48:24.505');
INSERT INTO `ACT_HI_VARINST` VALUES ('42d92c1a-3694-11ea-aa63-00163e2e65eb', 0, '264a7336-3678-11ea-aa63-00163e2e65eb', '264a7336-3678-11ea-aa63-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-14 14:08:19.281', '2020-01-14 14:08:19.281');
INSERT INTO `ACT_HI_VARINST` VALUES ('4f4b8d37-4d82-11ea-9b4e-00163e2e65eb', 0, 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL, '2020-02-12 18:27:45.966', '2020-02-12 18:27:45.966');
INSERT INTO `ACT_HI_VARINST` VALUES ('51e9f22a-3029-11ea-9129-00163e2e65eb', 1, '3d309e0e-3029-11ea-9129-00163e2e65eb', '3d309e0e-3029-11ea-9129-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-06 10:07:41.430', '2020-01-07 14:48:24.507');
INSERT INTO `ACT_HI_VARINST` VALUES ('546376e6-38fc-11ea-aa63-00163e2e65eb', 0, 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL, '2020-01-17 15:38:18.694', '2020-01-17 15:38:18.694');
INSERT INTO `ACT_HI_VARINST` VALUES ('56631327-383f-11ea-aa63-00163e2e65eb', 0, '56631326-383f-11ea-aa63-00163e2e65eb', '56631326-383f-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-16 17:05:27.165', '2020-01-16 17:05:27.165');
INSERT INTO `ACT_HI_VARINST` VALUES ('56631329-383f-11ea-aa63-00163e2e65eb', 0, '56631326-383f-11ea-aa63-00163e2e65eb', '56631326-383f-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-16 17:05:27.165', '2020-01-16 17:05:27.165');
INSERT INTO `ACT_HI_VARINST` VALUES ('5663132a-383f-11ea-aa63-00163e2e65eb', 0, '56631326-383f-11ea-aa63-00163e2e65eb', '56631326-383f-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL, '2020-01-16 17:05:27.165', '2020-01-16 17:05:27.165');
INSERT INTO `ACT_HI_VARINST` VALUES ('5da62738-32b9-11ea-aa63-00163e2e65eb', 0, '5da62737-32b9-11ea-aa63-00163e2e65eb', '5da62737-32b9-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-09 16:23:50.974', '2020-01-09 16:23:50.974');
INSERT INTO `ACT_HI_VARINST` VALUES ('5da64e4a-32b9-11ea-aa63-00163e2e65eb', 0, '5da62737-32b9-11ea-aa63-00163e2e65eb', '5da62737-32b9-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-09 16:23:50.974', '2020-01-09 16:23:50.974');
INSERT INTO `ACT_HI_VARINST` VALUES ('5da64e4b-32b9-11ea-aa63-00163e2e65eb', 0, '5da62737-32b9-11ea-aa63-00163e2e65eb', '5da62737-32b9-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsiteuser', NULL, '2020-01-09 16:23:50.974', '2020-01-09 16:23:50.974');
INSERT INTO `ACT_HI_VARINST` VALUES ('66bf8990-3827-11ea-aa63-00163e2e65eb', 0, '66bf898f-3827-11ea-aa63-00163e2e65eb', '66bf898f-3827-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-16 14:14:06.693', '2020-01-16 14:14:06.693');
INSERT INTO `ACT_HI_VARINST` VALUES ('66bf8992-3827-11ea-aa63-00163e2e65eb', 0, '66bf898f-3827-11ea-aa63-00163e2e65eb', '66bf898f-3827-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-16 14:14:06.693', '2020-01-16 14:14:06.693');
INSERT INTO `ACT_HI_VARINST` VALUES ('66bf8993-3827-11ea-aa63-00163e2e65eb', 0, '66bf898f-3827-11ea-aa63-00163e2e65eb', '66bf898f-3827-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL, '2020-01-16 14:14:06.693', '2020-01-16 14:14:06.693');
INSERT INTO `ACT_HI_VARINST` VALUES ('707d072f-337a-11ea-aa63-00163e2e65eb', 0, 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-10 15:25:55.449', '2020-01-10 15:25:55.449');
INSERT INTO `ACT_HI_VARINST` VALUES ('783d0126-2e95-11ea-9129-00163e2e65eb', 1, '783cda15-2e95-11ea-9129-00163e2e65eb', '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-04 09:56:49.052', '2020-01-06 14:56:15.720');
INSERT INTO `ACT_HI_VARINST` VALUES ('78400e68-2e95-11ea-9129-00163e2e65eb', 2, '783cda15-2e95-11ea-9129-00163e2e65eb', '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-04 09:56:49.069', '2020-01-06 14:57:07.085');
INSERT INTO `ACT_HI_VARINST` VALUES ('78400e69-2e95-11ea-9129-00163e2e65eb', 2, '783cda15-2e95-11ea-9129-00163e2e65eb', '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL, '2020-01-04 09:56:49.069', '2020-01-06 14:57:07.089');
INSERT INTO `ACT_HI_VARINST` VALUES ('79cef3a3-2df3-11ea-aa23-c85b7643dd9e', 2, '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-03 14:37:13.216', '2020-01-03 14:44:01.886');
INSERT INTO `ACT_HI_VARINST` VALUES ('79cfde05-2df3-11ea-aa23-c85b7643dd9e', 3, '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-03 14:37:13.219', '2020-01-03 14:44:01.809');
INSERT INTO `ACT_HI_VARINST` VALUES ('79cfde06-2df3-11ea-aa23-c85b7643dd9e', 3, '79cea582-2df3-11ea-aa23-c85b7643dd9e', '79cea582-2df3-11ea-aa23-c85b7643dd9e', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-03 14:37:13.219', '2020-01-03 14:44:01.809');
INSERT INTO `ACT_HI_VARINST` VALUES ('7c8a32c6-35d1-11ea-aa63-00163e2e65eb', 1, '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-13 14:54:04.209', '2020-01-13 15:24:20.810');
INSERT INTO `ACT_HI_VARINST` VALUES ('7c8a32c8-35d1-11ea-aa63-00163e2e65eb', 1, '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-13 14:54:04.209', '2020-01-13 15:24:20.802');
INSERT INTO `ACT_HI_VARINST` VALUES ('7c8a32c9-35d1-11ea-aa63-00163e2e65eb', 1, '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-13 14:54:04.209', '2020-01-13 15:24:20.802');
INSERT INTO `ACT_HI_VARINST` VALUES ('8290f1e4-3387-11ea-aa63-00163e2e65eb', 0, '8290f1e3-3387-11ea-aa63-00163e2e65eb', '8290f1e3-3387-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-10 16:59:29.236', '2020-01-10 16:59:29.236');
INSERT INTO `ACT_HI_VARINST` VALUES ('829118f6-3387-11ea-aa63-00163e2e65eb', 0, '8290f1e3-3387-11ea-aa63-00163e2e65eb', '8290f1e3-3387-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-10 16:59:29.237', '2020-01-10 16:59:29.237');
INSERT INTO `ACT_HI_VARINST` VALUES ('829118f7-3387-11ea-aa63-00163e2e65eb', 0, '8290f1e3-3387-11ea-aa63-00163e2e65eb', '8290f1e3-3387-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL, '2020-01-10 16:59:29.237', '2020-01-10 16:59:29.237');
INSERT INTO `ACT_HI_VARINST` VALUES ('8d10eeef-3387-11ea-aa63-00163e2e65eb', 1, '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-10 16:59:46.852', '2020-01-13 15:24:04.059');
INSERT INTO `ACT_HI_VARINST` VALUES ('8db9ba54-3379-11ea-aa63-00163e2e65eb', 0, '8db9ba53-3379-11ea-aa63-00163e2e65eb', '8db9ba53-3379-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL, '2020-01-10 15:19:35.005', '2020-01-10 15:19:35.005');
INSERT INTO `ACT_HI_VARINST` VALUES ('8db9e166-3379-11ea-aa63-00163e2e65eb', 0, '8db9ba53-3379-11ea-aa63-00163e2e65eb', '8db9ba53-3379-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-10 15:19:35.005', '2020-01-10 15:19:35.005');
INSERT INTO `ACT_HI_VARINST` VALUES ('8db9e167-3379-11ea-aa63-00163e2e65eb', 0, '8db9ba53-3379-11ea-aa63-00163e2e65eb', '8db9ba53-3379-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL, '2020-01-10 15:19:35.005', '2020-01-10 15:19:35.005');
INSERT INTO `ACT_HI_VARINST` VALUES ('90237411-35d5-11ea-aa63-00163e2e65eb', 1, '90237410-35d5-11ea-aa63-00163e2e65eb', '90237410-35d5-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-13 15:23:15.077', '2020-01-13 15:24:11.674');
INSERT INTO `ACT_HI_VARINST` VALUES ('90237413-35d5-11ea-aa63-00163e2e65eb', 1, '90237410-35d5-11ea-aa63-00163e2e65eb', '90237410-35d5-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-13 15:23:15.077', '2020-01-13 15:24:11.665');
INSERT INTO `ACT_HI_VARINST` VALUES ('90237414-35d5-11ea-aa63-00163e2e65eb', 1, '90237410-35d5-11ea-aa63-00163e2e65eb', '90237410-35d5-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-13 15:23:15.077', '2020-01-13 15:24:11.665');
INSERT INTO `ACT_HI_VARINST` VALUES ('917bdb42-3678-11ea-aa63-00163e2e65eb', 1, '979e7168-3368-11ea-aa63-00163e2e65eb', '979e7168-3368-11ea-aa63-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL, '2020-01-14 10:50:05.301', '2020-01-17 15:37:10.312');
INSERT INTO `ACT_HI_VARINST` VALUES ('94f086c1-312a-11ea-9129-00163e2e65eb', 0, '3414bed4-3046-11ea-9129-00163e2e65eb', '3414bed4-3046-11ea-9129-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL, '2020-01-07 16:49:14.540', '2020-01-07 16:49:14.540');
INSERT INTO `ACT_HI_VARINST` VALUES ('979e7169-3368-11ea-aa63-00163e2e65eb', 2, '979e7168-3368-11ea-aa63-00163e2e65eb', '979e7168-3368-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-10 13:18:10.158', '2020-01-17 15:37:10.316');
INSERT INTO `ACT_HI_VARINST` VALUES ('979e716b-3368-11ea-aa63-00163e2e65eb', 3, '979e7168-3368-11ea-aa63-00163e2e65eb', '979e7168-3368-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-10 13:18:10.158', '2020-01-17 15:37:10.308');
INSERT INTO `ACT_HI_VARINST` VALUES ('979e716c-3368-11ea-aa63-00163e2e65eb', 3, '979e7168-3368-11ea-aa63-00163e2e65eb', '979e7168-3368-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-10 13:18:10.158', '2020-01-17 15:37:10.308');
INSERT INTO `ACT_HI_VARINST` VALUES ('a0af89dc-2df4-11ea-aa23-c85b7643dd9e', 1, 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-03 14:45:27.935', '2020-01-17 15:37:25.718');
INSERT INTO `ACT_HI_VARINST` VALUES ('a0af89de-2df4-11ea-aa23-c85b7643dd9e', 1, 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-03 14:45:27.935', '2020-01-17 15:37:25.708');
INSERT INTO `ACT_HI_VARINST` VALUES ('a0af89df-2df4-11ea-aa23-c85b7643dd9e', 1, 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-03 14:45:27.935', '2020-01-17 15:37:25.708');
INSERT INTO `ACT_HI_VARINST` VALUES ('a2087a30-3051-11ea-9129-00163e2e65eb', 0, '783cda15-2e95-11ea-9129-00163e2e65eb', '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL, '2020-01-06 14:56:15.717', '2020-01-06 14:56:15.717');
INSERT INTO `ACT_HI_VARINST` VALUES ('aa3c297e-3349-11ea-aa63-00163e2e65eb', 1, 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-10 09:36:46.992', '2020-01-10 15:25:55.453');
INSERT INTO `ACT_HI_VARINST` VALUES ('aa3c2980-3349-11ea-aa63-00163e2e65eb', 1, 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-10 09:36:46.992', '2020-01-10 15:25:55.446');
INSERT INTO `ACT_HI_VARINST` VALUES ('aa3c2981-3349-11ea-aa63-00163e2e65eb', 1, 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-10 09:36:46.992', '2020-01-10 15:25:55.446');
INSERT INTO `ACT_HI_VARINST` VALUES ('b1783e70-31c4-11ea-9129-00163e2e65eb', 0, '143f81f3-3029-11ea-9129-00163e2e65eb', '143f81f3-3029-11ea-9129-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-08 11:12:24.902', '2020-01-08 11:12:24.902');
INSERT INTO `ACT_HI_VARINST` VALUES ('b1dedc36-35d5-11ea-aa63-00163e2e65eb', 0, '90237410-35d5-11ea-aa63-00163e2e65eb', '90237410-35d5-11ea-aa63-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-13 15:24:11.670', '2020-01-13 15:24:11.670');
INSERT INTO `ACT_HI_VARINST` VALUES ('b750e73b-35d5-11ea-aa63-00163e2e65eb', 0, '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-13 15:24:20.806', '2020-01-13 15:24:20.806');
INSERT INTO `ACT_HI_VARINST` VALUES ('c5247235-3156-11ea-9129-00163e2e65eb', 0, 'c5247234-3156-11ea-9129-00163e2e65eb', 'c5247234-3156-11ea-9129-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-07 22:05:33.267', '2020-01-07 22:05:33.267');
INSERT INTO `ACT_HI_VARINST` VALUES ('c5247237-3156-11ea-9129-00163e2e65eb', 0, 'c5247234-3156-11ea-9129-00163e2e65eb', 'c5247234-3156-11ea-9129-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-07 22:05:33.267', '2020-01-07 22:05:33.267');
INSERT INTO `ACT_HI_VARINST` VALUES ('c5247238-3156-11ea-9129-00163e2e65eb', 0, 'c5247234-3156-11ea-9129-00163e2e65eb', 'c5247234-3156-11ea-9129-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL, '2020-01-07 22:05:33.267', '2020-01-07 22:05:33.267');
INSERT INTO `ACT_HI_VARINST` VALUES ('c81fe554-36b7-11ea-aa63-00163e2e65eb', 0, 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-14 18:22:35.267', '2020-01-14 18:22:35.267');
INSERT INTO `ACT_HI_VARINST` VALUES ('c81fe556-36b7-11ea-aa63-00163e2e65eb', 0, 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-14 18:22:35.267', '2020-01-14 18:22:35.267');
INSERT INTO `ACT_HI_VARINST` VALUES ('c81fe557-36b7-11ea-aa63-00163e2e65eb', 0, 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsiteuser', NULL, '2020-01-14 18:22:35.267', '2020-01-14 18:22:35.267');
INSERT INTO `ACT_HI_VARINST` VALUES ('c87ffc93-32bd-11ea-aa63-00163e2e65eb', 0, 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-09 16:55:28.226', '2020-01-09 16:55:28.226');
INSERT INTO `ACT_HI_VARINST` VALUES ('c87ffc95-32bd-11ea-aa63-00163e2e65eb', 0, 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-09 16:55:28.226', '2020-01-09 16:55:28.226');
INSERT INTO `ACT_HI_VARINST` VALUES ('c87ffc96-32bd-11ea-aa63-00163e2e65eb', 0, 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL, '2020-01-09 16:55:28.226', '2020-01-09 16:55:28.226');
INSERT INTO `ACT_HI_VARINST` VALUES ('ca5a65b0-4d6c-11ea-9b4e-00163e2e65eb', 0, 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-02-12 15:53:43.495', '2020-02-12 15:53:43.495');
INSERT INTO `ACT_HI_VARINST` VALUES ('ca5a65b2-4d6c-11ea-9b4e-00163e2e65eb', 0, 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-02-12 15:53:43.495', '2020-02-12 15:53:43.495');
INSERT INTO `ACT_HI_VARINST` VALUES ('ca5a65b3-4d6c-11ea-9b4e-00163e2e65eb', 0, 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL, '2020-02-12 15:53:43.496', '2020-02-12 15:53:43.496');
INSERT INTO `ACT_HI_VARINST` VALUES ('caea8d19-36ae-11ea-aa63-00163e2e65eb', 1, 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-14 17:18:14.480', '2020-01-15 11:00:03.631');
INSERT INTO `ACT_HI_VARINST` VALUES ('caea8d1b-36ae-11ea-aa63-00163e2e65eb', 1, 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-14 17:18:14.481', '2020-01-15 11:00:03.620');
INSERT INTO `ACT_HI_VARINST` VALUES ('caeab42c-36ae-11ea-aa63-00163e2e65eb', 1, 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-14 17:18:14.481', '2020-01-15 11:00:03.620');
INSERT INTO `ACT_HI_VARINST` VALUES ('d698a7fc-4d81-11ea-9b4e-00163e2e65eb', 1, 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-02-12 18:24:23.468', '2020-02-12 18:27:45.969');
INSERT INTO `ACT_HI_VARINST` VALUES ('d698cf0e-4d81-11ea-9b4e-00163e2e65eb', 1, 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-02-12 18:24:23.468', '2020-02-12 18:27:45.962');
INSERT INTO `ACT_HI_VARINST` VALUES ('d698cf0f-4d81-11ea-9b4e-00163e2e65eb', 1, 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-02-12 18:24:23.468', '2020-02-12 18:27:45.963');
INSERT INTO `ACT_HI_VARINST` VALUES ('dafeba39-367d-11ea-aa63-00163e2e65eb', 1, 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'dafeba38-367d-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-14 11:27:56.116', '2020-02-11 14:29:46.263');
INSERT INTO `ACT_HI_VARINST` VALUES ('dafeba3b-367d-11ea-aa63-00163e2e65eb', 1, 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'dafeba38-367d-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-14 11:27:56.117', '2020-02-11 14:29:46.251');
INSERT INTO `ACT_HI_VARINST` VALUES ('dafee14c-367d-11ea-aa63-00163e2e65eb', 1, 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'dafeba38-367d-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-14 11:27:56.117', '2020-02-11 14:29:46.252');
INSERT INTO `ACT_HI_VARINST` VALUES ('de63e403-4bdf-11ea-9b4e-00163e2e65eb', 0, 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL, '2020-02-10 16:32:26.912', '2020-02-10 16:32:26.912');
INSERT INTO `ACT_HI_VARINST` VALUES ('de648045-4bdf-11ea-9b4e-00163e2e65eb', 0, 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-02-10 16:32:26.914', '2020-02-10 16:32:26.914');
INSERT INTO `ACT_HI_VARINST` VALUES ('de648046-4bdf-11ea-9b4e-00163e2e65eb', 0, 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL, '2020-02-10 16:32:26.914', '2020-02-10 16:32:26.914');
INSERT INTO `ACT_HI_VARINST` VALUES ('e08f1ee0-4c97-11ea-9b4e-00163e2e65eb', 0, 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL, '2020-02-11 14:29:37.947', '2020-02-11 14:29:37.947');
INSERT INTO `ACT_HI_VARINST` VALUES ('e08f45f2-4c97-11ea-9b4e-00163e2e65eb', 0, 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-02-11 14:29:37.948', '2020-02-11 14:29:37.948');
INSERT INTO `ACT_HI_VARINST` VALUES ('e08f45f3-4c97-11ea-9b4e-00163e2e65eb', 0, 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL, '2020-02-11 14:29:37.948', '2020-02-11 14:29:37.948');
INSERT INTO `ACT_HI_VARINST` VALUES ('e318c61d-305b-11ea-9129-00163e2e65eb', 1, 'e318c61c-305b-11ea-9129-00163e2e65eb', 'e318c61c-305b-11ea-9129-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL, '2020-01-06 16:09:39.844', '2020-01-06 16:10:38.492');
INSERT INTO `ACT_HI_VARINST` VALUES ('e318ed2f-305b-11ea-9129-00163e2e65eb', 1, 'e318c61c-305b-11ea-9129-00163e2e65eb', 'e318c61c-305b-11ea-9129-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-06 16:09:39.844', '2020-01-06 16:10:38.482');
INSERT INTO `ACT_HI_VARINST` VALUES ('e318ed30-305b-11ea-9129-00163e2e65eb', 1, 'e318c61c-305b-11ea-9129-00163e2e65eb', 'e318c61c-305b-11ea-9129-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-06 16:09:39.844', '2020-01-06 16:10:38.482');
INSERT INTO `ACT_HI_VARINST` VALUES ('e582f93b-4c97-11ea-9b4e-00163e2e65eb', 0, 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'dafeba38-367d-11ea-aa63-00163e2e65eb', NULL, 'leave_leader_audit', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL, '2020-02-11 14:29:46.256', '2020-02-11 14:29:46.256');
INSERT INTO `ACT_HI_VARINST` VALUES ('ec0a0dfd-38fb-11ea-aa63-00163e2e65eb', 1, 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-17 15:35:23.625', '2020-01-17 15:38:18.698');
INSERT INTO `ACT_HI_VARINST` VALUES ('ec0a0dff-38fb-11ea-aa63-00163e2e65eb', 1, 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-17 15:35:23.625', '2020-01-17 15:38:18.690');
INSERT INTO `ACT_HI_VARINST` VALUES ('ec0a0e00-38fb-11ea-aa63-00163e2e65eb', 1, 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-17 15:35:23.625', '2020-01-17 15:38:18.690');
INSERT INTO `ACT_HI_VARINST` VALUES ('f5d4437f-3733-11ea-aa63-00163e2e65eb', 1, 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-15 09:11:29.541', '2020-01-17 15:36:39.229');
INSERT INTO `ACT_HI_VARINST` VALUES ('f5d44381-3733-11ea-aa63-00163e2e65eb', 1, 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-15 09:11:29.541', '2020-01-17 15:36:39.221');
INSERT INTO `ACT_HI_VARINST` VALUES ('f5d44382-3733-11ea-aa63-00163e2e65eb', 1, 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'end', NULL, '2020-01-15 09:11:29.541', '2020-01-17 15:36:39.221');
INSERT INTO `ACT_HI_VARINST` VALUES ('f9e02282-38f5-11ea-aa63-00163e2e65eb', 0, 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-17 14:52:49.858', '2020-01-17 14:52:49.858');
INSERT INTO `ACT_HI_VARINST` VALUES ('f9e02284-38f5-11ea-aa63-00163e2e65eb', 0, 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-17 14:52:49.858', '2020-01-17 14:52:49.858');
INSERT INTO `ACT_HI_VARINST` VALUES ('f9e02285-38f5-11ea-aa63-00163e2e65eb', 0, 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL, '2020-01-17 14:52:49.858', '2020-01-17 14:52:49.858');
INSERT INTO `ACT_HI_VARINST` VALUES ('fc715bee-36a6-11ea-aa63-00163e2e65eb', 0, 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', NULL, 'applyUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL, '2020-01-14 16:22:21.598', '2020-01-14 16:22:21.598');
INSERT INTO `ACT_HI_VARINST` VALUES ('fc718300-36a6-11ea-aa63-00163e2e65eb', 0, 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', NULL, 'auditPass', 'boolean', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-01-14 16:22:21.599', '2020-01-14 16:22:21.599');
INSERT INTO `ACT_HI_VARINST` VALUES ('fc718301-36a6-11ea-aa63-00163e2e65eb', 0, 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', NULL, 'leaderName', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL, '2020-01-14 16:22:21.599', '2020-01-14 16:22:21.599');

-- ----------------------------
-- Table structure for ACT_ID_BYTEARRAY
-- ----------------------------
DROP TABLE IF EXISTS `ACT_ID_BYTEARRAY`;
CREATE TABLE `ACT_ID_BYTEARRAY`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTES_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_ID_GROUP
-- ----------------------------
DROP TABLE IF EXISTS `ACT_ID_GROUP`;
CREATE TABLE `ACT_ID_GROUP`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_ID_INFO
-- ----------------------------
DROP TABLE IF EXISTS `ACT_ID_INFO`;
CREATE TABLE `ACT_ID_INFO`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `USER_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `VALUE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PASSWORD_` longblob NULL,
  `PARENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_ID_MEMBERSHIP
-- ----------------------------
DROP TABLE IF EXISTS `ACT_ID_MEMBERSHIP`;
CREATE TABLE `ACT_ID_MEMBERSHIP`  (
  `USER_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`USER_ID_`, `GROUP_ID_`) USING BTREE,
  INDEX `ACT_FK_MEMB_GROUP`(`GROUP_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_MEMB_GROUP` FOREIGN KEY (`GROUP_ID_`) REFERENCES `ACT_ID_GROUP` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MEMB_USER` FOREIGN KEY (`USER_ID_`) REFERENCES `ACT_ID_USER` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_ID_PRIV
-- ----------------------------
DROP TABLE IF EXISTS `ACT_ID_PRIV`;
CREATE TABLE `ACT_ID_PRIV`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_UNIQ_PRIV_NAME`(`NAME_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_ID_PRIV_MAPPING
-- ----------------------------
DROP TABLE IF EXISTS `ACT_ID_PRIV_MAPPING`;
CREATE TABLE `ACT_ID_PRIV_MAPPING`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PRIV_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_FK_PRIV_MAPPING`(`PRIV_ID_`) USING BTREE,
  INDEX `ACT_IDX_PRIV_USER`(`USER_ID_`) USING BTREE,
  INDEX `ACT_IDX_PRIV_GROUP`(`GROUP_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_PRIV_MAPPING` FOREIGN KEY (`PRIV_ID_`) REFERENCES `ACT_ID_PRIV` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_ID_PROPERTY
-- ----------------------------
DROP TABLE IF EXISTS `ACT_ID_PROPERTY`;
CREATE TABLE `ACT_ID_PROPERTY`  (
  `NAME_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VALUE_` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`NAME_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_ID_PROPERTY
-- ----------------------------
INSERT INTO `ACT_ID_PROPERTY` VALUES ('schema.version', '6.4.0.0', 1);

-- ----------------------------
-- Table structure for ACT_ID_TOKEN
-- ----------------------------
DROP TABLE IF EXISTS `ACT_ID_TOKEN`;
CREATE TABLE `ACT_ID_TOKEN`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TOKEN_VALUE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TOKEN_DATE_` timestamp(3) NULL DEFAULT NULL,
  `IP_ADDRESS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_AGENT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TOKEN_DATA_` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_ID_USER
-- ----------------------------
DROP TABLE IF EXISTS `ACT_ID_USER`;
CREATE TABLE `ACT_ID_USER`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `FIRST_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LAST_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DISPLAY_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EMAIL_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PWD_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PICTURE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_PROCDEF_INFO
-- ----------------------------
DROP TABLE IF EXISTS `ACT_PROCDEF_INFO`;
CREATE TABLE `ACT_PROCDEF_INFO`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `INFO_JSON_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_UNIQ_INFO_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  INDEX `ACT_IDX_INFO_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  INDEX `ACT_FK_INFO_JSON_BA`(`INFO_JSON_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_INFO_JSON_BA` FOREIGN KEY (`INFO_JSON_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_INFO_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `ACT_RE_PROCDEF` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_RE_DEPLOYMENT
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RE_DEPLOYMENT`;
CREATE TABLE `ACT_RE_DEPLOYMENT`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `DEPLOY_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DERIVED_FROM_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_FROM_ROOT_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ENGINE_VERSION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_RE_DEPLOYMENT
-- ----------------------------
INSERT INTO `ACT_RE_DEPLOYMENT` VALUES ('4eafd3fe-2df3-11ea-aa23-c85b7643dd9e', '请假流程', NULL, 'jsite_leave', '', '2020-01-03 14:36:00.867', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for ACT_RE_MODEL
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RE_MODEL`;
CREATE TABLE `ACT_RE_MODEL`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LAST_UPDATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `VERSION_` int(11) NULL DEFAULT NULL,
  `META_INFO_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EDITOR_SOURCE_VALUE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EDITOR_SOURCE_EXTRA_VALUE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_FK_MODEL_SOURCE`(`EDITOR_SOURCE_VALUE_ID_`) USING BTREE,
  INDEX `ACT_FK_MODEL_SOURCE_EXTRA`(`EDITOR_SOURCE_EXTRA_VALUE_ID_`) USING BTREE,
  INDEX `ACT_FK_MODEL_DEPLOYMENT`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_MODEL_DEPLOYMENT` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `ACT_RE_DEPLOYMENT` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MODEL_SOURCE` FOREIGN KEY (`EDITOR_SOURCE_VALUE_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MODEL_SOURCE_EXTRA` FOREIGN KEY (`EDITOR_SOURCE_EXTRA_VALUE_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_RE_PROCDEF
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RE_PROCDEF`;
CREATE TABLE `ACT_RE_PROCDEF`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION_` int(11) NOT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DGRM_RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HAS_START_FORM_KEY_` tinyint(4) NULL DEFAULT NULL,
  `HAS_GRAPHICAL_NOTATION_` tinyint(4) NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `ENGINE_VERSION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_FROM_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_FROM_ROOT_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_VERSION_` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_UNIQ_PROCDEF`(`KEY_`, `VERSION_`, `DERIVED_VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_RE_PROCDEF
-- ----------------------------
INSERT INTO `ACT_RE_PROCDEF` VALUES ('jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', 2, 'leave', '请假流程', 'jsite_leave', 1, '4eafd3fe-2df3-11ea-aa23-c85b7643dd9e', '请假流程.bpmn20.xml', '请假流程.jsite_leave.png', '请假流程', 0, 1, 1, '', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for ACT_RU_DEADLETTER_JOB
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_DEADLETTER_JOB`;
CREATE TABLE `ACT_RU_DEADLETTER_JOB`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_DEADLETTER_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_DEADLETTER_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_DJOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_DJOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_DJOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_DEADLETTER_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_DEADLETTER_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_DEADLETTER_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `ACT_RE_PROCDEF` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_RU_EVENT_SUBSCR
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_EVENT_SUBSCR`;
CREATE TABLE `ACT_RU_EVENT_SUBSCR`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `EVENT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EVENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACTIVITY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CONFIGURATION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATED_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_EVENT_SUBSCR_CONFIG_`(`CONFIGURATION_`) USING BTREE,
  INDEX `ACT_FK_EVENT_EXEC`(`EXECUTION_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_EVENT_EXEC` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_RU_EXECUTION
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_EXECUTION`;
CREATE TABLE `ACT_RU_EXECUTION`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUPER_EXEC_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ROOT_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `IS_ACTIVE_` tinyint(4) NULL DEFAULT NULL,
  `IS_CONCURRENT_` tinyint(4) NULL DEFAULT NULL,
  `IS_SCOPE_` tinyint(4) NULL DEFAULT NULL,
  `IS_EVENT_SCOPE_` tinyint(4) NULL DEFAULT NULL,
  `IS_MI_ROOT_` tinyint(4) NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) NULL DEFAULT NULL,
  `CACHED_ENT_STATE_` int(11) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `IS_COUNT_ENABLED_` tinyint(4) NULL DEFAULT NULL,
  `EVT_SUBSCR_COUNT_` int(11) NULL DEFAULT NULL,
  `TASK_COUNT_` int(11) NULL DEFAULT NULL,
  `JOB_COUNT_` int(11) NULL DEFAULT NULL,
  `TIMER_JOB_COUNT_` int(11) NULL DEFAULT NULL,
  `SUSP_JOB_COUNT_` int(11) NULL DEFAULT NULL,
  `DEADLETTER_JOB_COUNT_` int(11) NULL DEFAULT NULL,
  `VAR_COUNT_` int(11) NULL DEFAULT NULL,
  `ID_LINK_COUNT_` int(11) NULL DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_EXEC_BUSKEY`(`BUSINESS_KEY_`) USING BTREE,
  INDEX `ACT_IDC_EXEC_ROOT`(`ROOT_PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_FK_EXE_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_FK_EXE_PARENT`(`PARENT_ID_`) USING BTREE,
  INDEX `ACT_FK_EXE_SUPER`(`SUPER_EXEC_`) USING BTREE,
  INDEX `ACT_FK_EXE_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_EXE_PARENT` FOREIGN KEY (`PARENT_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXE_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `ACT_RE_PROCDEF` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXE_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ACT_FK_EXE_SUPER` FOREIGN KEY (`SUPER_EXEC_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_RU_EXECUTION
-- ----------------------------
INSERT INTO `ACT_RU_EXECUTION` VALUES ('006ae1ba-3838-11ea-aa63-00163e2e65eb', 1, '006ae1ba-3838-11ea-aa63-00163e2e65eb', '627901d2c68e469189224b66471c9676', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '006ae1ba-3838-11ea-aa63-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-01-16 16:12:56.455', 'jsite', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('006ae1bf-3838-11ea-aa63-00163e2e65eb', 1, '006ae1ba-3838-11ea-aa63-00163e2e65eb', NULL, '006ae1ba-3838-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '006ae1ba-3838-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-01-16 16:12:56.455', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('1034b9e1-31f9-11ea-aa63-00163e2e65eb', 1, '1034b9e1-31f9-11ea-aa63-00163e2e65eb', '190b101831714360a8385c1d46a84df3', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '1034b9e1-31f9-11ea-aa63-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-01-08 17:27:17.673', 'jsite', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('1035cb56-31f9-11ea-aa63-00163e2e65eb', 1, '1034b9e1-31f9-11ea-aa63-00163e2e65eb', NULL, '1034b9e1-31f9-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '1034b9e1-31f9-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-01-08 17:27:17.680', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('56631326-383f-11ea-aa63-00163e2e65eb', 1, '56631326-383f-11ea-aa63-00163e2e65eb', '699ddfb824f04d96b76c42354dc2964f', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '56631326-383f-11ea-aa63-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-01-16 17:05:27.165', 'jsite', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('5663132b-383f-11ea-aa63-00163e2e65eb', 1, '56631326-383f-11ea-aa63-00163e2e65eb', NULL, '56631326-383f-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '56631326-383f-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-01-16 17:05:27.165', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('5da62737-32b9-11ea-aa63-00163e2e65eb', 1, '5da62737-32b9-11ea-aa63-00163e2e65eb', '346fbb981ff44d0eb231846927c61715', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '5da62737-32b9-11ea-aa63-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-01-09 16:23:50.973', 'jsite', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('5da64e4c-32b9-11ea-aa63-00163e2e65eb', 1, '5da62737-32b9-11ea-aa63-00163e2e65eb', NULL, '5da62737-32b9-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '5da62737-32b9-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-01-09 16:23:50.974', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('66bf898f-3827-11ea-aa63-00163e2e65eb', 1, '66bf898f-3827-11ea-aa63-00163e2e65eb', '2d498acf528f46b395a00fcdf93b5c54', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '66bf898f-3827-11ea-aa63-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-01-16 14:14:06.693', 'jsite', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('66bf8994-3827-11ea-aa63-00163e2e65eb', 1, '66bf898f-3827-11ea-aa63-00163e2e65eb', NULL, '66bf898f-3827-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '66bf898f-3827-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-01-16 14:14:06.693', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('783cda15-2e95-11ea-9129-00163e2e65eb', 2, '783cda15-2e95-11ea-9129-00163e2e65eb', '6dac5249254843e2a25d59de898686b9', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-01-04 09:56:49.048', 'jsite', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('78400e6a-2e95-11ea-9129-00163e2e65eb', 3, '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, '783cda15-2e95-11ea-9129-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '783cda15-2e95-11ea-9129-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-01-04 09:56:49.069', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('8290f1e3-3387-11ea-aa63-00163e2e65eb', 1, '8290f1e3-3387-11ea-aa63-00163e2e65eb', '797aa0d32095404f9c5796fd3bb69545', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '8290f1e3-3387-11ea-aa63-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-01-10 16:59:29.236', 'jsite', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('829118f8-3387-11ea-aa63-00163e2e65eb', 1, '8290f1e3-3387-11ea-aa63-00163e2e65eb', NULL, '8290f1e3-3387-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '8290f1e3-3387-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-01-10 16:59:29.237', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('8db9ba53-3379-11ea-aa63-00163e2e65eb', 1, '8db9ba53-3379-11ea-aa63-00163e2e65eb', '1a801d4529984f36998d9ac51851c353', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '8db9ba53-3379-11ea-aa63-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-01-10 15:19:35.004', 'dept', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('8db9e168-3379-11ea-aa63-00163e2e65eb', 1, '8db9ba53-3379-11ea-aa63-00163e2e65eb', NULL, '8db9ba53-3379-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, '8db9ba53-3379-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-01-10 15:19:35.005', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('c5247234-3156-11ea-9129-00163e2e65eb', 1, 'c5247234-3156-11ea-9129-00163e2e65eb', '815a7d8c847946a59dd4dcef7427fd2f', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'c5247234-3156-11ea-9129-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-01-07 22:05:33.267', 'jsite', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('c5247239-3156-11ea-9129-00163e2e65eb', 1, 'c5247234-3156-11ea-9129-00163e2e65eb', NULL, 'c5247234-3156-11ea-9129-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'c5247234-3156-11ea-9129-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-01-07 22:05:33.267', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('c81fe553-36b7-11ea-aa63-00163e2e65eb', 1, 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'b393f21fd12648bb9c7cf79241e30b40', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'c81fe553-36b7-11ea-aa63-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-01-14 18:22:35.267', 'jsite', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('c81fe558-36b7-11ea-aa63-00163e2e65eb', 1, 'c81fe553-36b7-11ea-aa63-00163e2e65eb', NULL, 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-01-14 18:22:35.267', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('c87ffc92-32bd-11ea-aa63-00163e2e65eb', 1, 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'dc37012c6fc649c3a82868288c8b5bd4', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-01-09 16:55:28.226', 'jsite', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('c87ffc97-32bd-11ea-aa63-00163e2e65eb', 1, 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', NULL, 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-01-09 16:55:28.226', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 1, 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', '6bcbe514462a45ff8778f010595169ef', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-02-12 15:53:43.495', 'jsite', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('ca5a8cc4-4d6c-11ea-9b4e-00163e2e65eb', 1, 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', NULL, 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-02-12 15:53:43.496', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 1, 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', '42d4054b569745f181d2d369703e5998', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-02-10 16:32:26.908', 'dept', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('de648047-4bdf-11ea-9b4e-00163e2e65eb', 1, 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', NULL, 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-02-10 16:32:26.914', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 1, 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', '1ca7c4347a9349d1aeec97e9bf828091', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-02-11 14:29:37.947', 'dept', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('e08f45f4-4c97-11ea-9b4e-00163e2e65eb', 1, 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', NULL, 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-02-11 14:29:37.948', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('f9e02281-38f5-11ea-aa63-00163e2e65eb', 1, 'f9e02281-38f5-11ea-aa63-00163e2e65eb', '38bc9bbdfcf14f09a9b8f3fdc50409ff', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'f9e02281-38f5-11ea-aa63-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-01-17 14:52:49.858', 'jsite', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('f9e02286-38f5-11ea-aa63-00163e2e65eb', 1, 'f9e02281-38f5-11ea-aa63-00163e2e65eb', NULL, 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-01-17 14:52:49.858', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('fc715bed-36a6-11ea-aa63-00163e2e65eb', 1, 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'eed2fae2591d46b3917cd0980e6c76a6', NULL, 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'fc715bed-36a6-11ea-aa63-00163e2e65eb', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startevent1', '2020-01-14 16:22:21.598', 'jsite', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `ACT_RU_EXECUTION` VALUES ('fc718302-36a6-11ea-aa63-00163e2e65eb', 1, 'fc715bed-36a6-11ea-aa63-00163e2e65eb', NULL, 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'leave_leader_audit', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2020-01-14 16:22:21.599', NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL);

-- ----------------------------
-- Table structure for ACT_RU_HISTORY_JOB
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_HISTORY_JOB`;
CREATE TABLE `ACT_RU_HISTORY_JOB`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(11) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ADV_HANDLER_CFG_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_RU_IDENTITYLINK
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_IDENTITYLINK`;
CREATE TABLE `ACT_RU_IDENTITYLINK`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_USER`(`USER_ID_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_GROUP`(`GROUP_ID_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_ATHRZ_PROCEDEF`(`PROC_DEF_ID_`) USING BTREE,
  INDEX `ACT_FK_TSKASS_TASK`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_FK_IDL_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_ATHRZ_PROCEDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `ACT_RE_PROCDEF` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_IDL_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TSKASS_TASK` FOREIGN KEY (`TASK_ID_`) REFERENCES `ACT_RU_TASK` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_RU_IDENTITYLINK
-- ----------------------------
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('006ae1bc-3838-11ea-aa63-00163e2e65eb', 1, NULL, 'starter', 'jsite', NULL, '006ae1ba-3838-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('006b08d4-3838-11ea-aa63-00163e2e65eb', 1, NULL, 'participant', 'jsitehr', NULL, '006ae1ba-3838-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('10357d33-31f9-11ea-aa63-00163e2e65eb', 1, NULL, 'starter', 'jsite', NULL, '1034b9e1-31f9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('103c5b0b-31f9-11ea-aa63-00163e2e65eb', 1, NULL, 'participant', 'jsitehr', NULL, '1034b9e1-31f9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('56631328-383f-11ea-aa63-00163e2e65eb', 1, NULL, 'starter', 'jsite', NULL, '56631326-383f-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('56633a40-383f-11ea-aa63-00163e2e65eb', 1, NULL, 'participant', 'jsitehr', NULL, '56631326-383f-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('5da64e49-32b9-11ea-aa63-00163e2e65eb', 1, NULL, 'starter', 'jsite', NULL, '5da62737-32b9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('5da67561-32b9-11ea-aa63-00163e2e65eb', 1, NULL, 'participant', 'jsiteuser', NULL, '5da62737-32b9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('66bf8991-3827-11ea-aa63-00163e2e65eb', 1, NULL, 'starter', 'jsite', NULL, '66bf898f-3827-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('66bfb0a9-3827-11ea-aa63-00163e2e65eb', 1, NULL, 'participant', 'jsitehr', NULL, '66bf898f-3827-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('783fe757-2e95-11ea-9129-00163e2e65eb', 1, NULL, 'starter', 'jsite', NULL, '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('784df11f-2e95-11ea-9129-00163e2e65eb', 1, NULL, 'participant', 'dept', NULL, '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('8290f1e5-3387-11ea-aa63-00163e2e65eb', 1, NULL, 'starter', 'jsite', NULL, '8290f1e3-3387-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('829118fd-3387-11ea-aa63-00163e2e65eb', 1, NULL, 'participant', 'jsitehr', NULL, '8290f1e3-3387-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('8db9e165-3379-11ea-aa63-00163e2e65eb', 1, NULL, 'starter', 'dept', NULL, '8db9ba53-3379-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('8db9e16d-3379-11ea-aa63-00163e2e65eb', 1, NULL, 'participant', 'jsitehr', NULL, '8db9ba53-3379-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('a20b8775-3051-11ea-9129-00163e2e65eb', 1, NULL, 'participant', 'jsite', NULL, '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('c5247236-3156-11ea-9129-00163e2e65eb', 1, NULL, 'starter', 'jsite', NULL, 'c5247234-3156-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('c524994e-3156-11ea-9129-00163e2e65eb', 1, NULL, 'participant', 'dept', NULL, 'c5247234-3156-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('c81fe555-36b7-11ea-aa63-00163e2e65eb', 1, NULL, 'starter', 'jsite', NULL, 'c81fe553-36b7-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('c8200c6d-36b7-11ea-aa63-00163e2e65eb', 1, NULL, 'participant', 'jsiteuser', NULL, 'c81fe553-36b7-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('c87ffc94-32bd-11ea-aa63-00163e2e65eb', 1, NULL, 'starter', 'jsite', NULL, 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('c88023ac-32bd-11ea-aa63-00163e2e65eb', 1, NULL, 'participant', 'jsitehr', NULL, 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('ca5a65b1-4d6c-11ea-9b4e-00163e2e65eb', 1, NULL, 'starter', 'jsite', NULL, 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('ca5a8cc9-4d6c-11ea-9b4e-00163e2e65eb', 1, NULL, 'participant', 'jsitehr', NULL, 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('de645934-4bdf-11ea-9b4e-00163e2e65eb', 1, NULL, 'starter', 'dept', NULL, 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('de6e444c-4bdf-11ea-9b4e-00163e2e65eb', 1, NULL, 'participant', 'jsitehr', NULL, 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('e08f1ee1-4c97-11ea-9b4e-00163e2e65eb', 1, NULL, 'starter', 'dept', NULL, 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('e08f6d09-4c97-11ea-9b4e-00163e2e65eb', 1, NULL, 'participant', 'dept', NULL, 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('f9e02283-38f5-11ea-aa63-00163e2e65eb', 1, NULL, 'starter', 'jsite', NULL, 'f9e02281-38f5-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('f9e0499b-38f5-11ea-aa63-00163e2e65eb', 1, NULL, 'participant', 'jsitehr', NULL, 'f9e02281-38f5-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('fc715bef-36a6-11ea-aa63-00163e2e65eb', 1, NULL, 'starter', 'jsite', NULL, 'fc715bed-36a6-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);
INSERT INTO `ACT_RU_IDENTITYLINK` VALUES ('fc718307-36a6-11ea-aa63-00163e2e65eb', 1, NULL, 'participant', 'jsitehr', NULL, 'fc715bed-36a6-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for ACT_RU_JOB
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_JOB`;
CREATE TABLE `ACT_RU_JOB`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(11) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_JOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_JOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_JOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `ACT_RE_PROCDEF` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_RU_SUSPENDED_JOB
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_SUSPENDED_JOB`;
CREATE TABLE `ACT_RU_SUSPENDED_JOB`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(11) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_SUSPENDED_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_SUSPENDED_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_SJOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_SJOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_SJOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_SUSPENDED_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_SUSPENDED_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_SUSPENDED_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `ACT_RE_PROCDEF` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_RU_TASK
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_TASK`;
CREATE TABLE `ACT_RU_TASK`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DELEGATION_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PRIORITY_` int(11) NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DUE_DATE_` datetime(3) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `FORM_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CLAIM_TIME_` datetime(3) NULL DEFAULT NULL,
  `IS_COUNT_ENABLED_` tinyint(4) NULL DEFAULT NULL,
  `VAR_COUNT_` int(11) NULL DEFAULT NULL,
  `ID_LINK_COUNT_` int(11) NULL DEFAULT NULL,
  `SUB_TASK_COUNT_` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_TASK_CREATE`(`CREATE_TIME_`) USING BTREE,
  INDEX `ACT_IDX_TASK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TASK_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TASK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_TASK_EXE`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_TASK_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_FK_TASK_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_TASK_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TASK_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `ACT_RE_PROCDEF` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TASK_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_RU_TASK
-- ----------------------------
INSERT INTO `ACT_RU_TASK` VALUES ('006ae1c2-3838-11ea-aa63-00163e2e65eb', 1, '006ae1bf-3838-11ea-aa63-00163e2e65eb', '006ae1ba-3838-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'jsitehr', NULL, 50, '2020-01-16 16:12:56.455', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('103bbec9-31f9-11ea-aa63-00163e2e65eb', 1, '1035cb56-31f9-11ea-aa63-00163e2e65eb', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'jsitehr', NULL, 50, '2020-01-08 17:27:17.691', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('5663132e-383f-11ea-aa63-00163e2e65eb', 1, '5663132b-383f-11ea-aa63-00163e2e65eb', '56631326-383f-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'jsitehr', NULL, 50, '2020-01-16 17:05:27.165', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('5da64e4f-32b9-11ea-aa63-00163e2e65eb', 1, '5da64e4c-32b9-11ea-aa63-00163e2e65eb', '5da62737-32b9-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'jsiteuser', NULL, 50, '2020-01-09 16:23:50.974', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('66bfb0a7-3827-11ea-aa63-00163e2e65eb', 1, '66bf8994-3827-11ea-aa63-00163e2e65eb', '66bf898f-3827-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'jsitehr', NULL, 50, '2020-01-16 14:14:06.694', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('829118fb-3387-11ea-aa63-00163e2e65eb', 1, '829118f8-3387-11ea-aa63-00163e2e65eb', '8290f1e3-3387-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'jsitehr', NULL, 50, '2020-01-10 16:59:29.237', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('8db9e16b-3379-11ea-aa63-00163e2e65eb', 1, '8db9e168-3379-11ea-aa63-00163e2e65eb', '8db9ba53-3379-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'jsitehr', NULL, 50, '2020-01-10 15:19:35.005', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('c0a8e6a9-3051-11ea-9129-00163e2e65eb', 1, '78400e6a-2e95-11ea-9129-00163e2e65eb', '783cda15-2e95-11ea-9129-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'dept', NULL, 50, '2020-01-06 14:57:07.100', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('c524994c-3156-11ea-9129-00163e2e65eb', 1, 'c5247239-3156-11ea-9129-00163e2e65eb', 'c5247234-3156-11ea-9129-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'dept', NULL, 50, '2020-01-07 22:05:33.268', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('c8200c6b-36b7-11ea-aa63-00163e2e65eb', 1, 'c81fe558-36b7-11ea-aa63-00163e2e65eb', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'jsiteuser', NULL, 50, '2020-01-14 18:22:35.268', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('c88023aa-32bd-11ea-aa63-00163e2e65eb', 1, 'c87ffc97-32bd-11ea-aa63-00163e2e65eb', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'jsitehr', NULL, 50, '2020-01-09 16:55:28.227', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('ca5a8cc7-4d6c-11ea-9b4e-00163e2e65eb', 1, 'ca5a8cc4-4d6c-11ea-9b4e-00163e2e65eb', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'jsitehr', NULL, 50, '2020-02-12 15:53:43.496', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('de6d80fa-4bdf-11ea-9b4e-00163e2e65eb', 1, 'de648047-4bdf-11ea-9b4e-00163e2e65eb', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'jsitehr', NULL, 50, '2020-02-10 16:32:26.927', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('e08f45f7-4c97-11ea-9b4e-00163e2e65eb', 1, 'e08f45f4-4c97-11ea-9b4e-00163e2e65eb', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'dept', NULL, 50, '2020-02-11 14:29:37.948', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('f9e04999-38f5-11ea-aa63-00163e2e65eb', 1, 'f9e02286-38f5-11ea-aa63-00163e2e65eb', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'jsitehr', NULL, 50, '2020-01-17 14:52:49.858', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);
INSERT INTO `ACT_RU_TASK` VALUES ('fc718305-36a6-11ea-aa63-00163e2e65eb', 1, 'fc718302-36a6-11ea-aa63-00163e2e65eb', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'jsite_leave:1:4f0c4be1-2df3-11ea-aa23-c85b7643dd9e', NULL, NULL, NULL, NULL, NULL, '领导审批', NULL, NULL, 'leave_leader_audit', NULL, 'jsitehr', NULL, 50, '2020-01-14 16:22:21.599', NULL, NULL, 1, '', 'leave_leader_audit', NULL, 1, 0, 0, 0);

-- ----------------------------
-- Table structure for ACT_RU_TIMER_JOB
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_TIMER_JOB`;
CREATE TABLE `ACT_RU_TIMER_JOB`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(11) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_TIMER_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_TIMER_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_TJOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TJOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TJOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_TIMER_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_TIMER_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_TIMER_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_TIMER_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `ACT_RE_PROCDEF` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ACT_RU_VARIABLE
-- ----------------------------
DROP TABLE IF EXISTS `ACT_RU_VARIABLE`;
CREATE TABLE `ACT_RU_VARIABLE`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DOUBLE_` double NULL DEFAULT NULL,
  `LONG_` bigint(20) NULL DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_VAR_SCOPE_ID_TYPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_RU_VAR_SUB_ID_TYPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_VAR_BYTEARRAY`(`BYTEARRAY_ID_`) USING BTREE,
  INDEX `ACT_IDX_VARIABLE_TASK_ID`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_FK_VAR_EXE`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_VAR_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_VAR_BYTEARRAY` FOREIGN KEY (`BYTEARRAY_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_VAR_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_VAR_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACT_RU_VARIABLE
-- ----------------------------
INSERT INTO `ACT_RU_VARIABLE` VALUES ('006ae1bb-3838-11ea-aa63-00163e2e65eb', 1, 'string', 'applyUserId', '006ae1ba-3838-11ea-aa63-00163e2e65eb', '006ae1ba-3838-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('006ae1bd-3838-11ea-aa63-00163e2e65eb', 1, 'boolean', 'auditPass', '006ae1ba-3838-11ea-aa63-00163e2e65eb', '006ae1ba-3838-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('006ae1be-3838-11ea-aa63-00163e2e65eb', 1, 'string', 'leaderName', '006ae1ba-3838-11ea-aa63-00163e2e65eb', '006ae1ba-3838-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('10350802-31f9-11ea-aa63-00163e2e65eb', 1, 'string', 'applyUserId', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('1035a444-31f9-11ea-aa63-00163e2e65eb', 1, 'boolean', 'auditPass', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('1035a445-31f9-11ea-aa63-00163e2e65eb', 1, 'string', 'leaderName', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('56631327-383f-11ea-aa63-00163e2e65eb', 1, 'string', 'applyUserId', '56631326-383f-11ea-aa63-00163e2e65eb', '56631326-383f-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('56631329-383f-11ea-aa63-00163e2e65eb', 1, 'boolean', 'auditPass', '56631326-383f-11ea-aa63-00163e2e65eb', '56631326-383f-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('5663132a-383f-11ea-aa63-00163e2e65eb', 1, 'string', 'leaderName', '56631326-383f-11ea-aa63-00163e2e65eb', '56631326-383f-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('5da62738-32b9-11ea-aa63-00163e2e65eb', 1, 'string', 'applyUserId', '5da62737-32b9-11ea-aa63-00163e2e65eb', '5da62737-32b9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('5da64e4a-32b9-11ea-aa63-00163e2e65eb', 1, 'boolean', 'auditPass', '5da62737-32b9-11ea-aa63-00163e2e65eb', '5da62737-32b9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('5da64e4b-32b9-11ea-aa63-00163e2e65eb', 1, 'string', 'leaderName', '5da62737-32b9-11ea-aa63-00163e2e65eb', '5da62737-32b9-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsiteuser', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('66bf8990-3827-11ea-aa63-00163e2e65eb', 1, 'string', 'applyUserId', '66bf898f-3827-11ea-aa63-00163e2e65eb', '66bf898f-3827-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('66bf8992-3827-11ea-aa63-00163e2e65eb', 1, 'boolean', 'auditPass', '66bf898f-3827-11ea-aa63-00163e2e65eb', '66bf898f-3827-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('66bf8993-3827-11ea-aa63-00163e2e65eb', 1, 'string', 'leaderName', '66bf898f-3827-11ea-aa63-00163e2e65eb', '66bf898f-3827-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('783d0126-2e95-11ea-9129-00163e2e65eb', 1, 'string', 'applyUserId', '783cda15-2e95-11ea-9129-00163e2e65eb', '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('78400e68-2e95-11ea-9129-00163e2e65eb', 3, 'boolean', 'auditPass', '783cda15-2e95-11ea-9129-00163e2e65eb', '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('78400e69-2e95-11ea-9129-00163e2e65eb', 3, 'string', 'leaderName', '783cda15-2e95-11ea-9129-00163e2e65eb', '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('8290f1e4-3387-11ea-aa63-00163e2e65eb', 1, 'string', 'applyUserId', '8290f1e3-3387-11ea-aa63-00163e2e65eb', '8290f1e3-3387-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('829118f6-3387-11ea-aa63-00163e2e65eb', 1, 'boolean', 'auditPass', '8290f1e3-3387-11ea-aa63-00163e2e65eb', '8290f1e3-3387-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('829118f7-3387-11ea-aa63-00163e2e65eb', 1, 'string', 'leaderName', '8290f1e3-3387-11ea-aa63-00163e2e65eb', '8290f1e3-3387-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('8db9ba54-3379-11ea-aa63-00163e2e65eb', 1, 'string', 'applyUserId', '8db9ba53-3379-11ea-aa63-00163e2e65eb', '8db9ba53-3379-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('8db9e166-3379-11ea-aa63-00163e2e65eb', 1, 'boolean', 'auditPass', '8db9ba53-3379-11ea-aa63-00163e2e65eb', '8db9ba53-3379-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('8db9e167-3379-11ea-aa63-00163e2e65eb', 1, 'string', 'leaderName', '8db9ba53-3379-11ea-aa63-00163e2e65eb', '8db9ba53-3379-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('a2087a30-3051-11ea-9129-00163e2e65eb', 1, 'string', 'leave_leader_audit', '783cda15-2e95-11ea-9129-00163e2e65eb', '783cda15-2e95-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('c5247235-3156-11ea-9129-00163e2e65eb', 1, 'string', 'applyUserId', 'c5247234-3156-11ea-9129-00163e2e65eb', 'c5247234-3156-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('c5247237-3156-11ea-9129-00163e2e65eb', 1, 'boolean', 'auditPass', 'c5247234-3156-11ea-9129-00163e2e65eb', 'c5247234-3156-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('c5247238-3156-11ea-9129-00163e2e65eb', 1, 'string', 'leaderName', 'c5247234-3156-11ea-9129-00163e2e65eb', 'c5247234-3156-11ea-9129-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('c81fe554-36b7-11ea-aa63-00163e2e65eb', 1, 'string', 'applyUserId', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('c81fe556-36b7-11ea-aa63-00163e2e65eb', 1, 'boolean', 'auditPass', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('c81fe557-36b7-11ea-aa63-00163e2e65eb', 1, 'string', 'leaderName', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsiteuser', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('c87ffc93-32bd-11ea-aa63-00163e2e65eb', 1, 'string', 'applyUserId', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('c87ffc95-32bd-11ea-aa63-00163e2e65eb', 1, 'boolean', 'auditPass', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('c87ffc96-32bd-11ea-aa63-00163e2e65eb', 1, 'string', 'leaderName', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('ca5a65b0-4d6c-11ea-9b4e-00163e2e65eb', 1, 'string', 'applyUserId', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('ca5a65b2-4d6c-11ea-9b4e-00163e2e65eb', 1, 'boolean', 'auditPass', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('ca5a65b3-4d6c-11ea-9b4e-00163e2e65eb', 1, 'string', 'leaderName', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('de63e403-4bdf-11ea-9b4e-00163e2e65eb', 1, 'string', 'applyUserId', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('de648045-4bdf-11ea-9b4e-00163e2e65eb', 1, 'boolean', 'auditPass', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('de648046-4bdf-11ea-9b4e-00163e2e65eb', 1, 'string', 'leaderName', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('e08f1ee0-4c97-11ea-9b4e-00163e2e65eb', 1, 'string', 'applyUserId', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('e08f45f2-4c97-11ea-9b4e-00163e2e65eb', 1, 'boolean', 'auditPass', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('e08f45f3-4c97-11ea-9b4e-00163e2e65eb', 1, 'string', 'leaderName', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dept', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('f9e02282-38f5-11ea-aa63-00163e2e65eb', 1, 'string', 'applyUserId', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('f9e02284-38f5-11ea-aa63-00163e2e65eb', 1, 'boolean', 'auditPass', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('f9e02285-38f5-11ea-aa63-00163e2e65eb', 1, 'string', 'leaderName', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('fc715bee-36a6-11ea-aa63-00163e2e65eb', 1, 'string', 'applyUserId', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsite', NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('fc718300-36a6-11ea-aa63-00163e2e65eb', 1, 'boolean', 'auditPass', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `ACT_RU_VARIABLE` VALUES ('fc718301-36a6-11ea-aa63-00163e2e65eb', 1, 'string', 'leaderName', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jsitehr', NULL);

-- ----------------------------
-- Table structure for QRTZ_BLOB_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_BLOB_TRIGGERS`;
CREATE TABLE `QRTZ_BLOB_TRIGGERS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `QRTZ_BLOB_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for QRTZ_CALENDARS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CALENDARS`;
CREATE TABLE `QRTZ_CALENDARS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for QRTZ_CRON_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CRON_TRIGGERS`;
CREATE TABLE `QRTZ_CRON_TRIGGERS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CRON_EXPRESSION` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `QRTZ_CRON_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_CRON_TRIGGERS
-- ----------------------------
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('quartzScheduler', 'com.jsite.common.quartz.job.HelloJob', '1', '0/30 * * * * ?', 'Asia/Shanghai');
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('quartzScheduler', 'com.jsite.common.quartz.job.NewJob', '2', '0/55 * * * * ?', 'Asia/Shanghai');
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('quartzScheduler', 'com.jsite.common.quartz.job.TestJob', '3', '0/40 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for QRTZ_FIRED_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_FIRED_TRIGGERS`;
CREATE TABLE `QRTZ_FIRED_TRIGGERS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ENTRY_ID` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for QRTZ_JOB_DETAILS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_JOB_DETAILS`;
CREATE TABLE `QRTZ_JOB_DETAILS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_JOB_DETAILS
-- ----------------------------
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('quartzScheduler', 'com.jsite.common.quartz.job.HelloJob', '1', NULL, 'com.jsite.common.quartz.job.HelloJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787000737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F40000000000010770800000010000000007800);
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('quartzScheduler', 'com.jsite.common.quartz.job.NewJob', '2', NULL, 'com.jsite.common.quartz.job.NewJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787000737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F40000000000010770800000010000000007800);
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('quartzScheduler', 'com.jsite.common.quartz.job.TestJob', '3', NULL, 'com.jsite.common.quartz.job.TestJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787000737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F40000000000010770800000010000000007800);

-- ----------------------------
-- Table structure for QRTZ_LOCKS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_LOCKS`;
CREATE TABLE `QRTZ_LOCKS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LOCK_NAME` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_LOCKS
-- ----------------------------
INSERT INTO `QRTZ_LOCKS` VALUES ('quartzScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_PAUSED_TRIGGER_GRPS`;
CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for QRTZ_SCHEDULER_STATE
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SCHEDULER_STATE`;
CREATE TABLE `QRTZ_SCHEDULER_STATE`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for QRTZ_SIMPLE_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPLE_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPLE_TRIGGERS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `QRTZ_SIMPLE_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for QRTZ_SIMPROP_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPROP_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPROP_TRIGGERS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INT_PROP_1` int(11) NULL DEFAULT NULL,
  `INT_PROP_2` int(11) NULL DEFAULT NULL,
  `LONG_PROP_1` bigint(20) NULL DEFAULT NULL,
  `LONG_PROP_2` bigint(20) NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `QRTZ_SIMPROP_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for QRTZ_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_TRIGGERS`;
CREATE TABLE `QRTZ_TRIGGERS`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PRIORITY` int(11) NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  CONSTRAINT `QRTZ_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `QRTZ_JOB_DETAILS` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_TRIGGERS
-- ----------------------------
INSERT INTO `QRTZ_TRIGGERS` VALUES ('quartzScheduler', 'com.jsite.common.quartz.job.HelloJob', '1', 'com.jsite.common.quartz.job.HelloJob', '1', NULL, 1547209260000, 1547209230000, 5, 'PAUSED', 'CRON', 1547207629000, 0, NULL, 0, '');
INSERT INTO `QRTZ_TRIGGERS` VALUES ('quartzScheduler', 'com.jsite.common.quartz.job.NewJob', '2', 'com.jsite.common.quartz.job.NewJob', '2', NULL, 1547207815000, 1547207793711, 5, 'PAUSED', 'CRON', 1547092828000, 0, NULL, 0, '');
INSERT INTO `QRTZ_TRIGGERS` VALUES ('quartzScheduler', 'com.jsite.common.quartz.job.TestJob', '3', 'com.jsite.common.quartz.job.TestJob', '3', NULL, 1547197680000, 1547197660000, 5, 'PAUSED', 'CRON', 1547092566000, 0, NULL, 0, '');

-- ----------------------------
-- Table structure for flow_form
-- ----------------------------
DROP TABLE IF EXISTS `flow_form`;
CREATE TABLE `flow_form`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '���',
  `model_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '����ģ��Id',
  `model_key` varchar(400) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '����ģ��key',
  `model_version` int(11) NULL DEFAULT NULL COMMENT '����ģ�Ͱ汾',
  `form_content` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT '�������',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '������',
  `create_date` datetime(0) NOT NULL COMMENT '����ʱ��',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '������',
  `update_date` datetime(0) NOT NULL COMMENT '����ʱ��',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '��ע��Ϣ',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT 'ɾ�����',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '���̱����' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flow_form
-- ----------------------------
INSERT INTO `flow_form` VALUES ('b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, '&lt;table style=&quot;width:1000px&quot;&gt;&lt;tbody&gt;&lt;tr class=&quot;firstRow&quot;&gt;&lt;td valign=&quot;middle&quot; align=&quot;right&quot; colspan=&quot;1&quot; rowspan=&quot;1&quot; style=&quot;word-break: break-all;&quot; width=&quot;145&quot;&gt;&lt;br/&gt;&lt;/td&gt;&lt;td valign=&quot;middle&quot; align=&quot;left&quot; colspan=&quot;1&quot; rowspan=&quot;1&quot; style=&quot;word-break: break-all;&quot; width=&quot;127&quot;&gt;&lt;input name=&quot;task_asignee&quot; id=&quot;task_asignee&quot; type=&quot;text&quot; title=&quot;下一步处理人&quot; value=&quot;&quot; readonly=&quot;readonly&quot; leipiplugins=&quot;task_assignee&quot; onclick=&quot;openSelectPersonDialog(&amp;#39;task_asignee&amp;#39;)&quot; orgalign=&quot;left&quot; orgwidth=&quot;150&quot; orgtype=&quot;text&quot; orgclick=&quot;openSelectPersonDialog&quot; style=&quot;text-align: left; width: 150px;&quot; placeholder=&quot;下一步处理人&quot; orgfontsize=&quot;&quot; orgheight=&quot;&quot;/&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td width=&quot;145&quot; valign=&quot;middle&quot; align=&quot;right&quot; style=&quot;word-break: break-all;&quot;&gt;请假类型：&lt;/td&gt;&lt;td width=&quot;651&quot; valign=&quot;top&quot; style=&quot;word-break: break-all;&quot;&gt;&lt;span leipiplugins=&quot;select&quot;&gt;&lt;select title=&quot;leave_type&quot; name=&quot;leave_type&quot; leipiplugins=&quot;select&quot; orgwidth=&quot;150&quot; orgtype=&quot;oa_leave_type&quot; style=&quot;width: 150px;&quot;&gt;&lt;option value=&quot;1&quot;&gt;公休&lt;/option&gt;&lt;option value=&quot;2&quot;&gt;病假&lt;/option&gt;&lt;option value=&quot;3&quot;&gt;事假&lt;/option&gt;&lt;option value=&quot;4&quot;&gt;调休&lt;/option&gt;&lt;option value=&quot;5&quot;&gt;婚假&lt;/option&gt;&lt;/select&gt;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td valign=&quot;middle&quot; align=&quot;right&quot; colspan=&quot;1&quot; rowspan=&quot;1&quot; style=&quot;word-break: break-all;&quot;&gt;请假时间：&lt;/td&gt;&lt;td valign=&quot;middle&quot; align=&quot;left&quot; colspan=&quot;1&quot; rowspan=&quot;1&quot; style=&quot;word-break: break-all;&quot;&gt;&lt;input name=&quot;leave_tm_begin&quot; type=&quot;text&quot; readonly=&quot;readonly&quot; leipiplugins=&quot;textdate&quot; onclick=&quot;WdatePicker({dateFmt:&amp;amp;#39;yyyy-MM-dd&amp;amp;#39;,isShowClear:false});&quot; orgrequired=&quot;0&quot; orgformat=&quot;yyyy-MM-dd&quot; orgalign=&quot;left&quot; orgwidth=&quot;150&quot; orgtype=&quot;text&quot; style=&quot;text-align: left; width: 150px;&quot; title=&quot;&quot; orgfontsize=&quot;&quot; orgheight=&quot;&quot;/&gt;&amp;nbsp;-&amp;nbsp;&lt;input name=&quot;leave_tm_end&quot; type=&quot;text&quot; readonly=&quot;readonly&quot; leipiplugins=&quot;textdate&quot; onclick=&quot;WdatePicker({dateFmt:&amp;amp;#39;yyyy-MM-dd&amp;amp;#39;,isShowClear:false});&quot; orgrequired=&quot;0&quot; orgformat=&quot;yyyy-MM-dd&quot; orgalign=&quot;left&quot; orgwidth=&quot;150&quot; orgtype=&quot;text&quot; style=&quot;text-align: left; width: 150px;&quot; title=&quot;&quot; orgfontsize=&quot;&quot; orgheight=&quot;&quot;/&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td valign=&quot;middle&quot; align=&quot;right&quot; colspan=&quot;1&quot; rowspan=&quot;1&quot; style=&quot;word-break: break-all;&quot;&gt;请假天数：&lt;/td&gt;&lt;td valign=&quot;middle&quot; align=&quot;left&quot; colspan=&quot;1&quot; rowspan=&quot;1&quot;&gt;&lt;input name=&quot;leave_days&quot; type=&quot;text&quot; title=&quot;leave_days&quot; value=&quot;&quot; leipiplugins=&quot;text&quot; orghide=&quot;0&quot; orgalign=&quot;left&quot; orgwidth=&quot;150&quot; orgtype=&quot;float&quot; style=&quot;text-align: left; width: 150px;&quot;/&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td width=&quot;145&quot; valign=&quot;middle&quot; align=&quot;right&quot; style=&quot;word-break: break-all;&quot;&gt;请假原因：&lt;br/&gt;&lt;/td&gt;&lt;td width=&quot;651&quot; valign=&quot;top&quot; style=&quot;word-break: break-all;&quot;&gt;&lt;textarea title=&quot;leave_reason&quot; name=&quot;leave_reason&quot; leipiplugins=&quot;textarea&quot; value=&quot;&quot; orgrich=&quot;0&quot; orgfontsize=&quot;&quot; orgwidth=&quot;700&quot; orgheight=&quot;100&quot; style=&quot;width: 700px; height: 100px;&quot;&gt;&lt;/textarea&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td valign=&quot;middle&quot; align=&quot;right&quot; colspan=&quot;1&quot; rowspan=&quot;1&quot; width=&quot;145&quot; style=&quot;word-break: break-all;&quot;&gt;审批意见：&lt;/td&gt;&lt;td valign=&quot;middle&quot; align=&quot;left&quot; colspan=&quot;1&quot; rowspan=&quot;1&quot; width=&quot;127&quot;&gt;&lt;textarea title=&quot;comment&quot; name=&quot;comment&quot; leipiplugins=&quot;textarea&quot; value=&quot;&quot; orgrich=&quot;0&quot; orgfontsize=&quot;&quot; orgwidth=&quot;700&quot; orgheight=&quot;100&quot; style=&quot;width: 700px; height: 100px;&quot;&gt;&lt;/textarea&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/tbody&gt;&lt;/table&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;', '1', '2020-01-02 09:34:11', '1', '2020-01-03 09:58:10', NULL, '0');

-- ----------------------------
-- Table structure for flow_form_authorize
-- ----------------------------
DROP TABLE IF EXISTS `flow_form_authorize`;
CREATE TABLE `flow_form_authorize`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '���',
  `model_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '����ģ��Id',
  `model_key` varchar(400) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '����ģ��key',
  `model_version` int(11) NULL DEFAULT NULL COMMENT '����ģ�Ͱ汾',
  `field_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '�ֶ�����',
  `resource_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '���̽ڵ�ID',
  `resource_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '���̽ڵ�����',
  `is_read` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '�Ƿ�ֻ��',
  `is_require` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `is_hide` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '�Ƿ�����',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '������',
  `create_date` datetime(0) NOT NULL COMMENT '����ʱ��',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '������',
  `update_date` datetime(0) NOT NULL COMMENT '����ʱ��',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '��ע��Ϣ',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT 'ɾ�����',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '���̱���ֶ�Ȩ�ޱ�' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flow_form_authorize
-- ----------------------------
INSERT INTO `flow_form_authorize` VALUES ('01aabe59d6134bf6b0e77cb937d8cf12', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_tm_end', 'leave_leader_audit', '领导审批', '1', '0', '0', '1', '2020-01-03 09:57:26', '1', '2020-01-03 09:57:26', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('0bf76f510ab84b96a8ff8fe0b28a488f', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'task_asignee', 'startevent1', '开始节点', '0', '1', '0', '1', '2020-01-03 09:53:59', '1', '2020-01-03 14:35:36', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('12624a2d5e5845a0aa4b5277f4007643', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_tm_end', 'startevent1', '开始节点', '0', '1', '0', '1', '2020-01-03 09:57:25', '1', '2020-01-03 09:57:25', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('2452d697f1974562b22844d7df4a3242', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_days', 'startevent1', '开始节点', '0', '1', '0', '1', '2020-01-03 09:57:32', '1', '2020-01-03 09:57:32', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('4af4c84194d54dbabc46cde2b9f787d5', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_tm_end', 'adjustApply', '调整申请', '0', '1', '0', '1', '2020-01-03 09:57:26', '1', '2020-01-03 09:57:26', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('57ff74602cac466dabae7f30ad052a38', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_tm_begin', 'startevent1', '开始节点', '0', '1', '0', '1', '2020-01-03 09:57:18', '1', '2020-01-03 09:57:18', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('5a38780eeca841e5bf5dcef1195263be', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_reason', 'adjustApply', '调整申请', '0', '1', '0', '1', '2020-01-03 09:57:41', '1', '2020-01-03 09:57:41', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('5c68226f5d334958b0a31383a879b678', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_days', 'leave_leader_audit', '领导审批', '1', '0', '0', '1', '2020-01-03 09:57:33', '1', '2020-01-03 09:57:33', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('680329b87fde4065b529b2dd548cb082', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_reason', 'startevent1', '开始节点', '0', '1', '0', '1', '2020-01-03 09:57:40', '1', '2020-01-03 09:57:40', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('81c2a5cfc66e4686993f3ef37c6806b2', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_type', 'leave_leader_audit', '领导审批', '1', '0', '0', '1', '2020-01-03 09:56:57', '1', '2020-01-03 09:56:57', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('8cffceae34ff4f548f9e97b5c2a9e8bf', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_days', 'adjustApply', '调整申请', '0', '1', '0', '1', '2020-01-03 09:57:33', '1', '2020-01-03 09:57:33', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('958d8870edc64fd8a8527a1516e92b8e', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'task_asignee', 'adjustApply', '调整申请', '0', '0', '1', '1', '2020-01-03 09:53:59', '1', '2020-01-03 14:35:36', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('a881fffcb01d4bc480927762dc398c0d', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'comment', 'startevent1', '开始节点', '1', '0', '0', '1', '2020-01-03 09:57:50', '1', '2020-01-03 09:57:50', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('bdb42e6cb4b047adb1a25cebbc7f9d17', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'task_asignee', 'leave_leader_audit', '领导审批', '0', '0', '1', '1', '2020-01-03 09:53:59', '1', '2020-01-03 14:35:36', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('d3127d6f2d4a4bfd83760e002bd4d259', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_type', 'adjustApply', '调整申请', '0', '0', '0', '1', '2020-01-03 09:56:57', '1', '2020-01-03 09:56:57', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('d76ac10b6de941b19ab143624a882da5', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'comment', 'adjustApply', '调整申请', '1', '0', '0', '1', '2020-01-03 09:57:52', '1', '2020-01-03 09:57:52', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('e5003452a17b4006976df4d4a0c65bca', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_type', 'startevent1', '开始节点', '0', '0', '0', '1', '2020-01-03 09:56:55', '1', '2020-01-03 09:56:55', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('ee805e150ad44645ae89990c3f6f9565', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_tm_begin', 'leave_leader_audit', '领导审批', '1', '0', '0', '1', '2020-01-03 09:57:19', '1', '2020-01-03 09:57:19', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('f26f62544f6b4085bf823b6fc2a87e36', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_reason', 'leave_leader_audit', '领导审批', '1', '0', '0', '1', '2020-01-03 09:57:41', '1', '2020-01-03 09:57:41', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('f60c91a950004a8a8f6c66a409bbf18b', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'leave_tm_begin', 'adjustApply', '调整申请', '0', '1', '0', '1', '2020-01-03 09:57:19', '1', '2020-01-03 09:57:19', NULL, '0');
INSERT INTO `flow_form_authorize` VALUES ('f62080fac3444178a0684f0ab41e5dd7', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'comment', 'leave_leader_audit', '领导审批', '0', '0', '0', '1', '2020-01-03 09:57:50', '1', '2020-01-03 09:57:50', NULL, '0');

-- ----------------------------
-- Table structure for flow_form_business
-- ----------------------------
DROP TABLE IF EXISTS `flow_form_business`;
CREATE TABLE `flow_form_business`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '���',
  `form_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '������',
  `proc_ins_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '����ʵ��ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '������',
  `create_date` datetime(0) NOT NULL COMMENT '����ʱ��',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '������',
  `update_date` datetime(0) NOT NULL COMMENT '����ʱ��',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '��ע��Ϣ',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT 'ɾ�����',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '���̱��ҵ���ܱ�' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flow_form_business
-- ----------------------------
INSERT INTO `flow_form_business` VALUES ('0150e3a778874b609905ba4f3c05aa14', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', '1', '2020-01-15 09:11:30', '1', '2020-01-15 09:11:30', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('0bcda03d39e843088a4c3d6fab230dab', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '979e7168-3368-11ea-aa63-00163e2e65eb', '1', '2020-01-10 13:18:10', '1', '2020-01-10 13:18:10', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('16957f196e45430982cf67f4c07586d2', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3414bed4-3046-11ea-9129-00163e2e65eb', '1', '2020-01-06 13:34:27', '1', '2020-01-06 13:34:27', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('190b101831714360a8385c1d46a84df3', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', '1', '2020-01-08 17:27:18', '1', '2020-01-08 17:27:18', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('1a801d4529984f36998d9ac51851c353', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8db9ba53-3379-11ea-aa63-00163e2e65eb', '2', '2020-01-10 15:19:35', '2', '2020-01-10 15:19:35', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('1ca7c4347a9349d1aeec97e9bf828091', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', '2', '2020-02-11 14:29:38', '2', '2020-02-11 14:29:38', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('20d017eef151490e86afa85894aa23c6', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '264a7336-3678-11ea-aa63-00163e2e65eb', '1', '2020-01-14 10:47:05', '1', '2020-01-14 10:47:05', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('2d498acf528f46b395a00fcdf93b5c54', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '66bf898f-3827-11ea-aa63-00163e2e65eb', '1', '2020-01-16 14:14:07', '1', '2020-01-16 14:14:07', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('346fbb981ff44d0eb231846927c61715', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '5da62737-32b9-11ea-aa63-00163e2e65eb', '1', '2020-01-09 16:23:51', '1', '2020-01-09 16:23:51', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('352b30c8d8e14b3592244daeca9a4a11', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'dafeba38-367d-11ea-aa63-00163e2e65eb', '1', '2020-01-14 11:27:56', '1', '2020-01-14 11:27:56', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('38bc9bbdfcf14f09a9b8f3fdc50409ff', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', '1', '2020-01-17 14:52:50', '1', '2020-01-17 14:52:50', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('42d4054b569745f181d2d369703e5998', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', '2', '2020-02-10 16:32:27', '2', '2020-02-10 16:32:27', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('54546dadb08e4dcbbc3c90ba739fcd91', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', '1', '2020-01-05 15:22:00', '1', '2020-01-05 15:22:00', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('5ea728e17994433aac9a509ac9944ed0', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', '1', '2020-01-08 22:27:48', '1', '2020-01-08 22:27:48', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('621b45fe147b4e1291d5acd272ba2f2e', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', '1', '2020-01-10 09:36:47', '1', '2020-01-10 09:36:47', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('627901d2c68e469189224b66471c9676', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '006ae1ba-3838-11ea-aa63-00163e2e65eb', '1', '2020-01-16 16:12:56', '1', '2020-01-16 16:12:56', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('699ddfb824f04d96b76c42354dc2964f', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '56631326-383f-11ea-aa63-00163e2e65eb', '1', '2020-01-16 17:05:27', '1', '2020-01-16 17:05:27', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('6bcbe514462a45ff8778f010595169ef', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', '1', '2020-02-12 15:53:43', '1', '2020-02-12 15:53:43', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('6dac5249254843e2a25d59de898686b9', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '783cda15-2e95-11ea-9129-00163e2e65eb', '1', '2020-01-04 09:56:49', '1', '2020-01-04 09:56:49', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('75294dca525741a480260d8820a46953', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', '1', '2020-01-03 14:45:28', '1', '2020-01-03 14:45:28', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('785f9f3ba20d443091c3969187f9b9a0', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', '1', '2020-01-14 17:18:14', '1', '2020-01-14 17:18:14', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('797aa0d32095404f9c5796fd3bb69545', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8290f1e3-3387-11ea-aa63-00163e2e65eb', '1', '2020-01-10 16:59:29', '1', '2020-01-10 16:59:29', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('815a7d8c847946a59dd4dcef7427fd2f', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c5247234-3156-11ea-9129-00163e2e65eb', '1', '2020-01-07 22:05:33', '1', '2020-01-07 22:05:33', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('81eb2f7819294ad6a5e065c42b1d0236', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '143f81f3-3029-11ea-9129-00163e2e65eb', '1', '2020-01-06 10:05:58', '1', '2020-01-06 10:05:58', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('9ea6742d060140979d2fa6309d8d8fe1', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', '1', '2020-01-17 15:35:24', '1', '2020-01-17 15:35:24', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('a9e2126951284868bceb43e81caca869', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3d309e0e-3029-11ea-9129-00163e2e65eb', '1', '2020-01-06 10:07:07', '1', '2020-01-06 10:07:07', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('b393f21fd12648bb9c7cf79241e30b40', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', '1', '2020-01-14 18:22:35', '1', '2020-01-14 18:22:35', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('bd03bf2b04664b51884db6b5a85b8abb', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '79cea582-2df3-11ea-aa23-c85b7643dd9e', '1', '2020-01-03 14:37:12', '1', '2020-01-03 14:37:12', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('cd536006afaf40eca5a7fc498e3aee63', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'bdc1bb3a-2dd7-11ea-a0ba-c85b7643dd9e', '1', '2020-01-03 11:18:41', '1', '2020-01-03 11:18:41', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('d40bdbaf8d9e426885097b21f390293a', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', '1', '2020-02-12 18:24:23', '1', '2020-02-12 18:24:23', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('dc37012c6fc649c3a82868288c8b5bd4', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', '1', '2020-01-09 16:55:28', '1', '2020-01-09 16:55:28', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('eed2fae2591d46b3917cd0980e6c76a6', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', '1', '2020-01-14 16:22:22', '1', '2020-01-14 16:22:22', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('ef2d9d44ab6e4c89a379f5b8c71d1f73', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '90237410-35d5-11ea-aa63-00163e2e65eb', '1', '2020-01-13 15:23:15', '1', '2020-01-13 15:23:15', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('f89ed621413846589bd433fee03826c5', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', '1', '2020-01-13 14:54:04', '1', '2020-01-13 14:54:04', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('fadf2b5870ab49d5bb527b90af2fa246', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd27dd303-2dea-11ea-b12b-c85b7643dd9e', '1', '2020-01-03 13:35:16', '1', '2020-01-03 13:35:16', NULL, '0');
INSERT INTO `flow_form_business` VALUES ('fc8a20a02b1d4b15a8bab8e2ee0efde9', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e318c61c-305b-11ea-9129-00163e2e65eb', '2', '2020-01-06 16:09:40', '2', '2020-01-06 16:09:40', NULL, '0');

-- ----------------------------
-- Table structure for flow_form_data
-- ----------------------------
DROP TABLE IF EXISTS `flow_form_data`;
CREATE TABLE `flow_form_data`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '���',
  `form_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '������',
  `proc_ins_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '����ʵ��ID',
  `field_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '�ֶ�����',
  `field_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '�ֶ�ֵ',
  `field_large_value` varchar(3000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '���ı��ֶ�ֵ',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '������',
  `create_date` datetime(0) NOT NULL COMMENT '����ʱ��',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '������',
  `update_date` datetime(0) NOT NULL COMMENT '����ʱ��',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '��ע��Ϣ',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT 'ɾ�����',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `form_id`(`form_id`) USING BTREE,
  CONSTRAINT `flow_form_data_ibfk_1` FOREIGN KEY (`form_id`) REFERENCES `flow_form` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '���̱���ֶ����ݱ�' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flow_form_data
-- ----------------------------
INSERT INTO `flow_form_data` VALUES ('03334f7d474c45d5bdca7d3703c037cc', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3414bed4-3046-11ea-9129-00163e2e65eb', 'leave_tm_begin', '2020-01-06', NULL, '1', '2020-01-06 13:34:27', '3', '2020-01-07 16:49:14', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('034493c821074c158b459cd489b11921', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'bdc1bb3a-2dd7-11ea-a0ba-c85b7643dd9e', 'leave_days', '2', NULL, '1', '2020-01-03 11:18:44', '1', '2020-01-03 12:05:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0430eea0db2546d3a422c7623f6421cd', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'leave_reason', '请假测试2222222', NULL, '1', '2020-01-03 14:45:31', '2', '2020-01-17 15:37:25', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('053bdd08193b4fbfae0f8fc46a42ef2f', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '783cda15-2e95-11ea-9129-00163e2e65eb', 'leave_reason', ' cnmd  bbb ', NULL, '1', '2020-01-04 09:56:50', '1', '2020-01-06 14:57:07', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('056be1ce46fb42f99628e2ef75df48ab', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'leave_days', '3', NULL, '2', '2020-02-11 14:29:38', '2', '2020-02-11 14:29:38', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0724efab244942d6abc2ab3a89bb8ba1', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'comment', '', NULL, '2', '2020-02-11 14:29:38', '2', '2020-02-11 14:29:38', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0848e029f4864ce2aced89b422a33411', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '006ae1ba-3838-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-16 16:12:57', '1', '2020-01-16 16:12:57', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0967518e26354e3ba9c688a1c348fef6', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', 'leave_days', '3', NULL, '1', '2020-01-08 17:27:18', '1', '2020-01-08 17:27:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('098f9ab127cb42f087420dd25bda6c02', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e318c61c-305b-11ea-9129-00163e2e65eb', 'leave_tm_end', '2020-01-08', NULL, '2', '2020-01-06 16:09:40', '2', '2020-01-06 16:10:38', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0abfbb56494e4eb78940b80a9599b1d0', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '90237410-35d5-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-13', NULL, '1', '2020-01-13 15:23:15', '1', '2020-01-13 15:24:11', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0b11dd839790470f851459b95c519b7b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'leave_type', '3', NULL, '2', '2020-02-10 16:32:27', '2', '2020-02-10 16:32:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0b150ae11f314636864904f786c68e48', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '66bf898f-3827-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-16 14:14:07', '1', '2020-01-16 14:14:07', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0bd8e1ef4d3f4099b724b40471167c77', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-08 22:27:48', '1', '2020-01-13 15:24:04', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0c01300da9214c47a8260af0966e17a4', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'leave_tm_begin', '2020-02-11', NULL, '2', '2020-02-10 16:32:27', '2', '2020-02-10 16:32:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0c1cd71684e84db1a44b8aaeed69d15b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'leave_type', '2', NULL, '1', '2020-01-14 16:22:22', '1', '2020-01-14 16:22:22', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0c6f2b99545145e692f7328a6de76441', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'leave_tm_end', '2020-02-21', NULL, '1', '2020-02-12 15:53:44', '1', '2020-02-12 15:53:44', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0c9015df08654682ba5b58f8f1104acd', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '264a7336-3678-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-14', NULL, '1', '2020-01-14 10:47:06', '1', '2020-01-14 14:08:19', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0cbd0712d9214d1aad393c21abd1061f', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-15 09:11:30', '2', '2020-01-17 15:36:39', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0ede4bd02a2b49f9b67317d8118de47d', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'bdc1bb3a-2dd7-11ea-a0ba-c85b7643dd9e', 'leave_reason', '請假測試', NULL, '1', '2020-01-03 11:18:45', '1', '2020-01-03 12:05:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0f034517573a49e296cdbac8bc0d1aeb', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', 'leave_reason', '的吃撒', NULL, '1', '2020-01-08 17:27:18', '1', '2020-01-08 17:27:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0f3f895452f44b829d81685f43c13b52', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '264a7336-3678-11ea-aa63-00163e2e65eb', 'leave_days', '5', NULL, '1', '2020-01-14 10:47:06', '1', '2020-01-14 14:08:19', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0f666c5c0ad14591b52bba071e6dc988', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-21', NULL, '1', '2020-01-17 14:52:50', '1', '2020-01-17 14:52:50', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0f731bc9380045fcac82ca9be9b863e4', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'leave_tm_end', '2020-02-14', NULL, '1', '2020-02-12 18:24:24', '2', '2020-02-12 18:27:46', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('0fdec8ea08c24858b7174ba5f5b78884', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '5da62737-32b9-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-06', NULL, '1', '2020-01-09 16:23:51', '1', '2020-01-09 16:23:51', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('11da34c38126424ab6fb3baab69c41c6', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd27dd303-2dea-11ea-b12b-c85b7643dd9e', 'leave_type', '3', NULL, '1', '2020-01-03 13:35:21', '1', '2020-01-03 14:00:53', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('137082d995194431aa87eb501ef2bf96', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-02-12 18:24:24', '2', '2020-02-12 18:27:46', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('13f12a6504d04c6490005b28c4b2ddbe', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'bdc1bb3a-2dd7-11ea-a0ba-c85b7643dd9e', 'task_asignee', '', NULL, '1', '2020-01-03 11:18:44', '1', '2020-01-03 12:05:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('14ca2bfa186048b9ae1acfee2b9b47d1', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'task_asignee', '人力资源,jsitehr', NULL, '1', '2020-02-12 15:53:44', '1', '2020-02-12 15:53:44', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('195e3cc86b1a4967a2b214aec30dae81', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '79cea582-2df3-11ea-aa23-c85b7643dd9e', 'task_asignee', '', NULL, '1', '2020-01-03 14:37:15', '1', '2020-01-03 14:43:59', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('1c43a3b076144c15864412aa34e7372f', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '90237410-35d5-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-13 15:23:15', '1', '2020-01-13 15:24:11', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('1d71d31bfc03425d9ec139000cb2c340', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '783cda15-2e95-11ea-9129-00163e2e65eb', 'comment', '啊啊啊啊啊啊啊啊啊啊啊  驳回', NULL, '1', '2020-01-04 09:56:50', '1', '2020-01-06 14:57:07', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('201484b0c5484bfe8930ab0d2b703e10', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd27dd303-2dea-11ea-b12b-c85b7643dd9e', 'task_asignee', '', NULL, '1', '2020-01-03 13:35:19', '1', '2020-01-03 14:00:52', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('20385ec3d1b64517ae3b9e63a00eb295', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c5247234-3156-11ea-9129-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-07 22:05:33', '1', '2020-01-07 22:06:53', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('213f0846abd14082a96e3443b7fbb4b2', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '783cda15-2e95-11ea-9129-00163e2e65eb', 'leave_tm_end', '2020-02-01', NULL, '1', '2020-01-04 09:56:50', '1', '2020-01-06 14:57:07', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('21cd65c04c9f4bb7b5e91b36afa7949b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3d309e0e-3029-11ea-9129-00163e2e65eb', 'comment', 'ddsfsfsdfsdfs', NULL, '1', '2020-01-06 10:07:07', '1', '2020-01-07 14:48:24', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('2228d510b7844367aab7da5283d52a0c', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-14 17:18:15', '3', '2020-01-15 11:00:03', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('24a70c3bd55448e8b20e9706e4cdac32', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-10 09:36:47', '1', '2020-01-10 15:25:55', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('2591e0be308548649a22751aa01ced9b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-17 14:52:50', '1', '2020-01-17 14:52:50', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('26fac7eccd1642fa9920d6fc9ea5dd39', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-10', NULL, '1', '2020-01-09 16:55:29', '1', '2020-01-09 16:55:29', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('270877ee9bd842b790327cd72b468610', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '979e7168-3368-11ea-aa63-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-10 13:18:10', '2', '2020-01-17 15:37:10', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('27597d6b5db547d988c2f9b16d5290d3', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'leave_days', '1', NULL, '1', '2020-01-14 11:27:57', '2', '2020-02-11 14:29:46', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('286cacb7128f4a418e17b785b18fe912', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8290f1e3-3387-11ea-aa63-00163e2e65eb', 'leave_reason', 'test', NULL, '1', '2020-01-10 16:59:30', '1', '2020-01-10 16:59:30', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('29c8a32cc05c4357ab7e0fbc376c9c2b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '79cea582-2df3-11ea-aa23-c85b7643dd9e', 'leave_tm_begin', '2020-01-03', NULL, '1', '2020-01-03 14:37:15', '1', '2020-01-03 14:43:59', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('29e7c48da7d648a588f2fd33832f2842', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '006ae1ba-3838-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-24', NULL, '1', '2020-01-16 16:12:57', '1', '2020-01-16 16:12:57', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('2a2cbef9f7cf47979a7bc3f2a5b3cc31', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-14', NULL, '1', '2020-01-14 11:27:57', '2', '2020-02-11 14:29:46', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('2babfc4745c8496bb6fbcfb4045a6c07', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '90237410-35d5-11ea-aa63-00163e2e65eb', 'leave_type', '4', NULL, '1', '2020-01-13 15:23:15', '1', '2020-01-13 15:24:11', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('2e7dd092fbd043aea014187880d80908', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-17', NULL, '1', '2020-01-17 15:35:24', '3', '2020-01-17 15:38:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('2f554818c7ac4559b7da01bf6f84a720', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'leave_tm_begin', '2020-02-11', NULL, '2', '2020-02-11 14:29:38', '2', '2020-02-11 14:29:38', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('2f6ce172b873441bbf5b2cb7496cdfa2', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-17', NULL, '1', '2020-01-17 15:35:24', '3', '2020-01-17 15:38:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('30264165a5d341ff9c88f11a3e497d48', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-15', NULL, '1', '2020-01-15 09:11:30', '2', '2020-01-17 15:36:39', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('32daf1a7bec44c3c8c16febe596607db', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '264a7336-3678-11ea-aa63-00163e2e65eb', 'leave_reason', '请假原因：\n', NULL, '1', '2020-01-14 10:47:06', '1', '2020-01-14 14:08:19', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('3469a6d8abaa4820a56c0ed7e5eed917', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3414bed4-3046-11ea-9129-00163e2e65eb', 'leave_days', '3', NULL, '1', '2020-01-06 13:34:27', '3', '2020-01-07 16:49:14', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('379898d8d8e8444180097bf48d1b523d', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', 'leave_tm_begin', '2020-01-05', NULL, '1', '2020-01-05 15:22:00', '1', '2020-01-05 15:22:17', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('37c0f379d1b54f0fb4f8cbc47790bf1b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'leave_days', '1', NULL, '1', '2020-01-15 09:11:30', '2', '2020-01-17 15:36:39', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('3b3fed9bbfda42ab9976352a655168a0', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c5247234-3156-11ea-9129-00163e2e65eb', 'leave_tm_end', '2020-01-08', NULL, '1', '2020-01-07 22:05:34', '1', '2020-01-07 22:06:53', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('3cf38d3082a64ce7a7b74b587b9d3cb6', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '006ae1ba-3838-11ea-aa63-00163e2e65eb', 'task_asignee', '人力资源,jsitehr', NULL, '1', '2020-01-16 16:12:57', '1', '2020-01-16 16:12:57', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('3d4ea0696c8f48588cf2748e4bb352e1', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'leave_reason', '1111111', NULL, '1', '2020-01-17 14:52:50', '1', '2020-01-17 14:52:50', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('3d5512ed3b704799829666141ab437df', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3414bed4-3046-11ea-9129-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-06 13:34:27', '3', '2020-01-07 16:49:14', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('3e0c27cb63af483ba41f3a74ee767c52', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '5da62737-32b9-11ea-aa63-00163e2e65eb', 'leave_type', '1', NULL, '1', '2020-01-09 16:23:51', '1', '2020-01-09 16:23:51', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('3e4c8f109a994892a39fd9aa25541387', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-17', NULL, '1', '2020-01-17 14:52:50', '1', '2020-01-17 14:52:50', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('3ed904de1319402098b70e1766d07acc', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-08', NULL, '1', '2020-01-08 17:27:18', '1', '2020-01-08 17:27:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('3f5342b0c3d2402f8af42a85cfa4b9b8', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'leave_reason', '22', NULL, '1', '2020-02-12 15:53:44', '1', '2020-02-12 15:53:44', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('3fea3f8dc9284aaab78a3ef5299fcfd7', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '979e7168-3368-11ea-aa63-00163e2e65eb', 'leave_type', '4', NULL, '1', '2020-01-10 13:18:10', '2', '2020-01-17 15:37:10', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('41335b639c88458bafaed48c1486039e', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '66bf898f-3827-11ea-aa63-00163e2e65eb', 'leave_reason', '1', NULL, '1', '2020-01-16 14:14:08', '1', '2020-01-16 14:14:08', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('418046073b5a4b70b37d162d44c4a150', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'leave_days', '5', NULL, '1', '2020-01-10 09:36:47', '1', '2020-01-10 15:25:55', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('430e8e6b394c4a99b51bcb0b3c3a1b96', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'task_asignee', '人力资源,jsitehr', NULL, '1', '2020-01-14 16:22:22', '1', '2020-01-14 16:22:22', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('44c31d5974e14c76954f56998ebef6ef', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '979e7168-3368-11ea-aa63-00163e2e65eb', 'leave_reason', '这是一个测试流程', NULL, '1', '2020-01-10 13:18:11', '2', '2020-01-17 15:37:10', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('44fe9959f5404d989a876ddcac6b6512', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3d309e0e-3029-11ea-9129-00163e2e65eb', 'leave_tm_begin', '2020-01-06', NULL, '1', '2020-01-06 10:07:07', '1', '2020-01-07 14:48:24', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('46406315d47e4a75adc399e087a6324b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '979e7168-3368-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-11', NULL, '1', '2020-01-10 13:18:11', '2', '2020-01-17 15:37:10', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('4a04cdd2cf6a4177a7abf858f843e121', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'leave_type', '3', NULL, '2', '2020-02-11 14:29:38', '2', '2020-02-11 14:29:38', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('4a476e1797584525aa1a069b977dfbd8', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8290f1e3-3387-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-10 16:59:30', '1', '2020-01-10 16:59:30', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('4b9766a0265b49aea78f0967b7a993a4', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'comment', '', NULL, '2', '2020-02-10 16:32:27', '2', '2020-02-10 16:32:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('4d049c08c5c64803becba686cde83c03', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8290f1e3-3387-11ea-aa63-00163e2e65eb', 'task_asignee', '人力资源,jsitehr', NULL, '1', '2020-01-10 16:59:29', '1', '2020-01-10 16:59:29', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('4d48647ce30a43c98b83997068be0c7b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'task_asignee', '人力资源,jsitehr', NULL, '1', '2020-01-17 14:52:50', '1', '2020-01-17 14:52:50', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('507e34f26dca47a3a5edf89bf40c05e6', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '264a7336-3678-11ea-aa63-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-14 10:47:06', '1', '2020-01-14 14:08:19', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('51578a0424c84e91899a8e28f8ecddb3', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '006ae1ba-3838-11ea-aa63-00163e2e65eb', 'leave_type', '2', NULL, '1', '2020-01-16 16:12:57', '1', '2020-01-16 16:12:57', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('51d560b40dbd4782955906a7a4061d10', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8290f1e3-3387-11ea-aa63-00163e2e65eb', 'leave_type', '2', NULL, '1', '2020-01-10 16:59:30', '1', '2020-01-10 16:59:30', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('527233dd2d574f2d8ce090d901dd6519', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c5247234-3156-11ea-9129-00163e2e65eb', 'leave_tm_begin', '2020-01-07', NULL, '1', '2020-01-07 22:05:33', '1', '2020-01-07 22:06:53', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('529e0ebb28154f83a4b2f7b7aaa2b114', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'bdc1bb3a-2dd7-11ea-a0ba-c85b7643dd9e', 'leave_tm_end', '2020-01-04', NULL, '1', '2020-01-03 11:18:45', '1', '2020-01-03 12:05:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('53a1319609c249238f0dad7e2f5a4e28', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-08 17:27:18', '1', '2020-01-08 17:27:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('58137163b319461d8174818bafae12c2', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', 'leave_tm_end', '2020-01-06', NULL, '1', '2020-01-05 15:22:01', '1', '2020-01-05 15:22:17', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('5b6b6894468049da80c8473a8cb3388b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '90237410-35d5-11ea-aa63-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-13 15:23:15', '1', '2020-01-13 15:24:11', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('5c5451fd9066429bb6da45438cf0d520', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-14 16:22:22', '1', '2020-01-14 16:22:22', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('5cd9d4aa70b542cb800e106816fa9513', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '143f81f3-3029-11ea-9129-00163e2e65eb', 'leave_tm_begin', '2020-01-06', NULL, '1', '2020-01-06 10:05:58', '1', '2020-01-08 11:12:25', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('5cf36a5f1dce49c6a5a5305f214c58aa', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '783cda15-2e95-11ea-9129-00163e2e65eb', 'leave_type', '3', NULL, '1', '2020-01-04 09:56:50', '1', '2020-01-06 14:57:07', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('5dc3c902df714644bdf3124d61822c0b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '5da62737-32b9-11ea-aa63-00163e2e65eb', 'leave_reason', '有事', NULL, '1', '2020-01-09 16:23:51', '1', '2020-01-09 16:23:51', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('5ecac06358a34b4cb3d01b85dc104d9f', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'leave_tm_begin', '2020-02-13', NULL, '1', '2020-02-12 18:24:24', '2', '2020-02-12 18:27:46', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('5f671d291e13410a8eb88925a76dcd82', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'leave_reason', '1111111111', NULL, '1', '2020-01-14 11:27:57', '2', '2020-02-11 14:29:46', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('634dd848aa344d05b5dabb9bd2874593', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '5da62737-32b9-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-09 16:23:51', '1', '2020-01-09 16:23:51', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('63b06f4a2d67489d9d0d7e56e2efce23', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-08 22:27:48', '1', '2020-01-13 15:24:04', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('64f6ed8c092849d58d96b3c362958db1', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd27dd303-2dea-11ea-b12b-c85b7643dd9e', 'leave_days', '2', NULL, '1', '2020-01-03 13:35:18', '1', '2020-01-03 14:00:53', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('67f04beb16704fd8b4cf3088b1f7f8fe', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '143f81f3-3029-11ea-9129-00163e2e65eb', 'leave_reason', '2', NULL, '1', '2020-01-06 10:05:59', '1', '2020-01-08 11:12:25', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('69997d997c6e446380d86b007cb2a475', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', 'leave_reason', '1', NULL, '1', '2020-01-05 15:22:01', '1', '2020-01-05 15:22:17', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('6a7db1a27c1349afb45f1135a2e85d7d', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3414bed4-3046-11ea-9129-00163e2e65eb', 'leave_type', '2', NULL, '1', '2020-01-06 13:34:27', '3', '2020-01-07 16:49:14', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('6b1eb5764cf44dadb5926824c2cb19da', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', 'leave_type', '2', NULL, '1', '2020-01-08 17:27:18', '1', '2020-01-08 17:27:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('6bdbc61258ac46ed85715fe8494fcb47', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd27dd303-2dea-11ea-b12b-c85b7643dd9e', 'leave_tm_end', '2020-01-04', NULL, '1', '2020-01-03 13:35:21', '1', '2020-01-03 14:00:52', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('6dcc0580356a43189fb7de4b6efff638', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '783cda15-2e95-11ea-9129-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-04 09:56:50', '1', '2020-01-06 14:57:07', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('6df0b4137a9845ac9e98ac752e1cce0d', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd27dd303-2dea-11ea-b12b-c85b7643dd9e', 'comment', '駁回測試', NULL, '1', '2020-01-03 13:35:21', '1', '2020-01-03 14:00:52', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('6e68e35c08714c4a8e8343d8e1f37327', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-17 15:35:24', '3', '2020-01-17 15:38:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('6fa6e482677746118f76ef07fc405977', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e318c61c-305b-11ea-9129-00163e2e65eb', 'leave_reason', 'xxxxxxxxxxxxx', NULL, '2', '2020-01-06 16:09:40', '2', '2020-01-06 16:10:38', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('70739b2796c54078882fd0961562be1e', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-23', NULL, '1', '2020-01-08 17:27:18', '1', '2020-01-08 17:27:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('70c293c7ad4f410ab382f32235d367b7', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '79cea582-2df3-11ea-aa23-c85b7643dd9e', 'leave_reason', '请假测试0103\n调整测试0103', NULL, '1', '2020-01-03 14:37:16', '1', '2020-01-03 14:43:59', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('7184b56e09bb42408d21af484672db3f', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '56631326-383f-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-16', NULL, '1', '2020-01-16 17:05:27', '1', '2020-01-16 17:05:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('71ae87e20142439a99c665ad7ca6ce3c', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '979e7168-3368-11ea-aa63-00163e2e65eb', 'leave_days', '1', NULL, '1', '2020-01-10 13:18:10', '2', '2020-01-17 15:37:10', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('71e098d651114a75a0f8b218c6493b04', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3414bed4-3046-11ea-9129-00163e2e65eb', 'comment', '不批准', NULL, '1', '2020-01-06 13:34:27', '3', '2020-01-07 16:49:14', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('72d2d4f4d84e4606a29af798671362b2', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-15 09:11:30', '2', '2020-01-17 15:36:39', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('72fd184111e44ba3a9160b7820c3c916', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-13 14:54:05', '1', '2020-01-13 15:24:20', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('75b143f705c84bf89a1336e7c668c05c', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'task_asignee', '部门经理,dept', NULL, '2', '2020-02-11 14:29:38', '2', '2020-02-11 14:29:38', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('75d685617c0f46e2b306ae7ccd4b568a', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e318c61c-305b-11ea-9129-00163e2e65eb', 'comment', '321312313', NULL, '2', '2020-01-06 16:09:40', '2', '2020-01-06 16:10:38', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('777cad43bb6b46e4a26d6362cac831fd', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3d309e0e-3029-11ea-9129-00163e2e65eb', 'leave_reason', '2', NULL, '1', '2020-01-06 10:07:07', '1', '2020-01-07 14:48:24', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('7913d1ca87774709a707547ea928511b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-14 18:22:36', '1', '2020-01-14 18:22:36', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('794cd422f1c24c3ca6f0f3f80ca6538b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'leave_tm_end', '2020-02-13', NULL, '2', '2020-02-10 16:32:28', '2', '2020-02-10 16:32:28', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('79643849a80844139cb7012848aed0c7', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'comment', '', NULL, '1', '2020-02-12 15:53:44', '1', '2020-02-12 15:53:44', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('79a57261e1924703872faa42d76f0abf', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '5da62737-32b9-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-09', NULL, '1', '2020-01-09 16:23:51', '1', '2020-01-09 16:23:51', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('7a0488f747d7486aa6293acfd6c751a8', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'leave_reason', '事假', NULL, '1', '2020-01-15 09:11:30', '2', '2020-01-17 15:36:39', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('7cf2476c291b4971a51992673523339d', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '79cea582-2df3-11ea-aa23-c85b7643dd9e', 'leave_days', '1', NULL, '1', '2020-01-03 14:37:14', '1', '2020-01-03 14:44:00', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('7d131d7fea384c67ac67612fee67e9e6', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3414bed4-3046-11ea-9129-00163e2e65eb', 'leave_reason', '33', NULL, '1', '2020-01-06 13:34:27', '3', '2020-01-07 16:49:14', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('7f33aed85d5344ff83b02c0f3f633ead', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8db9ba53-3379-11ea-aa63-00163e2e65eb', 'leave_days', '1', NULL, '2', '2020-01-10 15:19:35', '2', '2020-01-10 15:19:35', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('7fae3a5390fe405bb59824db576520aa', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8290f1e3-3387-11ea-aa63-00163e2e65eb', 'leave_days', '1', NULL, '1', '2020-01-10 16:59:29', '1', '2020-01-10 16:59:29', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('7fc20079220f4cf29ac8ee31a18fda94', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '56631326-383f-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-16 17:05:28', '1', '2020-01-16 17:05:28', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('7ff2ca3171b141978d01ecc1d767e8ec', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3d309e0e-3029-11ea-9129-00163e2e65eb', 'leave_tm_end', '2020-01-08', NULL, '1', '2020-01-06 10:07:07', '1', '2020-01-07 14:48:24', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('8039b27f85d64042ab5a46dc2e579167', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e318c61c-305b-11ea-9129-00163e2e65eb', 'task_asignee', '', NULL, '2', '2020-01-06 16:09:40', '2', '2020-01-06 16:10:38', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('811a5b06c3554dc7a52725ce87758b7a', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'leave_type', '1', NULL, '1', '2020-01-09 16:55:28', '1', '2020-01-09 16:55:28', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('8228ab32c4db4f90a1c9d3cbc051350a', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '56631326-383f-11ea-aa63-00163e2e65eb', 'leave_type', '1', NULL, '1', '2020-01-16 17:05:27', '1', '2020-01-16 17:05:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('823558eaa75a4c6da51d9d127433ed5d', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '006ae1ba-3838-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-16', NULL, '1', '2020-01-16 16:12:57', '1', '2020-01-16 16:12:57', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('823eeb9d29b04a4d8101c7c58db7618b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'comment', '', NULL, '1', '2020-01-03 14:45:30', '2', '2020-01-17 15:37:25', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('82a91744d3e444fbb9dd2e0e096bbca9', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'leave_type', '1', NULL, '1', '2020-01-14 17:18:15', '3', '2020-01-15 11:00:03', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('832ca4570548418a8a304b1b8b5ff971', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8db9ba53-3379-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-11', NULL, '2', '2020-01-10 15:19:35', '2', '2020-01-10 15:19:35', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('83db91aff8b7473291b20e625e0cc56f', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'task_asignee', '', NULL, '1', '2020-01-03 14:45:30', '2', '2020-01-17 15:37:25', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('83eef491ef39422db9d2eb6720e4bb42', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-16', NULL, '1', '2020-01-14 16:22:22', '1', '2020-01-14 16:22:22', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('84242a0b32fb48bba5da32804c107c8c', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '56631326-383f-11ea-aa63-00163e2e65eb', 'leave_days', '2', NULL, '1', '2020-01-16 17:05:27', '1', '2020-01-16 17:05:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('84be2624e3b74df4a9cf73db981d3422', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'leave_days', '2', NULL, '1', '2020-01-03 14:45:29', '2', '2020-01-17 15:37:25', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('8514d10c54894e47a93fc96a7c3e3bd9', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-14', NULL, '1', '2020-01-13 14:54:05', '1', '2020-01-13 15:24:20', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('85bcd1a4e5b34e189b6b32b30882ecbc', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'leave_reason', '1', NULL, '1', '2020-01-17 15:35:24', '3', '2020-01-17 15:38:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('85f2d6bbe42e444280c17f5691968790', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'task_asignee', '人力资源,jsitehr', NULL, '2', '2020-02-10 16:32:27', '2', '2020-02-10 16:32:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('8765088544d348eb9fad4b4e9356659f', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'bdc1bb3a-2dd7-11ea-a0ba-c85b7643dd9e', 'leave_tm_begin', '2020-01-03', NULL, '1', '2020-01-03 11:18:44', '1', '2020-01-03 12:05:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('88368b53b39143599818c09f2256b183', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'leave_reason', '/', NULL, '2', '2020-02-10 16:32:27', '2', '2020-02-10 16:32:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('8b3424ddd308468c8f9677e4dbb24fe3', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 'leave_days', '2', NULL, '2', '2020-02-10 16:32:27', '2', '2020-02-10 16:32:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('8e1ae4bca5aa4768b7e249fff5680c77', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '66bf898f-3827-11ea-aa63-00163e2e65eb', 'leave_type', '2', NULL, '1', '2020-01-16 14:14:07', '1', '2020-01-16 14:14:07', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('8e29ae364c1b4b52a4e0be08dd93b934', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-05 15:22:00', '1', '2020-01-05 15:22:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('8f303e90bc6e4df0aa20e13d1c25effe', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '5da62737-32b9-11ea-aa63-00163e2e65eb', 'task_asignee', '普通用户,jsiteuser', NULL, '1', '2020-01-09 16:23:51', '1', '2020-01-09 16:23:51', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('91e3eaf0b6654633aa252b40571e51db', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '783cda15-2e95-11ea-9129-00163e2e65eb', 'leave_tm_begin', '2020-01-24', NULL, '1', '2020-01-04 09:56:50', '1', '2020-01-06 14:57:07', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('9261f48f223a481a91f336ad688e4a2e', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'leave_tm_begin', '2020-02-12', NULL, '1', '2020-02-12 15:53:44', '1', '2020-02-12 15:53:44', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('93a096f39d414d89a3b4d4883e4fefc4', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c5247234-3156-11ea-9129-00163e2e65eb', 'leave_reason', '生病了哦', NULL, '1', '2020-01-07 22:05:34', '1', '2020-01-07 22:06:53', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('94a0d8b6e3e143e8a8fc527b31c823a1', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-14', NULL, '1', '2020-01-14 17:18:15', '3', '2020-01-15 11:00:03', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('95771144ed744cd9a32e57542593ec2b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'leave_days', '12', NULL, '1', '2020-01-14 16:22:22', '1', '2020-01-14 16:22:22', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('9671cbedb54c42fe9da6ef530c390a98', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8db9ba53-3379-11ea-aa63-00163e2e65eb', 'task_asignee', '人力资源,jsitehr', NULL, '2', '2020-01-10 15:19:35', '2', '2020-01-10 15:19:35', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('96c28e4002354e299e4b052eeb693559', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '783cda15-2e95-11ea-9129-00163e2e65eb', 'leave_days', '5', NULL, '1', '2020-01-04 09:56:49', '1', '2020-01-06 14:57:07', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('99280c39146449edb4a447a7ac1edfb1', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', 'leave_type', '1', NULL, '1', '2020-01-13 14:54:04', '1', '2020-01-13 15:24:21', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('9977a248de51409983fdcf1992711671', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '006ae1ba-3838-11ea-aa63-00163e2e65eb', 'leave_reason', '鄢康请假', NULL, '1', '2020-01-16 16:12:57', '1', '2020-01-16 16:12:57', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('9a2fe05ad45d4d46a3ce9b7a2fa0c902', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3d309e0e-3029-11ea-9129-00163e2e65eb', 'leave_type', '1', NULL, '1', '2020-01-06 10:07:07', '1', '2020-01-07 14:48:24', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('9ac2ec6213e84ac4b109a0764bd797c4', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'leave_type', '1', NULL, '1', '2020-01-10 09:36:47', '1', '2020-01-10 15:25:55', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('9b5af0c6b9484b1db33c354d4abd2db8', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'task_asignee', '普通用户,jsiteuser', NULL, '1', '2020-01-14 18:22:36', '1', '2020-01-14 18:22:36', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('9b90db6971c248edac20869c8d3e72bb', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'leave_days', '1', NULL, '1', '2020-02-12 18:24:24', '2', '2020-02-12 18:27:46', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('9c1698de503c4bc7b348a090979fee02', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'leave_days', '3', NULL, '1', '2020-01-14 18:22:35', '1', '2020-01-14 18:22:35', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('9e8def0cc066443d95bd1b4a299f9852', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8db9ba53-3379-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '2', '2020-01-10 15:19:35', '2', '2020-01-10 15:19:35', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('9ed1af5e7e6e405d93f88f7e5e296354', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-10 09:36:47', '1', '2020-01-10 15:25:55', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('9f10775ca2084fb69290eda121d29d8d', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'leave_reason', '33', NULL, '1', '2020-01-14 18:22:36', '1', '2020-01-14 18:22:36', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('9f54d492e683490cbbc2078545d5c95f', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '90237410-35d5-11ea-aa63-00163e2e65eb', 'leave_reason', 'DSSFDAA', NULL, '1', '2020-01-13 15:23:15', '1', '2020-01-13 15:24:11', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('a0f64fc4628e4a8f80f1716000bd6a4d', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'leave_type', '3', NULL, '1', '2020-02-12 15:53:44', '1', '2020-02-12 15:53:44', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('a146f8dc938a48888b3e517a96774e91', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '90237410-35d5-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-25', NULL, '1', '2020-01-13 15:23:15', '1', '2020-01-13 15:24:11', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('a1ba4a43ff8c472cba9d89725076c017', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'leave_tm_end', '2020-01-04', NULL, '1', '2020-01-03 14:45:31', '2', '2020-01-17 15:37:25', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('a3c37bed595849b5a8904c3c30a938d3', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'leave_days', '1', NULL, '1', '2020-01-14 17:18:15', '3', '2020-01-15 11:00:03', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('a4fe7f6072c44c7d96a74687b7c94d86', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '143f81f3-3029-11ea-9129-00163e2e65eb', 'leave_days', '2', NULL, '1', '2020-01-06 10:05:58', '1', '2020-01-08 11:12:25', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('a60671215f494feb8caf85eadf9c0cdd', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'leave_reason', '11', NULL, '1', '2020-01-14 17:18:15', '3', '2020-01-15 11:00:03', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('a6af745c545f489d80f02b0ab8771cf1', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'task_asignee', '人力资源,jsitehr', NULL, '1', '2020-01-09 16:55:28', '1', '2020-01-09 16:55:28', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('a853c6a3c2f14f17aeb44ec10de3dbc2', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '56631326-383f-11ea-aa63-00163e2e65eb', 'task_asignee', '人力资源,jsitehr', NULL, '1', '2020-01-16 17:05:27', '1', '2020-01-16 17:05:27', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('a89377089f594d3ab3c5b4932d36bf2a', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-13 14:54:04', '1', '2020-01-13 15:24:21', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('a8ad4ef3c06e4d4696b5fa59b0d4cce3', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-15', NULL, '1', '2020-01-14 16:22:22', '1', '2020-01-14 16:22:22', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('a8dacc14a9044a2b81b26e0956495340', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'bdc1bb3a-2dd7-11ea-a0ba-c85b7643dd9e', 'leave_type', '3', NULL, '1', '2020-01-03 11:18:45', '1', '2020-01-03 12:05:28', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('a9fe5e3c1fa0467f955153fa974e65ba', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '56631326-383f-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-18', NULL, '1', '2020-01-16 17:05:28', '1', '2020-01-16 17:05:28', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('aaaa8bb80b6748c0b0684c17560521eb', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'leave_tm_begin', '2020-01-03', NULL, '1', '2020-01-03 14:45:30', '2', '2020-01-17 15:37:25', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('aaca6b36b1604c15ae4f46dc76420290', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-14 11:27:57', '2', '2020-02-11 14:29:46', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('aaed9bc98c594158b9cab28ccbff18e6', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '79cea582-2df3-11ea-aa23-c85b7643dd9e', 'comment', '驳回1111', NULL, '1', '2020-01-03 14:37:15', '1', '2020-01-03 14:44:00', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('ab3951969d3b4c70b61905257db65583', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c5247234-3156-11ea-9129-00163e2e65eb', 'leave_days', '2', NULL, '1', '2020-01-07 22:05:33', '1', '2020-01-07 22:06:53', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('ab82627cbd7042538b08783164d963bc', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '264a7336-3678-11ea-aa63-00163e2e65eb', 'leave_type', '2', NULL, '1', '2020-01-14 10:47:06', '1', '2020-01-14 14:08:19', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('abca490c9fb4407fae4a605595cbdd71', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e318c61c-305b-11ea-9129-00163e2e65eb', 'leave_days', '2', NULL, '2', '2020-01-06 16:09:40', '2', '2020-01-06 16:10:38', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('acf24f0b217249608fabbcabd05202bb', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'leave_tm_end', '2020-02-14', NULL, '2', '2020-02-11 14:29:38', '2', '2020-02-11 14:29:38', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('ae3588ca0b3e487888868e027dacdef6', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8290f1e3-3387-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-11', NULL, '1', '2020-01-10 16:59:30', '1', '2020-01-10 16:59:30', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('af0a2f0a737f495cb652cbae7bf44614', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'leave_type', '1', NULL, '1', '2020-01-14 11:27:57', '2', '2020-02-11 14:29:46', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('af2faf5e22b745ff8316597f2578bbb5', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '66bf898f-3827-11ea-aa63-00163e2e65eb', 'task_asignee', '人力资源,jsitehr', NULL, '1', '2020-01-16 14:14:07', '1', '2020-01-16 14:14:07', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('b17fbe24d50944bdba1353cc0a92e50b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-14 11:27:57', '2', '2020-02-11 14:29:46', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('b1921be0e64945cb96dd8fcef290767a', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'leave_type', '1', NULL, '1', '2020-01-17 15:35:24', '3', '2020-01-17 15:38:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('b195481e4b264a4c9f489dd493e19784', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-14 17:18:15', '3', '2020-01-15 11:00:03', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('b2e0100072a14a318eba4a1816ad59bd', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', 'leave_days', '3', NULL, '1', '2020-01-13 14:54:04', '1', '2020-01-13 15:24:21', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('b3e27dcc396d410ea6eaeae5aa4a6695', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'leave_reason', '试试水', NULL, '1', '2020-01-10 09:36:47', '1', '2020-01-10 15:25:55', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('b4545c0ad6dc46ebb2a5cec00b285807', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'comment', '', NULL, '1', '2020-02-12 18:24:24', '2', '2020-02-12 18:27:46', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('b5d1d86fb7a140278b900a8ec4c11f73', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'leave_type', '1', NULL, '1', '2020-01-17 14:52:50', '1', '2020-01-17 14:52:50', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('b632c001544f44e4badaf0334c28ce65', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '979e7168-3368-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-10 13:18:11', '2', '2020-01-17 15:37:10', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('b64ff54e4e6b4ef6829bcc30951f809c', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '79cea582-2df3-11ea-aa23-c85b7643dd9e', 'leave_tm_end', '2020-01-03', NULL, '1', '2020-01-03 14:37:16', '1', '2020-01-03 14:43:59', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('b9b31634d07b4400bbe89a60545c1a1b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '5da62737-32b9-11ea-aa63-00163e2e65eb', 'leave_days', '8', NULL, '1', '2020-01-09 16:23:51', '1', '2020-01-09 16:23:51', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('b9e56b8e153c4332a54dbf52a85bc2fc', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 'leave_reason', 'virves', NULL, '2', '2020-02-11 14:29:38', '2', '2020-02-11 14:29:38', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('ba56061732e9422fb35f6e5fa1893ce0', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'leave_type', '3', NULL, '1', '2020-02-12 18:24:24', '2', '2020-02-12 18:27:46', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('bacbf63a0dd04df8b10cc69ddaa2a61b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'leave_days', '1', NULL, '1', '2020-01-17 15:35:24', '3', '2020-01-17 15:38:19', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('bad5e26fb07740b3971c65ee5810c488', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '79cea582-2df3-11ea-aa23-c85b7643dd9e', 'leave_type', '3', NULL, '1', '2020-01-03 14:37:15', '1', '2020-01-03 14:44:00', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('bb60586d64ea4a1bb083fa353ef860c3', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-31', NULL, '1', '2020-01-08 22:27:48', '1', '2020-01-13 15:24:04', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('bd74eea7979b486db0c0e1b947a2b0e2', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '66bf898f-3827-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-17', NULL, '1', '2020-01-16 14:14:08', '1', '2020-01-16 14:14:08', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('be77d3f2c95446768cb31ab790509526', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-16', NULL, '1', '2020-01-14 18:22:36', '1', '2020-01-14 18:22:36', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('bebbf6b41d474d44b2e546a1dad34449', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '143f81f3-3029-11ea-9129-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-06 10:05:58', '1', '2020-01-08 11:12:25', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('c3fc68bcade9425697c1b29f4cd2ba37', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-13', NULL, '1', '2020-01-13 14:54:04', '1', '2020-01-13 15:24:21', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('c401a2e18ec944dab2df05b4a5c0ad8b', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c5247234-3156-11ea-9129-00163e2e65eb', 'leave_type', '3', NULL, '1', '2020-01-07 22:05:33', '1', '2020-01-07 22:06:53', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('c5651ed319384444a43804a83ed5637e', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '264a7336-3678-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-30', NULL, '1', '2020-01-14 10:47:06', '1', '2020-01-14 14:08:19', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('c580b36b0b864e21a06d772dd1a194f8', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd698a7fb-4d81-11ea-9b4e-00163e2e65eb', 'leave_reason', '无', NULL, '1', '2020-02-12 18:24:24', '2', '2020-02-12 18:27:46', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('c7e7898da4e646b7bb889691e4913b80', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8db9ba53-3379-11ea-aa63-00163e2e65eb', 'leave_type', '1', NULL, '2', '2020-01-10 15:19:35', '2', '2020-01-10 15:19:35', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('cb700c0a1ed54cff9d6fa132df599314', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'leave_type', '3', NULL, '1', '2020-01-15 09:11:30', '2', '2020-01-17 15:36:39', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('cce7e79f211a44dbb699287890815f5e', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f5d4437e-3733-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-15', NULL, '1', '2020-01-15 09:11:30', '2', '2020-01-17 15:36:39', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('ce6c996a833f4487906706d50b029d24', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'dafeba38-367d-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-15', NULL, '1', '2020-01-14 11:27:57', '2', '2020-02-11 14:29:46', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('d1117f5bb0f14284804d72091bb5ba0c', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'leave_reason', 'test', NULL, '1', '2020-01-09 16:55:29', '1', '2020-01-09 16:55:29', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('d11f2dc846ec47b0ba97cbe9295a1013', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-09 16:55:28', '1', '2020-01-09 16:55:28', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('d479fbd58620406bb11188b6789b5bc9', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e318c61c-305b-11ea-9129-00163e2e65eb', 'leave_type', '1', NULL, '2', '2020-01-06 16:09:40', '2', '2020-01-06 16:10:38', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('d56e138bca854cc8846436d6b9f5b74c', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '143f81f3-3029-11ea-9129-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-06 10:05:59', '1', '2020-01-08 11:12:25', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('d5de1ee5dbab410caec3345703d91023', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '66bf898f-3827-11ea-aa63-00163e2e65eb', 'leave_days', '1', NULL, '1', '2020-01-16 14:14:07', '1', '2020-01-16 14:14:07', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('d7345f465cbd4b73ab427bd47940daf5', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', 'leave_days', '20', NULL, '1', '2020-01-08 22:27:48', '1', '2020-01-13 15:24:04', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('d8d7ed83e08a4082ae08e337eb429c03', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-05 15:22:01', '1', '2020-01-05 15:22:17', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('d901c5d10ebe412f880adf6b03eb66d1', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd27dd303-2dea-11ea-b12b-c85b7643dd9e', 'leave_tm_begin', '2020-01-03', NULL, '1', '2020-01-03 13:35:18', '1', '2020-01-03 14:00:53', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('da24504f061c4b40a613604b5d4418df', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-10', NULL, '1', '2020-01-10 09:36:47', '1', '2020-01-10 15:25:55', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('dc4d4a7c89094c048ab2e31ab610e56e', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '7c8a32c5-35d1-11ea-aa63-00163e2e65eb', 'leave_reason', '33', NULL, '1', '2020-01-13 14:54:05', '1', '2020-01-13 15:24:21', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('df49a0644ef546ff80fc67e1d38e3d6d', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '143f81f3-3029-11ea-9129-00163e2e65eb', 'leave_type', '1', NULL, '1', '2020-01-06 10:05:58', '1', '2020-01-08 11:12:25', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('e1a061a294e54beab05304c8212c81cc', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3d309e0e-3029-11ea-9129-00163e2e65eb', 'leave_days', '2', NULL, '1', '2020-01-06 10:07:07', '1', '2020-01-07 14:48:24', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('e21a10ae85fe4e428af07e4c8ebadddc', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', 'leave_reason', '私事', NULL, '1', '2020-01-08 22:27:48', '1', '2020-01-13 15:24:04', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('e22a99f0a6fa4512832d0ee71edf0c13', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '56631326-383f-11ea-aa63-00163e2e65eb', 'leave_reason', '测试测试', NULL, '1', '2020-01-16 17:05:28', '1', '2020-01-16 17:05:28', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('e27633bf44984510ae7a79d9817dbfa9', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'f9e02281-38f5-11ea-aa63-00163e2e65eb', 'leave_days', '2', NULL, '1', '2020-01-17 14:52:50', '1', '2020-01-17 14:52:50', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('e295b95a0b32485086a9dcd8adf0249d', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '90237410-35d5-11ea-aa63-00163e2e65eb', 'leave_days', '90', NULL, '1', '2020-01-13 15:23:15', '1', '2020-01-13 15:24:12', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('e2eef3b91bd94a64b3b628d2834b91bf', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 'leave_days', '1', NULL, '1', '2020-02-12 15:53:44', '1', '2020-02-12 15:53:44', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('e3fcff20ec6049bbaf599c2352ca3322', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8290f1e3-3387-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-10', NULL, '1', '2020-01-10 16:59:29', '1', '2020-01-10 16:59:29', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('e641abe856764795a8c6ff691fa44c11', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'bdc1bb3a-2dd7-11ea-a0ba-c85b7643dd9e', 'comment', '', NULL, '1', '2020-01-03 11:18:45', '1', '2020-01-03 12:05:28', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('e75869aa9d27462fa87ceab2c9156330', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', 'leave_type', '1', NULL, '1', '2020-01-08 22:27:48', '1', '2020-01-13 15:24:04', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('e7a6fd9aaa3d4bd485ba67ee47391ed5', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3414bed4-3046-11ea-9129-00163e2e65eb', 'leave_tm_end', '2020-01-08', NULL, '1', '2020-01-06 13:34:27', '3', '2020-01-07 16:49:14', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('e84c12a4092c4707ae7da5caeac07255', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '006ae1ba-3838-11ea-aa63-00163e2e65eb', 'leave_days', '10', NULL, '1', '2020-01-16 16:12:57', '1', '2020-01-16 16:12:57', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('ec3a2d2203254f028facb817fd89e9f3', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '979e7168-3368-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-10', NULL, '1', '2020-01-10 13:18:10', '2', '2020-01-17 15:37:10', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('ec84e27eb6f94ef0b562e70caa61e981', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-16', NULL, '1', '2020-01-14 18:22:36', '1', '2020-01-14 18:22:36', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('edc0dec455a6402e8fea560accf1307a', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'caea8d18-36ae-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-14', NULL, '1', '2020-01-14 17:18:15', '3', '2020-01-15 11:00:03', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('eed33773f34f45a6ac85d69366ef32d6', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'a0af89db-2df4-11ea-aa23-c85b7643dd9e', 'leave_type', '3', NULL, '1', '2020-01-03 14:45:30', '2', '2020-01-17 15:37:26', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('efa3f73bc93843bba6ae63f439e1a0b7', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', 'leave_days', '1', NULL, '1', '2020-01-05 15:22:00', '1', '2020-01-05 15:22:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('f236b2d0aefa4fbc809f3b571c5a5e60', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-09', NULL, '1', '2020-01-09 16:55:28', '1', '2020-01-09 16:55:28', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('f312a2997c3f4da2ada73d8573c75cf1', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-17 15:35:24', '3', '2020-01-17 15:38:19', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('f4bd02b0334f46c28cc57f1e03fd4879', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c81fe553-36b7-11ea-aa63-00163e2e65eb', 'leave_type', '1', NULL, '1', '2020-01-14 18:22:36', '1', '2020-01-14 18:22:36', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('f4bdd51688b9406c9d538370d253de3d', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c87ffc92-32bd-11ea-aa63-00163e2e65eb', 'leave_days', '1', NULL, '1', '2020-01-09 16:55:28', '1', '2020-01-09 16:55:28', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('f54c68681e3548348fc394f860a2e44c', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8db9ba53-3379-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-10', NULL, '2', '2020-01-10 15:19:35', '2', '2020-01-10 15:19:35', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('f55d16fcb18b4f038970580cfeaf06cb', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'e318c61c-305b-11ea-9129-00163e2e65eb', 'leave_tm_begin', '2020-01-06', NULL, '2', '2020-01-06 16:09:40', '2', '2020-01-06 16:10:38', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('f5a4b5f3a3d445c4b0496c34b61ae453', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '143f81f3-3029-11ea-9129-00163e2e65eb', 'leave_tm_end', '2020-01-07', NULL, '1', '2020-01-06 10:05:59', '1', '2020-01-08 11:12:25', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('f685d72ff99043feb5db2bc29673bb86', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '66bf898f-3827-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-16', NULL, '1', '2020-01-16 14:14:07', '1', '2020-01-16 14:14:07', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('f84c8c1ea5de4716a0529f9b50573ca0', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '0ad9c74c-3223-11ea-aa63-00163e2e65eb', 'leave_tm_begin', '2020-01-08', NULL, '1', '2020-01-08 22:27:48', '1', '2020-01-13 15:24:04', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('f8ec53360fa649ada1a366fecb42e3cf', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1034b9e1-31f9-11ea-aa63-00163e2e65eb', 'task_asignee', '人力资源,jsitehr', NULL, '1', '2020-01-08 17:27:18', '1', '2020-01-08 17:27:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('f9643e4141a14082b94cd2c1221e4d25', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'c5247234-3156-11ea-9129-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-07 22:05:33', '1', '2020-01-07 22:06:53', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('fa22650bffdc4262bcd818081fb90292', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '264a7336-3678-11ea-aa63-00163e2e65eb', 'comment', '', NULL, '1', '2020-01-14 10:47:06', '1', '2020-01-14 14:08:19', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('fac1bcc1569147ae90ea0a6a5e26a91f', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'd27dd303-2dea-11ea-b12b-c85b7643dd9e', 'leave_reason', '請假測試1111111111', NULL, '1', '2020-01-03 13:35:21', '1', '2020-01-03 14:00:52', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('fbeea9c3db7448c3a485614fa417edef', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'aa3c297d-3349-11ea-aa63-00163e2e65eb', 'leave_tm_end', '2020-01-15', NULL, '1', '2020-01-10 09:36:47', '1', '2020-01-10 15:25:55', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('fc666fe229794f0faa9afc223fd2b7cb', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', 'fc715bed-36a6-11ea-aa63-00163e2e65eb', 'leave_reason', '12', NULL, '1', '2020-01-14 16:22:22', '1', '2020-01-14 16:22:22', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('fde36f48c72747a79e3524f55aa19dde', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '1035a7c0-2f8c-11ea-9129-00163e2e65eb', 'leave_type', '2', NULL, '1', '2020-01-05 15:22:00', '1', '2020-01-05 15:22:18', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('ffb48a98676e4b0cba75dcce8ab17ef0', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '8db9ba53-3379-11ea-aa63-00163e2e65eb', 'leave_reason', '2222', NULL, '2', '2020-01-10 15:19:35', '2', '2020-01-10 15:19:35', NULL, '0');
INSERT INTO `flow_form_data` VALUES ('ffc9284d84e542a4899acf26e5411bc8', 'b50fc0fcc2194ac88d5a85d7c4ae2a2d', '3d309e0e-3029-11ea-9129-00163e2e65eb', 'task_asignee', '', NULL, '1', '2020-01-06 10:07:07', '1', '2020-01-07 14:48:24', NULL, '0');

-- ----------------------------
-- Table structure for flow_form_data_temp
-- ----------------------------
DROP TABLE IF EXISTS `flow_form_data_temp`;
CREATE TABLE `flow_form_data_temp`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '���',
  `form_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '������',
  `proc_def_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '���̶���key',
  `field_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '�ֶ�����',
  `field_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '�ֶ�ֵ',
  `field_large_value` varchar(3000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '���ı��ֶ�ֵ',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '������',
  `create_date` datetime(0) NOT NULL COMMENT '����ʱ��',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '������',
  `update_date` datetime(0) NOT NULL COMMENT '����ʱ��',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '��ע��Ϣ',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT 'ɾ�����',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '���̱���ֶ������ݴ��' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flow_form_user
-- ----------------------------
DROP TABLE IF EXISTS `flow_form_user`;
CREATE TABLE `flow_form_user`  (
  `model_key` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'ģ��key',
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '��ԱID'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '�����ԱȨ�ޱ�' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flow_form_user
-- ----------------------------
INSERT INTO `flow_form_user` VALUES ('jsite_leave', '3');
INSERT INTO `flow_form_user` VALUES ('jsite_leave', '4');
INSERT INTO `flow_form_user` VALUES ('jsite_leave', '1');
INSERT INTO `flow_form_user` VALUES ('jsite_leave', '2');

-- ----------------------------
-- Table structure for flow_form_vars
-- ----------------------------
DROP TABLE IF EXISTS `flow_form_vars`;
CREATE TABLE `flow_form_vars`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '���',
  `model_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '����ģ��Id',
  `model_key` varchar(400) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '����ģ��key',
  `model_version` int(11) NULL DEFAULT NULL COMMENT '����ģ�Ͱ汾',
  `field_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '�ֶ�����',
  `var_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '��������',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '������',
  `create_date` datetime(0) NOT NULL COMMENT '����ʱ��',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '������',
  `update_date` datetime(0) NOT NULL COMMENT '����ʱ��',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '��ע��Ϣ',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT 'ɾ�����',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '���̱��������' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flow_form_vars
-- ----------------------------
INSERT INTO `flow_form_vars` VALUES ('b39476c78a1942618f95de01dbe3d066', 'aee0d905-2d31-11ea-91ac-c85b7643dd9e', 'jsite_leave', 1, 'task_asignee', 'auditPass', '1', '2020-01-03 14:35:37', '1', '2020-01-03 14:35:37', NULL, '0');

-- ----------------------------
-- Table structure for flow_form_version
-- ----------------------------
DROP TABLE IF EXISTS `flow_form_version`;
CREATE TABLE `flow_form_version`  (
  `proc_ins_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '����ʵ��ID',
  `model_version` int(11) NULL DEFAULT NULL COMMENT '����ģ�Ͱ汾'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '���̱���汾��' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flow_form_version
-- ----------------------------
INSERT INTO `flow_form_version` VALUES ('bdc1bb3a-2dd7-11ea-a0ba-c85b7643dd9e', 1);
INSERT INTO `flow_form_version` VALUES ('d27dd303-2dea-11ea-b12b-c85b7643dd9e', 1);
INSERT INTO `flow_form_version` VALUES ('79cea582-2df3-11ea-aa23-c85b7643dd9e', 1);
INSERT INTO `flow_form_version` VALUES ('a0af89db-2df4-11ea-aa23-c85b7643dd9e', 1);
INSERT INTO `flow_form_version` VALUES ('783cda15-2e95-11ea-9129-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('1035a7c0-2f8c-11ea-9129-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('143f81f3-3029-11ea-9129-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('3d309e0e-3029-11ea-9129-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('3414bed4-3046-11ea-9129-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('e318c61c-305b-11ea-9129-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('c5247234-3156-11ea-9129-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('1034b9e1-31f9-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('0ad9c74c-3223-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('5da62737-32b9-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('c87ffc92-32bd-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('aa3c297d-3349-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('979e7168-3368-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('8db9ba53-3379-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('8290f1e3-3387-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('7c8a32c5-35d1-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('90237410-35d5-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('264a7336-3678-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('dafeba38-367d-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('fc715bed-36a6-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('caea8d18-36ae-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('c81fe553-36b7-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('f5d4437e-3733-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('66bf898f-3827-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('006ae1ba-3838-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('56631326-383f-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('f9e02281-38f5-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('ec0a0dfc-38fb-11ea-aa63-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('de63bcf2-4bdf-11ea-9b4e-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('e08f1edf-4c97-11ea-9b4e-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('ca5a65af-4d6c-11ea-9b4e-00163e2e65eb', 1);
INSERT INTO `flow_form_version` VALUES ('d698a7fb-4d81-11ea-9b4e-00163e2e65eb', 1);

-- ----------------------------
-- Table structure for gen_scheme
-- ----------------------------
DROP TABLE IF EXISTS `gen_scheme`;
CREATE TABLE `gen_scheme`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '名称',
  `category` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '分类',
  `package_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成模块名',
  `sub_module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成子模块名',
  `function_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成功能名',
  `function_name_simple` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成功能名（简写）',
  `function_author` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_table_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成表编号',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `gen_scheme_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '生成方案' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gen_scheme
-- ----------------------------
INSERT INTO `gen_scheme` VALUES ('325d42709242476aab909b8d54a5ad73', '树结构表生成', 'treeTable', 'com.jsite.modules', 'test', '', '树结构表生成', '树结构表生成', 'liuruijun', '3195020a929e41c7bc3dfbd539ed5351', '1', '2019-01-05 22:12:10', '1', '2019-01-08 15:22:56', '', '0');
INSERT INTO `gen_scheme` VALUES ('c544bf7da3744ab1aa285fa3bfef6000', '主子表生成', 'curd_many', 'com.jsite.modules', 'test', '', '主子表生成测试', '主子表生成', 'liuruijun', 'd7c3fff144a046698aa3766844582a4c', '1', '2018-12-29 11:50:24', '1', '2019-01-02 10:06:04', '', '0');
INSERT INTO `gen_scheme` VALUES ('f09437aa6b9c440481e40aa63daf81d7', '单表生成', 'curd', 'com.jsite.modules', 'test', '', '单表生成测试', '单表生成', 'liuruijun', '39c9a49ccd87400b9b19606a46b12dd6', '1', '2018-12-29 11:46:43', '1', '2019-01-02 10:01:38', '', '0');

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '名称',
  `comments` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '描述',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '实体类名称',
  `parent_table` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '关联父表',
  `parent_table_fk` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '关联父表外键',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `gen_table_name`(`name`) USING BTREE,
  INDEX `gen_table_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '业务表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES ('3195020a929e41c7bc3dfbd539ed5351', 'test_tree', '树结构表', 'TestTree', '', '', '1', '2019-01-05 21:22:37', '1', '2019-01-07 16:51:37', NULL, '0');
INSERT INTO `gen_table` VALUES ('39c9a49ccd87400b9b19606a46b12dd6', 'test_data', '业务数据表', 'TestData', '', '', '1', '2018-12-29 11:19:36', '1', '2018-12-29 11:19:36', NULL, '0');
INSERT INTO `gen_table` VALUES ('59cbed6a1419421c843858d75c42b939', 'test_data', '业务数据表', 'TestData', '', '', '1', '2019-12-27 17:15:30', '1', '2019-12-27 17:15:30', NULL, '0');
INSERT INTO `gen_table` VALUES ('794ea2d0349e4590ba4366529d499833', 'test_data_child', '业务数据子表', 'TestDataChild', 'test_data_main', 'test_data_main_id', '1', '2018-12-29 11:21:18', '1', '2018-12-29 11:21:18', NULL, '0');
INSERT INTO `gen_table` VALUES ('d7c3fff144a046698aa3766844582a4c', 'test_data_main', '业务数据表', 'TestDataMain', '', '', '1', '2018-12-29 11:20:51', '1', '2018-12-29 11:20:51', NULL, '0');
INSERT INTO `gen_table` VALUES ('dc8b0bfe3a184c3293a44ea4a2500474', 'test_data', '业务数据表', 'TestData', '', '', '1', '2019-12-27 17:16:54', '1', '2019-12-27 17:16:54', NULL, '0');

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `gen_table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '归属表编号',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '名称',
  `comments` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '描述',
  `jdbc_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '列的数据类型的字节长度',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_row` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否独占一行(流程页面布局使用)',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否主键',
  `is_null` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否可为空',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否为插入字段',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否编辑字段',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否列表字段',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否查询字段',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '查询方式（等于、不等于、大于、小于、范围、左LIKE、右LIKE、左右LIKE）',
  `show_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '字段生成方案（文本框、文本域、下拉框、复选框、单选框、字典选择、人员选择、部门选择、区域选择）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '字典类型',
  `settings` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '其它设置（扩展字段JSON）',
  `sort` decimal(10, 0) NULL DEFAULT NULL COMMENT '排序（升序）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `gen_table_column_table_id`(`gen_table_id`) USING BTREE,
  INDEX `gen_table_column_name`(`name`) USING BTREE,
  INDEX `gen_table_column_sort`(`sort`) USING BTREE,
  INDEX `gen_table_column_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '业务表字段' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES ('036c21c4346a4b9a9bd4729f5b1996ed', 'd7c3fff144a046698aa3766844582a4c', 'id', '编号', 'varchar(64)', 'String', 'id', '0', '1', '0', '1', '0', '0', '0', '=', 'input', '', NULL, 10, '1', '2018-12-29 11:20:51', '1', '2018-12-29 11:20:51', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('0739b3a11c40466b8b3d9c488e3a6bf8', '39c9a49ccd87400b9b19606a46b12dd6', 'area_id', '归属区域', 'varchar(64)', 'com.jsite.modules.sys.entity.Area', 'area.id|name', '0', '0', '1', '1', '1', '0', '0', '=', 'areaselect', '', NULL, 40, '1', '2018-12-29 11:19:37', '1', '2019-12-27 17:16:54', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('08b216da6fe042fa817db5a95e4399a7', '3195020a929e41c7bc3dfbd539ed5351', 'parent_id', '父级编号', 'varchar(64)', 'This', 'parent.id|name', '0', '0', '0', '1', '1', '0', '0', '=', 'treeselect', '', NULL, 20, '1', '2019-01-05 21:22:37', '1', '2019-01-07 16:51:37', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('0d48145e1a154e31b276963e1503682c', '3195020a929e41c7bc3dfbd539ed5351', 'create_date', '创建时间', 'datetime', 'java.util.Date', 'createDate', '0', '0', '0', '1', '0', '0', '0', '=', 'dateselect', '', NULL, 90, '1', '2019-01-05 21:22:37', '1', '2019-01-07 16:51:37', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('18bbfa2e86a14ca4a6be2d8a643c98d7', '794ea2d0349e4590ba4366529d499833', 'create_date', '创建时间', 'datetime', 'java.util.Date', 'createDate', '0', '0', '0', '1', '0', '0', '0', '=', 'dateselect', '', NULL, 50, '1', '2018-12-29 11:21:19', '1', '2018-12-29 11:21:19', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('1f36d1a2217f4b09a6b7dc2683b5a6a9', 'd7c3fff144a046698aa3766844582a4c', 'update_date', '更新时间', 'datetime', 'java.util.Date', 'updateDate', '0', '0', '0', '1', '1', '1', '0', '=', 'dateselect', '', NULL, 110, '1', '2018-12-29 11:20:52', '1', '2018-12-29 11:20:52', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('3421eb7ef5a3427f92c48a6033441a0e', '39c9a49ccd87400b9b19606a46b12dd6', 'update_by', '更新者', 'varchar(64)', 'com.jsite.modules.sys.entity.User', 'updateBy.id', '0', '0', '0', '1', '1', '0', '0', '=', 'input', '', NULL, 100, '1', '2018-12-29 11:19:37', '1', '2019-12-27 17:16:55', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('3a745b100cde4fc99eb0f4e35f27a1b7', '39c9a49ccd87400b9b19606a46b12dd6', 'create_by', '创建者', 'varchar(64)', 'com.jsite.modules.sys.entity.User', 'createBy.id', '0', '0', '0', '1', '0', '0', '0', '=', 'input', '', NULL, 80, '1', '2018-12-29 11:19:37', '1', '2019-12-27 17:16:55', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('3b9990bbe9144b52b316402cac1678e8', '3195020a929e41c7bc3dfbd539ed5351', 'update_date', '更新时间', 'datetime', 'java.util.Date', 'updateDate', '0', '0', '0', '1', '1', '1', '0', '=', 'dateselect', '', NULL, 110, '1', '2019-01-05 21:22:37', '1', '2019-01-07 16:51:37', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('404a90b909bb416baa030a1838af45d0', '39c9a49ccd87400b9b19606a46b12dd6', 'name', '名称', 'varchar(100)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'like', 'input', '', NULL, 50, '1', '2018-12-29 11:19:37', '1', '2019-12-27 17:16:54', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('40dc876733f4428a84349621d3ae7e79', '3195020a929e41c7bc3dfbd539ed5351', 'del_flag', '删除标记', 'char(1)', 'String', 'delFlag', '0', '0', '0', '1', '0', '0', '0', '=', 'radiobox', 'del_flag', NULL, 130, '1', '2019-01-05 21:22:37', '1', '2019-01-07 16:51:37', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('4316cdf1fbff40e591419cbb75de6742', '3195020a929e41c7bc3dfbd539ed5351', 'remarks', '备注信息', 'varchar(255)', 'String', 'remarks', '0', '0', '1', '1', '1', '1', '0', '=', 'textarea', '', NULL, 120, '1', '2019-01-05 21:22:37', '1', '2019-01-07 16:51:37', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('471db37a88de4c02a9c2db052faf5dec', '794ea2d0349e4590ba4366529d499833', 'id', '编号', 'varchar(64)', 'String', 'id', '0', '1', '0', '1', '0', '0', '0', '=', 'input', '', NULL, 10, '1', '2018-12-29 11:21:18', '1', '2018-12-29 11:21:18', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('4fa8affc845740448035c44686a6b4f1', 'd7c3fff144a046698aa3766844582a4c', 'create_by', '创建者', 'varchar(64)', 'com.jsite.modules.sys.entity.User', 'createBy.id', '0', '0', '0', '1', '0', '0', '0', '=', 'input', '', NULL, 80, '1', '2018-12-29 11:20:52', '1', '2018-12-29 11:20:52', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('518b26e7bd6c4a6aaac289678ef64ace', '39c9a49ccd87400b9b19606a46b12dd6', 'id', '编号', 'varchar(64)', 'String', 'id', '0', '1', '0', '1', '0', '0', '0', '=', 'input', '', NULL, 10, '1', '2018-12-29 11:19:36', '1', '2019-12-27 17:16:54', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('5442ba5226c149f5a12187e55f68ca4a', '39c9a49ccd87400b9b19606a46b12dd6', 'sex', '性别', 'char(1)', 'String', 'sex', '0', '0', '1', '1', '1', '0', '0', '=', 'input', '', NULL, 60, '1', '2018-12-29 11:19:37', '1', '2019-12-27 17:16:54', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('547037387f074c43a5912e32073e4ba7', '794ea2d0349e4590ba4366529d499833', 'test_data_main_id', '业务主表ID', 'varchar(64)', 'String', 'testDataMainId', '0', '0', '1', '1', '1', '0', '0', '=', 'input', '', NULL, 20, '1', '2018-12-29 11:21:18', '1', '2018-12-29 11:21:18', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('5542c325e3364eb98450c6b7c386885b', '3195020a929e41c7bc3dfbd539ed5351', 'parent_ids', '所有父级编号', 'varchar(2000)', 'String', 'parentIds', '0', '0', '0', '1', '1', '0', '0', 'like', 'input', '', NULL, 30, '1', '2019-01-05 21:22:37', '1', '2019-01-07 16:51:37', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('5806f2b63737423fabe0efde8cd638fc', 'd7c3fff144a046698aa3766844582a4c', 'create_date', '创建时间', 'datetime', 'java.util.Date', 'createDate', '0', '0', '0', '1', '0', '0', '0', '=', 'dateselect', '', NULL, 90, '1', '2018-12-29 11:20:52', '1', '2018-12-29 11:20:52', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('58749bef8ac24ad0a651ec63c6f0cf79', '794ea2d0349e4590ba4366529d499833', 'remarks', '备注信息', 'varchar(255)', 'String', 'remarks', '0', '0', '1', '1', '1', '1', '0', '=', 'textarea', '', NULL, 80, '1', '2018-12-29 11:21:19', '1', '2018-12-29 11:21:19', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('63a7634cd1c449d1966355825f9ae0a1', '3195020a929e41c7bc3dfbd539ed5351', 'create_by', '创建者', 'varchar(64)', 'com.jsite.modules.sys.entity.User', 'createBy.id', '0', '0', '0', '1', '0', '0', '0', '=', 'input', '', NULL, 80, '1', '2019-01-05 21:22:37', '1', '2019-01-07 16:51:37', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('660bcce031194ba195c23225e15f4ebc', 'd7c3fff144a046698aa3766844582a4c', 'sex', '性别', 'char(1)', 'String', 'sex', '0', '0', '1', '1', '1', '1', '0', '=', 'input', '', NULL, 60, '1', '2018-12-29 11:20:51', '1', '2018-12-29 11:20:51', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('7301b4d629db45209e07cdc8849c40f6', '39c9a49ccd87400b9b19606a46b12dd6', 'in_date', '加入日期', 'date', 'java.util.Date', 'inDate', '0', '0', '1', '1', '1', '0', '0', '=', 'dateselect', '', NULL, 70, '1', '2018-12-29 11:19:37', '1', '2019-12-27 17:16:54', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('8ee7f0a8f2804042bb200e24f23de333', '39c9a49ccd87400b9b19606a46b12dd6', 'user_id', '归属用户', 'varchar(64)', 'com.jsite.modules.sys.entity.User', 'user.id|name', '0', '0', '1', '1', '1', '0', '0', '=', 'userselect', '', NULL, 20, '1', '2018-12-29 11:19:36', '1', '2019-12-27 17:16:54', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('9a6afed319b744f79071887baecac3a1', '3195020a929e41c7bc3dfbd539ed5351', 'id', '编号', 'varchar(64)', 'String', 'id', '0', '1', '0', '1', '0', '0', '0', '=', 'input', '', NULL, 10, '1', '2019-01-05 21:22:37', '1', '2019-01-07 16:51:37', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('9a6d8fcf3fdc4eefad2a63b09cda5c7f', '39c9a49ccd87400b9b19606a46b12dd6', 'remarks', '备注信息', 'varchar(255)', 'String', 'remarks', '0', '0', '1', '1', '1', '1', '0', '=', 'textarea', '', NULL, 120, '1', '2018-12-29 11:19:37', '1', '2019-12-27 17:16:55', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('a04f28b86fd545a6b3ea4a85861b55f9', 'd7c3fff144a046698aa3766844582a4c', 'area_id', '归属区域', 'varchar(64)', 'com.jsite.modules.sys.entity.Area', 'area.id|name', '0', '0', '1', '1', '1', '0', '0', '=', 'areaselect', '', NULL, 40, '1', '2018-12-29 11:20:51', '1', '2018-12-29 11:20:51', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('a460e3cbfc4f4b87b07b5a8f88e37c63', '3195020a929e41c7bc3dfbd539ed5351', 'sort', '排序', 'decimal(10,0)', 'Integer', 'sort', '0', '0', '1', '1', '1', '0', '0', '=', 'input', '', NULL, 50, '1', '2019-01-05 21:22:37', '1', '2019-01-07 16:51:37', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('ac74bc0c9b7746249dbf92499b89f310', '794ea2d0349e4590ba4366529d499833', 'del_flag', '删除标记', 'char(1)', 'String', 'delFlag', '0', '0', '0', '1', '0', '0', '0', '=', 'radiobox', 'del_flag', NULL, 90, '1', '2018-12-29 11:21:19', '1', '2018-12-29 11:21:19', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('acc2ffe13abb49fdb3e76fc871caa46e', '794ea2d0349e4590ba4366529d499833', 'create_by', '创建者', 'varchar(64)', 'com.jsite.modules.sys.entity.User', 'createBy.id', '0', '0', '0', '1', '0', '0', '0', '=', 'input', '', NULL, 40, '1', '2018-12-29 11:21:19', '1', '2018-12-29 11:21:19', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('afad04f3c0334e35ae2781b3966e33bf', '3195020a929e41c7bc3dfbd539ed5351', 'name', '名称', 'varchar(100)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'like', 'input', '', NULL, 40, '1', '2019-01-05 21:22:37', '1', '2019-01-07 16:51:37', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('bb2f13421fa04287ac31263650f56522', 'd7c3fff144a046698aa3766844582a4c', 'remarks', '备注信息', 'varchar(255)', 'String', 'remarks', '0', '0', '1', '1', '1', '1', '0', '=', 'textarea', '', NULL, 120, '1', '2018-12-29 11:20:52', '1', '2018-12-29 11:20:52', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('bb984414563449d5a428b798432ebee4', '39c9a49ccd87400b9b19606a46b12dd6', 'office_id', '归属部门', 'varchar(64)', 'com.jsite.modules.sys.entity.Office', 'office.id|name', '0', '0', '1', '1', '1', '0', '0', '=', 'officeselect', '', NULL, 30, '1', '2018-12-29 11:19:36', '1', '2019-12-27 17:16:54', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('bdeae7b5f5764a219b773493ce7ce717', 'd7c3fff144a046698aa3766844582a4c', 'user_id', '归属用户', 'varchar(64)', 'com.jsite.modules.sys.entity.User', 'user.id|name', '0', '0', '1', '1', '1', '0', '0', '=', 'userselect', '', NULL, 20, '1', '2018-12-29 11:20:51', '1', '2018-12-29 11:20:51', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('cb23c6752a744f7c865fc4461189e9e5', '3195020a929e41c7bc3dfbd539ed5351', 'tree_level', '树形层级(0:根级)', 'decimal(4,0)', 'String', 'treeLevel', '0', '0', '1', '1', '1', '0', '0', '=', 'input', '', NULL, 70, '1', '2019-01-05 21:22:37', '1', '2019-01-07 16:51:37', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('ce459de5970642e6ab8791ac72d0dfc6', 'd7c3fff144a046698aa3766844582a4c', 'name', '名称', 'varchar(100)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'like', 'input', '', NULL, 50, '1', '2018-12-29 11:20:51', '1', '2018-12-29 11:20:51', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('d333b5918d2f4db1996a360d2ac74a99', '39c9a49ccd87400b9b19606a46b12dd6', 'update_date', '更新时间', 'datetime', 'java.util.Date', 'updateDate', '0', '0', '0', '1', '1', '1', '0', '=', 'dateselect', '', NULL, 110, '1', '2018-12-29 11:19:37', '1', '2019-12-27 17:16:55', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('d3952c7032284832b26a532543f4fe78', '794ea2d0349e4590ba4366529d499833', 'update_date', '更新时间', 'datetime', 'java.util.Date', 'updateDate', '0', '0', '0', '1', '1', '1', '0', '=', 'dateselect', '', NULL, 70, '1', '2018-12-29 11:21:19', '1', '2018-12-29 11:21:19', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('deca344f6dc146bfae6377e939a0c097', '39c9a49ccd87400b9b19606a46b12dd6', 'del_flag', '删除标记', 'char(1)', 'String', 'delFlag', '0', '0', '0', '1', '0', '0', '0', '=', 'radiobox', 'del_flag', NULL, 130, '1', '2018-12-29 11:19:37', '1', '2019-12-27 17:16:55', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('e05d48dae1354898ae9f9b7c469313db', '3195020a929e41c7bc3dfbd539ed5351', 'update_by', '更新者', 'varchar(64)', 'com.jsite.modules.sys.entity.User', 'updateBy.id', '0', '0', '0', '1', '1', '0', '0', '=', 'input', '', NULL, 100, '1', '2019-01-05 21:22:37', '1', '2019-01-07 16:51:37', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('e399ea7f86c44ace9a69c03e489797bb', 'd7c3fff144a046698aa3766844582a4c', 'in_date', '加入日期', 'date', 'java.util.Date', 'inDate', '0', '0', '1', '1', '1', '1', '0', '=', 'dateselect', '', NULL, 70, '1', '2018-12-29 11:20:52', '1', '2018-12-29 11:20:52', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('e8e3f6f2d66c44a08210a0c35e74caa8', '3195020a929e41c7bc3dfbd539ed5351', 'tree_leaf', '是否树形叶子节点（0:不是,1:是）', 'char(1)', 'String', 'treeLeaf', '0', '0', '1', '1', '1', '0', '0', '=', 'input', '', NULL, 60, '1', '2019-01-05 21:22:37', '1', '2019-01-07 16:51:37', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('e948c667cfe4486eb2119583ff9805b4', 'd7c3fff144a046698aa3766844582a4c', 'del_flag', '删除标记', 'char(1)', 'String', 'delFlag', '0', '0', '0', '1', '0', '0', '0', '=', 'radiobox', 'del_flag', NULL, 130, '1', '2018-12-29 11:20:52', '1', '2018-12-29 11:20:52', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('ea80f499adc049e897889b36c1cfa220', '794ea2d0349e4590ba4366529d499833', 'update_by', '更新者', 'varchar(64)', 'com.jsite.modules.sys.entity.User', 'updateBy.id', '0', '0', '0', '1', '1', '0', '0', '=', 'input', '', NULL, 60, '1', '2018-12-29 11:21:19', '1', '2018-12-29 11:21:19', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('f3cfb155ee3d433fa287759b9e471b72', '39c9a49ccd87400b9b19606a46b12dd6', 'create_date', '创建时间', 'datetime', 'java.util.Date', 'createDate', '0', '0', '0', '1', '0', '0', '0', '=', 'dateselect', '', NULL, 90, '1', '2018-12-29 11:19:37', '1', '2019-12-27 17:16:55', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('f4e277e2ae97497fa3ebef882f3ac50d', 'd7c3fff144a046698aa3766844582a4c', 'office_id', '归属部门', 'varchar(64)', 'com.jsite.modules.sys.entity.Office', 'office.id|name', '0', '0', '1', '1', '1', '0', '0', '=', 'officeselect', '', NULL, 30, '1', '2018-12-29 11:20:51', '1', '2018-12-29 11:20:51', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('f610077a6e774855802b92389241fef7', 'd7c3fff144a046698aa3766844582a4c', 'update_by', '更新者', 'varchar(64)', 'com.jsite.modules.sys.entity.User', 'updateBy.id', '0', '0', '0', '1', '1', '0', '0', '=', 'input', '', NULL, 100, '1', '2018-12-29 11:20:52', '1', '2018-12-29 11:20:52', NULL, '0');
INSERT INTO `gen_table_column` VALUES ('fe3e9323ad1841a99ad7788770ea3101', '794ea2d0349e4590ba4366529d499833', 'name', '名称', 'varchar(100)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'like', 'input', '', NULL, 30, '1', '2018-12-29 11:21:19', '1', '2018-12-29 11:21:19', NULL, '0');

-- ----------------------------
-- Table structure for gen_template
-- ----------------------------
DROP TABLE IF EXISTS `gen_template`;
CREATE TABLE `gen_template`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '名称',
  `category` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '分类',
  `file_path` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成文件路径',
  `file_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成文件名',
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT '内容',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `gen_template_del_falg`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '代码模板表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for oa_leave
-- ----------------------------
DROP TABLE IF EXISTS `oa_leave`;
CREATE TABLE `oa_leave`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `proc_ins_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '流程实例编号',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `leave_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请假类型',
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请假理由',
  `dept_lead_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `hr_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `apply_time` datetime(0) NULL DEFAULT NULL COMMENT '申请时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oa_leave_create_by`(`create_by`) USING BTREE,
  INDEX `oa_leave_process_instance_id`(`proc_ins_id`) USING BTREE,
  INDEX `oa_leave_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '请假流程表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of oa_leave
-- ----------------------------
INSERT INTO `oa_leave` VALUES ('0047333475a944159bf14775983ba078', 'bb3918c7-4a24-11e9-83f7-00163e2e65eb', '2019-03-19 16:55:12', '2019-03-19 16:55:14', '1', 'aaaa', '[驳回] 65465', NULL, '2019-03-19 16:55:23', '1', '2019-03-19 16:55:23', '1', '2019-03-28 13:51:17', NULL, '0');
INSERT INTO `oa_leave` VALUES ('015c940f5693444cab1c9645e8b3e600', '93070b34-5514-11e9-a619-00163e2e65eb', '2019-04-03 14:57:21', '2019-04-27 14:57:24', '1', '123', NULL, NULL, '2019-04-02 14:57:27', '1', '2019-04-02 14:57:27', '1', '2019-04-02 14:57:27', NULL, '0');
INSERT INTO `oa_leave` VALUES ('026c658bdbdf4fc1bd640ce4497b5ce5', 'b29559bc-8382-11e9-997e-00163e2e65eb', '2019-05-31 17:01:23', '2019-06-05 17:01:29', '1', '222', NULL, NULL, '2019-05-31 17:01:38', '1', '2019-05-31 17:01:38', '1', '2019-05-31 17:01:38', NULL, '0');
INSERT INTO `oa_leave` VALUES ('0329eb8ea76a4e2593518f41455a56a5', '054af35a-6957-11e9-85a1-00163e2e65eb', '2019-04-28 09:43:21', '2019-04-29 09:43:23', '1', 'youshi', NULL, NULL, '2019-04-28 09:43:28', '1', '2019-04-28 09:43:28', '1', '2019-04-28 09:43:28', NULL, '0');
INSERT INTO `oa_leave` VALUES ('037d44b4d5534f37956486192719ae7b', '42915037-5607-11e9-a619-00163e2e65eb', '2019-04-03 19:54:25', '2019-04-25 19:54:30', '1', 'test', NULL, NULL, '2019-04-03 19:54:39', '1', '2019-04-03 19:54:39', '1', '2019-04-03 19:54:39', NULL, '0');
INSERT INTO `oa_leave` VALUES ('0448081fedcd40c790bd38bb72ddd41a', '48ffb36d-8827-11e9-997e-00163e2e65eb', '2019-06-06 14:49:43', '2019-06-20 14:49:49', '1', 'ce', NULL, NULL, '2019-06-06 14:49:52', '1', '2019-06-06 14:49:52', '1', '2019-06-06 14:49:52', NULL, '0');
INSERT INTO `oa_leave` VALUES ('086f29135b6c4fc3a9b610f5729f0e87', '54f55399-916c-11e9-997e-00163e2e65eb', '2019-06-18 09:56:34', '2019-06-18 09:56:31', '2', 'xxx', NULL, NULL, '2019-06-18 09:56:48', '1', '2019-06-18 09:56:48', '1', '2019-06-18 09:56:48', NULL, '0');
INSERT INTO `oa_leave` VALUES ('08bdc2376af6446c962666f71e154e11', 'c7ee1565-923d-11e9-997e-00163e2e65eb', '2019-06-19 10:55:43', '2019-06-20 10:55:55', '1', '1111', '[同意] sdfghjkl', NULL, '2019-06-19 10:56:06', '1', '2019-06-19 10:56:06', '1', '2019-06-19 10:56:06', NULL, '0');
INSERT INTO `oa_leave` VALUES ('0a15420a62894e458f2ca8bea26d5c99', '8b177cdd-8db5-11e9-997e-00163e2e65eb', '2019-06-13 16:30:33', '2019-06-13 16:30:44', '1', '11', NULL, NULL, '2019-06-13 16:30:47', '1', '2019-06-13 16:30:47', '1', '2019-06-13 16:30:47', NULL, '0');
INSERT INTO `oa_leave` VALUES ('0ab8d3ac0a514f48b3639ea30e1bdd1f', '5738e96d-6a31-11e9-85a1-00163e2e65eb', '2019-04-29 11:46:04', '2019-05-03 11:46:06', '5', '我结婚我请假', '[同意] 我批准你回家结婚', '[同意] 人事这边同意你回家结婚', '2019-04-29 11:46:16', '1', '2019-04-29 11:46:16', '1', '2019-04-29 11:46:16', NULL, '0');
INSERT INTO `oa_leave` VALUES ('0b4f37753f714aaebfd2ceb442ad12c2', 'd11aa416-40ab-11e9-924e-00163e2e65eb', '2019-03-08 08:00:00', '2019-03-08 14:00:00', '1', '妇女节休假', '[同意] 同意休假', '[同意] 同意休假', '2019-03-07 15:37:10', '68225048f4b7465293feb9779448d0af', '2019-03-07 15:37:10', '68225048f4b7465293feb9779448d0af', '2019-03-07 15:37:10', NULL, '0');
INSERT INTO `oa_leave` VALUES ('0bc132116e054e9185fb40125cd11ab8', '91a0bbb9-59ea-11e9-a619-00163e2e65eb', '2019-04-09 18:39:05', '2019-04-12 18:39:09', '1', '测试请假', '[同意] 3', NULL, '2019-04-08 18:39:21', '1', '2019-04-08 18:39:21', '1', '2019-04-08 18:39:21', NULL, '0');
INSERT INTO `oa_leave` VALUES ('0cbfb9a3415243618e78b163919adee5', '606f2f59-46cd-11e9-83f7-00163e2e65eb', '2019-03-15 10:52:20', '2019-03-16 10:52:22', '1', '世界真的很大啊', '[同意] 算了，你去吧', '[同意] 老板说的算', '2019-03-15 10:52:31', '1', '2019-03-15 10:52:31', '1', '2019-03-15 14:37:04', NULL, '0');
INSERT INTO `oa_leave` VALUES ('0eb6dd918e574058a7ae6ab9b6b957c6', '82fa0bbc-5515-11e9-a619-00163e2e65eb', '2019-04-02 15:03:53', '2019-04-03 15:04:04', '4', 'dfg', NULL, NULL, '2019-04-02 15:04:09', '1', '2019-04-02 15:04:09', '1', '2019-04-02 15:04:09', NULL, '0');
INSERT INTO `oa_leave` VALUES ('0efb7c6ec38e445eb71df9bd53ad4127', '18beb84f-6021-11e9-85a1-00163e2e65eb', '2019-04-16 16:24:35', '2019-04-27 16:24:37', '1', 'asd', '[同意] 已同意啦啦啦', NULL, '2019-04-16 16:24:48', '1', '2019-04-16 16:24:48', '1', '2019-04-16 16:24:48', NULL, '0');
INSERT INTO `oa_leave` VALUES ('0fb1762e87d74baeb0c7f30b236c398f', 'd8d37e05-5c62-11e9-a619-00163e2e65eb', '2019-04-17 22:04:59', '2019-04-18 22:05:01', '1', 'fgnhgfh', '[同意] vgjjhvjhgjg', NULL, '2019-04-11 22:05:23', '3', '2019-04-11 22:05:23', '3', '2019-04-11 22:05:23', NULL, '0');
INSERT INTO `oa_leave` VALUES ('10c35d7b5164455fbd990c81d54993ea', '412fd2ce-46ec-11e9-83f7-00163e2e65eb', '2019-03-15 14:33:17', '2019-03-16 14:33:20', '4', '世界这么大', '[同意] 你去，你去，你赶紧去', '[同意] 那我真去了', '2019-03-15 14:33:33', '1', '2019-03-15 14:33:33', '1', '2019-03-15 14:33:33', NULL, '0');
INSERT INTO `oa_leave` VALUES ('11b00aa7f1b548b7bfd6e29c429b1bc2', '09ab659d-866f-11e9-997e-00163e2e65eb', '2019-06-04 10:18:13', '2019-06-07 10:18:16', '1', '了，', NULL, NULL, '2019-06-04 10:18:27', '1', '2019-06-04 10:18:27', '1', '2019-06-04 10:18:27', NULL, '0');
INSERT INTO `oa_leave` VALUES ('12db5264a49149b184c2a0661500ea8c', 'bb743d30-463c-11e9-83f7-00163e2e65eb', '2019-03-14 17:36:44', '2019-03-15 17:36:47', '2', '打针', '[同意] test', NULL, '2019-03-14 17:37:07', '1', '2019-03-14 17:37:07', '1', '2019-03-14 17:37:07', NULL, '0');
INSERT INTO `oa_leave` VALUES ('15a47916d0a448129cc3f6cb835c6fa2', 'eae02d01-4a06-11e9-83f7-00163e2e65eb', '2019-03-20 13:21:44', '2019-03-21 13:21:49', '3', 'TEST', '[同意] 1233', NULL, '2019-03-19 13:21:58', '1', '2019-03-19 13:21:58', '1', '2019-03-19 13:21:58', NULL, '0');
INSERT INTO `oa_leave` VALUES ('16017aa44beb4021ab503103b2444a15', '3dc3097b-65a2-11e9-85a1-00163e2e65eb', '2019-04-23 16:31:44', '2019-04-26 16:31:46', '2', '222', '[驳回] 2222', NULL, '2019-04-23 16:31:51', '1', '2019-04-23 16:31:51', '1', '2019-04-24 17:42:08', NULL, '0');
INSERT INTO `oa_leave` VALUES ('16c519bceb5c427389694ec3ede98b43', 'aec0aa51-5092-11e9-a619-00163e2e65eb', '2019-03-27 21:17:22', '2019-03-28 21:17:24', '1', '调休', NULL, NULL, '2019-03-27 21:17:34', '1', '2019-03-27 21:17:34', '1', '2019-03-27 21:17:34', NULL, '0');
INSERT INTO `oa_leave` VALUES ('170878cc119c4d11b2e2484ee0c45c59', 'e632cbad-551a-11e9-a619-00163e2e65eb', '2019-04-02 15:42:34', '2019-04-19 15:42:36', '2', '旅游', NULL, NULL, '2019-04-02 15:42:43', '1', '2019-04-02 15:42:43', '1', '2019-04-02 15:42:43', NULL, '0');
INSERT INTO `oa_leave` VALUES ('1a74924743bc4c5d8850f0a2aed4213a', '7431cc9d-72d2-11e9-85a1-00163e2e65eb', '2019-05-10 11:19:39', '2019-05-17 11:19:44', '3', '12312323', '[同意] sdfghjk', NULL, '2019-05-10 11:19:43', '1', '2019-05-10 11:19:43', '1', '2019-05-10 11:19:43', NULL, '0');
INSERT INTO `oa_leave` VALUES ('1b7754b9c33b4d95a89cd4fe86e006cd', '6b18d4c0-4653-11e9-83f7-00163e2e65eb', '2019-03-14 20:19:22', '2019-03-14 20:19:24', '1', '从VS v', '[驳回] OK', NULL, '2019-03-14 20:19:30', '1', '2019-03-14 20:19:30', '1', '2019-03-19 14:15:51', NULL, '0');
INSERT INTO `oa_leave` VALUES ('1be40d52609c4dd3aecef0a413038d39', '090370d6-5df2-11e9-a619-00163e2e65eb', '2019-04-26 21:42:42', '2019-05-03 21:42:47', '2', '1632626', NULL, NULL, '2019-04-13 21:42:53', '1', '2019-04-13 21:42:53', '1', '2019-04-13 21:42:53', NULL, '0');
INSERT INTO `oa_leave` VALUES ('1c098d8174704b27bddf621bfede7ee7', '143d1d1b-6020-11e9-85a1-00163e2e65eb', '2019-04-16 16:17:25', '2019-04-17 16:17:27', '1', '111', '[同意] 同意', NULL, '2019-04-16 16:17:31', '1', '2019-04-16 16:17:31', '1', '2019-04-16 16:17:31', NULL, '0');
INSERT INTO `oa_leave` VALUES ('1cf3d1474c75492c9f39cca624ef66bb', '9b15ba6e-5da5-11e9-a619-00163e2e65eb', '2019-04-11 11:35:02', '2019-04-24 11:35:00', '1', '31', NULL, NULL, '2019-04-13 12:35:46', '1', '2019-04-13 12:35:46', '1', '2019-04-13 12:35:46', NULL, '0');
INSERT INTO `oa_leave` VALUES ('1e4ab79b65c743acbb78915c52b051a7', 'd23fa8ad-7627-11e9-85a1-00163e2e65eb', '2019-05-14 17:08:07', '2019-05-23 17:08:09', '1', '领导请假测试', '[同意] 任天堂', NULL, '2019-05-14 17:08:21', '2', '2019-05-14 17:08:21', '2', '2019-05-14 17:08:21', NULL, '0');
INSERT INTO `oa_leave` VALUES ('1e52666fae6d497285797e895b39f3fc', 'ad8c4a17-5c1d-11e9-a619-00163e2e65eb', '2019-04-11 13:44:33', '2019-04-12 13:44:36', '3', 'we', NULL, NULL, '2019-04-11 13:50:15', '1', '2019-04-11 13:50:15', '1', '2019-04-11 13:50:15', NULL, '0');
INSERT INTO `oa_leave` VALUES ('20edf137cea648e39083993a1e646d33', 'e988b38e-5dfd-11e9-a619-00163e2e65eb', '2019-04-13 23:07:42', '2019-04-27 23:07:45', '2', 'qwe', '[同意] 123', NULL, '2019-04-13 23:07:54', '1', '2019-04-13 23:07:54', '1', '2019-04-13 23:07:54', NULL, '0');
INSERT INTO `oa_leave` VALUES ('227ed81b80634b2ea145d6b1e1a516ac', 'aeb87f9b-56ca-11e9-a619-00163e2e65eb', '2019-04-04 19:12:54', '2019-04-26 19:12:56', '1', '11', NULL, NULL, '2019-04-04 19:13:33', '1', '2019-04-04 19:13:33', '1', '2019-04-04 19:13:33', NULL, '0');
INSERT INTO `oa_leave` VALUES ('241d35344b4a4859a065acbc2e33c3fe', 'ce0b91fe-5b40-11e9-a619-00163e2e65eb', '2019-04-10 11:29:02', '2019-04-10 11:29:00', '1', '2', '[同意] 杀杀杀', '[同意] 123123', '2019-04-10 11:29:10', '1', '2019-04-10 11:29:10', '1', '2019-04-10 11:29:10', NULL, '0');
INSERT INTO `oa_leave` VALUES ('2427838d998f4428a3fe734a4986a276', 'd7fe7da2-7068-11e9-85a1-00163e2e65eb', '2019-05-07 09:38:40', '2019-05-10 09:38:42', '1', '&middot;1213123', NULL, NULL, '2019-05-07 09:38:41', '1', '2019-05-07 09:38:41', '1', '2019-05-07 09:38:41', NULL, '0');
INSERT INTO `oa_leave` VALUES ('24ff8443bf884c5e8dfe9be26d18788c', '178a04dd-4960-11e9-83f7-00163e2e65eb', '2019-03-18 17:27:26', '2019-03-28 17:27:29', '4', '就是想请假', '[同意] 同意', '[同意] 11211', '2019-03-18 17:27:47', '1', '2019-03-18 17:27:47', '1', '2019-03-18 17:27:47', NULL, '0');
INSERT INTO `oa_leave` VALUES ('2505d089c3ce4726aec9198cc1d566cd', '5535aff3-4b1c-11e9-83f7-00163e2e65eb', '2019-03-20 22:27:37', '2019-03-20 22:27:40', '1', 'mm', '[驳回] zzzzzz', NULL, '2019-03-20 22:27:47', '1', '2019-03-20 22:27:47', '1', '2019-03-25 21:57:03', NULL, '0');
INSERT INTO `oa_leave` VALUES ('2847c1e5a48b4ef5ad33f50c2c2c1594', '4b8250dd-760b-11e9-85a1-00163e2e65eb', '2019-05-14 13:46:33', '2019-05-31 13:46:35', '2', '12123', '[同意] fddd', NULL, '2019-05-14 13:44:09', '1', '2019-05-14 13:44:09', '1', '2019-05-14 13:44:09', NULL, '0');
INSERT INTO `oa_leave` VALUES ('28f55bf3e4ca4fbd81de7cd604be2b56', '6838dec0-61d0-11e9-85a1-00163e2e65eb', '2019-04-18 19:52:06', '2019-04-20 19:52:08', '2', 'Fsuvjdb', '[同意] Hfhcu', NULL, '2019-04-18 19:52:14', '1', '2019-04-18 19:52:14', '1', '2019-04-18 19:52:14', NULL, '0');
INSERT INTO `oa_leave` VALUES ('293b4121361e47738b2b1b761aa1fb1b', 'c4607d07-4ae3-11e9-83f7-00163e2e65eb', '2019-03-01 15:42:43', '2019-03-14 15:42:48', '2', 'jjjjj', '[同意] 2424234', '[同意] tttt', '2019-03-20 15:42:52', '1', '2019-03-20 15:42:52', '1', '2019-03-20 15:42:52', NULL, '0');
INSERT INTO `oa_leave` VALUES ('2a04862887c6441c875bafdb3f8fdd80', '34d70451-40b7-11e9-924e-00163e2e65eb', '2019-03-07 16:58:33', '2019-03-23 16:58:35', '1', '111', '[同意] asdf', NULL, '2019-03-07 16:58:42', '1', '2019-03-07 16:58:42', '1', '2019-03-14 11:54:59', NULL, '0');
INSERT INTO `oa_leave` VALUES ('2a0fe9e777664a549d3da55f2b7bce0f', '7fa23596-6122-11e9-85a1-00163e2e65eb', '2019-04-17 23:07:13', '2019-04-19 23:07:15', '1', '22', NULL, NULL, '2019-04-17 23:07:21', '1', '2019-04-17 23:07:21', '1', '2019-04-17 23:07:21', NULL, '0');
INSERT INTO `oa_leave` VALUES ('2a559d3d33764903a59c53fb3b9f0f85', '788cf425-7c72-11e9-85a1-00163e2e65eb', '2019-05-22 17:17:42', '2019-05-16 17:17:45', '2', '11111', NULL, NULL, '2019-05-22 17:17:50', '1', '2019-05-22 17:17:50', '1', '2019-05-22 17:17:50', NULL, '0');
INSERT INTO `oa_leave` VALUES ('2bdfffde7965402799ed8db97a858f8b', 'e910dc41-7786-11e9-85a1-00163e2e65eb', '2019-05-13 11:01:22', '2019-05-31 11:01:27', '2', 'llll', NULL, NULL, '2019-05-16 11:01:33', '1', '2019-05-16 11:01:33', '1', '2019-05-16 11:01:33', NULL, '0');
INSERT INTO `oa_leave` VALUES ('2d13d3444fd7495fb4ef171bf83f0f72', 'fbac42a6-536a-11e9-a619-00163e2e65eb', '2019-03-31 12:11:45', '2019-03-31 12:11:48', '1', '111', '[同意] 同意', '[同意] 同意', '2019-03-31 12:10:57', '1', '2019-03-31 12:10:57', '1', '2019-03-31 12:10:57', NULL, '0');
INSERT INTO `oa_leave` VALUES ('2f3ebcb0b20d4805974203a3ec3fd8c1', 'a61e7b47-60ea-11e9-85a1-00163e2e65eb', '2019-04-17 16:27:28', '2019-04-18 16:27:30', '1', '去玩', '[同意] 同意手动', '[同意] 是', '2019-04-17 16:27:34', '2', '2019-04-17 16:27:34', '2', '2019-04-17 16:31:57', NULL, '0');
INSERT INTO `oa_leave` VALUES ('32e34a730d0c4bd0b617fa6cbb9fe6c2', '6fd581fe-4a16-11e9-83f7-00163e2e65eb', '2019-03-12 15:12:52', '2019-03-30 15:12:56', '2', '特斯塔', NULL, NULL, '2019-03-19 15:13:04', '1', '2019-03-19 15:13:04', '1', '2019-03-19 15:13:04', NULL, '0');
INSERT INTO `oa_leave` VALUES ('345182a9838f4c39b91e4552ba844b3f', '7126e48a-4f6b-11e9-a619-00163e2e65eb', '2019-03-05 10:03:57', '2019-03-29 10:04:00', '3', '423', '[同意] 顶顶顶顶顶顶顶', NULL, '2019-03-26 10:04:09', '1', '2019-03-26 10:04:09', '1', '2019-03-26 10:04:09', NULL, '0');
INSERT INTO `oa_leave` VALUES ('346754c6dd33432f95a76471bc6d9a3b', '196fe910-51f6-11e9-a619-00163e2e65eb', '2019-03-29 15:41:12', '2019-03-29 15:41:13', '1', 'asdasd', NULL, NULL, '2019-03-29 15:41:44', '1', '2019-03-29 15:41:44', '1', '2019-03-29 15:41:44', NULL, '0');
INSERT INTO `oa_leave` VALUES ('36d87e5086da4ab39947b81f65f9bbcf', 'ade1c01e-5a99-11e9-a619-00163e2e65eb', '2019-04-09 15:32:39', '2019-04-30 15:32:43', '5', '老子要结婚了', NULL, NULL, '2019-04-09 15:32:51', '1', '2019-04-09 15:32:51', '1', '2019-04-09 15:32:51', NULL, '0');
INSERT INTO `oa_leave` VALUES ('38e47159743447848979c803545c1fcd', '9190bc59-81eb-11e9-997e-00163e2e65eb', '2019-05-29 16:26:57', '2019-05-30 16:27:01', '2', '回家相亲', NULL, NULL, '2019-05-29 16:27:17', '1', '2019-05-29 16:27:17', '1', '2019-05-29 16:27:17', NULL, '0');
INSERT INTO `oa_leave` VALUES ('3a8656902387491eba6c773c2448b31b', '7804f0c3-542a-11e9-a619-00163e2e65eb', '2019-04-01 11:01:09', '2019-04-05 11:01:11', '5', '回家结婚', '[同意] 这个时间可以', '[同意] 同意', '2019-04-01 11:01:39', '1', '2019-04-01 11:01:39', '1', '2019-04-01 11:18:35', NULL, '0');
INSERT INTO `oa_leave` VALUES ('3c24af8f8c17428fb99a42d79f40f71b', '96caac04-70a5-11e9-85a1-00163e2e65eb', '2019-05-07 16:53:25', '2019-05-07 16:53:27', '2', 'xx', '[同意] 统一', '[同意] hhh', '2019-05-07 16:53:31', '1', '2019-05-07 16:53:31', '1', '2019-05-07 16:53:31', NULL, '0');
INSERT INTO `oa_leave` VALUES ('3d14a65519da4fa4b5f307e56eda00a0', '8a4e18dd-73a6-11e9-85a1-00163e2e65eb', '2019-05-11 12:37:44', '2019-05-31 12:37:45', '1', '*请假原因：*请假原因：*请假原因：', '[同意] 非官方个', NULL, '2019-05-11 12:37:53', '1', '2019-05-11 12:37:53', '1', '2019-05-11 12:37:53', NULL, '0');
INSERT INTO `oa_leave` VALUES ('3d299d0811a0411fb32bd30cc2d39c51', '4c561ee1-542b-11e9-a619-00163e2e65eb', '2019-04-01 11:06:49', '2019-04-03 11:06:51', '3', '事假2天', '[同意] 同意', '[同意] 41978', '2019-04-01 11:07:35', '1', '2019-04-01 11:07:35', '1', '2019-04-01 11:07:35', NULL, '0');
INSERT INTO `oa_leave` VALUES ('3e14b8d680ae40cea4619b62c6d8d787', 'a23211be-40b1-11e9-924e-00163e2e65eb', '2019-03-07 16:18:38', '2019-03-09 16:18:41', '4', '奥术大师', '[同意] TTT', NULL, '2019-03-07 16:18:49', '1', '2019-03-07 16:18:49', '1', '2019-03-07 16:18:49', NULL, '0');
INSERT INTO `oa_leave` VALUES ('4040f25ff7a543c58b459d53c3dc4494', '6637a07a-5673-11e9-a619-00163e2e65eb', '2019-04-04 08:48:37', '2019-04-11 08:48:39', '3', 'f', NULL, NULL, '2019-04-04 08:48:45', '1', '2019-04-04 08:48:45', '1', '2019-04-04 08:48:45', NULL, '0');
INSERT INTO `oa_leave` VALUES ('41b080b2d1504e20b4f8288600d7802b', '79a39fec-4c7d-11e9-83f7-00163e2e65eb', '2019-04-01 16:35:21', '2019-08-30 16:35:28', '5', '啊', '[同意] 11111', NULL, '2019-03-22 16:35:41', '1', '2019-03-22 16:35:41', '1', '2019-03-22 16:35:41', NULL, '0');
INSERT INTO `oa_leave` VALUES ('4275cc3c6f594aa38017e24da91f8d93', '6ece55a4-461e-11e9-83f7-00163e2e65eb', '2019-03-15 13:59:56', '2019-03-16 14:00:01', '1', 'asdadsadasdad', '[同意] 同意', NULL, '2019-03-14 14:00:13', '1', '2019-03-14 14:00:13', '1', '2019-03-14 14:00:13', NULL, '0');
INSERT INTO `oa_leave` VALUES ('4c4e2fb71266499db04df284b2d24fec', '52a512bc-4ece-11e9-a619-00163e2e65eb', '2019-03-25 15:19:16', '2019-03-26 15:19:18', '5', '请假', NULL, NULL, '2019-03-25 15:19:27', '1', '2019-03-25 15:19:27', '1', '2019-03-25 15:19:27', NULL, '0');
INSERT INTO `oa_leave` VALUES ('4e349c1ce3ff4e66a5d3fa5a0f05507c', '95ddc09b-7a9e-11e9-85a1-00163e2e65eb', '2019-05-20 09:28:27', '2019-05-24 09:28:29', '1', 'dasfdsa', NULL, NULL, '2019-05-20 09:28:35', '1', '2019-05-20 09:28:35', '1', '2019-05-20 09:28:35', NULL, '0');
INSERT INTO `oa_leave` VALUES ('4ea92a684fd24970927af683f8500728', '1599ea01-916a-11e9-997e-00163e2e65eb', '2019-06-18 09:40:37', '2019-06-18 09:40:34', '2', '1', NULL, NULL, '2019-06-18 09:40:43', '1', '2019-06-18 09:40:43', '1', '2019-06-18 09:40:43', NULL, '0');
INSERT INTO `oa_leave` VALUES ('4f004b39bfc345719b3b87278d93482f', '136f5c57-82b4-11e9-997e-00163e2e65eb', '2019-05-31 16:22:17', '2019-05-31 16:22:24', '2', '1234', NULL, NULL, '2019-05-30 16:22:34', '1', '2019-05-30 16:22:34', '1', '2019-05-30 16:22:34', NULL, '0');
INSERT INTO `oa_leave` VALUES ('4f4e7a6d3dbc4711bf6e41518b5452b7', 'ad7f97f6-47df-11e9-83f7-00163e2e65eb', '2019-03-16 19:35:46', '2019-03-18 19:35:49', '1', '11', '[同意] test', NULL, '2019-03-16 19:36:03', '1', '2019-03-16 19:36:03', '1', '2019-03-16 19:36:03', NULL, '0');
INSERT INTO `oa_leave` VALUES ('509b79414e2243a78956c702dc443b75', '1e4353d9-4f69-11e9-a619-00163e2e65eb', '2019-03-26 09:47:18', '2019-03-29 09:47:21', '1', '公休', '[同意] 同意', NULL, '2019-03-26 09:47:31', '1', '2019-03-26 09:47:31', '1', '2019-03-26 09:47:31', NULL, '0');
INSERT INTO `oa_leave` VALUES ('521945ee7b3742c1b632707033e10f3a', 'db04d131-4f98-11e9-a619-00163e2e65eb', '2019-03-26 15:29:05', '2019-03-30 15:29:08', '3', '多对多', '[同意] 顶顶顶顶', NULL, '2019-03-26 15:29:14', '1', '2019-03-26 15:29:14', '1', '2019-03-26 15:29:14', NULL, '0');
INSERT INTO `oa_leave` VALUES ('54116e2bc07642e2ad82a7f815ff2925', '80c161f6-60f2-11e9-85a1-00163e2e65eb', '2019-04-17 17:23:25', '2019-04-20 17:23:28', '2', '生病', NULL, NULL, '2019-04-17 17:23:47', '1', '2019-04-17 17:23:47', '1', '2019-04-17 17:23:47', NULL, '0');
INSERT INTO `oa_leave` VALUES ('54c848c1ea0d4734b1453f46e40d824f', '261bb4b8-463d-11e9-83f7-00163e2e65eb', '2019-03-14 17:39:58', '2019-03-14 17:40:01', '1', '1234', '[同意] 双方的', '[同意] 是DVD v', '2019-03-14 17:40:06', '1', '2019-03-14 17:40:06', '1', '2019-03-14 17:40:06', NULL, '0');
INSERT INTO `oa_leave` VALUES ('568f7969b6674232bb2bb8aaf3b54261', 'ed443442-5cd5-11e9-a619-00163e2e65eb', '2019-04-12 11:48:59', '2019-04-12 11:49:02', '2', '111', '[驳回] 1', NULL, '2019-04-12 11:49:09', '1', '2019-04-12 11:49:09', '1', '2019-04-15 17:27:16', NULL, '0');
INSERT INTO `oa_leave` VALUES ('56c2ab09dd354327aa832602b6f3e1c8', '829314be-60f2-11e9-85a1-00163e2e65eb', '2019-04-17 17:23:42', '2019-04-18 17:23:44', '1', '1111', NULL, NULL, '2019-04-17 17:23:50', '1', '2019-04-17 17:23:50', '1', '2019-04-17 17:23:50', NULL, '0');
INSERT INTO `oa_leave` VALUES ('58b1f840124147559e7e7241b86f742a', '2a28ca69-40c2-11e9-924e-00163e2e65eb', '2019-03-07 18:17:01', '2019-03-07 18:17:04', '1', '1111111', '[同意] 同意  啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊', NULL, '2019-03-07 18:17:09', '1', '2019-03-07 18:17:09', '1', '2019-03-07 18:17:09', NULL, '0');
INSERT INTO `oa_leave` VALUES ('5bac2a51a2074bc68c20627878166d1d', '313ced06-5aa9-11e9-a619-00163e2e65eb', '2019-04-09 17:23:43', '2019-04-10 17:23:46', '4', '66', NULL, NULL, '2019-04-09 17:23:53', '1', '2019-04-09 17:23:53', '1', '2019-04-09 17:23:53', NULL, '0');
INSERT INTO `oa_leave` VALUES ('5c05ad76feb146c9abcf57ba9373b419', '49f12c2a-7099-11e9-85a1-00163e2e65eb', '2019-05-07 15:25:23', '2019-05-08 15:25:26', '2', '1111', '[同意] 213123', NULL, '2019-05-07 15:25:28', '1', '2019-05-07 15:25:28', '1', '2019-05-07 15:25:28', NULL, '0');
INSERT INTO `oa_leave` VALUES ('5d3d0f78d50c448d98dcba63fbd49c1e', 'e61dd5ba-6fd6-11e9-85a1-00163e2e65eb', '2019-05-06 16:13:46', '2019-05-10 16:13:48', '4', '调休', '[同意] 通过', '[同意] 通过', '2019-05-06 16:13:59', '1', '2019-05-06 16:13:59', '1', '2019-05-06 16:13:59', NULL, '0');
INSERT INTO `oa_leave` VALUES ('5e16ff12976f46a29b5f1ea71104f061', '021df61a-8767-11e9-997e-00163e2e65eb', '2019-06-05 15:53:17', '2019-06-05 15:53:19', '2', '哼', NULL, NULL, '2019-06-05 15:53:30', '1', '2019-06-05 15:53:30', '1', '2019-06-05 15:53:30', NULL, '0');
INSERT INTO `oa_leave` VALUES ('616b37be339c412f93ac4698d782b6db', '2def4a64-8444-11e9-997e-00163e2e65eb', '2019-06-20 16:06:19', '2019-06-28 16:06:29', '1', '77', NULL, NULL, '2019-06-01 16:06:37', '1', '2019-06-01 16:06:37', '1', '2019-06-01 16:06:37', NULL, '0');
INSERT INTO `oa_leave` VALUES ('6268cbd8ff7e4fd895eb5a5f7340811e', 'fbd0c1e3-7a08-11e9-85a1-00163e2e65eb', '2019-05-19 15:37:34', '2019-05-25 15:37:36', '5', '结婚', NULL, NULL, '2019-05-19 15:37:41', '1', '2019-05-19 15:37:41', '1', '2019-05-19 15:37:41', NULL, '0');
INSERT INTO `oa_leave` VALUES ('627607b677b245e7be0320971c21a5c3', '0d7f9715-4c54-11e9-83f7-00163e2e65eb', '2019-03-22 11:39:03', '2019-03-30 11:39:06', '1', '3123', NULL, NULL, '2019-03-22 11:39:10', '1', '2019-03-22 11:39:10', '1', '2019-03-22 11:39:10', NULL, '0');
INSERT INTO `oa_leave` VALUES ('64c1b78d4d054a58bd5d65d73c77e06d', '301d6408-90ba-11e9-997e-00163e2e65eb', '2019-06-17 12:41:31', '2019-06-17 12:41:22', '2', '11', NULL, NULL, '2019-06-17 12:41:36', '2', '2019-06-17 12:41:36', '2', '2019-06-17 12:41:36', NULL, '0');
INSERT INTO `oa_leave` VALUES ('6925b3abfb42409b98a8d2d8ffb9def7', '7a787ab8-4b00-11e9-83f7-00163e2e65eb', '2019-02-25 19:08:15', '2019-03-22 19:08:16', '3', 'xdxdxdxdxc', '[同意] 1', NULL, '2019-03-20 19:08:24', '1', '2019-03-20 19:08:24', '1', '2019-03-20 19:08:24', NULL, '0');
INSERT INTO `oa_leave` VALUES ('6ab75326bd5246419d7672a02efe27e8', '7364bef2-77bc-11e9-85a1-00163e2e65eb', '2019-05-16 17:22:28', '2019-05-16 17:22:30', '1', '111', '[驳回] 111', NULL, '2019-05-16 17:24:48', '1', '2019-05-16 17:24:48', '1', '2019-05-17 10:58:15', NULL, '0');
INSERT INTO `oa_leave` VALUES ('6bcb9c71f38846cb8bcb0f133a329d3a', '3fb51d22-698d-11e9-85a1-00163e2e65eb', '2019-04-02 16:11:19', '2019-04-19 16:11:21', '1', 'fff', '[驳回] 2222222222', NULL, '2019-04-28 16:11:39', '1', '2019-04-28 16:11:39', '1', '2019-05-05 18:18:47', NULL, '0');
INSERT INTO `oa_leave` VALUES ('6ce2b0be227d4575a9650b31219e19a2', '4eae1277-4cab-11e9-83f7-00163e2e65eb', '2019-03-23 22:03:13', '2019-03-29 22:03:19', '3', '有事', '[同意] 同意', NULL, '2019-03-22 22:03:45', '1', '2019-03-22 22:03:45', '1', '2019-03-22 22:03:45', NULL, '0');
INSERT INTO `oa_leave` VALUES ('6d5f7b890eb041648db40c8a28f2cd11', 'add62bea-636d-11e9-85a1-00163e2e65eb', '2019-04-20 21:10:27', '2019-04-27 21:10:29', '1', 'fuyh', '[同意] syrh', '[同意] sdfzv', '2019-04-20 21:10:33', '1', '2019-04-20 21:10:33', '1', '2019-04-20 21:10:33', NULL, '0');
INSERT INTO `oa_leave` VALUES ('6efc3eb58ebb4a9cb790f0aa5d5f92a7', '1e5624df-6a5f-11e9-85a1-00163e2e65eb', '2019-04-29 17:13:49', '2019-04-30 17:13:52', '1', 'yy', NULL, NULL, '2019-04-29 17:13:57', '1', '2019-04-29 17:13:57', '1', '2019-04-29 17:13:57', NULL, '0');
INSERT INTO `oa_leave` VALUES ('6f4351a054184a67923eadcc88504dbe', '4a4d9ae2-627b-11e9-85a1-00163e2e65eb', '2019-04-19 16:15:07', '2019-04-20 16:15:10', '2', '生病', '[同意] gvdsf', NULL, '2019-04-19 16:15:28', '1', '2019-04-19 16:15:28', '1', '2019-04-19 16:15:28', NULL, '0');
INSERT INTO `oa_leave` VALUES ('71523890d1ed4985b5008f0c9660b64b', 'eca90150-6011-11e9-85a1-00163e2e65eb', '2019-04-16 14:35:57', '2019-04-30 14:36:00', '2', '生病', NULL, NULL, '2019-04-16 14:36:11', '1', '2019-04-16 14:36:11', '1', '2019-04-16 14:36:11', NULL, '0');
INSERT INTO `oa_leave` VALUES ('71594573269d45c686f9b388e393b6b5', '4a73df9f-55ff-11e9-a619-00163e2e65eb', '2019-04-03 18:57:28', '2019-04-10 18:57:26', '2', 'test', NULL, NULL, '2019-04-03 18:57:37', '1', '2019-04-03 18:57:37', '1', '2019-04-03 18:57:37', NULL, '0');
INSERT INTO `oa_leave` VALUES ('73e4988912664863b72447054eff8b05', '859975a2-50ff-11e9-a619-00163e2e65eb', '2019-03-28 10:16:31', '2019-03-30 10:16:35', '2', '55', NULL, NULL, '2019-03-28 10:16:40', '1', '2019-03-28 10:16:40', '1', '2019-03-28 10:16:40', NULL, '0');
INSERT INTO `oa_leave` VALUES ('73f9bf8dad42462ab761df6fc4b2208c', '2308d2cd-5168-11e9-a619-00163e2e65eb', '2019-03-28 22:45:23', '2019-04-05 22:45:25', '1', 'qj', '[驳回] 不同意。', NULL, '2019-03-28 22:45:32', '1', '2019-03-28 22:45:32', '1', '2019-03-31 23:18:39', NULL, '0');
INSERT INTO `oa_leave` VALUES ('74f9b935dba74e9b82df628ce52b362e', 'e52787f0-51e3-11e9-a619-00163e2e65eb', '2019-03-29 13:29:55', '2019-03-30 13:29:57', '1', '0254', NULL, NULL, '2019-03-29 13:31:26', '1', '2019-03-29 13:31:26', '1', '2019-03-29 13:31:26', NULL, '0');
INSERT INTO `oa_leave` VALUES ('78733d4cf90c4d01b34da5930e2e88cd', '82063c3f-5c26-11e9-a619-00163e2e65eb', '2019-04-01 14:52:59', '2019-04-11 14:53:01', '1', '111', NULL, NULL, '2019-04-11 14:53:27', '1', '2019-04-11 14:53:27', '1', '2019-04-11 14:53:27', NULL, '0');
INSERT INTO `oa_leave` VALUES ('791a3383ebbd4ae8b75e6421044fd799', 'eb1c9656-5daf-11e9-a619-00163e2e65eb', '2019-04-09 13:49:23', '2019-04-13 13:49:27', '1', '3', NULL, NULL, '2019-04-13 13:49:36', '1', '2019-04-13 13:49:36', '1', '2019-04-13 13:49:36', NULL, '0');
INSERT INTO `oa_leave` VALUES ('7956b3e649264bc3a227617deda14f47', 'effc9831-643a-11e9-85a1-00163e2e65eb', '2019-04-21 21:39:46', '2019-04-25 21:39:48', '1', 'r23w', NULL, NULL, '2019-04-21 21:39:51', '1', '2019-04-21 21:39:51', '1', '2019-04-21 21:39:51', NULL, '0');
INSERT INTO `oa_leave` VALUES ('7a13e725a8ca440cb84a752f55ed93e0', 'e5e0a333-62a9-11e9-85a1-00163e2e65eb', '2019-04-19 21:48:41', '2019-04-26 21:48:43', '2', '324234234', NULL, NULL, '2019-04-19 21:49:06', '1', '2019-04-19 21:49:06', '1', '2019-04-19 21:49:06', NULL, '0');
INSERT INTO `oa_leave` VALUES ('7c5aec783f1f4c0f9b09350d1bfca266', '0c0000db-56a5-11e9-a619-00163e2e65eb', '2019-04-05 09:30:00', '2019-04-05 18:30:00', '1', '11111', NULL, NULL, '2019-04-04 14:44:08', '1', '2019-04-04 14:44:08', '1', '2019-04-04 14:44:08', NULL, '0');
INSERT INTO `oa_leave` VALUES ('7c65880b0bde47eca2f5c8081c8db898', '8565f5e9-4ad2-11e9-83f7-00163e2e65eb', '2019-03-20 13:39:14', '2019-03-23 13:39:16', '1', '789', '[同意] 3444', NULL, '2019-03-20 13:39:25', '1', '2019-03-20 13:39:25', '1', '2019-03-20 13:39:25', NULL, '0');
INSERT INTO `oa_leave` VALUES ('7f935238ab6b4ae3a90b854b6aa0ddce', '9ed529ae-4928-11e9-83f7-00163e2e65eb', '2019-03-18 10:50:24', '2019-03-19 10:50:31', '2', 'test', '[同意] 2222', '[同意] 额鹅鹅鹅', '2019-03-18 10:50:42', '1', '2019-03-18 10:50:42', '1', '2019-03-18 10:50:42', NULL, '0');
INSERT INTO `oa_leave` VALUES ('80466f8e8ae24392a8ed620038b574b8', 'ef2f8dc9-680f-11e9-85a1-00163e2e65eb', '2019-04-26 18:41:49', '2019-04-26 18:41:51', '1', '第三方', NULL, NULL, '2019-04-26 18:42:06', '3', '2019-04-26 18:42:06', '3', '2019-04-26 18:42:06', NULL, '0');
INSERT INTO `oa_leave` VALUES ('80751c4152ce447c837946b3c6850d4c', '955e2f86-90ec-11e9-997e-00163e2e65eb', '2019-06-17 18:42:16', '2019-06-20 18:42:12', '2', '江小杰在测试', NULL, NULL, '2019-06-17 18:42:20', '1', '2019-06-17 18:42:20', '1', '2019-06-17 18:42:20', NULL, '0');
INSERT INTO `oa_leave` VALUES ('813ce9ec84434e75baf0ec5f23aa8872', 'e352492c-59ee-11e9-a619-00163e2e65eb', '2019-04-08 19:10:06', '2019-04-26 19:10:10', '1', '222', '[同意] 同意', NULL, '2019-04-08 19:10:16', '1', '2019-04-08 19:10:16', '1', '2019-04-08 19:10:16', NULL, '0');
INSERT INTO `oa_leave` VALUES ('834278fe078042b49953f896400919d5', '8cda6b49-7d2f-11e9-85a1-00163e2e65eb', '2019-05-23 15:51:12', '2019-05-23 15:51:14', '3', 'ter', '[同意] 12341234', '[同意] 111', '2019-05-23 15:51:19', '3', '2019-05-23 15:51:19', '3', '2019-05-23 15:51:19', NULL, '0');
INSERT INTO `oa_leave` VALUES ('83ced3a0218943c58a2beaacf6e900e6', 'bbf5ea21-5490-11e9-a619-00163e2e65eb', '2019-04-05 23:13:31', '2019-04-12 23:13:35', '1', '威威', NULL, NULL, '2019-04-01 23:13:42', '1', '2019-04-01 23:13:42', '1', '2019-04-01 23:13:42', NULL, '0');
INSERT INTO `oa_leave` VALUES ('847a202482714d85a2385e5766a40b1e', '9b6b32ca-8739-11e9-997e-00163e2e65eb', '2019-06-05 10:28:13', '2019-06-06 10:28:18', '1', 'as', NULL, NULL, '2019-06-05 10:28:30', '1', '2019-06-05 10:28:30', '1', '2019-06-05 10:28:30', NULL, '0');
INSERT INTO `oa_leave` VALUES ('84dcb9133c204a3f813631e605c06f5e', '66be0ab1-6825-11e9-85a1-00163e2e65eb', '2019-04-26 21:15:37', '2019-04-18 21:15:40', '2', '1', NULL, NULL, '2019-04-26 21:15:46', '1', '2019-04-26 21:15:46', '1', '2019-04-26 21:15:46', NULL, '0');
INSERT INTO `oa_leave` VALUES ('856468d52c364353a0af7b4887a2e61c', 'c81387f6-7bb9-11e9-85a1-00163e2e65eb', '2019-05-14 19:15:38', '2019-05-08 19:15:41', '2', '111', NULL, NULL, '2019-05-21 19:15:47', '1', '2019-05-21 19:15:47', '1', '2019-05-21 19:15:47', NULL, '0');
INSERT INTO `oa_leave` VALUES ('884517d5b5e44c979fe84d8ebe1edded', 'c86e5fd5-72e3-11e9-85a1-00163e2e65eb', '2019-05-10 13:23:28', '2019-05-10 19:23:29', '4', '加班调休', NULL, NULL, '2019-05-10 13:23:46', '1', '2019-05-10 13:23:46', '1', '2019-05-10 13:23:46', NULL, '0');
INSERT INTO `oa_leave` VALUES ('8979ba162d164848ba88dfcc25abf11f', 'd9f97036-4ae5-11e9-83f7-00163e2e65eb', '2019-03-29 15:57:27', '2019-03-30 15:57:32', '4', 'cxh结婚了，去吃喜酒', '[同意] 同意', NULL, '2019-03-20 15:57:48', '1', '2019-03-20 15:57:48', '1', '2019-03-20 15:57:48', NULL, '0');
INSERT INTO `oa_leave` VALUES ('89f8e3a341fc49229126ad895507871c', '6f3e3faf-4ae4-11e9-83f7-00163e2e65eb', '2019-03-20 15:47:34', '2019-03-04 15:47:35', '2', 'sdf', '[同意] 11111111', NULL, '2019-03-20 15:47:39', '1', '2019-03-20 15:47:39', '1', '2019-03-21 16:44:23', NULL, '0');
INSERT INTO `oa_leave` VALUES ('8b0ad071f01846d2ba6eb5bd51f2a9d1', '60436d25-760b-11e9-85a1-00163e2e65eb', '2019-05-14 13:47:07', '2019-05-30 13:47:10', '3', 'dddddd', '[驳回] dddddddd', NULL, '2019-05-14 13:44:44', '1', '2019-05-14 13:44:44', '1', '2019-05-16 10:14:54', NULL, '0');
INSERT INTO `oa_leave` VALUES ('8b557f7dc27f473b90a2f065c13dd1e8', 'ff0cfd78-47d8-11e9-83f7-00163e2e65eb', '2019-03-05 18:48:02', '2019-03-30 18:48:06', '1', '测试', '[同意] test', NULL, '2019-03-16 18:48:13', '1', '2019-03-16 18:48:13', '1', '2019-03-16 18:48:13', NULL, '0');
INSERT INTO `oa_leave` VALUES ('8c3a71af94e34b9abebae6edf1be1640', '10e53850-5c4f-11e9-a619-00163e2e65eb', '2019-04-11 19:45:52', '2019-04-11 19:45:54', '1', '11', NULL, NULL, '2019-04-11 19:43:47', '1', '2019-04-11 19:43:47', '1', '2019-04-11 19:43:47', NULL, '0');
INSERT INTO `oa_leave` VALUES ('8c4d11faf7134064812e765e4784366b', '2f17dfc7-5c30-11e9-a619-00163e2e65eb', '2019-04-11 16:02:41', '2019-04-12 16:02:47', '1', '111', NULL, NULL, '2019-04-11 16:02:43', '1', '2019-04-11 16:02:43', '1', '2019-04-11 16:02:43', NULL, '0');
INSERT INTO `oa_leave` VALUES ('8d709ba6b44441949109072227068154', '2091f825-8827-11e9-997e-00163e2e65eb', '2019-06-06 14:48:38', '2019-06-06 14:48:42', '1', 'ces', NULL, NULL, '2019-06-06 14:48:44', '1', '2019-06-06 14:48:44', '1', '2019-06-06 14:48:44', NULL, '0');
INSERT INTO `oa_leave` VALUES ('8ff694589eb94976a86bed5b2590a87c', 'a8560828-5ff4-11e9-85a1-00163e2e65eb', '2019-04-23 11:06:17', '2019-04-16 11:06:36', '2', '12', NULL, NULL, '2019-04-16 11:06:41', '2', '2019-04-16 11:06:41', '2', '2019-04-16 11:06:41', NULL, '0');
INSERT INTO `oa_leave` VALUES ('908272bb40de49c9b9d0471ce5cd7974', 'a7cd6d63-7852-11e9-85a1-00163e2e65eb', '2019-05-16 11:19:47', '2019-05-24 11:19:51', '1', 'jhgj', NULL, NULL, '2019-05-17 11:20:01', '1', '2019-05-17 11:20:01', '1', '2019-05-17 11:20:01', NULL, '0');
INSERT INTO `oa_leave` VALUES ('90fb4fcebe63455fadbb1c6ec4732c26', '8cce4de4-542e-11e9-a619-00163e2e65eb', '2019-04-01 11:30:45', '2019-04-17 11:30:46', '1', 'test', NULL, NULL, '2019-04-01 11:30:52', '1', '2019-04-01 11:30:52', '1', '2019-04-01 11:30:52', NULL, '0');
INSERT INTO `oa_leave` VALUES ('91e13526b6fa424181e4ee84bd7af5b4', 'b5e52189-542b-11e9-a619-00163e2e65eb', '2019-04-01 11:10:18', '2019-04-02 11:10:20', '1', '有事', '[同意] 统一', NULL, '2019-04-01 11:10:32', '1', '2019-04-01 11:10:32', '1', '2019-04-01 11:10:32', NULL, '0');
INSERT INTO `oa_leave` VALUES ('9209a6210a254ad8b8b467d7574a3541', '9aaecb07-4a56-11e9-83f7-00163e2e65eb', '2019-03-09 22:52:13', '2019-03-21 22:52:20', '5', 'kjhkjh', '[同意] 3244', NULL, '2019-03-19 22:52:23', '1', '2019-03-19 22:52:23', '1', '2019-03-19 22:52:23', NULL, '0');
INSERT INTO `oa_leave` VALUES ('937e19db5d1b4052a8b428a5e5e86d03', '4d06b9de-5aa9-11e9-a619-00163e2e65eb', '2019-04-11 17:24:31', '2019-04-18 17:24:35', '4', '6666', '[同意] approve', '[同意] approve', '2019-04-09 17:24:40', '1', '2019-04-09 17:24:40', '1', '2019-04-09 17:24:40', NULL, '0');
INSERT INTO `oa_leave` VALUES ('94e82084c56d4378924ba323408f3994', 'a53ec828-4edb-11e9-a619-00163e2e65eb', '2019-03-25 16:54:52', '2019-03-28 16:54:54', '1', '啥', NULL, NULL, '2019-03-25 16:54:49', '1', '2019-03-25 16:54:49', '1', '2019-03-25 16:54:49', NULL, '0');
INSERT INTO `oa_leave` VALUES ('950c7d99ed754126a463514749cd8ee5', '8ee2a735-551a-11e9-a619-00163e2e65eb', '2019-04-24 15:41:00', '2019-04-30 15:41:04', '5', 'ddaa', NULL, NULL, '2019-04-02 15:40:17', '1', '2019-04-02 15:40:17', '1', '2019-04-02 15:40:17', NULL, '0');
INSERT INTO `oa_leave` VALUES ('95c93852573745988912a99be676b7f6', 'df78f46f-82b8-11e9-997e-00163e2e65eb', '2019-05-30 16:58:57', '2019-05-31 16:59:04', '1', '222', NULL, NULL, '2019-05-30 16:56:55', '1', '2019-05-30 16:56:55', '1', '2019-05-30 16:56:55', NULL, '0');
INSERT INTO `oa_leave` VALUES ('97041a3de3a04f1987ceaa9a607526db', 'a5f35b48-523e-11e9-a619-00163e2e65eb', '2019-03-30 00:20:51', '2019-03-30 00:20:54', '1', '1', NULL, NULL, '2019-03-30 00:21:04', '1', '2019-03-30 00:21:04', '1', '2019-03-30 00:21:04', NULL, '0');
INSERT INTO `oa_leave` VALUES ('980ee6c57c3d44748e9a9849b91ccbdf', 'b88e920c-54f6-11e9-a619-00163e2e65eb', '2019-04-02 11:23:37', '2019-04-03 11:23:39', '1', '休息', NULL, NULL, '2019-04-02 11:23:45', '2', '2019-04-02 11:23:45', '2', '2019-04-02 11:23:45', NULL, '0');
INSERT INTO `oa_leave` VALUES ('990fe19b582246ad971ec45b8f637dde', '5fa53d68-53c8-11e9-a619-00163e2e65eb', '2019-03-05 23:19:22', '2019-03-19 23:19:24', '1', 'dfd', '[驳回] tttt', NULL, '2019-03-31 23:19:27', '1', '2019-03-31 23:19:27', '1', '2019-04-01 16:14:12', NULL, '0');
INSERT INTO `oa_leave` VALUES ('992e23109a28490684fb4a8a710f9a24', '8a3281c1-54f4-11e9-a619-00163e2e65eb', '2019-04-02 11:07:58', '2019-04-03 11:08:00', '1', '测试一下', '[同意] 12', '[同意] OK', '2019-04-02 11:08:08', '1', '2019-04-02 11:08:08', '1', '2019-04-02 11:08:08', NULL, '0');
INSERT INTO `oa_leave` VALUES ('9a0eb3d8e360447091a564752a5d2d84', '1e839884-7dc9-11e9-85a1-00163e2e65eb', '2019-05-24 10:10:25', '2019-05-25 10:10:26', '3', '办事', NULL, NULL, '2019-05-24 10:10:37', '1', '2019-05-24 10:10:37', '1', '2019-05-24 10:10:37', NULL, '0');
INSERT INTO `oa_leave` VALUES ('9b167e105685427f842a98a7af6d1ab0', '0aa7784e-7c61-11e9-85a1-00163e2e65eb', '2019-05-22 15:12:56', '2019-05-24 15:12:58', '1', '45454', NULL, NULL, '2019-05-22 15:13:04', '1', '2019-05-22 15:13:04', '1', '2019-05-22 15:13:04', NULL, '0');
INSERT INTO `oa_leave` VALUES ('9bf11b333374419cb4d6edeb7b125723', '133f707a-53c8-11e9-a619-00163e2e65eb', '2019-03-05 23:17:13', '2019-03-19 23:17:16', '1', 'ee', NULL, NULL, '2019-03-31 23:17:19', '1', '2019-03-31 23:17:19', '1', '2019-03-31 23:17:19', NULL, '0');
INSERT INTO `oa_leave` VALUES ('9c57f07cf62648e2b8a823381bced65e', '49035bd8-51c3-11e9-a619-00163e2e65eb', '2019-03-29 09:37:11', '2019-03-29 09:37:13', '3', '回家有点事情', NULL, NULL, '2019-03-29 09:38:00', '1', '2019-03-29 09:38:00', '1', '2019-03-29 09:38:00', NULL, '0');
INSERT INTO `oa_leave` VALUES ('9c5c594c24d745e7bdb5c62d8cdf4cfd', 'd9d2704d-70a6-11e9-85a1-00163e2e65eb', '2019-05-07 17:02:24', '2019-05-30 17:02:25', '4', '车祸', '[同意] 同意请假', '[同意] 也同意', '2019-05-07 17:02:33', '1', '2019-05-07 17:02:33', '1', '2019-05-07 17:02:33', NULL, '0');
INSERT INTO `oa_leave` VALUES ('9cfa5990805c465aa2ce9f8018405036', '9d2e1f38-4bb5-11e9-83f7-00163e2e65eb', '2019-03-14 16:44:40', '2019-03-16 16:44:43', '1', '34453', '[同意] 11', '[同意] 同意2', '2019-03-21 16:45:01', '1', '2019-03-21 16:45:01', '1', '2019-03-21 16:45:01', NULL, '0');
INSERT INTO `oa_leave` VALUES ('9df0780046214872a1d4684bb5b26105', 'f9ede8ef-560e-11e9-a619-00163e2e65eb', '2019-04-03 20:49:40', '2019-04-04 20:49:42', '1', 'aasa', NULL, NULL, '2019-04-03 20:49:53', '1', '2019-04-03 20:49:53', '1', '2019-04-03 20:49:53', NULL, '0');
INSERT INTO `oa_leave` VALUES ('9eb264cbfd0845b4b95aff32af75998b', 'da78b90d-75f6-11e9-85a1-00163e2e65eb', '2019-05-20 11:17:39', '2019-05-29 11:17:43', '5', '999', NULL, NULL, '2019-05-14 11:17:50', '1', '2019-05-14 11:17:50', '1', '2019-05-14 11:17:50', NULL, '0');
INSERT INTO `oa_leave` VALUES ('9f4e2d91955045ce9863e19279144f81', '77cfa152-62b1-11e9-85a1-00163e2e65eb', '2019-04-19 22:43:08', '2019-04-19 22:43:11', '2', '328/93', '[同意] 22', NULL, '2019-04-19 22:43:17', '2', '2019-04-19 22:43:17', '2', '2019-04-19 22:43:17', NULL, '0');
INSERT INTO `oa_leave` VALUES ('9f7a64cc167d4be0987db5cd041c1622', 'ef398be0-47d6-11e9-83f7-00163e2e65eb', '2019-03-16 18:33:13', '2019-03-31 18:33:15', '1', '休息', '[同意] test', NULL, '2019-03-16 18:33:27', '1', '2019-03-16 18:33:27', '1', '2019-03-16 18:33:27', NULL, '0');
INSERT INTO `oa_leave` VALUES ('a230e91aea7e4afaad0497d92d230b38', 'e96c2d29-5ab2-11e9-a619-00163e2e65eb', '2019-04-09 18:33:19', '2019-04-09 18:33:21', '1', '的点点滴滴', NULL, NULL, '2019-04-09 18:33:28', '1', '2019-04-09 18:33:28', '1', '2019-04-09 18:33:28', NULL, '0');
INSERT INTO `oa_leave` VALUES ('a2aff7abbcdd494197363e7a521f25b8', '1c1bf353-574e-11e9-a619-00163e2e65eb', '2019-04-05 10:54:07', '2019-04-26 10:54:09', '2', '测试', '[同意] 同意', '[同意] 是的，同意', '2019-04-05 10:54:20', '1', '2019-04-05 10:54:20', '1', '2019-04-05 10:54:20', NULL, '0');
INSERT INTO `oa_leave` VALUES ('a5c0c301584d4c1da0adc2b4397ef95b', '7af3aceb-6054-11e9-85a1-00163e2e65eb', '2019-04-16 22:32:32', '2019-04-25 22:32:35', '1', '矛', NULL, NULL, '2019-04-16 22:32:37', '1', '2019-04-16 22:32:37', '1', '2019-04-16 22:32:37', NULL, '0');
INSERT INTO `oa_leave` VALUES ('a5c556ea09ef407daa0411f4f48d0260', 'fa8f28f0-4c63-11e9-83f7-00163e2e65eb', '2019-03-14 13:33:02', '2019-03-30 13:33:06', '1', 'sdfsdf', NULL, NULL, '2019-03-22 13:33:10', '1', '2019-03-22 13:33:10', '1', '2019-03-22 13:33:10', NULL, '0');
INSERT INTO `oa_leave` VALUES ('a7b022ff97914f21aa89193221d892a1', '21c05f69-7782-11e9-85a1-00163e2e65eb', '2019-05-01 10:27:06', '2019-05-16 10:27:10', '3', '顶顶顶顶顶顶顶', NULL, NULL, '2019-05-16 10:27:21', '1', '2019-05-16 10:27:21', '1', '2019-05-16 10:27:21', NULL, '0');
INSERT INTO `oa_leave` VALUES ('a7f7124f56da4d21aaeb39dbdfe3c266', 'dd69eb83-56bb-11e9-a619-00163e2e65eb', '2019-04-04 17:27:18', '2019-04-12 17:27:21', '1', '11111', NULL, NULL, '2019-04-04 17:27:29', '1', '2019-04-04 17:27:29', '1', '2019-04-04 17:27:29', NULL, '0');
INSERT INTO `oa_leave` VALUES ('a8a5e8be078b41f895c9790a715a63f5', '8730ab83-568a-11e9-a619-00163e2e65eb', '2019-04-04 11:34:13', '2019-04-30 11:34:15', '1', '123123', NULL, NULL, '2019-04-04 11:34:19', '1', '2019-04-04 11:34:19', '1', '2019-04-04 11:34:19', NULL, '0');
INSERT INTO `oa_leave` VALUES ('a943a266ee514e8ba5c25d1dc4f0f13e', '5c3dfd55-75fe-11e9-85a1-00163e2e65eb', '2019-05-14 12:11:22', '2019-05-15 12:11:24', '2', '烦烦烦', NULL, NULL, '2019-05-14 12:11:34', '1', '2019-05-14 12:11:34', '1', '2019-05-14 12:11:34', NULL, '0');
INSERT INTO `oa_leave` VALUES ('a9fdf5d6974c4fbe91abdcea1950b620', '32d3d6b9-40b6-11e9-924e-00163e2e65eb', '2019-03-07 16:51:20', '2019-03-30 16:51:23', '2', '让挖人', '[同意] key', '[驳回] 不同意！！！！！！！！！', '2019-03-07 16:51:29', '1', '2019-03-07 16:51:29', '1', '2019-03-14 11:55:45', NULL, '0');
INSERT INTO `oa_leave` VALUES ('ab0641e593a6493ba0af7c4a382892f9', 'f69776ea-5122-11e9-a619-00163e2e65eb', '2019-03-28 14:30:29', '2019-04-04 14:30:27', '5', '2', NULL, NULL, '2019-03-28 14:30:22', '1', '2019-03-28 14:30:22', '1', '2019-03-28 14:30:22', NULL, '0');
INSERT INTO `oa_leave` VALUES ('accfd02b7a1143f0bf9669cc6b3e6989', 'afeb7280-7131-11e9-85a1-00163e2e65eb', '2019-05-03 09:36:24', '2019-05-08 09:36:27', '1', 'bojhb', NULL, NULL, '2019-05-08 09:36:23', '1', '2019-05-08 09:36:23', '1', '2019-05-08 09:36:23', NULL, '0');
INSERT INTO `oa_leave` VALUES ('ade5db18b59441819bd082814f0c0571', '8167f582-9191-11e9-997e-00163e2e65eb', '2019-06-18 14:21:54', '2019-06-18 14:21:58', '4', '23', NULL, NULL, '2019-06-18 14:22:54', '1', '2019-06-18 14:22:54', '1', '2019-06-18 14:22:54', NULL, '0');
INSERT INTO `oa_leave` VALUES ('ae56a15840f44022b947e9e982b62023', 'd1e784fb-4bb7-11e9-83f7-00163e2e65eb', '2019-03-20 17:00:39', '2019-03-22 17:00:42', '2', '454654645654', NULL, NULL, '2019-03-21 17:00:48', '1', '2019-03-21 17:00:48', '1', '2019-03-21 17:00:48', NULL, '0');
INSERT INTO `oa_leave` VALUES ('af3fd448c8e14aa78c2c8c95f14d90b2', '6ccaa855-72d2-11e9-85a1-00163e2e65eb', '2019-05-10 11:19:21', '2019-05-25 11:19:25', '1', '11', NULL, NULL, '2019-05-10 11:19:31', '1', '2019-05-10 11:19:31', '1', '2019-05-10 11:19:31', NULL, '0');
INSERT INTO `oa_leave` VALUES ('afbc753317f84e1c9df985c6f8c4a1f6', '84733644-4ae5-11e9-83f7-00163e2e65eb', '2019-03-21 15:55:14', '2019-03-29 15:55:17', '5', 'zc0000001', '[同意] 同意', NULL, '2019-03-20 15:55:24', '1', '2019-03-20 15:55:24', '1', '2019-03-20 15:55:24', NULL, '0');
INSERT INTO `oa_leave` VALUES ('afc141e24979463b85973671c37e70ac', 'c2408e8f-932e-11e9-997e-00163e2e65eb', '2019-06-20 15:40:51', '2019-06-21 15:40:53', '2', '1111', NULL, NULL, '2019-06-20 15:41:05', '1', '2019-06-20 15:41:05', '1', '2019-06-20 15:41:05', NULL, '0');
INSERT INTO `oa_leave` VALUES ('b1d232f963f54620ab95cfafb20026f1', 'ff75d2c4-8da6-11e9-997e-00163e2e65eb', '2019-06-14 14:46:19', '2019-06-21 14:46:27', '1', 'sad', NULL, NULL, '2019-06-13 14:46:40', '1', '2019-06-13 14:46:40', '1', '2019-06-13 14:46:40', NULL, '0');
INSERT INTO `oa_leave` VALUES ('b2183ea62a8d4260a405136caae18ab4', '842fe498-61a5-11e9-85a1-00163e2e65eb', '2019-04-18 14:45:04', '2019-04-26 14:45:05', '2', '测试123222', NULL, NULL, '2019-04-18 14:45:13', '1', '2019-04-18 14:45:13', '1', '2019-04-18 14:45:13', NULL, '0');
INSERT INTO `oa_leave` VALUES ('b2e5156746ed4a62ab953d414bed5447', 'b23d38aa-8cfa-11e9-997e-00163e2e65eb', '2019-06-12 18:13:11', '2019-06-12 18:13:13', '1', 'asda', NULL, NULL, '2019-06-12 18:13:17', '1', '2019-06-12 18:13:17', '1', '2019-06-12 18:13:17', NULL, '0');
INSERT INTO `oa_leave` VALUES ('b8600b949d6d4de5b11cc6eb870225af', 'aa94ed91-59da-11e9-a619-00163e2e65eb', '2019-04-09 16:45:40', '2019-04-12 16:45:42', '5', '之', '[同意] yes', '[同意] 123123213', '2019-04-08 16:45:31', '1', '2019-04-08 16:45:31', '1', '2019-04-08 16:45:31', NULL, '0');
INSERT INTO `oa_leave` VALUES ('bbb4b07f2635483f94fd1cd39cd3c2e9', 'be053cc7-695d-11e9-85a1-00163e2e65eb', '2019-04-28 10:31:29', '2019-04-25 10:31:31', '2', '123', NULL, NULL, '2019-04-28 10:31:35', '1', '2019-04-28 10:31:35', '1', '2019-04-28 10:31:35', NULL, '0');
INSERT INTO `oa_leave` VALUES ('bd0d4646e08a4352806b2a06cd6ada3b', '1a484ab8-5476-11e9-a619-00163e2e65eb', '2019-04-11 20:02:52', '2019-04-11 20:02:55', '2', '呃呃呃额', NULL, NULL, '2019-04-01 20:03:04', '1', '2019-04-01 20:03:04', '1', '2019-04-01 20:03:04', NULL, '0');
INSERT INTO `oa_leave` VALUES ('bfd1cab3fa5e4811a2d6a0eccbfa2d5d', '145378bb-71fd-11e9-85a1-00163e2e65eb', '2019-05-09 09:52:15', '2019-05-10 09:52:17', '3', '哈哈哈哈', NULL, NULL, '2019-05-09 09:52:19', '1', '2019-05-09 09:52:19', '1', '2019-05-09 09:52:19', NULL, '0');
INSERT INTO `oa_leave` VALUES ('c0b5d9adb79f4309a3c147c631f7038c', '2e3cae8d-7856-11e9-85a1-00163e2e65eb', '2019-05-17 11:44:49', '2019-05-25 11:44:51', '3', '三生三世', '[驳回] sfdfds', NULL, '2019-05-17 11:45:15', '1', '2019-05-17 11:45:15', '1', '2019-05-22 17:18:44', NULL, '0');
INSERT INTO `oa_leave` VALUES ('c1a9b00414bf4664b83c7790ec480a4c', '8150dc0e-5db3-11e9-a619-00163e2e65eb', '2019-04-13 14:15:11', '2019-04-13 14:15:13', '2', '1231', NULL, NULL, '2019-04-13 14:15:16', '1', '2019-04-13 14:15:16', '1', '2019-04-13 14:15:16', NULL, '0');
INSERT INTO `oa_leave` VALUES ('c420a00feb194a92834bf299ca468a6a', '65141b2f-4a56-11e9-83f7-00163e2e65eb', '2019-03-13 22:50:47', '2019-03-22 22:50:49', '2', 'kjkj', NULL, NULL, '2019-03-19 22:50:53', '1', '2019-03-19 22:50:53', '1', '2019-03-19 22:50:53', NULL, '0');
INSERT INTO `oa_leave` VALUES ('c5a5b6b1d46f45f6bf03497e1495e665', '116997ad-8b2e-11e9-997e-00163e2e65eb', '2019-06-10 11:15:36', '2019-06-10 11:15:41', '5', '病假', NULL, NULL, '2019-06-10 11:15:59', '1', '2019-06-10 11:15:59', '1', '2019-06-10 11:15:59', NULL, '0');
INSERT INTO `oa_leave` VALUES ('c623dfc130cc41fba930e6c46d1d5451', '69b16eac-40a8-11e9-934c-c85b7643dd9e', '2019-03-07 15:12:30', '2019-03-08 15:12:33', '2', '生病请假 啊啊啊啊啊啊啊啊  测试', '[同意] 同意11111111111111111111111', '[同意] 同意', '2019-03-07 15:12:48', '1', '2019-03-07 15:12:48', '1', '2019-03-07 15:12:48', NULL, '0');
INSERT INTO `oa_leave` VALUES ('c75df65a306448a981b3f14bf469c4e0', '7c030e43-5127-11e9-a619-00163e2e65eb', '2019-03-28 15:02:38', '2019-03-28 15:02:40', '4', 'cs', NULL, NULL, '2019-03-28 15:02:44', '1', '2019-03-28 15:02:44', '1', '2019-03-28 15:02:44', NULL, '0');
INSERT INTO `oa_leave` VALUES ('c7b90f0e0b3f4fcbaef48e23a5ad900f', 'e91255f9-6507-11e9-85a1-00163e2e65eb', '2019-04-22 22:07:12', '2019-04-25 22:07:14', '1', '就很快回家', NULL, NULL, '2019-04-22 22:07:06', '1', '2019-04-22 22:07:06', '1', '2019-04-22 22:07:06', NULL, '0');
INSERT INTO `oa_leave` VALUES ('c7f5d0686b434c05b3513d5e9741c1e4', '59498fd5-861c-11e9-997e-00163e2e65eb', '2019-06-03 00:26:16', '2019-06-06 00:26:22', '2', '呃呃e', NULL, NULL, '2019-06-04 00:26:33', '1', '2019-06-04 00:26:33', '1', '2019-06-04 00:26:33', NULL, '0');
INSERT INTO `oa_leave` VALUES ('c830b69bde0845f78248f85a3abd2ec5', '08e9f85e-4bac-11e9-83f7-00163e2e65eb', '2019-03-30 15:36:17', '2019-03-27 15:36:20', '1', '123', '[同意] 111', NULL, '2019-03-21 15:36:27', '1', '2019-03-21 15:36:27', '1', '2019-03-21 15:36:27', NULL, '0');
INSERT INTO `oa_leave` VALUES ('ca9e58b37c1e496d86202c94d299c205', 'b70e03b8-8cbc-11e9-997e-00163e2e65eb', '2019-06-12 10:49:29', '2019-06-12 10:49:33', '3', 'aa', NULL, NULL, '2019-06-12 10:49:36', '1', '2019-06-12 10:49:36', '1', '2019-06-12 10:49:36', NULL, '0');
INSERT INTO `oa_leave` VALUES ('ccb852c6224a4a31b90e3aca0ab9e70b', 'a0915666-81d4-11e9-85a1-00163e2e65eb', '2019-06-14 13:42:53', '2019-06-21 13:42:57', '1', 'u5', NULL, NULL, '2019-05-29 13:43:04', '1', '2019-05-29 13:43:04', '1', '2019-05-29 13:43:04', NULL, '0');
INSERT INTO `oa_leave` VALUES ('cefde5097bc0474ead755a7a1bf92ed5', 'f0123e20-53c9-11e9-a619-00163e2e65eb', '2019-03-31 23:29:46', '2019-04-02 23:29:48', '1', '456', NULL, NULL, '2019-03-31 23:30:39', '2', '2019-03-31 23:30:39', '2', '2019-03-31 23:30:39', NULL, '0');
INSERT INTO `oa_leave` VALUES ('d154ccab2c0949569a4d72f11f0f947a', '4ee28d10-6187-11e9-85a1-00163e2e65eb', '2019-04-18 11:08:42', '2019-04-26 11:08:45', '2', '打', NULL, NULL, '2019-04-18 11:08:58', '1', '2019-04-18 11:08:58', '1', '2019-04-18 11:08:58', NULL, '0');
INSERT INTO `oa_leave` VALUES ('d26fe4b3961e46de9ed7927ef39cb4f3', '6ed96873-6588-11e9-85a1-00163e2e65eb', '2019-04-09 13:26:58', '2019-04-11 13:27:01', '1', 'rrr', '[同意] 222', NULL, '2019-04-23 13:27:06', '1', '2019-04-23 13:27:06', '1', '2019-04-25 15:23:33', NULL, '0');
INSERT INTO `oa_leave` VALUES ('d31227dcf70b4d639c5cbe291df61cc5', 'd2e373c5-494a-11e9-83f7-00163e2e65eb', '2019-03-18 14:55:03', '2019-03-20 14:55:08', '2', '柔柔弱弱', '[同意] 对对对', '[同意] 222', '2019-03-18 14:55:33', '3', '2019-03-18 14:55:33', '3', '2019-03-18 14:55:33', NULL, '0');
INSERT INTO `oa_leave` VALUES ('d3823a06916745589d4fd5dcff3ffd2c', 'c6093dc5-6a46-11e9-85a1-00163e2e65eb', '2019-04-29 14:19:19', '2019-04-30 14:19:22', '3', 'luoxiqing', NULL, NULL, '2019-04-29 14:19:41', '2', '2019-04-29 14:19:41', '2', '2019-04-29 14:19:41', NULL, '0');
INSERT INTO `oa_leave` VALUES ('d776c24b8c3d4e94bece3f91372aee0e', '37d6ba49-54df-11e9-a619-00163e2e65eb', '2019-04-02 08:35:11', '2019-04-03 18:35:13', '2', '请病假', '[同意] 同意', '[同意] 同意', '2019-04-02 08:35:30', '2', '2019-04-02 08:35:30', '2', '2019-04-02 08:35:30', NULL, '0');
INSERT INTO `oa_leave` VALUES ('d7fa2c398846493da535f4d47e601cf9', 'fa6728d7-4f0b-11e9-a619-00163e2e65eb', '2019-03-25 22:41:09', '2019-03-27 22:41:11', '1', 'ewe', '[同意] 111111111', '[同意] 111111', '2019-03-25 22:40:48', '1', '2019-03-25 22:40:48', '1', '2019-03-25 22:40:48', NULL, '0');
INSERT INTO `oa_leave` VALUES ('d88bae6e8899498a8b277fb36226dacd', '992135d7-4ae4-11e9-83f7-00163e2e65eb', '2019-03-25 15:48:38', '2019-03-28 15:48:42', '3', 'xxoo', '[驳回] asdf', NULL, '2019-03-20 15:48:49', '1', '2019-03-20 15:48:49', '1', '2019-03-22 11:20:15', NULL, '0');
INSERT INTO `oa_leave` VALUES ('d8ed72090046486cad9daf846539c454', '2565a7a2-8760-11e9-997e-00163e2e65eb', '2019-06-05 15:03:58', '2019-06-14 15:04:05', '2', '刚刚', NULL, NULL, '2019-06-05 15:04:23', '1', '2019-06-05 15:04:23', '1', '2019-06-05 15:04:23', NULL, '0');
INSERT INTO `oa_leave` VALUES ('df1c12c442184caab80ca4c320b89966', 'b5d37856-5c73-11e9-a619-00163e2e65eb', '2019-04-12 00:06:44', '2019-04-26 00:06:46', '3', '测试请假流程', NULL, NULL, '2019-04-12 00:06:05', '1', '2019-04-12 00:06:05', '1', '2019-04-12 00:06:05', NULL, '0');
INSERT INTO `oa_leave` VALUES ('e0651eb60677499a8c0d4a0178d7c4a3', '16f1ab1d-6659-11e9-85a1-00163e2e65eb', '2019-04-10 14:20:22', '2019-04-20 14:20:29', '2', '222', '[同意] 同意', NULL, '2019-04-24 14:20:43', '2', '2019-04-24 14:20:43', '2', '2019-04-24 14:20:43', NULL, '0');
INSERT INTO `oa_leave` VALUES ('e0793d4308364cabb7f0d575ce274a40', 'ddb6e4b9-4f8b-11e9-a619-00163e2e65eb', '2019-03-26 13:56:07', '2019-03-26 13:56:09', '1', 'j', '[同意] 11111111111111', '[驳回] 不同意，辞职修长假', '2019-03-26 13:56:15', '1', '2019-03-26 13:56:15', '1', '2019-03-27 15:02:37', NULL, '0');
INSERT INTO `oa_leave` VALUES ('e1429ac59a8e42209f7bc9d40e1c1aab', '2f5bb0fd-72c6-11e9-85a1-00163e2e65eb', '2019-05-10 09:51:41', '2019-05-10 09:51:44', '1', '测试', NULL, NULL, '2019-05-10 09:51:54', '1', '2019-05-10 09:51:54', '1', '2019-05-10 09:51:54', NULL, '0');
INSERT INTO `oa_leave` VALUES ('e1ef06bb6fde463a94e312b4af94676d', 'abc61fac-76e4-11e9-85a1-00163e2e65eb', '2019-05-16 15:39:45', '2019-05-25 15:39:50', '4', '0515 test', '[同意] fffff', '[驳回] dddd', '2019-05-15 15:40:12', '3', '2019-05-15 15:40:12', '3', '2019-05-15 15:45:03', NULL, '0');
INSERT INTO `oa_leave` VALUES ('e340fe69500945e297364d42ba1ec0f9', '085bae11-505c-11e9-a619-00163e2e65eb', '2019-03-06 14:43:58', '2019-03-27 14:44:00', '1', 'aaa', NULL, NULL, '2019-03-27 14:46:22', '1', '2019-03-27 14:46:22', '1', '2019-03-27 14:46:22', NULL, '0');
INSERT INTO `oa_leave` VALUES ('e5eb948b4a1d45d981cfcd9f0e45cb62', '1d610f50-7160-11e9-85a1-00163e2e65eb', '2019-05-08 15:08:38', '2019-05-08 15:08:40', '2', 'sdfafas', NULL, NULL, '2019-05-08 15:08:44', '1', '2019-05-08 15:08:44', '1', '2019-05-08 15:08:44', NULL, '0');
INSERT INTO `oa_leave` VALUES ('e6245d4034b148deadf5817c4f55ca08', '312c932f-5cc8-11e9-a619-00163e2e65eb', '2019-04-12 10:10:39', '2019-04-27 10:10:44', '5', 'kkkkkkkk', '[驳回] aa', NULL, '2019-04-12 10:10:50', '3', '2019-04-12 10:10:50', '3', '2019-04-17 09:47:53', NULL, '0');
INSERT INTO `oa_leave` VALUES ('e6e2f77998c2419e89158bc21b60b0f1', '10d6f0b0-4a0e-11e9-83f7-00163e2e65eb', '2019-03-19 14:13:02', '2019-03-20 14:13:05', '2', '88', NULL, NULL, '2019-03-19 14:13:08', '1', '2019-03-19 14:13:08', '1', '2019-03-19 14:13:08', NULL, '0');
INSERT INTO `oa_leave` VALUES ('e6f6a93c980c497ea791c3b4ddad23cf', '5cfec067-5612-11e9-a619-00163e2e65eb', '2019-04-02 21:11:14', '2019-04-13 21:11:22', '3', '报名参加培训', NULL, NULL, '2019-04-03 21:14:08', '1', '2019-04-03 21:14:08', '1', '2019-04-03 21:14:08', NULL, '0');
INSERT INTO `oa_leave` VALUES ('e97a4c48dbab43ebb06226ddf845ffde', 'b89f9649-509b-11e9-a619-00163e2e65eb', '2019-03-28 22:22:08', '2019-03-29 22:22:12', '1', 'ff', NULL, NULL, '2019-03-27 22:22:16', '1', '2019-03-27 22:22:16', '1', '2019-03-27 22:22:16', NULL, '0');
INSERT INTO `oa_leave` VALUES ('ea3155cab1cc46c18726a700ffce37d3', 'a371f101-7c72-11e9-85a1-00163e2e65eb', '2019-05-22 17:18:57', '2019-05-24 17:18:59', '2', '123', '[驳回] 123', NULL, '2019-05-22 17:19:02', '1', '2019-05-22 17:19:02', '1', '2019-05-23 15:23:25', NULL, '0');
INSERT INTO `oa_leave` VALUES ('ebd91b2ec463466e8b4b6fe28618e26b', '89f64da4-49fb-11e9-83f7-00163e2e65eb', '2019-03-05 12:00:19', '2019-03-30 12:00:23', '1', 'test', '[同意] test', NULL, '2019-03-19 12:00:31', '1', '2019-03-19 12:00:31', '1', '2019-03-19 12:00:31', NULL, '0');
INSERT INTO `oa_leave` VALUES ('eff1caffbd4e44b9878179ec35755458', '34d5f482-4f6c-11e9-a619-00163e2e65eb', '2019-03-26 10:09:31', '2019-03-28 10:09:33', '1', '11111', '[同意] 11', NULL, '2019-03-26 10:09:37', '2', '2019-03-26 10:09:37', '2', '2019-03-26 10:09:37', NULL, '0');
INSERT INTO `oa_leave` VALUES ('f03f05ab00a54c1e98dd83a296aab5d9', '3ae46745-70a6-11e9-85a1-00163e2e65eb', '2019-05-07 16:58:02', '2019-05-08 16:58:04', '2', '121212', NULL, NULL, '2019-05-07 16:58:07', '1', '2019-05-07 16:58:07', '1', '2019-05-07 16:58:07', NULL, '0');
INSERT INTO `oa_leave` VALUES ('f4ba37b9181e48588cfe7bd4d73a7f45', '42212ba9-77a5-11e9-85a1-00163e2e65eb', '2019-05-16 14:38:41', '2019-05-17 14:38:43', '1', '测试', NULL, NULL, '2019-05-16 14:38:47', '1', '2019-05-16 14:38:47', '1', '2019-05-16 14:38:47', NULL, '0');
INSERT INTO `oa_leave` VALUES ('f52e046eb32b4802908b5b359db6ba5a', 'b61b4604-59ee-11e9-a619-00163e2e65eb', '2019-04-08 19:09:03', '2019-04-09 19:09:04', '2', 'g', NULL, NULL, '2019-04-08 19:09:00', '1', '2019-04-08 19:09:00', '1', '2019-04-08 19:09:00', NULL, '0');
INSERT INTO `oa_leave` VALUES ('f61ddf3e358b4e6caa768000b878dc8b', '1e179d33-60b2-11e9-85a1-00163e2e65eb', '2019-04-17 09:42:31', '2019-04-19 09:42:41', '1', '任性', NULL, NULL, '2019-04-17 09:42:54', '2', '2019-04-17 09:42:54', '2', '2019-04-17 09:42:54', NULL, '0');
INSERT INTO `oa_leave` VALUES ('f661b0b7b50e425798243814c4505a12', '48936b06-7c71-11e9-85a1-00163e2e65eb', '2019-05-24 17:09:14', '2019-05-31 17:09:16', '2', 'ffdsfsdf', NULL, NULL, '2019-05-22 17:09:20', '1', '2019-05-22 17:09:20', '1', '2019-05-22 17:09:20', NULL, '0');
INSERT INTO `oa_leave` VALUES ('f6a79538b53848818fa9893d68c703a0', 'af3f9bb0-582b-11e9-a619-00163e2e65eb', '2019-04-06 13:20:10', '2019-04-07 13:20:13', '2', '333', NULL, NULL, '2019-04-06 13:20:26', '1', '2019-04-06 13:20:26', '1', '2019-04-06 13:20:26', NULL, '0');
INSERT INTO `oa_leave` VALUES ('f6e767852eec477d960c82e7be935798', 'f5240958-7151-11e9-85a1-00163e2e65eb', '2019-05-08 13:26:56', '2019-05-22 13:26:59', '3', '十分士大夫', NULL, NULL, '2019-05-08 13:27:23', '1', '2019-05-08 13:27:23', '1', '2019-05-08 13:27:23', NULL, '0');
INSERT INTO `oa_leave` VALUES ('f725d0293f274c828f703de794364091', '08b473f1-4607-11e9-83f7-00163e2e65eb', '2019-03-01 11:12:38', '2019-03-14 11:12:42', '2', '1111', '[驳回] asdf', NULL, '2019-03-14 11:12:44', '1', '2019-03-14 11:12:44', '1', '2019-03-22 10:49:00', NULL, '0');
INSERT INTO `oa_leave` VALUES ('f8157833a04044a0809e6da5a2a61ba0', 'a2cd4513-7237-11e9-85a1-00163e2e65eb', '2019-05-08 16:51:21', '2019-05-15 16:51:24', '2', 'fff', NULL, NULL, '2019-05-09 16:51:29', '1', '2019-05-09 16:51:29', '1', '2019-05-09 16:51:29', NULL, '0');
INSERT INTO `oa_leave` VALUES ('fa2e8f270699446188d90f0cf45339ff', '258c42ff-5c32-11e9-a619-00163e2e65eb', '2019-04-12 16:16:35', '2019-04-12 16:16:41', '2', 'qq', NULL, NULL, '2019-04-11 16:16:46', '1', '2019-04-11 16:16:46', '1', '2019-04-11 16:16:46', NULL, '0');
INSERT INTO `oa_leave` VALUES ('fb2901c1161b4487b85fb48a9c83d0d4', '20482e45-76da-11e9-85a1-00163e2e65eb', '2019-05-16 14:24:17', '2019-05-22 14:24:23', '1', '出去旅游', '[同意] ddd', NULL, '2019-05-15 14:24:43', '3', '2019-05-15 14:24:43', '3', '2019-05-15 14:24:43', NULL, '0');
INSERT INTO `oa_leave` VALUES ('fbce0078d4fb448282d5d3ad611076f6', '51f018c3-6020-11e9-85a1-00163e2e65eb', '2019-04-17 16:19:00', '2019-04-18 16:19:05', '1', '12312312', '[同意] 同意', NULL, '2019-04-16 16:19:14', '2', '2019-04-16 16:19:14', '2', '2019-04-16 16:19:14', NULL, '0');
INSERT INTO `oa_leave` VALUES ('fc5de5014e1e4234b65676d0797fae44', 'bab7137b-5f60-11e9-85a1-00163e2e65eb', '2019-04-16 17:27:42', '2019-04-26 17:27:44', '3', '盛大的共同飞天狗', '[同意] 同意', NULL, '2019-04-15 17:27:47', '1', '2019-04-15 17:27:47', '1', '2019-04-15 17:27:47', NULL, '0');
INSERT INTO `oa_leave` VALUES ('fcd2aa18bcb14d019fcf4dff67f2de43', '3d14a529-59fa-11e9-a619-00163e2e65eb', '2019-04-09 20:31:32', '2019-04-10 20:31:37', '1', 'fd', '[同意] m\'', NULL, '2019-04-08 20:31:31', '1', '2019-04-08 20:31:31', '1', '2019-04-08 20:31:31', NULL, '0');
INSERT INTO `oa_leave` VALUES ('fcf94d954db64208bbe7282760f43457', 'f3a6e515-6a36-11e9-85a1-00163e2e65eb', '2019-04-29 12:26:19', '2019-04-30 12:26:21', '1', '123123', '[同意] 13123123', '[同意] hahahh', '2019-04-29 12:26:26', '1', '2019-04-29 12:26:26', '1', '2019-04-29 12:26:26', NULL, '0');
INSERT INTO `oa_leave` VALUES ('fd062b9dd8ba4156bbf18a5ff05982ee', '823d0439-59da-11e9-a619-00163e2e65eb', '2019-04-08 16:44:08', '2019-04-18 16:44:17', '4', '加班调休', NULL, NULL, '2019-04-08 16:44:23', '1', '2019-04-08 16:44:23', '1', '2019-04-08 16:44:23', NULL, '0');
INSERT INTO `oa_leave` VALUES ('fd1942d12f904bc8838609c5ccc448a1', 'e8bf69af-5461-11e9-a619-00163e2e65eb', '2019-04-01 17:38:22', '2019-04-01 17:38:25', '2', '111', NULL, NULL, '2019-04-01 17:38:30', '1', '2019-04-01 17:38:30', '1', '2019-04-01 17:38:30', NULL, '0');
INSERT INTO `oa_leave` VALUES ('fd34d61cb93e4f5a93984f8b8263faff', 'e78dd590-55e3-11e9-a619-00163e2e65eb', '2019-04-03 15:41:15', '2019-04-04 15:41:17', '1', '有事', '[同意] 同意', '[同意] ok', '2019-04-03 15:41:34', '1', '2019-04-03 15:41:34', '1', '2019-04-03 15:41:34', NULL, '0');
INSERT INTO `oa_leave` VALUES ('feb262ccbd14488c98252f127aac0f7a', '2b684e0c-5430-11e9-a619-00163e2e65eb', '2019-04-02 11:42:16', '2019-04-04 11:42:20', '1', 'ttttttt', '[同意] ttt', NULL, '2019-04-01 11:42:27', '1', '2019-04-01 11:42:27', '1', '2019-04-01 11:42:27', NULL, '0');
INSERT INTO `oa_leave` VALUES ('ff2094d1db334313b38f2a23c5849362', 'c3e01ea5-8835-11e9-997e-00163e2e65eb', '2019-06-06 16:33:23', '2019-06-06 16:33:31', '2', 'ces', NULL, NULL, '2019-06-06 16:33:31', '1', '2019-06-06 16:33:31', '1', '2019-06-06 16:33:31', NULL, '0');
INSERT INTO `oa_leave` VALUES ('ff62af7229b44bf3a823f808167d84ea', 'a8938228-51e9-11e9-a619-00163e2e65eb', '2019-03-29 14:12:35', '2019-03-30 14:12:37', '1', 'fffffff', NULL, NULL, '2019-03-29 14:12:41', '1', '2019-03-29 14:12:41', '1', '2019-03-29 14:12:41', NULL, '0');
INSERT INTO `oa_leave` VALUES ('ff73726a6e3a4f539dd4f015e8782384', '7c72e9a5-76e3-11e9-85a1-00163e2e65eb', '2019-05-15 15:31:19', '2019-05-30 15:31:21', '1', 'dddsy', '[同意] yyyyyyyy', '[同意] dddd', '2019-05-15 15:31:43', '1', '2019-05-15 15:31:43', '1', '2019-05-15 15:31:43', NULL, '0');

-- ----------------------------
-- Table structure for oa_notify
-- ----------------------------
DROP TABLE IF EXISTS `oa_notify`;
CREATE TABLE `oa_notify`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '类型',
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '标题',
  `content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '内容',
  `files` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '附件',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '状态',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oa_notify_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '通知通告' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for oa_notify_record
-- ----------------------------
DROP TABLE IF EXISTS `oa_notify_record`;
CREATE TABLE `oa_notify_record`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `oa_notify_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '通知通告ID',
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '接受人',
  `read_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '阅读标记',
  `read_date` date NULL DEFAULT NULL COMMENT '阅读时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oa_notify_record_notify_id`(`oa_notify_id`) USING BTREE,
  INDEX `oa_notify_record_user_id`(`user_id`) USING BTREE,
  INDEX `oa_notify_record_read_flag`(`read_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '通知通告发送记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_area
-- ----------------------------
DROP TABLE IF EXISTS `sys_area`;
CREATE TABLE `sys_area`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `parent_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '所有父级编号',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '名称',
  `sort` decimal(10, 0) NOT NULL COMMENT '排序',
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '区域编码',
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '区域类型',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_area_parent_id`(`parent_id`) USING BTREE,
  INDEX `sys_area_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '区域表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_area
-- ----------------------------
INSERT INTO `sys_area` VALUES ('015345144e9e40d6bbe832a9f1cba1f7', 'd36735ce07044a49ad205854fb2ca078', '0,d36735ce07044a49ad205854fb2ca078,', 'California', 10, '01', '2', '1', '2018-06-20 17:32:51', '1', '2019-01-02 00:11:18', '', '0');
INSERT INTO `sys_area` VALUES ('1', '0', '0,', '中国', 10, '1', '1', '1', '2013-05-27 08:00:00', '1', '2019-03-11 09:19:57', '', '0');
INSERT INTO `sys_area` VALUES ('16166c3ecb954be0ad47159c087cd8af', 'd36735ce07044a49ad205854fb2ca078', '0,d36735ce07044a49ad205854fb2ca078,', '明尼苏达', 30, '', '2', '1', '2019-01-25 17:40:25', '1', '2019-01-25 17:40:25', '', '0');
INSERT INTO `sys_area` VALUES ('3056b09b396f4247bce94a553822afdf', '0', '0,', '美国', 30, '', '1', '1', '2019-04-16 15:57:38', '1', '2019-12-27 14:31:59', '', '0');
INSERT INTO `sys_area` VALUES ('3fbaf3a3f58a47118f13976cb2e2544d', 'd36735ce07044a49ad205854fb2ca078', '0,d36735ce07044a49ad205854fb2ca078,', '佛罗里达州', 20, '02', '2', '1', '2018-06-20 17:38:08', '1', '2018-06-20 17:38:08', '', '0');
INSERT INTO `sys_area` VALUES ('a3a704fb719048aaa0be2d36300dd909', 'da2da01621b64be5a85b07a8c883228f', '0,1,da2da01621b64be5a85b07a8c883228f,', '青浦区', 30, '', '4', '1', '2019-04-17 09:35:58', '1', '2019-04-18 10:53:01', '', '0');
INSERT INTO `sys_area` VALUES ('d36735ce07044a49ad205854fb2ca078', '0', '0,', '美国', 30, '2', '1', '1', '2018-06-20 15:36:19', '1', '2018-06-20 15:36:19', '', '0');
INSERT INTO `sys_area` VALUES ('da2da01621b64be5a85b07a8c883228f', '1', '0,1,', '上海', 30, '34243', '2', '1', '2018-12-10 14:01:09', '1', '2019-04-17 09:35:58', '', '0');
INSERT INTO `sys_area` VALUES ('f762fb569d8445f28a226a9b831cfb90', '1', '0,1,', '北京', 30, '150', '2', '1', '2018-02-02 17:37:51', '1', '2018-11-20 10:22:36', '', '0');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `value` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '数据值',
  `label` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '标签名',
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '类型',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '描述',
  `sort` decimal(10, 0) NOT NULL COMMENT '排序（升序）',
  `parent_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_dict_value`(`value`) USING BTREE,
  INDEX `sys_dict_label`(`label`) USING BTREE,
  INDEX `sys_dict_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '字典表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', '0', '正常', 'del_flag', '删除标记', 10, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('10', 'yellow', '黄色', 'color', '颜色值', 40, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('100', 'java.util.Date', 'Date', 'gen_java_type', 'Java类型', 50, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('101', 'com.jsite.modules.sys.entity.User', 'User', 'gen_java_type', 'Java类型', 60, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('102', 'com.jsite.modules.sys.entity.Office', 'Office', 'gen_java_type', 'Java类型', 70, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('103', 'com.jsite.modules.sys.entity.Area', 'Area', 'gen_java_type', 'Java类型', 80, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('104', 'Custom', 'Custom', 'gen_java_type', 'Java类型', 90, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('105', '1', '会议通告\0\0', 'oa_notify_type', '通知通告类型', 10, '0', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('106', '2', '奖惩通告\0\0', 'oa_notify_type', '通知通告类型', 20, '0', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('107', '3', '活动通告\0\0', 'oa_notify_type', '通知通告类型', 30, '0', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('108', '0', '草稿', 'oa_notify_status', '通知通告状态', 10, '0', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('109', '1', '发布', 'oa_notify_status', '通知通告状态', 20, '0', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('11', 'orange', '橙色', 'color', '颜色值', 50, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('110', '0', '未读', 'oa_notify_read', '通知通告状态', 10, '0', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('111', '1', '已读', 'oa_notify_read', '通知通告状态', 20, '0', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('112', 'zh_CN', '简体中文', 'sys_lang_type', '国际化多语言类型', 30, '0', '1', '2019-03-22 08:00:00', '1', '2019-03-22 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('113', 'en_US', 'English', 'sys_lang_type', '国际化多语言类型', 60, '0', '1', '2019-03-22 08:00:00', '1', '2019-03-22 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('17', '1', '国家', 'sys_area_type', '区域类型', 10, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('18', '2', '省份/直辖市', 'sys_area_type', '区域类型', 20, '0', '1', '2013-05-27 08:00:00', '1', '2018-06-15 14:46:37', '', '0');
INSERT INTO `sys_dict` VALUES ('19', '3', '地市/州', 'sys_area_type', '区域类型', 30, '0', '1', '2013-05-27 08:00:00', '1', '2018-06-15 14:47:38', '', '0');
INSERT INTO `sys_dict` VALUES ('2', '1', '删除', 'del_flag', '删除标记', 20, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('20', '4', '县/区', 'sys_area_type', '区域类型', 40, '0', '1', '2013-05-27 08:00:00', '1', '2018-06-15 14:48:01', '', '0');
INSERT INTO `sys_dict` VALUES ('21', '1', '公司', 'sys_office_type', '机构类型', 60, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('22', '2', '部门', 'sys_office_type', '机构类型', 70, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('23', '3', '小组', 'sys_office_type', '机构类型', 80, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('24', '4', '其它', 'sys_office_type', '机构类型', 90, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('25', '1', '综合部', 'sys_office_common', '快捷通用部门', 30, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('26', '2', '开发部', 'sys_office_common', '快捷通用部门', 40, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('263374341bb04a88a149456094a5bd4f', 'WAITING', '等待执行', 'sys_job_status', '定时任务状态（PAUSED-暂停，ACQUIRED-正在执行，WAITING-等待执行）', 30, '0', '1', '2019-01-10 11:58:07', '1', '2019-01-11 12:02:01', '', '0');
INSERT INTO `sys_dict` VALUES ('26f56c93297146ca869fb1924569d5fc', '0', '停用', 'sys_user_status', '用户状态（0-停用，1-正常）', 10, '0', '1', '2018-06-29 08:40:36', '1', '2020-01-03 09:12:19', '', '0');
INSERT INTO `sys_dict` VALUES ('27', '3', '人力部', 'sys_office_common', '快捷通用部门', 50, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('28', '1', '一级', 'sys_office_grade', '机构等级', 10, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('29', '2', '二级', 'sys_office_grade', '机构等级', 20, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('3', '1', '显示', 'show_hide', '显示/隐藏', 10, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('30', '3', '三级', 'sys_office_grade', '机构等级', 30, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('31', '4', '四级', 'sys_office_grade', '机构等级', 40, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('32', '1', '所有数据', 'sys_data_scope', '数据范围', 10, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('33', '2', '所在公司及以下数据', 'sys_data_scope', '数据范围', 20, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('34', '3', '所在公司数据', 'sys_data_scope', '数据范围', 30, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('35', '4', '所在部门及以下数据', 'sys_data_scope', '数据范围', 40, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('36', '5', '所在部门数据', 'sys_data_scope', '数据范围', 50, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('37', '8', '仅本人数据', 'sys_data_scope', '数据范围', 90, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('38', '9', '按明细设置', 'sys_data_scope', '数据范围', 100, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('39', '1', '系统管理', 'sys_user_type', '用户类型', 10, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('4', '0', '隐藏', 'show_hide', '显示/隐藏', 20, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('40', '2', '部门经理', 'sys_user_type', '用户类型', 20, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('41', '3', '普通用户', 'sys_user_type', '用户类型', 30, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('49fefff665614e80b8c0e91902f2c817', 'images/userinfo.png', '默认头像', 'default_headphoto_big', '默认头像(大图)', 10, '0', '1', '2018-07-30 11:58:29', '1', '2018-07-30 13:41:11', '默认头像(原图)', '0');
INSERT INTO `sys_dict` VALUES ('5', '1', '是', 'yes_no', '是/否', 10, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('5ff3843961654e639a419410afc89bbc', '4', '人力资源', 'sys_user_type', '用户类型', 10, '0', '1', '2018-12-28 13:51:04', '1', '2018-12-28 13:51:04', '', '0');
INSERT INTO `sys_dict` VALUES ('6', '0', '否', 'yes_no', '是/否', 20, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('62', '1', '公休', 'oa_leave_type', '请假类型', 10, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('63', '2', '病假', 'oa_leave_type', '请假类型', 20, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('631f38d9f5e24dc88fdb472f4855d68d', '1', '正常', 'sys_user_status', '用户状态（0-停用，1-正常）', 20, '0', '1', '2018-06-29 08:41:42', '1', '2018-06-29 08:41:42', '', '0');
INSERT INTO `sys_dict` VALUES ('64', '3', '事假', 'oa_leave_type', '请假类型', 30, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('6437d5741af64dca861fe5bec7994891', '1', '显示', 'sys_show_hide', '显示-隐藏', 10, '0', '1', '2018-06-25 18:21:38', '1', '2018-06-25 18:21:38', '显示-隐藏', '0');
INSERT INTO `sys_dict` VALUES ('65', '4', '调休', 'oa_leave_type', '请假类型', 40, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('66', '5', '婚假', 'oa_leave_type', '请假类型', 60, '0', '1', '2013-05-27 08:00:00', '1', '2020-01-03 09:17:39', '', '0');
INSERT INTO `sys_dict` VALUES ('67', '1', '接入日志', 'sys_log_type', '日志类型', 30, '0', '1', '2013-06-03 08:00:00', '1', '2020-01-03 09:11:48', '', '0');
INSERT INTO `sys_dict` VALUES ('68', '2', '异常日志', 'sys_log_type', '日志类型', 40, '0', '1', '2013-06-03 08:00:00', '1', '2013-06-03 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('7', 'red', '红色', 'color', '颜色值', 10, '0', '1', '2013-05-27 08:00:00', '1', '2020-01-06 01:17:03', '', '0');
INSERT INTO `sys_dict` VALUES ('71', 'leave', '请假流程', 'act_category', '流程分类', 10, '0', '1', '2013-06-03 08:00:00', '1', '2020-01-03 17:10:34', '', '0');
INSERT INTO `sys_dict` VALUES ('72', 'metting', '会议室申请流程', 'act_category', '流程分类', 20, '0', '1', '2013-06-03 08:00:00', '1', '2013-06-03 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('73', 'crud', '增删改查', 'gen_category', '代码生成分类', 10, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('74', 'crud_many', '增删改查（包含从表）', 'gen_category', '代码生成分类', 20, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('75', 'tree', '树结构', 'gen_category', '代码生成分类', 30, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('76', '=', '=', 'gen_query_type', '查询方式', 10, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('77', '!=', '!=', 'gen_query_type', '查询方式', 20, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('78', '&gt;', '&gt;', 'gen_query_type', '查询方式', 30, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('79', '&lt;', '&lt;', 'gen_query_type', '查询方式', 40, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('8', 'green', '绿色', 'color', '颜色值', 20, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('80', 'between', 'Between', 'gen_query_type', '查询方式', 50, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('81', 'like', 'Like', 'gen_query_type', '查询方式', 60, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('82', 'left_like', 'Left Like', 'gen_query_type', '查询方式', 70, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('83', 'right_like', 'Right Like', 'gen_query_type', '查询方式', 80, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('84', 'input', '文本框', 'gen_show_type', '字段生成方案', 10, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('85', 'textarea', '文本域', 'gen_show_type', '字段生成方案', 20, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('86', 'select', '下拉框', 'gen_show_type', '字段生成方案', 30, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('87', 'checkbox', '复选框', 'gen_show_type', '字段生成方案', 40, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('88', 'radiobox', '单选框', 'gen_show_type', '字段生成方案', 50, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('89', 'dateselect', '日期选择', 'gen_show_type', '字段生成方案', 60, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('9', 'blue', '蓝色', 'color', '颜色值', 30, '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('90', 'userselect', '人员选择', 'gen_show_type', '字段生成方案', 70, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('91', 'officeselect', '部门选择', 'gen_show_type', '字段生成方案', 80, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('92', 'areaselect', '区域选择', 'gen_show_type', '字段生成方案', 90, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('93', 'String', 'String', 'gen_java_type', 'Java类型', 10, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('94', 'Long', 'Long', 'gen_java_type', 'Java类型', 20, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('95', 'dao', '仅持久层', 'gen_category', '代码生成分类\0\0\0\0', 40, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('96', '1', '男', 'sex', '性别', 10, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('97', '2', '女', 'sex', '性别', 20, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES ('98', 'Integer', 'Integer', 'gen_java_type', 'Java类型', 30, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('99', 'Double', 'Double', 'gen_java_type', 'Java类型', 40, '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '1');
INSERT INTO `sys_dict` VALUES ('a1367ff32ec044b08ead3a56f69748db', 'ACQUIRED', '正在执行', 'sys_job_status', '定时任务状态（PAUSED-暂停，ACQUIRED-正在执行，WAITING-等待执行）', 20, '0', '1', '2019-01-09 16:27:54', '1', '2019-01-11 12:01:52', '', '0');
INSERT INTO `sys_dict` VALUES ('a6aff6268f1f4d77952f3424b5e843d6', '123456', 'default_password', 'default_pass', '系统默认配置密码', 10, '0', '1', '2018-07-03 11:40:03', '1', '2018-07-03 11:40:03', '', '0');
INSERT INTO `sys_dict` VALUES ('a6c017055f7b4ce2bb644ec8bb240d4a', 'PAUSED', '暂停', 'sys_job_status', '定时任务状态（PAUSED-暂停，ACQUIRED-正在执行，WAITING-等待执行）', 10, '0', '1', '2019-01-09 15:37:38', '1', '2019-01-11 20:53:20', '', '0');
INSERT INTO `sys_dict` VALUES ('a8a0aeb824d84d73ad1cca1bdc35c8dc', '5', '乡/镇', 'sys_area_type', '区域类型', 50, '0', '1', '2018-06-15 14:48:45', '1', '2018-06-15 14:48:45', '', '0');
INSERT INTO `sys_dict` VALUES ('c3f4554124e64c4a9cf96ebe94e55ade', '6', '村/社区', 'sys_area_type', '区域类型', 60, '0', '1', '2018-06-15 14:49:25', '1', '2020-01-03 09:11:35', '', '0');
INSERT INTO `sys_dict` VALUES ('da5b5781a8604398aea411a3949b8486', '0', '隐藏', 'sys_show_hide', '显示-隐藏', 10, '0', '1', '2018-06-25 18:22:06', '1', '2018-06-25 18:22:06', '显示-隐藏', '0');
INSERT INTO `sys_dict` VALUES ('edc2e89282bb4eeba397857a2cfb6b33', 'com.jsite.modules.oa.leave', '请假流程1', 'act_category', '请假流程111', 10, '0', '1', '2018-08-03 10:37:05', '1', '2019-12-26 14:29:25', '', '1');
INSERT INTO `sys_dict` VALUES ('ee843534a2884752bf2ebd2444551771', 'images/userinfo.png', '默认头像', 'default_headphoto_small', '默认头像(缩略图)', 10, '0', '1', '2018-07-30 11:52:22', '1', '2018-07-30 11:56:54', '默认小头像(缩略图)图片路径', '0');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `file_tree_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '文件分类',
  `name` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '文件名称',
  `save_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '服务器文件保存名称',
  `file_size` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '文件大小',
  `path` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '文件路径',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '文件' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file` VALUES ('15e0f0655f0949899e6df9353654e41e', 'b7803810cc734b90a3f44643fad06bd7', 'git命令.txt', NULL, '539', 'common/b7803810cc734b90a3f44643fad06bd7\\20180727\\git命令.txt', '1', '2018-07-27 15:40:29', '1', '2018-07-27 15:40:29', NULL, '0');
INSERT INTO `sys_file` VALUES ('354d0ad6df5f4b81a6df003967b8dde3', '018abc93050045dd8f419c14c1a5bfd5', 'picture.jpg', NULL, '157296', 'common/018abc93050045dd8f419c14c1a5bfd5\\20180625\\picture.jpg', '1', '2018-06-25 15:44:40', '1', '2018-06-25 15:44:40', NULL, '1');
INSERT INTO `sys_file` VALUES ('47348b3a5ee94d8e8d0a6d4b0d728b12', 'f1d7c9459a5a44adb38ed662bd5d682f', '新建文本文档.txt', NULL, '4890', 'common/f1d7c9459a5a44adb38ed662bd5d682f\\20180613\\新建文本文档.txt', '1', '2018-06-13 14:40:09', '1', '2018-06-13 14:40:09', NULL, '1');
INSERT INTO `sys_file` VALUES ('48b4b3ab2e064d4a9113318bcf5ddec8', '018abc93050045dd8f419c14c1a5bfd5', 'picture.jpg', NULL, '157296', 'common/7a0440b426f94db1b5b79772210437d2\\20180709\\picture.jpg', '1', '2018-07-09 10:41:49', '1', '2018-07-09 10:46:44', NULL, '0');
INSERT INTO `sys_file` VALUES ('4e9aea81e3234813bae688162b032e88', '8c5b5246330e42f4adb0593b1db498b4', 'BluetoothGet.zip', NULL, '2774021', 'F:\\git\\itamcs\\src\\main\\webapp\\upload\\common/7a0440b426f94db1b5b79772210437d2\\20180611\\BluetoothGet.zip', '1', '2018-06-11 10:20:13', '1', '2018-06-27 09:27:01', NULL, '1');
INSERT INTO `sys_file` VALUES ('5121fa6273e743a7912a58cc5a15ba2f', '8c5b5246330e42f4adb0593b1db498b4', '安装配置教程.docx', NULL, '429193', 'common/8c5b5246330e42f4adb0593b1db498b4/20181214/安装配置教程.docx', '1', '2018-12-14 09:37:00', '1', '2018-12-14 09:37:00', NULL, '0');
INSERT INTO `sys_file` VALUES ('5e3fff31d0724f5d89ab106519e8634a', '8c5b5246330e42f4adb0593b1db498b4', 'XXX旅游APP项目功能模块20180528.xlsx', NULL, '19530', 'common/8c5b5246330e42f4adb0593b1db498b4\\20180803\\XXX旅游APP项目功能模块20180528.xlsx', '1', '2018-08-03 09:37:09', '1', '2018-08-03 09:37:09', NULL, '0');
INSERT INTO `sys_file` VALUES ('6770be70d39d432d802150b41af00370', '8c5b5246330e42f4adb0593b1db498b4', 'PictureUnlock_gf_11393.pictureunlock.jpg', NULL, '103041', 'common/8c5b5246330e42f4adb0593b1db498b4/20181214/PictureUnlock_gf_11393.pictureunlock.jpg', '1', '2018-12-14 21:25:23', '1', '2018-12-14 21:25:23', NULL, '1');
INSERT INTO `sys_file` VALUES ('6f061e5b673c4a35981aba68b5feb771', '8c5b5246330e42f4adb0593b1db498b4', 'git命令.txt', NULL, '871', 'common/8c5b5246330e42f4adb0593b1db498b4\\20181211\\git命令.txt', '1', '2018-12-11 10:42:40', '1', '2018-12-11 10:42:40', NULL, '1');
INSERT INTO `sys_file` VALUES ('802ae641d0d84c488143885487a2d937', '8c5b5246330e42f4adb0593b1db498b4', 'IT资产管控平台初设说明书-20180408001.docx', NULL, '1438387', 'common/8c5b5246330e42f4adb0593b1db498b4\\20180803\\IT资产管控平台初设说明书-20180408001.docx', '1', '2018-08-03 09:44:42', '1', '2018-08-03 09:44:42', NULL, '0');
INSERT INTO `sys_file` VALUES ('a3ececf035d84d9280b1c4b2ae614074', 'f1d7c9459a5a44adb38ed662bd5d682f', 'BeyondAdmin-master.zip', NULL, '2425874', 'F:\\git\\itamcs\\src\\main\\webapp\\upload\\common/f1d7c9459a5a44adb38ed662bd5d682f\\20180611\\BeyondAdmin-master.zip', '1', '2018-06-11 11:06:27', '1', '2018-06-11 11:06:27', NULL, '1');
INSERT INTO `sys_file` VALUES ('bee545f738e941b18c321189fd962f8b', '8c5b5246330e42f4adb0593b1db498b4', '解决问题罗列.txt', NULL, '22351', 'common/8c5b5246330e42f4adb0593b1db498b4\\20180709\\解决问题罗列.txt', '1', '2018-07-09 16:15:38', '1', '2018-07-09 16:15:38', NULL, '0');
INSERT INTO `sys_file` VALUES ('cde9fce0e36e479682f87121bbedb3be', '11da634ddacb4129bb2c4f14854585f5', 'imageclip.html', NULL, '7707', 'common/11da634ddacb4129bb2c4f14854585f5\\20180709\\imageclip.html', '1', '2018-07-09 11:46:48', '1', '2018-07-09 11:46:48', NULL, '0');
INSERT INTO `sys_file` VALUES ('d67606c039784624955e8155032662de', '8c5b5246330e42f4adb0593b1db498b4', '大屏修改反馈-20180416.docx', NULL, '2297953', 'common/8c5b5246330e42f4adb0593b1db498b4\\20180709\\大屏修改反馈-20180416.docx', '1', '2018-07-09 16:15:39', '1', '2018-07-09 16:15:39', NULL, '0');
INSERT INTO `sys_file` VALUES ('da2ac86c3d934295a4d2bfd15518698b', '7a0440b426f94db1b5b79772210437d2', 'note.html', NULL, '1063', 'common/7a0440b426f94db1b5b79772210437d2/20180612/note.html', '1', '2018-06-12 11:56:54', '1', '2018-06-12 11:56:54', NULL, '1');
INSERT INTO `sys_file` VALUES ('fc7cb4ccf7ae42dfa6f81463006bf8eb', '8c5b5246330e42f4adb0593b1db498b4', 'aac.txt', NULL, '3484', 'common/8c5b5246330e42f4adb0593b1db498b4\\20180823\\aac.txt', '1', '2018-08-23 10:57:50', '1', '2018-08-23 10:57:50', NULL, '0');

-- ----------------------------
-- Table structure for sys_file_tree
-- ----------------------------
DROP TABLE IF EXISTS `sys_file_tree`;
CREATE TABLE `sys_file_tree`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `parent_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '所有父级编号',
  `tree_leaf` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '是否树形叶子节点（0:不是,1:是）',
  `tree_level` decimal(4, 0) NOT NULL COMMENT '树形层级(0:根级)',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '名称',
  `sort` decimal(10, 0) NOT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '文件分类' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_file_tree
-- ----------------------------
INSERT INTO `sys_file_tree` VALUES ('018abc93050045dd8f419c14c1a5bfd5', '2bacb604df77451d9ef9c60494c4a5b3', '0,c9265b260c1845b6b46462acd36e4304,2bacb604df77451d9ef9c60494c4a5b3', '1', 2, 'ffff', 1, '1', '2018-06-04 14:40:37', '1', '2018-06-04 14:40:37', '', '0');
INSERT INTO `sys_file_tree` VALUES ('11da634ddacb4129bb2c4f14854585f5', '5dbb8533c98b4869901270ae2eb4d0ad', '0,5dbb8533c98b4869901270ae2eb4d0ad', '0', 1, 'aaa', 3, '1', '2018-06-05 09:58:58', '1', '2018-06-06 18:11:33', '', '0');
INSERT INTO `sys_file_tree` VALUES ('12fe7758f865410283b26ee134f8a0bf', '11da634ddacb4129bb2c4f14854585f5', '0,5dbb8533c98b4869901270ae2eb4d0ad,11da634ddacb4129bb2c4f14854585f5', '1', 2, 'bbb', 5, '1', '2018-06-05 09:59:24', '1', '2018-06-06 18:11:33', '', '0');
INSERT INTO `sys_file_tree` VALUES ('2bacb604df77451d9ef9c60494c4a5b3', 'c9265b260c1845b6b46462acd36e4304', '0,c9265b260c1845b6b46462acd36e4304', '0', 1, '2', 2, '1', '2018-03-28 14:41:39', '1', '2018-06-04 17:01:59', '2', '0');
INSERT INTO `sys_file_tree` VALUES ('5dbb8533c98b4869901270ae2eb4d0ad', '0', '0', '0', 0, 'mmm', 4, '1', '2018-06-06 09:58:30', '1', '2019-12-27 17:21:38', '', '0');
INSERT INTO `sys_file_tree` VALUES ('7a0440b426f94db1b5b79772210437d2', 'f1d7c9459a5a44adb38ed662bd5d682f', '0,c9265b260c1845b6b46462acd36e4304,2bacb604df77451d9ef9c60494c4a5b3,f1d7c9459a5a44adb38ed662bd5d682f', '1', 3, 'ccc', 10, '1', '2018-06-05 09:59:11', '1', '2018-06-06 18:11:55', '', '0');
INSERT INTO `sys_file_tree` VALUES ('885944d2b2784fe68a4a33c276eaa229', '9e31a73796a242009c89995c2de86bbb', '0,5dbb8533c98b4869901270ae2eb4d0ad,9e31a73796a242009c89995c2de86bbb', '1', 2, 'ggg', 3, '1', '2018-06-04 13:56:48', '1', '2018-06-06 17:34:56', 'dxxa', '0');
INSERT INTO `sys_file_tree` VALUES ('8c5b5246330e42f4adb0593b1db498b4', 'b7803810cc734b90a3f44643fad06bd7', '0,c9265b260c1845b6b46462acd36e4304,b7803810cc734b90a3f44643fad06bd7', '1', 2, '5', 3, '1', '2018-06-04 13:54:18', '1', '2018-06-04 13:54:18', '', '0');
INSERT INTO `sys_file_tree` VALUES ('9e31a73796a242009c89995c2de86bbb', '5dbb8533c98b4869901270ae2eb4d0ad', '0,5dbb8533c98b4869901270ae2eb4d0ad', '0', 1, 'wowowow', 4, '1', '2018-06-04 16:45:08', '1', '2018-06-06 17:45:28', '', '0');
INSERT INTO `sys_file_tree` VALUES ('b7803810cc734b90a3f44643fad06bd7', 'c9265b260c1845b6b46462acd36e4304', '0,c9265b260c1845b6b46462acd36e4304', '0', 1, '1', 1, '1', '2018-03-28 14:41:23', '1', '2018-03-28 14:41:23', '1', '0');
INSERT INTO `sys_file_tree` VALUES ('c07436d34ed8412ebfd65c7d7139283d', '0', '0', '0', 0, 'testMode', 120, '1', '2018-07-27 15:30:33', '1', '2018-07-27 16:55:25', 'safsf', '0');
INSERT INTO `sys_file_tree` VALUES ('f1d7c9459a5a44adb38ed662bd5d682f', '2bacb604df77451d9ef9c60494c4a5b3', '0,c9265b260c1845b6b46462acd36e4304,2bacb604df77451d9ef9c60494c4a5b3', '0', 2, 'test', 3, '1', '2018-06-04 14:39:45', '1', '2018-06-06 18:11:55', '1111', '0');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '1' COMMENT '日志类型',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '日志标题',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `remote_addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '操作IP地址',
  `user_agent` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户代理',
  `request_uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请求URI',
  `method` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '操作方式',
  `params` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT '操作提交的数据',
  `exception` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT '异常信息',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_log_create_by`(`create_by`) USING BTREE,
  INDEX `sys_log_request_uri`(`request_uri`) USING BTREE,
  INDEX `sys_log_type`(`type`) USING BTREE,
  INDEX `sys_log_create_date`(`create_date`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_mdict
-- ----------------------------
DROP TABLE IF EXISTS `sys_mdict`;
CREATE TABLE `sys_mdict`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `parent_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '所有父级编号',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '名称',
  `sort` decimal(10, 0) NOT NULL COMMENT '排序',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '描述',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_mdict_parent_id`(`parent_id`) USING BTREE,
  INDEX `sys_mdict_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '多级字典表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `parent_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '所有父级编号',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '名称',
  `sort` decimal(10, 0) NOT NULL COMMENT '排序',
  `href` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '链接',
  `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '目标',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '图标',
  `is_show` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '是否在菜单中显示',
  `permission` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '权限标识',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_menu_parent_id`(`parent_id`) USING BTREE,
  INDEX `sys_menu_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '菜单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '0,', '在线办公', 30, '', '', 'icon-social-tumblr', '1', '', '1', '2013-05-27 08:00:00', '1', '2019-12-29 21:06:05', '', '0');
INSERT INTO `sys_menu` VALUES ('100', '1', '0,1,', '待办任务', 20, '/act/task/todo', '', 'far fa-circle', '1', 'act:process:view,act:process:edit', '1', '2018-08-07 14:57:37', '1', '2019-12-26 15:16:07', '', '0');
INSERT INTO `sys_menu` VALUES ('101', '1', '0,1,', '已办任务', 30, '/act/task/historic', '', 'far fa-circle', '1', 'act:process:view,act:process:edit', '1', '2018-08-07 14:58:35', '1', '2019-12-03 17:23:59', '', '0');
INSERT INTO `sys_menu` VALUES ('102', '1', '0,1,', '已发任务', 40, '/act/task/hasSent', '', 'far fa-circle', '1', 'act:process:view,act:process:edit', '1', '2018-08-09 20:28:01', '1', '2019-12-04 08:38:03', '', '0');
INSERT INTO `sys_menu` VALUES ('103', '1', '0,1,', '发起流程', 50, '/act/flowForm/process', '', 'far fa-circle', '1', 'act:process:view,act:process:edit', '1', '2018-08-07 15:07:41', '1', '2019-12-04 09:46:55', '', '0');
INSERT INTO `sys_menu` VALUES ('104', '1', '0,1,', '请假流程：发起|办理权限', 60, '', '', '', '0', 'oa:leave:view,oa:leave:edit', '1', '2018-08-15 14:40:25', '1', '2019-03-25 09:34:58', '', '0');
INSERT INTO `sys_menu` VALUES ('2', '0', '0,', '机构用户', 60, '', '', 'icon-social-dropbox', '1', '', '1', '2013-05-27 08:00:00', '1', '2019-12-27 17:30:27', '', '0');
INSERT INTO `sys_menu` VALUES ('200', '2', '0,2,', '用户管理', 20, '/sys/user/index', '', 'far fa-circle', '1', '', '1', '2013-05-27 08:00:00', '1', '2018-07-05 10:32:22', '', '0');
INSERT INTO `sys_menu` VALUES ('2000', '200', '0,2,200,', '查看', 30, '', '', 'fa fa-circle-thin', '0', 'sys:user:view', '1', '2018-07-05 10:32:42', '1', '2018-07-05 10:32:42', '', '0');
INSERT INTO `sys_menu` VALUES ('2001', '200', '0,2,200,', '编辑', 60, '', '', 'fa fa-circle-thin', '0', 'sys:user:edit', '1', '2018-07-05 10:33:04', '1', '2018-07-05 10:33:04', '', '0');
INSERT INTO `sys_menu` VALUES ('201', '2', '0,2,', '机构管理', 30, '/sys/office/', '', 'far fa-circle', '1', '', '1', '2013-05-27 08:00:00', '1', '2018-07-05 10:35:25', '', '0');
INSERT INTO `sys_menu` VALUES ('2010', '201', '0,2,201,', '查看', 30, '', '', 'fa fa-circle-thin', '0', 'sys:office:view', '1', '2018-07-05 10:33:26', '1', '2018-07-05 10:33:26', '', '0');
INSERT INTO `sys_menu` VALUES ('2011', '201', '0,2,201,', '编辑', 60, '', '', 'fa fa-circle-thin', '0', 'sys:office:edit', '1', '2018-07-05 10:33:43', '1', '2018-07-05 10:33:43', '', '0');
INSERT INTO `sys_menu` VALUES ('202', '2', '0,2,', '区域管理', 40, '/sys/area/', '', 'far fa-circle', '1', '', '1', '2013-05-27 08:00:00', '1', '2018-07-05 10:36:49', '', '0');
INSERT INTO `sys_menu` VALUES ('2020', '202', '0,2,202,', '查看', 30, '', '', 'fa fa-circle-thin', '0', 'sys:area:view', '1', '2018-07-05 10:36:00', '1', '2018-07-05 10:36:00', '', '0');
INSERT INTO `sys_menu` VALUES ('2021', '202', '0,2,202,', '编辑', 60, '', '', 'fa fa-circle-thin', '0', 'sys:area:edit', '1', '2018-07-05 10:36:25', '1', '2018-07-05 10:36:25', '', '0');
INSERT INTO `sys_menu` VALUES ('3', '0', '0,', '系统设置', 90, '', '', 'icon-settings', '1', '', '1', '2013-05-27 08:00:00', '1', '2019-03-13 10:17:47', '', '0');
INSERT INTO `sys_menu` VALUES ('300', '3', '0,3,', '菜单管理', 30, '/sys/menu/', '', 'far fa-circle', '1', '', '1', '2013-05-27 08:00:00', '1', '2018-07-04 16:17:54', '', '0');
INSERT INTO `sys_menu` VALUES ('3000', '300', '0,3,300,', '查看', 30, '', '', 'far fa-circle', '1', 'sys:menu:view', '1', '2018-07-04 16:16:09', '1', '2018-07-04 16:16:37', '', '0');
INSERT INTO `sys_menu` VALUES ('3001', '300', '0,3,300,', '编辑', 60, '', '', 'far fa-circle', '1', 'sys:menu:edit', '1', '2018-07-04 16:17:40', '1', '2018-07-04 16:17:40', '', '0');
INSERT INTO `sys_menu` VALUES ('301', '3', '0,3,', '角色管理', 40, '/sys/role/', '', 'far fa-circle', '1', '', '1', '2013-05-27 08:00:00', '1', '2019-03-13 10:34:19', '', '0');
INSERT INTO `sys_menu` VALUES ('3010', '301', '0,3,301,', '查看', 30, '', '', 'far fa-circle', '1', 'sys:role:view', '1', '2018-07-04 16:45:37', '1', '2018-07-04 16:45:37', '', '0');
INSERT INTO `sys_menu` VALUES ('3011', '301', '0,3,301,', '编辑', 60, '', '', 'far fa-circle', '1', 'sys:role:edit', '1', '2018-07-04 16:46:18', '1', '2018-07-04 16:47:17', '', '0');
INSERT INTO `sys_menu` VALUES ('302', '3', '0,3,', '字典管理', 50, '/sys/dict/', '', 'far fa-circle', '1', '', '1', '2013-05-27 08:00:00', '1', '2018-07-04 17:14:49', '', '0');
INSERT INTO `sys_menu` VALUES ('3020', '302', '0,3,302,', '查看', 30, '', '', 'far fa-circle', '1', 'sys:dict:view', '1', '2018-07-04 17:14:31', '1', '2018-07-04 17:14:31', '', '0');
INSERT INTO `sys_menu` VALUES ('3021', '302', '0,3,302,', '编辑', 60, '', '', 'far fa-circle', '1', 'sys:dict:edit', '1', '2018-07-04 17:15:17', '1', '2018-07-04 17:15:17', '', '0');
INSERT INTO `sys_menu` VALUES ('303', '3', '0,3,', '定时任务', 60, '/sys/job/', '', 'far fa-circle', '1', '', '1', '2019-01-07 16:29:56', '1', '2019-03-13 11:05:43', '', '0');
INSERT INTO `sys_menu` VALUES ('3030', '303', '0,3,303,', '查看', 30, '', '', 'far fa-circle', '1', 'sys:job:view', '1', '2019-01-07 16:33:42', '1', '2019-01-07 16:33:42', '', '0');
INSERT INTO `sys_menu` VALUES ('3031', '303', '0,3,303,', '编辑', 60, '', '', 'far fa-circle', '1', 'sys:job:edit', '1', '2019-01-08 14:58:22', '1', '2019-01-08 15:02:23', '', '0');
INSERT INTO `sys_menu` VALUES ('3032', '303', '0,3,303,', '添加', 90, '', '', 'far fa-circle', '1', 'sys:job:add', '1', '2019-01-09 17:33:37', '1', '2019-01-09 17:33:37', '', '0');
INSERT INTO `sys_menu` VALUES ('3033', '303', '0,3,303,', '暂停', 120, '', '', 'far fa-circle', '1', 'sys:job:pause', '1', '2019-01-11 09:31:49', '1', '2019-01-11 09:31:49', '', '0');
INSERT INTO `sys_menu` VALUES ('3034', '303', '0,3,303,', '恢复', 150, '', '', 'far fa-circle', '1', 'sys:job:resume', '1', '2019-01-11 09:46:56', '1', '2019-01-11 09:46:56', '', '0');
INSERT INTO `sys_menu` VALUES ('3035', '303', '0,3,303,', '删除', 180, '', '', 'far fa-circle', '1', 'sys:job:del', '1', '2019-01-11 11:00:11', '1', '2019-01-11 11:00:11', '', '0');
INSERT INTO `sys_menu` VALUES ('304', '3', '0,3,', '日志查询', 70, '/sys/log', '', 'far fa-circle', '1', 'sys:log:view', '1', '2013-06-03 08:00:00', '1', '2018-05-11 10:16:51', '', '0');
INSERT INTO `sys_menu` VALUES ('305', '3', '0,3,', '连接池监视', 80, '/../druid', '', 'far fa-circle', '1', '', '1', '2013-10-18 08:00:00', '1', '2018-05-11 10:16:02', '', '0');
INSERT INTO `sys_menu` VALUES ('4', '0', '0,', '文件管理', 150, '', '', 'icon-folder', '1', '', '1', '2013-05-27 08:00:00', '1', '2018-06-28 08:47:09', '', '0');
INSERT INTO `sys_menu` VALUES ('400', '4', '0,4,', '文件夹管理', 30, '/filetree/sysFileTree/index', '', 'far fa-circle', '1', '', '1', '2018-03-28 09:21:04', '1', '2018-07-27 15:20:46', '', '0');
INSERT INTO `sys_menu` VALUES ('4000', '400', '0,4,400,', '查看', 30, '', '', 'fa fa-circle-thin', '0', 'filetree:sysFileTree:view', '1', '2018-07-05 09:08:10', '1', '2018-07-27 15:21:28', '', '0');
INSERT INTO `sys_menu` VALUES ('4001', '400', '0,4,400,', '编辑', 60, '', '', 'fa fa-circle-thin', '0', 'filetree:sysFileTree:edit', '1', '2018-07-05 09:08:33', '1', '2018-07-27 15:21:44', '', '0');
INSERT INTO `sys_menu` VALUES ('401', '4', '0,4,', '文件管理', 40, '/file/sysFile/index', '', 'far fa-circle', '1', '', '1', '2018-03-28 17:04:29', '1', '2018-07-27 15:21:03', '', '0');
INSERT INTO `sys_menu` VALUES ('4010', '401', '0,4,401,', '查看', 30, '', '', '', '0', 'file:sysFile:view', '1', '2018-03-28 17:05:35', '1', '2018-07-27 15:22:00', '', '0');
INSERT INTO `sys_menu` VALUES ('4011', '401', '0,4,401,', '编辑', 60, '', '', '', '0', 'file:sysFile:edit', '1', '2018-03-28 17:06:13', '1', '2018-07-27 15:22:13', '', '0');
INSERT INTO `sys_menu` VALUES ('5', '0', '0,', '代码生成', 180, '', '', 'icon-wrench', '1', '', '1', '2018-05-14 16:26:49', '1', '2019-03-13 10:21:31', '', '0');
INSERT INTO `sys_menu` VALUES ('500', '5', '0,5,', '业务表配置', 30, '/gen/genTable', '', 'far fa-circle', '1', 'gen:genTable:view,gen:genTable:edit,gen:genTableColumn:view,gen:genTableColumn:edit', '1', '2013-10-16 08:00:00', '1', '2018-05-11 10:03:11', '', '0');
INSERT INTO `sys_menu` VALUES ('501', '5', '0,5,', '生成方案配置', 60, '/gen/genScheme', '', 'far fa-circle', '1', 'gen:genScheme:view,gen:genScheme:edit', '1', '2013-10-16 08:00:00', '1', '2018-05-11 10:03:25', '', '0');
INSERT INTO `sys_menu` VALUES ('6', '0', '0,', '代码生成测试', 210, '', '', 'icon-direction', '1', '', '1', '2018-12-19 16:59:07', '1', '2018-12-19 16:59:07', '', '0');
INSERT INTO `sys_menu` VALUES ('600', '6', '0,6,', '单表生成', 30, '/test/testData', '', 'far fa-circle', '1', 'test:testData:view,test:testData:edit', '1', '2018-12-19 17:00:12', '1', '2019-01-02 10:05:25', '', '0');
INSERT INTO `sys_menu` VALUES ('601', '6', '0,6,', '主子表生成', 60, '/test/testDataMain', '', 'far fa-circle', '1', 'test:testDataMain:view,test:testDataMain:edit', '1', '2018-12-29 13:34:17', '1', '2019-01-02 10:09:10', '', '0');
INSERT INTO `sys_menu` VALUES ('602', '6', '0,6,', '树结构表生成', 90, '/test/testTree', '', 'far fa-circle', '1', 'test:testTree:view,test:testTree:edit', '1', '2019-01-07 11:49:29', '1', '2019-01-07 11:49:29', '', '0');
INSERT INTO `sys_menu` VALUES ('7', '0', '0,', '流程管理', 120, '', '', 'icon-equalizer', '1', '', '1', '2013-05-27 08:00:00', '1', '2018-08-02 09:08:56', '', '0');
INSERT INTO `sys_menu` VALUES ('700', '7', '0,7,', '运行中的流程', 30, '/act/process/running', '', 'far fa-circle', '1', 'act:process:edit', '1', '2018-08-02 08:58:48', '1', '2018-08-02 09:01:36', '', '0');
INSERT INTO `sys_menu` VALUES ('701', '7', '0,7,', '历史流程', 40, '/act/process/history', '', 'far fa-circle', '1', 'act:process:edit', '1', '2019-11-08 09:29:34', '1', '2019-11-08 09:33:33', '', '0');
INSERT INTO `sys_menu` VALUES ('702', '7', '0,7,', '部署流程', 60, '/act/process/deploy', '', 'far fa-circle', '1', 'act:process:edit', '1', '2018-08-02 08:53:30', '1', '2018-08-02 09:02:26', '', '0');
INSERT INTO `sys_menu` VALUES ('703', '7', '0,7,', '流程管理', 90, '/act/process', '', 'far fa-circle', '1', 'act:process:edit', '1', '2013-05-27 08:00:00', '1', '2018-08-02 09:09:21', '', '0');
INSERT INTO `sys_menu` VALUES ('704', '7', '0,7,', '模型管理', 150, '/act/model', '', 'far fa-circle', '1', 'act:model:edit', '1', '2013-09-20 08:00:00', '1', '2018-08-02 09:10:06', '', '0');
INSERT INTO `sys_menu` VALUES ('705', '7', '0,7,', '表单管理', 180, '/act/flowForm', '', 'far fa-circle', '1', '', '1', '2019-12-20 11:06:30', '1', '2019-12-24 13:37:52', '', '0');
INSERT INTO `sys_menu` VALUES ('7050', '705', '0,7,705,', '查看', 30, NULL, NULL, NULL, '0', 'flowable:flowForm:view', '1', '2020-01-02 17:07:45', '1', '2020-01-02 17:07:45', NULL, '0');
INSERT INTO `sys_menu` VALUES ('7051', '705', '0,7,705,', '编辑', 60, NULL, NULL, NULL, '0', 'flowable:flowForm:edit', '1', '2020-01-01 17:10:45', '1', '2020-01-02 17:10:45', NULL, '0');

-- ----------------------------
-- Table structure for sys_office
-- ----------------------------
DROP TABLE IF EXISTS `sys_office`;
CREATE TABLE `sys_office`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `parent_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '所有父级编号',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '名称',
  `sort` decimal(10, 0) NOT NULL COMMENT '排序',
  `area_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '归属区域',
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '区域编码',
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '机构类型',
  `grade` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '机构等级',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '联系地址',
  `zip_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '邮政编码',
  `master` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '电话',
  `fax` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '传真',
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '邮箱',
  `USEABLE` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否启用',
  `PRIMARY_PERSON` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '主负责人',
  `DEPUTY_PERSON` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '副负责人',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_office_parent_id`(`parent_id`) USING BTREE,
  INDEX `sys_office_del_flag`(`del_flag`) USING BTREE,
  INDEX `sys_office_type`(`type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '机构表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_office
-- ----------------------------
INSERT INTO `sys_office` VALUES ('1', '0', '0,', 'Jsite测试公司', 10, 'f762fb569d8445f28a226a9b831cfb90', '100000', '1', '1', '', '', '', '', '', '', '1', '', '', '1', '2013-05-27 08:00:00', '1', '2020-01-13 10:22:50', '', '0');
INSERT INTO `sys_office` VALUES ('3', '1', '0,1,', '综合部111', 20, '16166c3ecb954be0ad47159c087cd8af', '100002', '2', '1', '', '', '', '', '', '', '1', '', '', '1', '2013-05-27 08:00:00', '1', '2019-12-30 03:01:13', '', '0');
INSERT INTO `sys_office` VALUES ('4', '1', '0,1,', '市场部', 30, 'f762fb569d8445f28a226a9b831cfb90', '100003', '2', '1', '', '', '', '', '', '', '1', '', '', '1', '2013-05-27 08:00:00', '1', '2019-12-30 02:57:46', '', '0');
INSERT INTO `sys_office` VALUES ('5', '1', '0,1,', '技术部', 40, 'f762fb569d8445f28a226a9b831cfb90', '100004', '2', '1', '北京市海淀区', '100010', '王伟', '010-67545935', '', 'lzyang6@grgbanking.com', '1', '', '', '1', '2013-05-27 08:00:00', '1', '2020-01-13 10:23:43', '测试机构管理', '0');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `office_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '归属机构',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色名称',
  `enname` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '英文名称',
  `role_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '角色类型',
  `user_type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户类型',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '数据范围',
  `is_sys` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否系统数据',
  `useable` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否可用',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_role_del_flag`(`del_flag`) USING BTREE,
  INDEX `sys_role_enname`(`enname`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '5', '系统管理员', 'Manager', 'security-role', '1', '1', '1', '1', '1', '2013-05-27 08:00:00', '1', '2019-01-11 20:55:30', 'AA', '0');
INSERT INTO `sys_role` VALUES ('2', '5', '部门经理', 'dept', 'security-role', '2', '3', '1', '1', '1', '2018-12-22 22:21:56', '1', '2020-01-06 14:54:03', '', '0');
INSERT INTO `sys_role` VALUES ('3', '3', 'hr', 'hr', 'assignment', '4', '1', '1', '1', '1', '2018-08-20 14:33:33', '1', '2020-01-06 14:54:19', '', '0');
INSERT INTO `sys_role` VALUES ('4', '4', '普通用户', 'Comuser', 'assignment', '3', '8', '1', '1', '1', '2013-05-27 08:00:00', '1', '2020-01-06 14:54:30', '', '0');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色编号',
  `menu_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '角色-菜单' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', '1');
INSERT INTO `sys_role_menu` VALUES ('1', '100');
INSERT INTO `sys_role_menu` VALUES ('1', '101');
INSERT INTO `sys_role_menu` VALUES ('1', '102');
INSERT INTO `sys_role_menu` VALUES ('1', '103');
INSERT INTO `sys_role_menu` VALUES ('1', '2');
INSERT INTO `sys_role_menu` VALUES ('1', '200');
INSERT INTO `sys_role_menu` VALUES ('1', '2000');
INSERT INTO `sys_role_menu` VALUES ('1', '2001');
INSERT INTO `sys_role_menu` VALUES ('1', '201');
INSERT INTO `sys_role_menu` VALUES ('1', '2010');
INSERT INTO `sys_role_menu` VALUES ('1', '2011');
INSERT INTO `sys_role_menu` VALUES ('1', '202');
INSERT INTO `sys_role_menu` VALUES ('1', '2020');
INSERT INTO `sys_role_menu` VALUES ('1', '2021');
INSERT INTO `sys_role_menu` VALUES ('1', '3');
INSERT INTO `sys_role_menu` VALUES ('1', '300');
INSERT INTO `sys_role_menu` VALUES ('1', '3000');
INSERT INTO `sys_role_menu` VALUES ('1', '3001');
INSERT INTO `sys_role_menu` VALUES ('1', '301');
INSERT INTO `sys_role_menu` VALUES ('1', '3010');
INSERT INTO `sys_role_menu` VALUES ('1', '3011');
INSERT INTO `sys_role_menu` VALUES ('1', '302');
INSERT INTO `sys_role_menu` VALUES ('1', '3020');
INSERT INTO `sys_role_menu` VALUES ('1', '3021');
INSERT INTO `sys_role_menu` VALUES ('1', '303');
INSERT INTO `sys_role_menu` VALUES ('1', '3030');
INSERT INTO `sys_role_menu` VALUES ('1', '3031');
INSERT INTO `sys_role_menu` VALUES ('1', '3032');
INSERT INTO `sys_role_menu` VALUES ('1', '3033');
INSERT INTO `sys_role_menu` VALUES ('1', '3034');
INSERT INTO `sys_role_menu` VALUES ('1', '3035');
INSERT INTO `sys_role_menu` VALUES ('1', '304');
INSERT INTO `sys_role_menu` VALUES ('1', '305');
INSERT INTO `sys_role_menu` VALUES ('1', '4');
INSERT INTO `sys_role_menu` VALUES ('1', '400');
INSERT INTO `sys_role_menu` VALUES ('1', '4000');
INSERT INTO `sys_role_menu` VALUES ('1', '4001');
INSERT INTO `sys_role_menu` VALUES ('1', '401');
INSERT INTO `sys_role_menu` VALUES ('1', '4010');
INSERT INTO `sys_role_menu` VALUES ('1', '4011');
INSERT INTO `sys_role_menu` VALUES ('1', '5');
INSERT INTO `sys_role_menu` VALUES ('1', '500');
INSERT INTO `sys_role_menu` VALUES ('1', '501');
INSERT INTO `sys_role_menu` VALUES ('1', '6');
INSERT INTO `sys_role_menu` VALUES ('1', '600');
INSERT INTO `sys_role_menu` VALUES ('1', '601');
INSERT INTO `sys_role_menu` VALUES ('1', '602');
INSERT INTO `sys_role_menu` VALUES ('1', '7');
INSERT INTO `sys_role_menu` VALUES ('1', '700');
INSERT INTO `sys_role_menu` VALUES ('1', '701');
INSERT INTO `sys_role_menu` VALUES ('1', '702');
INSERT INTO `sys_role_menu` VALUES ('1', '703');
INSERT INTO `sys_role_menu` VALUES ('2', '1');
INSERT INTO `sys_role_menu` VALUES ('2', '100');
INSERT INTO `sys_role_menu` VALUES ('2', '101');
INSERT INTO `sys_role_menu` VALUES ('2', '102');
INSERT INTO `sys_role_menu` VALUES ('2', '103');
INSERT INTO `sys_role_menu` VALUES ('2', '104');
INSERT INTO `sys_role_menu` VALUES ('2', '4');
INSERT INTO `sys_role_menu` VALUES ('2', '400');
INSERT INTO `sys_role_menu` VALUES ('2', '4000');
INSERT INTO `sys_role_menu` VALUES ('2', '4001');
INSERT INTO `sys_role_menu` VALUES ('2', '401');
INSERT INTO `sys_role_menu` VALUES ('2', '4010');
INSERT INTO `sys_role_menu` VALUES ('2', '4011');
INSERT INTO `sys_role_menu` VALUES ('2', '7');
INSERT INTO `sys_role_menu` VALUES ('2', '705');
INSERT INTO `sys_role_menu` VALUES ('2', '7050');
INSERT INTO `sys_role_menu` VALUES ('2', '7051');
INSERT INTO `sys_role_menu` VALUES ('3', '1');
INSERT INTO `sys_role_menu` VALUES ('3', '100');
INSERT INTO `sys_role_menu` VALUES ('3', '101');
INSERT INTO `sys_role_menu` VALUES ('3', '102');
INSERT INTO `sys_role_menu` VALUES ('3', '103');
INSERT INTO `sys_role_menu` VALUES ('3', '104');
INSERT INTO `sys_role_menu` VALUES ('3', '4');
INSERT INTO `sys_role_menu` VALUES ('3', '400');
INSERT INTO `sys_role_menu` VALUES ('3', '4000');
INSERT INTO `sys_role_menu` VALUES ('3', '4001');
INSERT INTO `sys_role_menu` VALUES ('3', '401');
INSERT INTO `sys_role_menu` VALUES ('3', '4010');
INSERT INTO `sys_role_menu` VALUES ('3', '4011');
INSERT INTO `sys_role_menu` VALUES ('3', '7');
INSERT INTO `sys_role_menu` VALUES ('3', '705');
INSERT INTO `sys_role_menu` VALUES ('3', '7050');
INSERT INTO `sys_role_menu` VALUES ('3', '7051');
INSERT INTO `sys_role_menu` VALUES ('4', '1');
INSERT INTO `sys_role_menu` VALUES ('4', '100');
INSERT INTO `sys_role_menu` VALUES ('4', '101');
INSERT INTO `sys_role_menu` VALUES ('4', '102');
INSERT INTO `sys_role_menu` VALUES ('4', '103');
INSERT INTO `sys_role_menu` VALUES ('4', '104');
INSERT INTO `sys_role_menu` VALUES ('4', '4');
INSERT INTO `sys_role_menu` VALUES ('4', '400');
INSERT INTO `sys_role_menu` VALUES ('4', '4000');
INSERT INTO `sys_role_menu` VALUES ('4', '4001');
INSERT INTO `sys_role_menu` VALUES ('4', '401');
INSERT INTO `sys_role_menu` VALUES ('4', '4010');
INSERT INTO `sys_role_menu` VALUES ('4', '4011');
INSERT INTO `sys_role_menu` VALUES ('4', '7');
INSERT INTO `sys_role_menu` VALUES ('4', '705');
INSERT INTO `sys_role_menu` VALUES ('4', '7050');
INSERT INTO `sys_role_menu` VALUES ('4', '7051');

-- ----------------------------
-- Table structure for sys_role_office
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_office`;
CREATE TABLE `sys_role_office`  (
  `role_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色编号',
  `office_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '机构编号',
  PRIMARY KEY (`role_id`, `office_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '角色-机构' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `company_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '归属公司',
  `office_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '归属部门',
  `login_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '登录名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '密码',
  `no` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '工号',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '姓名',
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '电话',
  `mobile` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '手机',
  `user_post` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户类型',
  `photo` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户头像',
  `login_ip` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '最后登陆IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
  `login_flag` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否可登录',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_user_office_id`(`office_id`) USING BTREE,
  INDEX `sys_user_login_name`(`login_name`) USING BTREE,
  INDEX `sys_user_company_id`(`company_id`) USING BTREE,
  INDEX `sys_user_update_date`(`update_date`) USING BTREE,
  INDEX `sys_user_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '1', '5', 'jsite', '57b89d1421898c692ebf98b530c3869b56d32e1e80f2bb703aba8a39', '0001', '系统管理员', 'admin@admin.com', '', '1111d', '1', '/jsite//userfiles/headphoto/1.jpg', '110.16.203.72', '2020-02-13 10:19:35', '1', '1', '2013-05-27 08:00:00', '1', '2019-08-07 17:30:46', '11221eee', '0');
INSERT INTO `sys_user` VALUES ('2', '1', '5', 'dept', '56c52163316679d8c83c84f03d44a592246d76a70bacb1da76cdc149', '000002', '部门经理', '', '', '', '2', NULL, '36.47.162.123', '2020-02-12 18:27:26', '1', '1', '2018-12-22 22:22:25', '1', '2020-01-02 13:26:34', '', '0');
INSERT INTO `sys_user` VALUES ('3', '1', '3', 'jsitehr', '7e14e4c693492493db3bc12ba2bdd9cec2629613deae9ebd9c42dac1', '000003', '人力资源', '', '', '', '4', NULL, '202.100.34.177', '2020-02-12 11:32:48', '1', '1', '2018-12-22 22:23:39', '1', '2020-01-17 08:39:51', '', '0');
INSERT INTO `sys_user` VALUES ('4', '1', '5', 'jsiteuser', '2f445129a012d18383f4fbdc9fdaca446794a89e80c384dc3a1e1f1a', '000005', '普通用户', '', '', '', '3', NULL, '114.255.144.33', '2019-12-09 11:59:54', '1', '1', '2018-12-28 13:55:56', '1', '2018-12-28 16:44:40', '', '0');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '用户编号',
  `role_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户-角色' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '2');
INSERT INTO `sys_user_role` VALUES ('3', '3');
INSERT INTO `sys_user_role` VALUES ('3', '4');
INSERT INTO `sys_user_role` VALUES ('4', '4');

-- ----------------------------
-- Table structure for test_data
-- ----------------------------
DROP TABLE IF EXISTS `test_data`;
CREATE TABLE `test_data`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '归属用户',
  `office_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '归属部门',
  `area_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '归属区域',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '名称',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '性别',
  `in_date` date NULL DEFAULT NULL COMMENT '加入日期',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `test_data_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '业务数据表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of test_data
-- ----------------------------
INSERT INTO `test_data` VALUES ('01383a5f7ec84434b3723170aceb47b6', '56620c5ef567427c8debe68875c0eda2', '5', '015345144e9e40d6bbe832a9f1cba1f7', '', '', NULL, '1', '2019-02-26 14:56:58', '1', '2019-02-26 14:56:58', '', '1');
INSERT INTO `test_data` VALUES ('0709c77e68bd40d090c7cb1e9aa85f07', '3', '3', 'da2da01621b64be5a85b07a8c883228f', '', '', NULL, '1', '2019-03-13 22:48:19', '1', '2019-03-13 22:48:19', '', '1');
INSERT INTO `test_data` VALUES ('09548470b6cd4b13bba5d632e1d993f9', '43f6a4a84e784e5e98b5b1c530bef73b', '4', 'f762fb569d8445f28a226a9b831cfb90', '美团外卖', '男', '2019-01-28', '1', '2019-01-28 09:58:24', '1', '2019-02-25 09:45:51', '呃呃呃d', '1');
INSERT INTO `test_data` VALUES ('1308ea640764443898a5673b3712c447', '', '', '', '', '', NULL, '1', '2019-01-03 01:55:41', '1', '2019-01-03 01:55:41', '', '1');
INSERT INTO `test_data` VALUES ('1e795ae9156a4b23992fb583ec2e53b6', '3', '3', 'da2da01621b64be5a85b07a8c883228f', '发发', '女', '2019-04-12', '1', '2019-04-12 01:29:13', '1', '2019-04-12 01:29:13', '暗示法法师', '1');
INSERT INTO `test_data` VALUES ('2864a7f9ec2243218ab0ac03f1da410d', '56620c5ef567427c8debe68875c0eda2', '5', 'f762fb569d8445f28a226a9b831cfb90', '', '', NULL, '1', '2019-02-28 10:42:50', '1', '2019-02-28 10:42:50', '', '1');
INSERT INTO `test_data` VALUES ('2bb40c327b974c7e8dcf13ad31429072', '4', '1834d4fdba4a4a8b94a905fa2f4f83fb', 'da2da01621b64be5a85b07a8c883228f', '张琨', '', NULL, '1', '2019-03-01 16:53:43', '1', '2019-03-01 16:53:43', '', '1');
INSERT INTO `test_data` VALUES ('3d69c848d3ce404eb41bc8a219a6fe62', '3', '3', 'f762fb569d8445f28a226a9b831cfb90', 'jjjjj', '', NULL, '1', '2019-04-11 01:58:46', '1', '2019-04-11 01:58:46', '', '1');
INSERT INTO `test_data` VALUES ('44069377d5d545bf9829634086a15346', '3', '1', '1', '中国测试', '男', '2019-05-10', '1', '2019-05-10 16:08:29', '1', '2019-05-27 17:28:57', 'ddddd', '0');
INSERT INTO `test_data` VALUES ('4491abf519834a078b307fcfb1aee334', '56620c5ef567427c8debe68875c0eda2', '3', '', '', '', NULL, '1', '2019-01-02 11:19:24', '1', '2019-01-02 11:19:24', '', '1');
INSERT INTO `test_data` VALUES ('4d9402977fd04c90a64b79b0b1b1d333', '1', '4', 'da2da01621b64be5a85b07a8c883228f', '质量检查', '', NULL, '1', '2019-05-29 19:32:17', '1', '2020-01-15 17:17:14', '质量检查', '0');
INSERT INTO `test_data` VALUES ('4da752d01e904cf691fd1e99563d5ef6', '3', '4', 'da2da01621b64be5a85b07a8c883228f', '11', '1', NULL, '1', '2019-03-14 09:16:48', '1', '2019-03-16 16:02:56', '1', '1');
INSERT INTO `test_data` VALUES ('5175cc88b1c44c30925e441118f4d482', '1', '1', '1', 'sbww', '', NULL, '1', '2020-01-09 23:45:01', '1', '2020-01-09 23:45:01', 'sdaf', '0');
INSERT INTO `test_data` VALUES ('5236322296bb49e0bffc05ce6f747fb4', '1', '1', '1', '', '', NULL, '1', '2020-01-14 11:18:13', '1', '2020-01-16 20:57:10', '', '0');
INSERT INTO `test_data` VALUES ('54e1a54b6b1f4176a49d7cb9645a5e56', '1', '1', '1', 'asdasd', 'a', NULL, '1', '2020-01-10 08:57:55', '1', '2020-01-10 13:16:14', '', '0');
INSERT INTO `test_data` VALUES ('67c875cab94b4ad6a2bcd5e2d1965871', 'ac1f0f2eda8042738b1ca021201e0e2f', 'ac1f0f2eda8042738b1ca021201e0e2f', '015345144e9e40d6bbe832a9f1cba1f7', '', '', NULL, '1', '2019-04-12 11:28:57', '1', '2019-04-12 11:28:57', '', '1');
INSERT INTO `test_data` VALUES ('6f313437e7cd4c3da54be0ec2f296967', '3', '3', 'f762fb569d8445f28a226a9b831cfb90', '恩恩', '嘻', NULL, '1', '2020-01-06 13:32:11', '1', '2020-01-06 13:32:29', '', '0');
INSERT INTO `test_data` VALUES ('765125f4b06f4654ae26fc1095d2a019', '43f6a4a84e784e5e98b5b1c530bef73b', '3', 'f762fb569d8445f28a226a9b831cfb90', '智能客情', '', '2019-01-30', '1', '2019-01-28 15:26:36', '1', '2019-01-31 15:29:23', '', '1');
INSERT INTO `test_data` VALUES ('7b485d544ed443e4b4f071fd53a4139f', 'f3a7fb53be0448aaa7b3e20d81d58207', '', 'f762fb569d8445f28a226a9b831cfb90', '', '', NULL, '1', '2019-05-05 22:08:10', '1', '2019-05-05 22:08:10', '', '0');
INSERT INTO `test_data` VALUES ('7c0a0ccb7dbe4b76baaf8cb9afd83524', '56620c5ef567427c8debe68875c0eda2', '5', '015345144e9e40d6bbe832a9f1cba1f7', '', '', NULL, '1', '2019-01-31 15:29:41', '1', '2019-01-31 15:29:41', '', '1');
INSERT INTO `test_data` VALUES ('7f00483f1d574cfebe9990cdd0657435', '', '', '', '', '', NULL, '1', '2019-03-15 12:48:06', '1', '2019-03-15 12:48:06', '', '1');
INSERT INTO `test_data` VALUES ('882996ee70c44ab4b0333afdc5351e8c', 'a1694d423a444b6e932fe1a4cad11f53', 'a1694d423a444b6e932fe1a4cad11f53', 'da2da01621b64be5a85b07a8c883228f', 'test01', '', NULL, '1', '2019-04-12 11:32:44', '1', '2019-05-05 22:08:23', '', '0');
INSERT INTO `test_data` VALUES ('8d82e39ec4154f7487b9ac983abd021e', '56620c5ef567427c8debe68875c0eda2', '3', 'da2da01621b64be5a85b07a8c883228f', '系统监控', '1', '2019-03-06', '1', '2019-03-06 08:32:52', '1', '2019-04-01 21:22:15', '', '1');
INSERT INTO `test_data` VALUES ('957013aa02404a43a288853ec7716d61', '1', '3', 'da2da01621b64be5a85b07a8c883228f', '111', 'f', '2019-01-30', '1', '2019-01-30 16:24:42', '1', '2019-03-02 17:15:43', 'ff', '1');
INSERT INTO `test_data` VALUES ('9e6776cc39a1473cba0b280b41cbe988', '2', '5', '3fbaf3a3f58a47118f13976cb2e2544d', '北京', '', NULL, '1', '2019-04-03 13:42:22', '1', '2019-04-15 16:21:16', '', '0');
INSERT INTO `test_data` VALUES ('a58232169dd7418a9d62f88a19a49860', '', '3', 'da2da01621b64be5a85b07a8c883228f', '', '', NULL, '1', '2019-03-20 15:03:54', '1', '2019-03-25 00:17:49', '', '1');
INSERT INTO `test_data` VALUES ('ab44fc81912f4cafb7bf0856b266c026', 'a1694d423a444b6e932fe1a4cad11f53', '30808ed5d34f4e64b5d9d93ff0967f0d', '3fbaf3a3f58a47118f13976cb2e2544d', 'xxx', 'x', '2019-05-07', '1', '2019-05-07 16:52:31', '1', '2019-05-07 16:52:31', 'xxxxx', '0');
INSERT INTO `test_data` VALUES ('afe7545608304f05b79246285e05affc', '1', '5', 'f762fb569d8445f28a226a9b831cfb90', 'test_my', 'm', '2019-01-28', '1', '2019-01-28 18:01:20', '1', '2019-01-28 18:01:20', '', '1');
INSERT INTO `test_data` VALUES ('b1263ec2813a4ccdbebdc3060fe94c96', '3', '5', 'da2da01621b64be5a85b07a8c883228f', '', '', NULL, '1', '2019-03-26 17:49:46', '1', '2019-03-26 17:49:46', '', '1');
INSERT INTO `test_data` VALUES ('b714a70961564d2b9b20556f06817df4', '1', '3', 'da2da01621b64be5a85b07a8c883228f', '', '', NULL, '1', '2019-01-14 09:52:38', '1', '2019-01-14 09:52:38', '', '1');
INSERT INTO `test_data` VALUES ('ba7c670abe4a49d1af8ded2256810f36', '56620c5ef567427c8debe68875c0eda2', '5', '1', '', 'a', '2019-01-08', '1', '2019-01-08 16:12:42', '1', '2019-01-10 19:00:11', 'aaaaaaa', '1');
INSERT INTO `test_data` VALUES ('bbd94222359142af97c01e5f9920ea32', '56620c5ef567427c8debe68875c0eda2', '4', '3fbaf3a3f58a47118f13976cb2e2544d', '改革', '', '2019-01-02', '1', '2019-01-02 11:10:22', '1', '2019-01-24 16:55:50', '', '1');
INSERT INTO `test_data` VALUES ('c59589dba7fd454bb73593f8d692b07c', '', '', '', '', '', NULL, '1', '2019-06-01 15:26:57', '1', '2019-06-01 15:26:57', '', '0');
INSERT INTO `test_data` VALUES ('cae0e3f27fd54a9f91645d4d503cbb78', '4', '3', 'da2da01621b64be5a85b07a8c883228f', 'ccc', '1', '2019-03-18', '1', '2019-03-18 18:10:02', '1', '2019-03-19 22:42:52', '', '1');
INSERT INTO `test_data` VALUES ('d4df25cb179b4e158c6f0e38342c67aa', '3', '4', 'da2da01621b64be5a85b07a8c883228f', '88', '', NULL, '1', '2020-01-17 17:22:23', '1', '2020-01-17 17:22:43', '', '0');
INSERT INTO `test_data` VALUES ('dd4b760594de4228ab01c48f508e6994', '1', '1', '1', '啊啊', 'n', '2019-05-05', '1', '2019-05-05 18:17:01', '1', '2019-05-05 18:17:01', 'sdaf', '0');
INSERT INTO `test_data` VALUES ('e3391cb15ebb4562874bbac1fb30da12', '1', '4', '', '00826622', '', NULL, '1', '2019-01-30 15:40:41', '1', '2019-01-30 15:40:41', '', '1');
INSERT INTO `test_data` VALUES ('e55163dab0e74737968584bc6c2d7507', '43f6a4a84e784e5e98b5b1c530bef73b', '3', 'da2da01621b64be5a85b07a8c883228f', '', '', '2019-01-02', '1', '2019-01-02 13:07:00', '1', '2019-01-02 13:07:00', '', '1');
INSERT INTO `test_data` VALUES ('e5ced960dabe403e880706695fd44c4b', '3', '3', 'f762fb569d8445f28a226a9b831cfb90', 'FF', '男', '2019-05-16', '1', '2019-05-16 15:29:04', '1', '2019-05-16 15:29:25', '水电费', '1');
INSERT INTO `test_data` VALUES ('e74d0a99dbff4659a818b250199f00e2', '56620c5ef567427c8debe68875c0eda2', '3', 'f762fb569d8445f28a226a9b831cfb90', '111', '男', '2019-02-21', '1', '2019-02-21 16:12:32', '1', '2019-02-25 09:45:56', 'd', '1');
INSERT INTO `test_data` VALUES ('f67da4bbbb7648148285897c0e41c88d', '', '', '', '', '', NULL, '1', '2019-05-29 12:19:29', '1', '2019-05-29 12:19:29', '', '1');
INSERT INTO `test_data` VALUES ('fe4b265a02ac481d8a83cfdb4b5e2b38', '5', '1', '1', 'test', '', NULL, '1', '2020-01-13 16:49:53', '1', '2020-01-13 16:50:14', '', '0');

-- ----------------------------
-- Table structure for test_data_child
-- ----------------------------
DROP TABLE IF EXISTS `test_data_child`;
CREATE TABLE `test_data_child`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `test_data_main_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '业务主表ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '名称',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `test_data_child_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '业务数据子表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of test_data_child
-- ----------------------------
INSERT INTO `test_data_child` VALUES ('01486928eb214626b6d2289c37e4af65', '8d312ac090e14a2ba79c3cd9d4bc53c8', 'ewewe', '1', '2020-01-10 13:35:15', '1', '2020-01-10 13:35:15', '', '1');
INSERT INTO `test_data_child` VALUES ('0762990f4c2347149a3f64dd74e4892d', 'cf0ca8a9fbb142cc826d2af75164fdc3', 'dsfdf', '1', '2019-04-01 13:10:29', '1', '2019-04-01 13:10:29', 'gfgf', '0');
INSERT INTO `test_data_child` VALUES ('0fec8507383444d7b6b3cd0d0d8c2817', NULL, '12121', '1', '2019-04-13 14:16:47', '1', '2019-06-04 15:33:06', '1212', '0');
INSERT INTO `test_data_child` VALUES ('2357df346f1642d99a15a49e1be09cfb', '40a0fd9625e742f19e310a9469341260', 'aaabb', '1', '2019-01-04 15:59:34', '1', '2019-01-04 15:59:34', 'abbb', '1');
INSERT INTO `test_data_child` VALUES ('2ae73764e7b84f4292218d18ad87b8de', 'f9422e63fcc94319a8e2cbb0b53a1120', '', '1', '2019-02-25 16:53:58', '1', '2019-02-25 16:53:58', '', '1');
INSERT INTO `test_data_child` VALUES ('3a0b078939154c02a56be126c38b9a4c', '2298e9fe6bcc47db9707f308d02414cf', '2222', '1', '2019-03-20 17:57:39', '1', '2019-03-20 17:57:39', '2222', '0');
INSERT INTO `test_data_child` VALUES ('3e425fcb69de4bdeaf1293bde7920899', NULL, '123123', '1', '2019-04-13 14:16:47', '1', '2019-06-04 15:33:06', '123123', '0');
INSERT INTO `test_data_child` VALUES ('40122aace3044a46afdf14d7e44fddec', '40a0fd9625e742f19e310a9469341260', '1', '1', '2019-03-25 10:36:33', '1', '2019-03-25 10:36:33', '1', '0');
INSERT INTO `test_data_child` VALUES ('40844af4b30d4da6b89a57abdcda676a', NULL, '123123', '1', '2019-04-13 14:16:47', '1', '2019-06-04 15:33:06', '1231231', '0');
INSERT INTO `test_data_child` VALUES ('4bd2b735a42b478eaa2423f41de079d0', NULL, '收到', '1', '2019-02-25 16:56:11', '1', '2019-03-25 10:36:33', 'asfd', '0');
INSERT INTO `test_data_child` VALUES ('4ea3065da8ea4d3eba50403cf2c29972', NULL, 'werw', '1', '2019-01-21 09:20:52', '1', '2019-02-25 16:56:37', '44', '0');
INSERT INTO `test_data_child` VALUES ('4fd234bb5ba147cf8103ab5e97ff0a08', NULL, '333', '1', '2019-01-05 21:49:14', '1', '2019-01-05 21:55:37', '444', '0');
INSERT INTO `test_data_child` VALUES ('5ad9527740534f518fd03df5504e8311', '8f7f90c21d7248a6a9f231b904b04d39', '', '1', '2019-03-03 19:49:38', '1', '2019-03-03 19:49:38', '', '0');
INSERT INTO `test_data_child` VALUES ('5b238dd8864a4afcb376936591dc373e', '135e474961044a42b1d480c7dfe816f0', '是', '1', '2019-02-25 16:56:50', '1', '2019-02-25 16:56:50', '2222', '0');
INSERT INTO `test_data_child` VALUES ('5c393aca623f49b9badf54ff44463fb6', NULL, 'testtbl1', '1', '2019-01-11 18:15:43', '1', '2019-01-21 09:20:52', '', '0');
INSERT INTO `test_data_child` VALUES ('602c718de7e64281a5d72845aa5348ed', '8f7f90c21d7248a6a9f231b904b04d39', '', '1', '2019-03-03 19:49:38', '1', '2019-03-03 19:49:38', '', '0');
INSERT INTO `test_data_child` VALUES ('609adb32576748539919acf48e902d3d', NULL, '测试子表', '1', '2019-02-25 16:53:58', '1', '2019-02-25 16:56:22', '', '0');
INSERT INTO `test_data_child` VALUES ('68fd0ae27b3b47c0a90d188b290a8384', '1aa274239ae040acac49e132de1eeb0d', '地方三房', '1', '2019-03-15 14:40:45', '1', '2019-03-15 14:40:45', '发送', '0');
INSERT INTO `test_data_child` VALUES ('6c4be2cc266c44baa087d6a71dfee9e2', NULL, 'wre', '1', '2019-01-07 13:35:47', '1', '2019-01-08 11:53:29', 'ewr', '0');
INSERT INTO `test_data_child` VALUES ('6cbf4b86873840c08954387b3898084d', NULL, 'rwerw', '1', '2019-01-07 13:35:47', '1', '2019-01-08 11:53:29', 'erwewr', '0');
INSERT INTO `test_data_child` VALUES ('6ef7e924ed704747a0e8c5473f152f0a', NULL, '', '1', '2019-01-29 19:13:09', '1', '2019-01-30 22:06:05', '', '0');
INSERT INTO `test_data_child` VALUES ('75b9a6dd4f28482fb56c8021ec749197', '0514f4e253f645e680e7acf9ad9c4bef', '', '1', '2019-04-02 09:50:58', '1', '2019-04-02 09:50:58', '', '1');
INSERT INTO `test_data_child` VALUES ('87dc993eb5264f66be64d253e5fa6933', '40a0fd9625e742f19e310a9469341260', '123213', '1', '2019-02-25 16:53:08', '1', '2019-02-25 16:53:08', '21421313213', '1');
INSERT INTO `test_data_child` VALUES ('89d2e2e7ed214dc094bf6cedc711148c', 'f9422e63fcc94319a8e2cbb0b53a1120', '222222', '1', '2019-03-03 19:49:58', '1', '2019-03-03 19:49:58', '22222', '0');
INSERT INTO `test_data_child` VALUES ('9b9b80c449904d1db187cccd3f01db04', '0514f4e253f645e680e7acf9ad9c4bef', '', '1', '2019-04-02 09:50:58', '1', '2019-04-02 09:50:58', '', '1');
INSERT INTO `test_data_child` VALUES ('a1e907508a8d4502a84cb27d2bf0a260', NULL, '11', '1', '2019-04-08 21:21:02', '1', '2019-05-19 15:38:43', '11', '0');
INSERT INTO `test_data_child` VALUES ('a29be038d7634c1cba8f9c16de7c6cc4', '832c429504574bb39b2f42a5f7c1e33d', '444', '1', '2020-01-07 22:06:56', '1', '2020-01-07 22:06:56', '45646456', '0');
INSERT INTO `test_data_child` VALUES ('a6b8d00d29664df99bf6dbecb08f719b', 'c557c0a63c854975beed9ec276129b3f', 'sfsadf', '1', '2019-01-02 12:28:28', '1', '2019-01-02 12:28:28', 'sadf', '1');
INSERT INTO `test_data_child` VALUES ('ae53651e705449a0b14d49a53fd38184', 'f9422e63fcc94319a8e2cbb0b53a1120', '22222', '1', '2019-03-03 19:49:58', '1', '2019-03-03 19:49:58', '2222222222', '0');
INSERT INTO `test_data_child` VALUES ('b49de329fb6f4663b026576eab6cb729', NULL, '111', '1', '2019-01-05 21:49:14', '1', '2019-01-05 21:55:37', '222', '0');
INSERT INTO `test_data_child` VALUES ('b4d0a97e71fb472b89a99948bd7820df', '72416d03367448ac9298f2f80573c4fb', '', '1', '2019-02-26 14:57:14', '1', '2019-02-26 14:57:14', '', '0');
INSERT INTO `test_data_child` VALUES ('b616f755ff89413482dad12bfa61a167', NULL, 'asfd', '1', '2019-02-25 16:56:37', '1', '2019-02-25 16:56:50', 'sdf', '0');
INSERT INTO `test_data_child` VALUES ('b7a0e143c155407588fece3039360ded', 'cf0ca8a9fbb142cc826d2af75164fdc3', 'fdsfd', '1', '2019-04-01 13:10:29', '1', '2019-04-01 13:10:29', 'dfsdf', '0');
INSERT INTO `test_data_child` VALUES ('be693b17d3214f5dbb4285e07d1fd3aa', NULL, '', '1', '2019-01-29 19:13:09', '1', '2019-01-30 22:06:05', '', '0');
INSERT INTO `test_data_child` VALUES ('bf2028ae232647b39f117e7a121aed6f', '72416d03367448ac9298f2f80573c4fb', '', '1', '2019-02-26 14:57:14', '1', '2019-02-26 14:57:14', '', '0');
INSERT INTO `test_data_child` VALUES ('c00d88eb0eea4bd496cce09b4c5e6025', 'cf0ca8a9fbb142cc826d2af75164fdc3', 'dsfsd', '1', '2019-04-01 13:10:29', '1', '2019-04-01 13:10:29', 'sdfsdfsd', '0');
INSERT INTO `test_data_child` VALUES ('c048e370336e41bb92c7f45791aa4872', NULL, 'erwrw', '1', '2019-01-07 13:35:47', '1', '2019-01-08 11:53:29', 'rewrew', '0');
INSERT INTO `test_data_child` VALUES ('c264f20204c04f7fbe1cd96e80e11f47', NULL, 'ddd', '1', '2019-01-30 22:06:05', '1', '2019-02-25 09:46:11', 'ddd', '0');
INSERT INTO `test_data_child` VALUES ('c7cda1f2e6b444afa68f78970b32d77d', '2298e9fe6bcc47db9707f308d02414cf', '11111', '1', '2019-03-20 17:57:39', '1', '2019-03-20 17:57:39', '11111', '0');
INSERT INTO `test_data_child` VALUES ('c9988aa5a78042e5aa51a34996006adb', '82331375175a4a8ebddc0aa11c09fc02', '', '1', '2019-04-19 14:03:21', '1', '2019-04-19 14:03:21', '', '0');
INSERT INTO `test_data_child` VALUES ('da097b02c67640c88f51ddab62f6097a', NULL, '', '1', '2019-03-07 16:52:52', '1', '2019-03-15 14:40:17', '', '0');
INSERT INTO `test_data_child` VALUES ('e09ddbd55180465f9a8ab4dc8e6fd990', '838616e721d440c88c56605aee981c99', '', '1', '2019-01-07 09:40:27', '1', '2019-01-07 09:40:27', '', '1');
INSERT INTO `test_data_child` VALUES ('e1ada6eb1b004e5da5df693b4b02a3c9', 'f9422e63fcc94319a8e2cbb0b53a1120', '', '1', '2019-02-25 16:53:58', '1', '2019-02-25 16:53:58', '', '1');
INSERT INTO `test_data_child` VALUES ('e25c048833974c4ca4908c00caaaa299', 'f9422e63fcc94319a8e2cbb0b53a1120', '', '1', '2019-03-03 19:49:58', '1', '2019-03-03 19:49:58', '', '0');
INSERT INTO `test_data_child` VALUES ('e4f228d237204430a18eeea8101c7a1f', '40a0fd9625e742f19e310a9469341260', 'bbbbcc', '1', '2019-01-04 15:59:34', '1', '2019-01-04 15:59:34', 'ccdd', '1');
INSERT INTO `test_data_child` VALUES ('e853549c46154b07b1d62085958e21d4', '8d312ac090e14a2ba79c3cd9d4bc53c8', 'wwww', '1', '2020-01-10 13:35:15', '1', '2020-01-10 13:35:15', '', '1');
INSERT INTO `test_data_child` VALUES ('f1c77a564b924e948ab5c193aab3b27d', NULL, 'werw', '1', '2019-01-21 09:20:42', '1', '2019-01-26 10:53:42', 'aew', '0');
INSERT INTO `test_data_child` VALUES ('f65f6099f4db4f6aac5dfda733f8697f', '8f7f90c21d7248a6a9f231b904b04d39', '', '1', '2019-03-03 19:49:38', '1', '2019-03-03 19:49:38', '', '0');
INSERT INTO `test_data_child` VALUES ('fc4114fd1b8b4cd2aac56c4ce202247b', NULL, '11', '1', '2019-04-08 21:21:02', '1', '2019-05-19 15:38:43', '111', '0');
INSERT INTO `test_data_child` VALUES ('ffebe89e782440e5bf5009e5dc646cc1', 'f9422e63fcc94319a8e2cbb0b53a1120', '222222', '1', '2019-03-03 19:49:58', '1', '2019-03-03 19:49:58', '2222', '0');

-- ----------------------------
-- Table structure for test_data_main
-- ----------------------------
DROP TABLE IF EXISTS `test_data_main`;
CREATE TABLE `test_data_main`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '归属用户',
  `office_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '归属部门',
  `area_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '归属区域',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '名称',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '性别',
  `in_date` date NULL DEFAULT NULL COMMENT '加入日期',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `test_data_main_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '业务数据表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of test_data_main
-- ----------------------------
INSERT INTO `test_data_main` VALUES ('0514f4e253f645e680e7acf9ad9c4bef', '', '', '', '', '', '2019-05-19', '1', '2019-04-02 09:50:58', '1', '2019-05-19 15:38:43', '', '0');
INSERT INTO `test_data_main` VALUES ('135e474961044a42b1d480c7dfe816f0', '3', '4', 'f762fb569d8445f28a226a9b831cfb90', '站点', '难', '2019-01-05', '1', '2019-01-05 21:49:14', '1', '2019-02-25 16:56:50', '', '0');
INSERT INTO `test_data_main` VALUES ('1aa274239ae040acac49e132de1eeb0d', '1', '3', 'da2da01621b64be5a85b07a8c883228f', '', '', NULL, '1', '2019-03-07 16:52:52', '1', '2019-03-15 14:40:45', '', '0');
INSERT INTO `test_data_main` VALUES ('2298e9fe6bcc47db9707f308d02414cf', '', '', '', '', '', NULL, '1', '2019-03-20 17:57:39', '1', '2019-03-20 17:57:39', '', '0');
INSERT INTO `test_data_main` VALUES ('40a0fd9625e742f19e310a9469341260', '68225048f4b7465293feb9779448d0af', '5', '3fbaf3a3f58a47118f13976cb2e2544d', 'aaaabbb', 'a', '2019-01-04', '1', '2019-01-04 15:59:34', '1', '2019-03-25 10:36:33', 'aaa', '0');
INSERT INTO `test_data_main` VALUES ('72416d03367448ac9298f2f80573c4fb', '', '', '', '', '', NULL, '1', '2019-02-26 14:57:14', '1', '2019-02-26 14:57:14', '', '0');
INSERT INTO `test_data_main` VALUES ('82331375175a4a8ebddc0aa11c09fc02', '', '', '', '', '', NULL, '1', '2019-04-19 14:03:21', '1', '2019-04-19 14:03:21', '', '0');
INSERT INTO `test_data_main` VALUES ('832c429504574bb39b2f42a5f7c1e33d', '3', '3', '16166c3ecb954be0ad47159c087cd8af', '工作会', 'a', '2019-04-13', '1', '2019-04-13 14:16:47', '1', '2020-01-07 22:06:56', '', '0');
INSERT INTO `test_data_main` VALUES ('838616e721d440c88c56605aee981c99', '', '', '', '', '', NULL, '1', '2019-01-07 09:40:27', '1', '2019-01-07 09:40:27', '', '1');
INSERT INTO `test_data_main` VALUES ('8d312ac090e14a2ba79c3cd9d4bc53c8', '', '', '', '', '', NULL, '1', '2020-01-10 13:35:15', '1', '2020-01-10 13:35:15', '', '1');
INSERT INTO `test_data_main` VALUES ('8f7f90c21d7248a6a9f231b904b04d39', '', '', '', '', '', NULL, '1', '2019-03-03 19:49:38', '1', '2019-03-03 19:49:38', '', '0');
INSERT INTO `test_data_main` VALUES ('bf07d5f74023446db5b070e25bbdae26', '4', '3', 'f762fb569d8445f28a226a9b831cfb90', '', 'a', NULL, '1', '2019-01-02 12:28:49', '1', '2019-01-02 17:27:17', 'asdf', '1');
INSERT INTO `test_data_main` VALUES ('c557c0a63c854975beed9ec276129b3f', '', '', '', '', '', NULL, '1', '2019-01-02 12:28:28', '1', '2019-01-02 12:28:28', '', '1');
INSERT INTO `test_data_main` VALUES ('ccd6f2f4e93d47f38d299c520ad6979b', '', '', '', '', '', NULL, '1', '2019-12-19 11:49:43', '1', '2019-12-19 11:49:43', '', '1');
INSERT INTO `test_data_main` VALUES ('cf0ca8a9fbb142cc826d2af75164fdc3', '3', '30808ed5d34f4e64b5d9d93ff0967f0d', '3fbaf3a3f58a47118f13976cb2e2544d', '', '', NULL, '1', '2019-04-01 13:10:29', '1', '2019-04-01 13:10:29', '', '0');
INSERT INTO `test_data_main` VALUES ('f9422e63fcc94319a8e2cbb0b53a1120', '171eb600266c4d32998c7265a20a8648', '5', 'f762fb569d8445f28a226a9b831cfb90', '测试', '', '2019-02-25', '1', '2019-02-25 16:53:58', '1', '2019-03-03 19:49:58', '测试主子', '0');

-- ----------------------------
-- Table structure for test_tree
-- ----------------------------
DROP TABLE IF EXISTS `test_tree`;
CREATE TABLE `test_tree`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '编号',
  `parent_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '所有父级编号',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '名称',
  `sort` decimal(10, 0) NULL DEFAULT NULL COMMENT '排序',
  `tree_leaf` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否树形叶子节点（0:不是,1:是）',
  `tree_level` decimal(4, 0) NULL DEFAULT NULL COMMENT '树形层级(0:根级)',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `test_tree_del_flag`(`del_flag`) USING BTREE,
  INDEX `test_data_parent_id`(`parent_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '树结构表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of test_tree
-- ----------------------------
INSERT INTO `test_tree` VALUES ('0e5f08e37f354783b83d4c12292bb8ce', '0', '0,', '', 30, '1', 0, '1', '2019-01-10 21:44:15', '1', '2019-03-11 17:51:38', '', '0');
INSERT INTO `test_tree` VALUES ('219b2b960041419db042fdc8e22e5fff', '381f24a283e54099a55473eb0c4a56e8', '0,381f24a283e54099a55473eb0c4a56e8,', '', 30, '1', 1, '1', '2019-03-01 16:37:43', '1', '2019-03-15 14:40:56', '', '0');
INSERT INTO `test_tree` VALUES ('31f28474706a4530b8913ead4d0e2689', '0', '0,', '', 30, '1', 0, '1', '2019-03-28 21:30:53', '1', '2019-03-28 21:30:53', '', '0');
INSERT INTO `test_tree` VALUES ('381f24a283e54099a55473eb0c4a56e8', '0', '0,', '', 30, '0', 0, '1', '2019-03-01 16:37:29', '1', '2019-03-01 16:37:43', '', '0');
INSERT INTO `test_tree` VALUES ('3ec40e64877f44b6a94332770346aa36', '0', '0,', '电力交易项目', 30, '1', 0, '1', '2019-02-28 10:17:15', '1', '2019-02-28 10:17:15', '', '0');
INSERT INTO `test_tree` VALUES ('4568301f13a3491482ff62946775110d', '8b68dda1a112488ba29a07a311e8ff21', '0,8b68dda1a112488ba29a07a311e8ff21,', '331', 30, '1', 1, '1', '2019-01-25 17:57:37', '1', '2019-03-12 11:01:40', '', '0');
INSERT INTO `test_tree` VALUES ('73eb554899594ee0b45eb72360586351', 'e323c4775e6240fbb924b6442ab8a49d', '0,e323c4775e6240fbb924b6442ab8a49d,', '', 30, '1', 1, '1', '2019-01-24 14:24:59', '1', '2019-01-24 14:24:59', '', '0');
INSERT INTO `test_tree` VALUES ('77bffd3657a84b439e6d2524de0bd2bd', '0', '0,', '', 30, '1', 0, '1', '2019-05-27 23:45:32', '1', '2019-05-27 23:45:32', '', '0');
INSERT INTO `test_tree` VALUES ('7c471d46c95a440abc0bab50c0b60994', '0', '0,', '', 30, '1', 0, '1', '2019-01-11 21:32:12', '1', '2019-01-11 21:32:12', '', '0');
INSERT INTO `test_tree` VALUES ('816b96133fbb4ab997e811d27dc14429', '0', '0,', '', 30, '1', 0, '1', '2019-04-13 14:17:21', '1', '2019-04-13 14:17:21', '', '0');
INSERT INTO `test_tree` VALUES ('8b68dda1a112488ba29a07a311e8ff21', '0', '0,', '333', 30, '0', 0, '1', '2019-01-25 17:57:30', '1', '2019-01-25 17:57:37', '', '0');
INSERT INTO `test_tree` VALUES ('a8456765a18f46e18e2ed11f6217f038', '0', '0,', '', NULL, '1', 0, '1', '2019-05-27 23:45:39', '1', '2019-05-27 23:45:39', '', '0');
INSERT INTO `test_tree` VALUES ('cb47b0adc17443619c3e27bbae69d5a1', '0', '0,', '杨磊', 30, '1', 0, '1', '2019-02-28 10:22:59', '1', '2019-02-28 10:22:59', '', '0');
INSERT INTO `test_tree` VALUES ('d54b05fdf1a24592a3656d74c5b3a4cd', '0', '0,', 'test1', 30, '1', 0, '1', '2019-06-04 09:42:30', '1', '2019-06-11 14:57:16', '', '0');
INSERT INTO `test_tree` VALUES ('e323c4775e6240fbb924b6442ab8a49d', '0', '0,', '树结构父节点', 30, '0', 0, '1', '2019-01-07 16:55:15', '1', '2019-04-15 16:21:23', 'aa', '0');
INSERT INTO `test_tree` VALUES ('e5a9917fecb94db4b1e6ceede9716715', 'e323c4775e6240fbb924b6442ab8a49d', '0,e323c4775e6240fbb924b6442ab8a49d,', '树结构子节点', 30, '1', 1, '1', '2019-01-07 17:39:06', '1', '2019-01-15 11:00:44', '啊啊', '0');

SET FOREIGN_KEY_CHECKS = 1;
